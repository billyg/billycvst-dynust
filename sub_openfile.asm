; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _OPENFILE
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _OPENFILE
_OPENFILE	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.16
        mov       ebp, esp                                      ;1.16
        and       esp, -16                                      ;1.16
        push      esi                                           ;1.16
        push      edi                                           ;1.16
        push      ebx                                           ;1.16
        sub       esp, 1236                                     ;1.16
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;29.18
        jne       .B1.300       ; Prob 50%                      ;29.18
                                ; LOE
.B1.2:                          ; Preds .B1.1
        mov       DWORD PTR [480+esp], 0                        ;30.2
        lea       edi, DWORD PTR [480+esp]                      ;30.2
        mov       DWORD PTR [440+esp], 12                       ;30.2
        lea       eax, DWORD PTR [440+esp]                      ;30.2
        mov       DWORD PTR [444+esp], OFFSET FLAT: __STRLITPACK_190 ;30.2
        mov       DWORD PTR [448+esp], 7                        ;30.2
        mov       DWORD PTR [452+esp], OFFSET FLAT: __STRLITPACK_189 ;30.2
        push      32                                            ;30.2
        push      eax                                           ;30.2
        push      OFFSET FLAT: __STRLITPACK_191.0.1             ;30.2
        push      -2088435965                                   ;30.2
        push      911                                           ;30.2
        push      edi                                           ;30.2
        call      _for_open                                     ;30.2
                                ; LOE eax edi
.B1.204:                        ; Preds .B1.2
        add       esp, 24                                       ;30.2
                                ; LOE eax edi
.B1.3:                          ; Preds .B1.204
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;30.2
        test      eax, eax                                      ;31.11
        jne       .B1.114       ; Prob 5%                       ;31.11
                                ; LOE edi
.B1.299:                        ; Preds .B1.3
        xor       esi, esi                                      ;197.8
                                ; LOE esi edi
.B1.4:                          ; Preds .B1.255 .B1.299
        mov       DWORD PTR [480+esp], 0                        ;35.2
        lea       eax, DWORD PTR [456+esp]                      ;35.2
        mov       DWORD PTR [456+esp], 12                       ;35.2
        mov       DWORD PTR [460+esp], OFFSET FLAT: __STRLITPACK_186 ;35.2
        mov       DWORD PTR [464+esp], 7                        ;35.2
        mov       DWORD PTR [468+esp], OFFSET FLAT: __STRLITPACK_185 ;35.2
        push      32                                            ;35.2
        push      eax                                           ;35.2
        push      OFFSET FLAT: __STRLITPACK_194.0.1             ;35.2
        push      -2088435965                                   ;35.2
        push      5559                                          ;35.2
        push      edi                                           ;35.2
        call      _for_open                                     ;35.2
                                ; LOE eax esi edi
.B1.205:                        ; Preds .B1.4
        add       esp, 24                                       ;35.2
                                ; LOE eax esi edi
.B1.5:                          ; Preds .B1.205
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;35.2
        test      eax, eax                                      ;36.11
        jne       .B1.112       ; Prob 5%                       ;36.11
        jmp       .B1.6         ; Prob 100%                     ;36.11
                                ; LOE esi edi
.B1.300:                        ; Preds .B1.1
        xor       esi, esi                                      ;197.8
        lea       edi, DWORD PTR [480+esp]                      ;197.8
                                ; LOE esi edi
.B1.6:                          ; Preds .B1.254 .B1.5 .B1.300
        mov       DWORD PTR [480+esp], 0                        ;42.2
        lea       eax, DWORD PTR [512+esp]                      ;42.2
        mov       DWORD PTR [512+esp], 11                       ;42.2
        mov       DWORD PTR [516+esp], OFFSET FLAT: __STRLITPACK_182 ;42.2
        mov       DWORD PTR [520+esp], 3                        ;42.2
        mov       DWORD PTR [524+esp], OFFSET FLAT: __STRLITPACK_181 ;42.2
        push      32                                            ;42.2
        push      eax                                           ;42.2
        push      OFFSET FLAT: __STRLITPACK_197.0.1             ;42.2
        push      -2088435965                                   ;42.2
        push      41                                            ;42.2
        push      edi                                           ;42.2
        call      _for_open                                     ;42.2
                                ; LOE eax esi edi
.B1.206:                        ; Preds .B1.6
        add       esp, 24                                       ;42.2
                                ; LOE eax esi edi
.B1.7:                          ; Preds .B1.206
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;42.2
        test      eax, eax                                      ;43.11
        jne       .B1.200       ; Prob 5%                       ;43.11
                                ; LOE esi edi
.B1.8:                          ; Preds .B1.298 .B1.7
        mov       DWORD PTR [480+esp], 0                        ;48.2
        lea       eax, DWORD PTR [528+esp]                      ;48.2
        mov       DWORD PTR [528+esp], 10                       ;48.2
        mov       DWORD PTR [532+esp], OFFSET FLAT: __STRLITPACK_178 ;48.2
        mov       DWORD PTR [536+esp], 3                        ;48.2
        mov       DWORD PTR [540+esp], OFFSET FLAT: __STRLITPACK_177 ;48.2
        push      32                                            ;48.2
        push      eax                                           ;48.2
        push      OFFSET FLAT: __STRLITPACK_200.0.1             ;48.2
        push      -2088435965                                   ;48.2
        push      42                                            ;48.2
        push      edi                                           ;48.2
        call      _for_open                                     ;48.2
                                ; LOE eax esi edi
.B1.207:                        ; Preds .B1.8
        add       esp, 24                                       ;48.2
                                ; LOE eax esi edi
.B1.9:                          ; Preds .B1.207
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;48.2
        test      eax, eax                                      ;49.11
        jne       .B1.198       ; Prob 5%                       ;49.11
                                ; LOE esi edi
.B1.10:                         ; Preds .B1.297 .B1.9
        mov       DWORD PTR [480+esp], 0                        ;54.2
        lea       eax, DWORD PTR [544+esp]                      ;54.2
        mov       DWORD PTR [544+esp], 12                       ;54.2
        mov       DWORD PTR [548+esp], OFFSET FLAT: __STRLITPACK_174 ;54.2
        mov       DWORD PTR [552+esp], 3                        ;54.2
        mov       DWORD PTR [556+esp], OFFSET FLAT: __STRLITPACK_173 ;54.2
        push      32                                            ;54.2
        push      eax                                           ;54.2
        push      OFFSET FLAT: __STRLITPACK_203.0.1             ;54.2
        push      -2088435965                                   ;54.2
        push      43                                            ;54.2
        push      edi                                           ;54.2
        call      _for_open                                     ;54.2
                                ; LOE eax esi edi
.B1.208:                        ; Preds .B1.10
        add       esp, 24                                       ;54.2
                                ; LOE eax esi edi
.B1.11:                         ; Preds .B1.208
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;54.2
        test      eax, eax                                      ;55.11
        jne       .B1.196       ; Prob 5%                       ;55.11
                                ; LOE esi edi
.B1.12:                         ; Preds .B1.296 .B1.11
        mov       DWORD PTR [480+esp], 0                        ;60.2
        lea       eax, DWORD PTR [560+esp]                      ;60.2
        mov       DWORD PTR [560+esp], 11                       ;60.2
        mov       DWORD PTR [564+esp], OFFSET FLAT: __STRLITPACK_170 ;60.2
        mov       DWORD PTR [568+esp], 3                        ;60.2
        mov       DWORD PTR [572+esp], OFFSET FLAT: __STRLITPACK_169 ;60.2
        push      32                                            ;60.2
        push      eax                                           ;60.2
        push      OFFSET FLAT: __STRLITPACK_206.0.1             ;60.2
        push      -2088435965                                   ;60.2
        push      44                                            ;60.2
        push      edi                                           ;60.2
        call      _for_open                                     ;60.2
                                ; LOE eax esi edi
.B1.209:                        ; Preds .B1.12
        add       esp, 24                                       ;60.2
                                ; LOE eax esi edi
.B1.13:                         ; Preds .B1.209
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;60.2
        test      eax, eax                                      ;61.11
        jne       .B1.194       ; Prob 5%                       ;61.11
                                ; LOE esi edi
.B1.14:                         ; Preds .B1.295 .B1.13
        mov       DWORD PTR [480+esp], 0                        ;66.2
        lea       eax, DWORD PTR [576+esp]                      ;66.2
        mov       DWORD PTR [576+esp], 8                        ;66.2
        mov       DWORD PTR [580+esp], OFFSET FLAT: __STRLITPACK_166 ;66.2
        mov       DWORD PTR [584+esp], 3                        ;66.2
        mov       DWORD PTR [588+esp], OFFSET FLAT: __STRLITPACK_165 ;66.2
        push      32                                            ;66.2
        push      eax                                           ;66.2
        push      OFFSET FLAT: __STRLITPACK_209.0.1             ;66.2
        push      -2088435965                                   ;66.2
        push      45                                            ;66.2
        push      edi                                           ;66.2
        call      _for_open                                     ;66.2
                                ; LOE eax esi edi
.B1.210:                        ; Preds .B1.14
        add       esp, 24                                       ;66.2
                                ; LOE eax esi edi
.B1.15:                         ; Preds .B1.210
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;66.2
        test      eax, eax                                      ;67.11
        jne       .B1.192       ; Prob 5%                       ;67.11
                                ; LOE esi edi
.B1.16:                         ; Preds .B1.294 .B1.15
        mov       DWORD PTR [480+esp], 0                        ;72.2
        lea       eax, DWORD PTR [592+esp]                      ;72.2
        mov       DWORD PTR [592+esp], 12                       ;72.2
        mov       DWORD PTR [596+esp], OFFSET FLAT: __STRLITPACK_162 ;72.2
        mov       DWORD PTR [600+esp], 3                        ;72.2
        mov       DWORD PTR [604+esp], OFFSET FLAT: __STRLITPACK_161 ;72.2
        push      32                                            ;72.2
        push      eax                                           ;72.2
        push      OFFSET FLAT: __STRLITPACK_212.0.1             ;72.2
        push      -2088435965                                   ;72.2
        push      46                                            ;72.2
        push      edi                                           ;72.2
        call      _for_open                                     ;72.2
                                ; LOE eax esi edi
.B1.211:                        ; Preds .B1.16
        add       esp, 24                                       ;72.2
                                ; LOE eax esi edi
.B1.17:                         ; Preds .B1.211
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;72.2
        test      eax, eax                                      ;73.11
        jne       .B1.190       ; Prob 5%                       ;73.11
                                ; LOE esi edi
.B1.18:                         ; Preds .B1.293 .B1.17
        mov       DWORD PTR [480+esp], 0                        ;78.2
        lea       eax, DWORD PTR [608+esp]                      ;78.2
        mov       DWORD PTR [608+esp], 12                       ;78.2
        mov       DWORD PTR [612+esp], OFFSET FLAT: __STRLITPACK_158 ;78.2
        mov       DWORD PTR [616+esp], 3                        ;78.2
        mov       DWORD PTR [620+esp], OFFSET FLAT: __STRLITPACK_157 ;78.2
        push      32                                            ;78.2
        push      eax                                           ;78.2
        push      OFFSET FLAT: __STRLITPACK_215.0.1             ;78.2
        push      -2088435965                                   ;78.2
        push      47                                            ;78.2
        push      edi                                           ;78.2
        call      _for_open                                     ;78.2
                                ; LOE eax esi edi
.B1.212:                        ; Preds .B1.18
        add       esp, 24                                       ;78.2
                                ; LOE eax esi edi
.B1.19:                         ; Preds .B1.212
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;78.2
        test      eax, eax                                      ;79.11
        jne       .B1.188       ; Prob 5%                       ;79.11
                                ; LOE esi edi
.B1.20:                         ; Preds .B1.292 .B1.19
        mov       DWORD PTR [480+esp], 0                        ;84.2
        lea       eax, DWORD PTR [624+esp]                      ;84.2
        mov       DWORD PTR [624+esp], 11                       ;84.2
        mov       DWORD PTR [628+esp], OFFSET FLAT: __STRLITPACK_154 ;84.2
        mov       DWORD PTR [632+esp], 3                        ;84.2
        mov       DWORD PTR [636+esp], OFFSET FLAT: __STRLITPACK_153 ;84.2
        push      32                                            ;84.2
        push      eax                                           ;84.2
        push      OFFSET FLAT: __STRLITPACK_218.0.1             ;84.2
        push      -2088435965                                   ;84.2
        push      48                                            ;84.2
        push      edi                                           ;84.2
        call      _for_open                                     ;84.2
                                ; LOE eax esi edi
.B1.213:                        ; Preds .B1.20
        add       esp, 24                                       ;84.2
                                ; LOE eax esi edi
.B1.21:                         ; Preds .B1.213
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;84.2
        test      eax, eax                                      ;85.11
        jne       .B1.186       ; Prob 5%                       ;85.11
                                ; LOE esi edi
.B1.22:                         ; Preds .B1.291 .B1.21
        mov       DWORD PTR [480+esp], 0                        ;90.2
        lea       eax, DWORD PTR [640+esp]                      ;90.2
        mov       DWORD PTR [640+esp], 7                        ;90.2
        mov       DWORD PTR [644+esp], OFFSET FLAT: __STRLITPACK_150 ;90.2
        mov       DWORD PTR [648+esp], 3                        ;90.2
        mov       DWORD PTR [652+esp], OFFSET FLAT: __STRLITPACK_149 ;90.2
        push      32                                            ;90.2
        push      eax                                           ;90.2
        push      OFFSET FLAT: __STRLITPACK_221.0.1             ;90.2
        push      -2088435965                                   ;90.2
        push      49                                            ;90.2
        push      edi                                           ;90.2
        call      _for_open                                     ;90.2
                                ; LOE eax esi edi
.B1.214:                        ; Preds .B1.22
        add       esp, 24                                       ;90.2
                                ; LOE eax esi edi
.B1.23:                         ; Preds .B1.214
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;90.2
        test      eax, eax                                      ;91.11
        jne       .B1.184       ; Prob 5%                       ;91.11
                                ; LOE esi edi
.B1.24:                         ; Preds .B1.290 .B1.23
        mov       DWORD PTR [480+esp], 0                        ;96.2
        lea       eax, DWORD PTR [656+esp]                      ;96.2
        mov       DWORD PTR [656+esp], 10                       ;96.2
        mov       DWORD PTR [660+esp], OFFSET FLAT: __STRLITPACK_146 ;96.2
        mov       DWORD PTR [664+esp], 3                        ;96.2
        mov       DWORD PTR [668+esp], OFFSET FLAT: __STRLITPACK_145 ;96.2
        push      32                                            ;96.2
        push      eax                                           ;96.2
        push      OFFSET FLAT: __STRLITPACK_224.0.1             ;96.2
        push      -2088435965                                   ;96.2
        push      52                                            ;96.2
        push      edi                                           ;96.2
        call      _for_open                                     ;96.2
                                ; LOE eax esi edi
.B1.215:                        ; Preds .B1.24
        add       esp, 24                                       ;96.2
                                ; LOE eax esi edi
.B1.25:                         ; Preds .B1.215
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;96.2
        test      eax, eax                                      ;97.11
        jne       .B1.182       ; Prob 5%                       ;97.11
                                ; LOE esi edi
.B1.26:                         ; Preds .B1.289 .B1.25
        mov       DWORD PTR [480+esp], 0                        ;102.2
        lea       eax, DWORD PTR [672+esp]                      ;102.2
        mov       DWORD PTR [672+esp], 15                       ;102.2
        mov       DWORD PTR [676+esp], OFFSET FLAT: __STRLITPACK_142 ;102.2
        mov       DWORD PTR [680+esp], 3                        ;102.2
        mov       DWORD PTR [684+esp], OFFSET FLAT: __STRLITPACK_141 ;102.2
        push      32                                            ;102.2
        push      eax                                           ;102.2
        push      OFFSET FLAT: __STRLITPACK_227.0.1             ;102.2
        push      -2088435965                                   ;102.2
        push      53                                            ;102.2
        push      edi                                           ;102.2
        call      _for_open                                     ;102.2
                                ; LOE eax esi edi
.B1.216:                        ; Preds .B1.26
        add       esp, 24                                       ;102.2
                                ; LOE eax esi edi
.B1.27:                         ; Preds .B1.216
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;102.2
        test      eax, eax                                      ;103.11
        jne       .B1.180       ; Prob 5%                       ;103.11
                                ; LOE esi edi
.B1.28:                         ; Preds .B1.288 .B1.27
        mov       DWORD PTR [480+esp], 0                        ;108.13
        lea       ebx, DWORD PTR [1164+esp]                     ;108.13
        mov       DWORD PTR [1152+esp], 16                      ;108.13
        lea       eax, DWORD PTR [1152+esp]                     ;108.13
        mov       DWORD PTR [1156+esp], OFFSET FLAT: __STRLITPACK_138 ;108.13
        mov       DWORD PTR [1160+esp], ebx                     ;108.13
        push      32                                            ;108.13
        push      eax                                           ;108.13
        push      OFFSET FLAT: __STRLITPACK_230.0.1             ;108.13
        push      -2088435968                                   ;108.13
        push      -1                                            ;108.13
        push      edi                                           ;108.13
        call      _for_inquire                                  ;108.13
                                ; LOE ebx esi edi
.B1.217:                        ; Preds .B1.28
        add       esp, 24                                       ;108.13
                                ; LOE ebx esi edi
.B1.29:                         ; Preds .B1.217
        test      BYTE PTR [1164+esp], 1                        ;109.8
        je        .B1.32        ; Prob 60%                      ;109.8
                                ; LOE ebx esi edi
.B1.30:                         ; Preds .B1.29
        mov       DWORD PTR [480+esp], 0                        ;110.2
        lea       eax, DWORD PTR [360+esp]                      ;110.2
        mov       DWORD PTR [360+esp], 16                       ;110.2
        mov       DWORD PTR [364+esp], OFFSET FLAT: __STRLITPACK_137 ;110.2
        mov       DWORD PTR [368+esp], 3                        ;110.2
        mov       DWORD PTR [372+esp], OFFSET FLAT: __STRLITPACK_136 ;110.2
        push      32                                            ;110.2
        push      eax                                           ;110.2
        push      OFFSET FLAT: __STRLITPACK_231.0.1             ;110.2
        push      -2088435965                                   ;110.2
        push      54                                            ;110.2
        push      edi                                           ;110.2
        call      _for_open                                     ;110.2
                                ; LOE eax ebx esi edi
.B1.218:                        ; Preds .B1.30
        add       esp, 24                                       ;110.2
                                ; LOE eax ebx esi edi
.B1.31:                         ; Preds .B1.218
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;110.2
        test      eax, eax                                      ;111.11
        jne       .B1.116       ; Prob 5%                       ;111.11
                                ; LOE ebx esi edi
.B1.32:                         ; Preds .B1.256 .B1.31 .B1.29
        mov       DWORD PTR [480+esp], 0                        ;117.13
        lea       eax, DWORD PTR [1168+esp]                     ;117.13
        mov       DWORD PTR [1168+esp], 14                      ;117.13
        mov       DWORD PTR [1172+esp], OFFSET FLAT: __STRLITPACK_133 ;117.13
        mov       DWORD PTR [1176+esp], ebx                     ;117.13
        push      32                                            ;117.13
        push      eax                                           ;117.13
        push      OFFSET FLAT: __STRLITPACK_234.0.1             ;117.13
        push      -2088435968                                   ;117.13
        push      -1                                            ;117.13
        push      edi                                           ;117.13
        call      _for_inquire                                  ;117.13
                                ; LOE ebx esi edi
.B1.219:                        ; Preds .B1.32
        add       esp, 24                                       ;117.13
                                ; LOE ebx esi edi
.B1.33:                         ; Preds .B1.219
        test      BYTE PTR [1164+esp], 1                        ;118.8
        je        .B1.36        ; Prob 60%                      ;118.8
                                ; LOE ebx esi edi
.B1.34:                         ; Preds .B1.33
        mov       DWORD PTR [480+esp], 0                        ;119.2
        lea       eax, DWORD PTR [376+esp]                      ;119.2
        mov       DWORD PTR [376+esp], 14                       ;119.2
        mov       DWORD PTR [380+esp], OFFSET FLAT: __STRLITPACK_132 ;119.2
        mov       DWORD PTR [384+esp], 3                        ;119.2
        mov       DWORD PTR [388+esp], OFFSET FLAT: __STRLITPACK_131 ;119.2
        push      32                                            ;119.2
        push      eax                                           ;119.2
        push      OFFSET FLAT: __STRLITPACK_235.0.1             ;119.2
        push      -2088435965                                   ;119.2
        push      544                                           ;119.2
        push      edi                                           ;119.2
        call      _for_open                                     ;119.2
                                ; LOE eax ebx esi edi
.B1.220:                        ; Preds .B1.34
        add       esp, 24                                       ;119.2
                                ; LOE eax ebx esi edi
.B1.35:                         ; Preds .B1.220
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;119.2
        test      eax, eax                                      ;120.11
        jne       .B1.118       ; Prob 5%                       ;120.11
                                ; LOE ebx esi edi
.B1.36:                         ; Preds .B1.257 .B1.35 .B1.33
        mov       DWORD PTR [480+esp], 0                        ;126.13
        lea       eax, DWORD PTR [1184+esp]                     ;126.13
        mov       DWORD PTR [1184+esp], 20                      ;126.13
        mov       DWORD PTR [1188+esp], OFFSET FLAT: __STRLITPACK_128 ;126.13
        mov       DWORD PTR [1192+esp], ebx                     ;126.13
        push      32                                            ;126.13
        push      eax                                           ;126.13
        push      OFFSET FLAT: __STRLITPACK_238.0.1             ;126.13
        push      -2088435968                                   ;126.13
        push      -1                                            ;126.13
        push      edi                                           ;126.13
        call      _for_inquire                                  ;126.13
                                ; LOE esi edi
.B1.221:                        ; Preds .B1.36
        add       esp, 24                                       ;126.13
                                ; LOE esi edi
.B1.37:                         ; Preds .B1.221
        test      BYTE PTR [1164+esp], 1                        ;127.8
        je        .B1.40        ; Prob 60%                      ;127.8
                                ; LOE esi edi
.B1.38:                         ; Preds .B1.37
        mov       DWORD PTR [480+esp], 0                        ;128.2
        lea       eax, DWORD PTR [392+esp]                      ;128.2
        mov       DWORD PTR [392+esp], 20                       ;128.2
        mov       DWORD PTR [396+esp], OFFSET FLAT: __STRLITPACK_127 ;128.2
        mov       DWORD PTR [400+esp], 3                        ;128.2
        mov       DWORD PTR [404+esp], OFFSET FLAT: __STRLITPACK_126 ;128.2
        push      32                                            ;128.2
        push      eax                                           ;128.2
        push      OFFSET FLAT: __STRLITPACK_239.0.1             ;128.2
        push      -2088435965                                   ;128.2
        push      545                                           ;128.2
        push      edi                                           ;128.2
        call      _for_open                                     ;128.2
                                ; LOE eax esi edi
.B1.222:                        ; Preds .B1.38
        add       esp, 24                                       ;128.2
                                ; LOE eax esi edi
.B1.39:                         ; Preds .B1.222
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;128.2
        test      eax, eax                                      ;129.11
        jne       .B1.120       ; Prob 5%                       ;129.11
                                ; LOE esi edi
.B1.40:                         ; Preds .B1.258 .B1.39 .B1.37
        mov       DWORD PTR [480+esp], 0                        ;135.2
        lea       eax, DWORD PTR [688+esp]                      ;135.2
        mov       DWORD PTR [688+esp], 15                       ;135.2
        mov       DWORD PTR [692+esp], OFFSET FLAT: __STRLITPACK_123 ;135.2
        mov       DWORD PTR [696+esp], 3                        ;135.2
        mov       DWORD PTR [700+esp], OFFSET FLAT: __STRLITPACK_122 ;135.2
        push      32                                            ;135.2
        push      eax                                           ;135.2
        push      OFFSET FLAT: __STRLITPACK_242.0.1             ;135.2
        push      -2088435965                                   ;135.2
        push      56                                            ;135.2
        push      edi                                           ;135.2
        call      _for_open                                     ;135.2
                                ; LOE eax esi edi
.B1.223:                        ; Preds .B1.40
        add       esp, 24                                       ;135.2
                                ; LOE eax esi edi
.B1.41:                         ; Preds .B1.223
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;135.2
        test      eax, eax                                      ;136.11
        jne       .B1.178       ; Prob 5%                       ;136.11
                                ; LOE esi edi
.B1.42:                         ; Preds .B1.287 .B1.41
        mov       DWORD PTR [480+esp], 0                        ;141.2
        lea       eax, DWORD PTR [704+esp]                      ;141.2
        mov       DWORD PTR [704+esp], 15                       ;141.2
        mov       DWORD PTR [708+esp], OFFSET FLAT: __STRLITPACK_119 ;141.2
        mov       DWORD PTR [712+esp], 3                        ;141.2
        mov       DWORD PTR [716+esp], OFFSET FLAT: __STRLITPACK_118 ;141.2
        push      32                                            ;141.2
        push      eax                                           ;141.2
        push      OFFSET FLAT: __STRLITPACK_245.0.1             ;141.2
        push      -2088435965                                   ;141.2
        push      57                                            ;141.2
        push      edi                                           ;141.2
        call      _for_open                                     ;141.2
                                ; LOE eax esi edi
.B1.224:                        ; Preds .B1.42
        add       esp, 24                                       ;141.2
                                ; LOE eax esi edi
.B1.43:                         ; Preds .B1.224
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;141.2
        test      eax, eax                                      ;142.11
        jne       .B1.176       ; Prob 5%                       ;142.11
                                ; LOE esi edi
.B1.44:                         ; Preds .B1.286 .B1.43
        mov       DWORD PTR [480+esp], 0                        ;147.2
        lea       eax, DWORD PTR [720+esp]                      ;147.2
        mov       DWORD PTR [720+esp], 12                       ;147.2
        mov       DWORD PTR [724+esp], OFFSET FLAT: __STRLITPACK_115 ;147.2
        mov       DWORD PTR [728+esp], 3                        ;147.2
        mov       DWORD PTR [732+esp], OFFSET FLAT: __STRLITPACK_114 ;147.2
        push      32                                            ;147.2
        push      eax                                           ;147.2
        push      OFFSET FLAT: __STRLITPACK_248.0.1             ;147.2
        push      -2088435965                                   ;147.2
        push      60                                            ;147.2
        push      edi                                           ;147.2
        call      _for_open                                     ;147.2
                                ; LOE eax esi edi
.B1.225:                        ; Preds .B1.44
        add       esp, 24                                       ;147.2
                                ; LOE eax esi edi
.B1.45:                         ; Preds .B1.225
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;147.2
        test      eax, eax                                      ;148.11
        jne       .B1.174       ; Prob 5%                       ;148.11
                                ; LOE esi edi
.B1.46:                         ; Preds .B1.285 .B1.45
        mov       DWORD PTR [480+esp], 0                        ;154.2
        lea       eax, DWORD PTR [736+esp]                      ;154.2
        mov       DWORD PTR [736+esp], 12                       ;154.2
        mov       DWORD PTR [740+esp], OFFSET FLAT: __STRLITPACK_111 ;154.2
        mov       DWORD PTR [744+esp], 3                        ;154.2
        mov       DWORD PTR [748+esp], OFFSET FLAT: __STRLITPACK_110 ;154.2
        push      32                                            ;154.2
        push      eax                                           ;154.2
        push      OFFSET FLAT: __STRLITPACK_251.0.1             ;154.2
        push      -2088435965                                   ;154.2
        push      58                                            ;154.2
        push      edi                                           ;154.2
        call      _for_open                                     ;154.2
                                ; LOE eax esi edi
.B1.226:                        ; Preds .B1.46
        add       esp, 24                                       ;154.2
                                ; LOE eax esi edi
.B1.47:                         ; Preds .B1.226
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;154.2
        test      eax, eax                                      ;155.11
        jne       .B1.172       ; Prob 5%                       ;155.11
                                ; LOE esi edi
.B1.48:                         ; Preds .B1.284 .B1.47
        mov       DWORD PTR [480+esp], 0                        ;160.5
        lea       eax, DWORD PTR [752+esp]                      ;160.5
        mov       DWORD PTR [752+esp], 18                       ;160.5
        mov       DWORD PTR [756+esp], OFFSET FLAT: __STRLITPACK_107 ;160.5
        mov       DWORD PTR [760+esp], 3                        ;160.5
        mov       DWORD PTR [764+esp], OFFSET FLAT: __STRLITPACK_106 ;160.5
        push      32                                            ;160.5
        push      eax                                           ;160.5
        push      OFFSET FLAT: __STRLITPACK_254.0.1             ;160.5
        push      -2088435965                                   ;160.5
        push      59                                            ;160.5
        push      edi                                           ;160.5
        call      _for_open                                     ;160.5
                                ; LOE eax esi edi
.B1.227:                        ; Preds .B1.48
        add       esp, 24                                       ;160.5
                                ; LOE eax esi edi
.B1.49:                         ; Preds .B1.227
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;160.5
        test      eax, eax                                      ;161.11
        jne       .B1.170       ; Prob 5%                       ;161.11
                                ; LOE esi edi
.B1.50:                         ; Preds .B1.283 .B1.49
        mov       DWORD PTR [480+esp], 0                        ;166.2
        lea       eax, DWORD PTR [768+esp]                      ;166.2
        mov       DWORD PTR [768+esp], 10                       ;166.2
        mov       DWORD PTR [772+esp], OFFSET FLAT: __STRLITPACK_103 ;166.2
        mov       DWORD PTR [776+esp], 3                        ;166.2
        mov       DWORD PTR [780+esp], OFFSET FLAT: __STRLITPACK_102 ;166.2
        push      32                                            ;166.2
        push      eax                                           ;166.2
        push      OFFSET FLAT: __STRLITPACK_257.0.1             ;166.2
        push      -2088435965                                   ;166.2
        push      95                                            ;166.2
        push      edi                                           ;166.2
        call      _for_open                                     ;166.2
                                ; LOE eax esi edi
.B1.228:                        ; Preds .B1.50
        add       esp, 24                                       ;166.2
                                ; LOE eax esi edi
.B1.51:                         ; Preds .B1.228
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;166.2
        test      eax, eax                                      ;167.11
        jne       .B1.168       ; Prob 5%                       ;167.11
                                ; LOE esi edi
.B1.52:                         ; Preds .B1.282 .B1.51
        mov       DWORD PTR [480+esp], 0                        ;172.2
        lea       eax, DWORD PTR [784+esp]                      ;172.2
        mov       DWORD PTR [784+esp], 17                       ;172.2
        mov       DWORD PTR [788+esp], OFFSET FLAT: __STRLITPACK_99 ;172.2
        mov       DWORD PTR [792+esp], 3                        ;172.2
        mov       DWORD PTR [796+esp], OFFSET FLAT: __STRLITPACK_98 ;172.2
        push      32                                            ;172.2
        push      eax                                           ;172.2
        push      OFFSET FLAT: __STRLITPACK_260.0.1             ;172.2
        push      -2088435965                                   ;172.2
        push      101                                           ;172.2
        push      edi                                           ;172.2
        call      _for_open                                     ;172.2
                                ; LOE eax esi edi
.B1.229:                        ; Preds .B1.52
        add       esp, 24                                       ;172.2
                                ; LOE eax esi edi
.B1.53:                         ; Preds .B1.229
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;172.2
        test      eax, eax                                      ;173.11
        jne       .B1.166       ; Prob 5%                       ;173.11
                                ; LOE esi edi
.B1.54:                         ; Preds .B1.281 .B1.53
        mov       DWORD PTR [480+esp], 0                        ;178.5
        lea       eax, DWORD PTR [800+esp]                      ;178.5
        mov       DWORD PTR [800+esp], 11                       ;178.5
        mov       DWORD PTR [804+esp], OFFSET FLAT: __STRLITPACK_95 ;178.5
        mov       DWORD PTR [808+esp], 7                        ;178.5
        mov       DWORD PTR [812+esp], OFFSET FLAT: __STRLITPACK_94 ;178.5
        push      32                                            ;178.5
        push      eax                                           ;178.5
        push      OFFSET FLAT: __STRLITPACK_263.0.1             ;178.5
        push      -2088435965                                   ;178.5
        push      500                                           ;178.5
        push      edi                                           ;178.5
        call      _for_open                                     ;178.5
                                ; LOE eax esi edi
.B1.230:                        ; Preds .B1.54
        add       esp, 24                                       ;178.5
                                ; LOE eax esi edi
.B1.55:                         ; Preds .B1.230
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;178.5
        test      eax, eax                                      ;179.11
        jne       .B1.164       ; Prob 5%                       ;179.11
                                ; LOE esi edi
.B1.56:                         ; Preds .B1.280 .B1.55
        mov       DWORD PTR [480+esp], 0                        ;184.2
        lea       eax, DWORD PTR [816+esp]                      ;184.2
        mov       DWORD PTR [816+esp], 8                        ;184.2
        mov       DWORD PTR [820+esp], OFFSET FLAT: __STRLITPACK_91 ;184.2
        mov       DWORD PTR [824+esp], 7                        ;184.2
        mov       DWORD PTR [828+esp], OFFSET FLAT: __STRLITPACK_90 ;184.2
        push      32                                            ;184.2
        push      eax                                           ;184.2
        push      OFFSET FLAT: __STRLITPACK_266.0.1             ;184.2
        push      -2088435965                                   ;184.2
        push      550                                           ;184.2
        push      edi                                           ;184.2
        call      _for_open                                     ;184.2
                                ; LOE eax esi edi
.B1.231:                        ; Preds .B1.56
        add       esp, 24                                       ;184.2
                                ; LOE eax esi edi
.B1.57:                         ; Preds .B1.231
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;184.2
        test      eax, eax                                      ;185.11
        jne       .B1.162       ; Prob 5%                       ;185.11
                                ; LOE esi edi
.B1.58:                         ; Preds .B1.279 .B1.57
        mov       DWORD PTR [480+esp], 0                        ;190.2
        lea       eax, DWORD PTR [832+esp]                      ;190.2
        mov       DWORD PTR [832+esp], 9                        ;190.2
        mov       DWORD PTR [836+esp], OFFSET FLAT: __STRLITPACK_87 ;190.2
        mov       DWORD PTR [840+esp], 7                        ;190.2
        mov       DWORD PTR [844+esp], OFFSET FLAT: __STRLITPACK_86 ;190.2
        push      32                                            ;190.2
        push      eax                                           ;190.2
        push      OFFSET FLAT: __STRLITPACK_269.0.1             ;190.2
        push      -2088435965                                   ;190.2
        push      912                                           ;190.2
        push      edi                                           ;190.2
        call      _for_open                                     ;190.2
                                ; LOE eax esi edi
.B1.232:                        ; Preds .B1.58
        add       esp, 24                                       ;190.2
                                ; LOE eax esi edi
.B1.59:                         ; Preds .B1.232
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;190.2
        test      eax, eax                                      ;191.11
        jne       .B1.160       ; Prob 5%                       ;191.11
                                ; LOE esi edi
.B1.60:                         ; Preds .B1.278 .B1.59
        mov       DWORD PTR [480+esp], 0                        ;196.2
        lea       eax, DWORD PTR [472+esp]                      ;196.2
        mov       DWORD PTR [472+esp], 21                       ;196.2
        mov       DWORD PTR [476+esp], OFFSET FLAT: __STRLITPACK_82 ;196.2
        push      32                                            ;196.2
        push      eax                                           ;196.2
        push      OFFSET FLAT: __STRLITPACK_272.0.1             ;196.2
        push      -2088435968                                   ;196.2
        push      912                                           ;196.2
        push      edi                                           ;196.2
        call      _for_write_seq_lis                            ;196.2
                                ; LOE esi edi
.B1.61:                         ; Preds .B1.60
        mov       DWORD PTR [504+esp], esi                      ;197.8
        push      32                                            ;197.8
        push      esi                                           ;197.8
        push      OFFSET FLAT: __STRLITPACK_273.0.1             ;197.8
        push      -2088435968                                   ;197.8
        push      912                                           ;197.8
        push      edi                                           ;197.8
        call      _for_close                                    ;197.8
                                ; LOE esi edi
.B1.62:                         ; Preds .B1.61
        mov       DWORD PTR [528+esp], 0                        ;199.2
        lea       eax, DWORD PTR [896+esp]                      ;199.2
        mov       DWORD PTR [896+esp], 17                       ;199.2
        mov       DWORD PTR [900+esp], OFFSET FLAT: __STRLITPACK_81 ;199.2
        mov       DWORD PTR [904+esp], 7                        ;199.2
        mov       DWORD PTR [908+esp], OFFSET FLAT: __STRLITPACK_80 ;199.2
        push      32                                            ;199.2
        push      eax                                           ;199.2
        push      OFFSET FLAT: __STRLITPACK_274.0.1             ;199.2
        push      -2088435965                                   ;199.2
        push      18                                            ;199.2
        push      edi                                           ;199.2
        call      _for_open                                     ;199.2
                                ; LOE eax esi edi
.B1.233:                        ; Preds .B1.62
        add       esp, 72                                       ;199.2
                                ; LOE eax esi edi
.B1.63:                         ; Preds .B1.233
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;199.2
        test      eax, eax                                      ;200.11
        jne       .B1.158       ; Prob 5%                       ;200.11
                                ; LOE esi edi
.B1.64:                         ; Preds .B1.277 .B1.63
        mov       DWORD PTR [480+esp], 0                        ;205.2
        lea       eax, DWORD PTR [864+esp]                      ;205.2
        mov       DWORD PTR [864+esp], 17                       ;205.2
        mov       DWORD PTR [868+esp], OFFSET FLAT: __STRLITPACK_77 ;205.2
        mov       DWORD PTR [872+esp], 7                        ;205.2
        mov       DWORD PTR [876+esp], OFFSET FLAT: __STRLITPACK_76 ;205.2
        push      32                                            ;205.2
        push      eax                                           ;205.2
        push      OFFSET FLAT: __STRLITPACK_277.0.1             ;205.2
        push      -2088435965                                   ;205.2
        push      188                                           ;205.2
        push      edi                                           ;205.2
        call      _for_open                                     ;205.2
                                ; LOE eax esi edi
.B1.234:                        ; Preds .B1.64
        add       esp, 24                                       ;205.2
                                ; LOE eax esi edi
.B1.65:                         ; Preds .B1.234
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;205.2
        test      eax, eax                                      ;206.11
        jne       .B1.156       ; Prob 5%                       ;206.11
                                ; LOE esi edi
.B1.66:                         ; Preds .B1.276 .B1.65
        mov       DWORD PTR [480+esp], 0                        ;211.7
        lea       eax, DWORD PTR [880+esp]                      ;211.7
        mov       DWORD PTR [880+esp], 15                       ;211.7
        mov       DWORD PTR [884+esp], OFFSET FLAT: __STRLITPACK_73 ;211.7
        mov       DWORD PTR [888+esp], 7                        ;211.7
        mov       DWORD PTR [892+esp], OFFSET FLAT: __STRLITPACK_72 ;211.7
        push      32                                            ;211.7
        push      eax                                           ;211.7
        push      OFFSET FLAT: __STRLITPACK_280.0.1             ;211.7
        push      -2088435965                                   ;211.7
        push      666                                           ;211.7
        push      edi                                           ;211.7
        call      _for_open                                     ;211.7
                                ; LOE eax esi edi
.B1.235:                        ; Preds .B1.66
        add       esp, 24                                       ;211.7
                                ; LOE eax esi edi
.B1.67:                         ; Preds .B1.235
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;211.7
        test      eax, eax                                      ;212.11
        jne       .B1.154       ; Prob 5%                       ;212.11
                                ; LOE esi edi
.B1.68:                         ; Preds .B1.275 .B1.67
        mov       DWORD PTR [480+esp], 0                        ;217.2
        lea       eax, DWORD PTR [896+esp]                      ;217.2
        mov       DWORD PTR [896+esp], 10                       ;217.2
        mov       DWORD PTR [900+esp], OFFSET FLAT: __STRLITPACK_69 ;217.2
        mov       DWORD PTR [904+esp], 7                        ;217.2
        mov       DWORD PTR [908+esp], OFFSET FLAT: __STRLITPACK_68 ;217.2
        push      32                                            ;217.2
        push      eax                                           ;217.2
        push      OFFSET FLAT: __STRLITPACK_283.0.1             ;217.2
        push      -2088435965                                   ;217.2
        push      180                                           ;217.2
        push      edi                                           ;217.2
        call      _for_open                                     ;217.2
                                ; LOE eax esi edi
.B1.236:                        ; Preds .B1.68
        add       esp, 24                                       ;217.2
                                ; LOE eax esi edi
.B1.69:                         ; Preds .B1.236
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;217.2
        test      eax, eax                                      ;218.11
        jne       .B1.152       ; Prob 5%                       ;218.11
                                ; LOE esi edi
.B1.70:                         ; Preds .B1.274 .B1.69
        mov       DWORD PTR [480+esp], 0                        ;223.3
        lea       eax, DWORD PTR [912+esp]                      ;223.3
        mov       DWORD PTR [912+esp], 8                        ;223.3
        mov       DWORD PTR [916+esp], OFFSET FLAT: __STRLITPACK_65 ;223.3
        mov       DWORD PTR [920+esp], 7                        ;223.3
        mov       DWORD PTR [924+esp], OFFSET FLAT: __STRLITPACK_64 ;223.3
        push      32                                            ;223.3
        push      eax                                           ;223.3
        push      OFFSET FLAT: __STRLITPACK_286.0.1             ;223.3
        push      -2088435965                                   ;223.3
        push      600                                           ;223.3
        push      edi                                           ;223.3
        call      _for_open                                     ;223.3
                                ; LOE eax esi edi
.B1.237:                        ; Preds .B1.70
        add       esp, 24                                       ;223.3
                                ; LOE eax esi edi
.B1.71:                         ; Preds .B1.237
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;223.3
        test      eax, eax                                      ;224.11
        jne       .B1.150       ; Prob 5%                       ;224.11
                                ; LOE esi edi
.B1.72:                         ; Preds .B1.273 .B1.71
        mov       DWORD PTR [480+esp], 0                        ;229.2
        lea       eax, DWORD PTR [928+esp]                      ;229.2
        mov       DWORD PTR [928+esp], 8                        ;229.2
        mov       DWORD PTR [932+esp], OFFSET FLAT: __STRLITPACK_61 ;229.2
        mov       DWORD PTR [936+esp], 7                        ;229.2
        mov       DWORD PTR [940+esp], OFFSET FLAT: __STRLITPACK_60 ;229.2
        push      32                                            ;229.2
        push      eax                                           ;229.2
        push      OFFSET FLAT: __STRLITPACK_289.0.1             ;229.2
        push      -2088435965                                   ;229.2
        push      700                                           ;229.2
        push      edi                                           ;229.2
        call      _for_open                                     ;229.2
                                ; LOE eax esi edi
.B1.238:                        ; Preds .B1.72
        add       esp, 24                                       ;229.2
                                ; LOE eax esi edi
.B1.73:                         ; Preds .B1.238
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;229.2
        test      eax, eax                                      ;230.11
        jne       .B1.148       ; Prob 5%                       ;230.11
                                ; LOE esi edi
.B1.74:                         ; Preds .B1.272 .B1.73
        mov       DWORD PTR [480+esp], 0                        ;235.2
        lea       eax, DWORD PTR [944+esp]                      ;235.2
        mov       DWORD PTR [944+esp], 8                        ;235.2
        mov       DWORD PTR [948+esp], OFFSET FLAT: __STRLITPACK_57 ;235.2
        mov       DWORD PTR [952+esp], 7                        ;235.2
        mov       DWORD PTR [956+esp], OFFSET FLAT: __STRLITPACK_56 ;235.2
        push      32                                            ;235.2
        push      eax                                           ;235.2
        push      OFFSET FLAT: __STRLITPACK_292.0.1             ;235.2
        push      -2088435965                                   ;235.2
        push      800                                           ;235.2
        push      edi                                           ;235.2
        call      _for_open                                     ;235.2
                                ; LOE eax esi edi
.B1.239:                        ; Preds .B1.74
        add       esp, 24                                       ;235.2
                                ; LOE eax esi edi
.B1.75:                         ; Preds .B1.239
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;235.2
        test      eax, eax                                      ;236.11
        jne       .B1.146       ; Prob 5%                       ;236.11
                                ; LOE esi edi
.B1.76:                         ; Preds .B1.271 .B1.75
        mov       DWORD PTR [480+esp], 0                        ;241.2
        lea       eax, DWORD PTR [960+esp]                      ;241.2
        mov       DWORD PTR [960+esp], 8                        ;241.2
        mov       DWORD PTR [964+esp], OFFSET FLAT: __STRLITPACK_53 ;241.2
        mov       DWORD PTR [968+esp], 7                        ;241.2
        mov       DWORD PTR [972+esp], OFFSET FLAT: __STRLITPACK_52 ;241.2
        push      32                                            ;241.2
        push      eax                                           ;241.2
        push      OFFSET FLAT: __STRLITPACK_295.0.1             ;241.2
        push      -2088435965                                   ;241.2
        push      900                                           ;241.2
        push      edi                                           ;241.2
        call      _for_open                                     ;241.2
                                ; LOE eax esi edi
.B1.240:                        ; Preds .B1.76
        add       esp, 24                                       ;241.2
                                ; LOE eax esi edi
.B1.77:                         ; Preds .B1.240
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;241.2
        test      eax, eax                                      ;242.11
        jne       .B1.144       ; Prob 5%                       ;242.11
                                ; LOE esi edi
.B1.78:                         ; Preds .B1.270 .B1.77
        mov       DWORD PTR [480+esp], 0                        ;247.2
        lea       eax, DWORD PTR [976+esp]                      ;247.2
        mov       DWORD PTR [976+esp], 10                       ;247.2
        mov       DWORD PTR [980+esp], OFFSET FLAT: __STRLITPACK_49 ;247.2
        mov       DWORD PTR [984+esp], 7                        ;247.2
        mov       DWORD PTR [988+esp], OFFSET FLAT: __STRLITPACK_48 ;247.2
        push      32                                            ;247.2
        push      eax                                           ;247.2
        push      OFFSET FLAT: __STRLITPACK_298.0.1             ;247.2
        push      -2088435965                                   ;247.2
        push      461                                           ;247.2
        push      edi                                           ;247.2
        call      _for_open                                     ;247.2
                                ; LOE eax esi edi
.B1.241:                        ; Preds .B1.78
        add       esp, 24                                       ;247.2
                                ; LOE eax esi edi
.B1.79:                         ; Preds .B1.241
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;247.2
        test      eax, eax                                      ;248.11
        jne       .B1.142       ; Prob 5%                       ;248.11
                                ; LOE esi edi
.B1.80:                         ; Preds .B1.269 .B1.79
        mov       DWORD PTR [480+esp], 0                        ;253.11
        lea       eax, DWORD PTR [1200+esp]                     ;253.11
        mov       DWORD PTR [1200+esp], 8                       ;253.11
        mov       DWORD PTR [1204+esp], OFFSET FLAT: __STRLITPACK_45 ;253.11
        mov       DWORD PTR [1208+esp], OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_TOLLEXIST ;253.11
        push      32                                            ;253.11
        push      eax                                           ;253.11
        push      OFFSET FLAT: __STRLITPACK_301.0.1             ;253.11
        push      -2088435968                                   ;253.11
        push      -1                                            ;253.11
        push      edi                                           ;253.11
        call      _for_inquire                                  ;253.11
                                ; LOE esi edi
.B1.242:                        ; Preds .B1.80
        add       esp, 24                                       ;253.11
                                ; LOE esi edi
.B1.81:                         ; Preds .B1.242
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_TOLLEXIST], 1 ;254.12
        je        .B1.84        ; Prob 60%                      ;254.12
                                ; LOE esi edi
.B1.82:                         ; Preds .B1.81
        mov       DWORD PTR [480+esp], 0                        ;254.23
        lea       eax, DWORD PTR [408+esp]                      ;254.23
        mov       DWORD PTR [408+esp], 8                        ;254.23
        mov       DWORD PTR [412+esp], OFFSET FLAT: __STRLITPACK_44 ;254.23
        mov       DWORD PTR [416+esp], 3                        ;254.23
        mov       DWORD PTR [420+esp], OFFSET FLAT: __STRLITPACK_43 ;254.23
        push      32                                            ;254.23
        push      eax                                           ;254.23
        push      OFFSET FLAT: __STRLITPACK_302.0.1             ;254.23
        push      -2088435965                                   ;254.23
        push      62                                            ;254.23
        push      edi                                           ;254.23
        call      _for_open                                     ;254.23
                                ; LOE eax esi edi
.B1.243:                        ; Preds .B1.82
        add       esp, 24                                       ;254.23
                                ; LOE eax esi edi
.B1.83:                         ; Preds .B1.243
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;254.23
        jmp       .B1.85        ; Prob 100%                     ;254.23
                                ; LOE eax esi edi
.B1.84:                         ; Preds .B1.81
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR] ;255.2
                                ; LOE eax esi edi
.B1.85:                         ; Preds .B1.83 .B1.84
        test      eax, eax                                      ;255.11
        jne       .B1.140       ; Prob 5%                       ;255.11
                                ; LOE esi edi
.B1.86:                         ; Preds .B1.268 .B1.85
        mov       DWORD PTR [480+esp], 0                        ;260.5
        lea       eax, DWORD PTR [992+esp]                      ;260.5
        mov       DWORD PTR [992+esp], 10                       ;260.5
        mov       DWORD PTR [996+esp], OFFSET FLAT: __STRLITPACK_40 ;260.5
        mov       DWORD PTR [1000+esp], 7                       ;260.5
        mov       DWORD PTR [1004+esp], OFFSET FLAT: __STRLITPACK_39 ;260.5
        push      32                                            ;260.5
        push      eax                                           ;260.5
        push      OFFSET FLAT: __STRLITPACK_305.0.1             ;260.5
        push      -2088435965                                   ;260.5
        push      888                                           ;260.5
        push      edi                                           ;260.5
        call      _for_open                                     ;260.5
                                ; LOE eax esi edi
.B1.244:                        ; Preds .B1.86
        add       esp, 24                                       ;260.5
                                ; LOE eax esi edi
.B1.87:                         ; Preds .B1.244
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;260.5
        test      eax, eax                                      ;261.11
        jne       .B1.138       ; Prob 5%                       ;261.11
                                ; LOE esi edi
.B1.88:                         ; Preds .B1.267 .B1.87
        mov       DWORD PTR [480+esp], esi                      ;265.2
        push      32                                            ;265.2
        push      esi                                           ;265.2
        push      OFFSET FLAT: __STRLITPACK_308.0.1             ;265.2
        push      -2088435968                                   ;265.2
        push      888                                           ;265.2
        push      edi                                           ;265.2
        call      _for_write_seq_lis                            ;265.2
                                ; LOE esi edi
.B1.89:                         ; Preds .B1.88
        mov       DWORD PTR [504+esp], 0                        ;267.5
        lea       eax, DWORD PTR [1032+esp]                     ;267.5
        mov       DWORD PTR [1032+esp], 18                      ;267.5
        mov       DWORD PTR [1036+esp], OFFSET FLAT: __STRLITPACK_36 ;267.5
        mov       DWORD PTR [1040+esp], 7                       ;267.5
        mov       DWORD PTR [1044+esp], OFFSET FLAT: __STRLITPACK_35 ;267.5
        push      32                                            ;267.5
        push      eax                                           ;267.5
        push      OFFSET FLAT: __STRLITPACK_309.0.1             ;267.5
        push      -2088435968                                   ;267.5
        push      97                                            ;267.5
        push      edi                                           ;267.5
        call      _for_open                                     ;267.5
                                ; LOE esi edi
.B1.90:                         ; Preds .B1.89
        mov       DWORD PTR [528+esp], 0                        ;268.2
        lea       eax, DWORD PTR [1072+esp]                     ;268.2
        mov       DWORD PTR [1072+esp], 15                      ;268.2
        mov       DWORD PTR [1076+esp], OFFSET FLAT: __STRLITPACK_34 ;268.2
        mov       DWORD PTR [1080+esp], 7                       ;268.2
        mov       DWORD PTR [1084+esp], OFFSET FLAT: __STRLITPACK_33 ;268.2
        push      32                                            ;268.2
        push      eax                                           ;268.2
        push      OFFSET FLAT: __STRLITPACK_310.0.1             ;268.2
        push      -2088435968                                   ;268.2
        push      98                                            ;268.2
        push      edi                                           ;268.2
        call      _for_open                                     ;268.2
                                ; LOE esi edi
.B1.91:                         ; Preds .B1.90
        mov       DWORD PTR [552+esp], 0                        ;270.11
        lea       eax, DWORD PTR [1252+esp]                     ;270.11
        mov       DWORD PTR [1288+esp], 7                       ;270.11
        lea       edx, DWORD PTR [1288+esp]                     ;270.11
        mov       DWORD PTR [1292+esp], OFFSET FLAT: __STRLITPACK_32 ;270.11
        mov       DWORD PTR [1296+esp], eax                     ;270.11
        push      32                                            ;270.11
        push      edx                                           ;270.11
        push      OFFSET FLAT: __STRLITPACK_311.0.1             ;270.11
        push      -2088435968                                   ;270.11
        push      -1                                            ;270.11
        push      edi                                           ;270.11
        call      _for_inquire                                  ;270.11
                                ; LOE esi edi
.B1.245:                        ; Preds .B1.91
        add       esp, 96                                       ;270.11
                                ; LOE esi edi
.B1.92:                         ; Preds .B1.245
        test      BYTE PTR [1180+esp], 1                        ;271.12
        je        .B1.95        ; Prob 60%                      ;271.12
                                ; LOE esi edi
.B1.93:                         ; Preds .B1.92
        mov       DWORD PTR [480+esp], 0                        ;271.22
        lea       eax, DWORD PTR [424+esp]                      ;271.22
        mov       DWORD PTR [424+esp], 7                        ;271.22
        mov       DWORD PTR [428+esp], OFFSET FLAT: __STRLITPACK_31 ;271.22
        mov       DWORD PTR [432+esp], 3                        ;271.22
        mov       DWORD PTR [436+esp], OFFSET FLAT: __STRLITPACK_30 ;271.22
        push      32                                            ;271.22
        push      eax                                           ;271.22
        push      OFFSET FLAT: __STRLITPACK_312.0.1             ;271.22
        push      -2088435965                                   ;271.22
        push      63                                            ;271.22
        push      edi                                           ;271.22
        call      _for_open                                     ;271.22
                                ; LOE eax esi edi
.B1.246:                        ; Preds .B1.93
        add       esp, 24                                       ;271.22
                                ; LOE eax esi edi
.B1.94:                         ; Preds .B1.246
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;271.22
        jmp       .B1.96        ; Prob 100%                     ;271.22
                                ; LOE eax esi edi
.B1.95:                         ; Preds .B1.92
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR] ;272.2
                                ; LOE eax esi edi
.B1.96:                         ; Preds .B1.94 .B1.95
        test      eax, eax                                      ;272.11
        jne       .B1.136       ; Prob 5%                       ;272.11
                                ; LOE esi edi
.B1.97:                         ; Preds .B1.266 .B1.96
        mov       DWORD PTR [480+esp], 0                        ;278.5
        lea       eax, DWORD PTR [1040+esp]                     ;278.5
        mov       DWORD PTR [1040+esp], 16                      ;278.5
        mov       DWORD PTR [1044+esp], OFFSET FLAT: __STRLITPACK_27 ;278.5
        mov       DWORD PTR [1048+esp], 7                       ;278.5
        mov       DWORD PTR [1052+esp], OFFSET FLAT: __STRLITPACK_26 ;278.5
        push      32                                            ;278.5
        push      eax                                           ;278.5
        push      OFFSET FLAT: __STRLITPACK_315.0.1             ;278.5
        push      -2088435965                                   ;278.5
        push      881                                           ;278.5
        push      edi                                           ;278.5
        call      _for_open                                     ;278.5
                                ; LOE eax esi edi
.B1.247:                        ; Preds .B1.97
        add       esp, 24                                       ;278.5
                                ; LOE eax esi edi
.B1.98:                         ; Preds .B1.247
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;278.5
        test      eax, eax                                      ;279.11
        jne       .B1.134       ; Prob 5%                       ;279.11
                                ; LOE esi edi
.B1.99:                         ; Preds .B1.265 .B1.98
        mov       DWORD PTR [480+esp], 0                        ;285.5
        lea       eax, DWORD PTR [1056+esp]                     ;285.5
        mov       DWORD PTR [1056+esp], 11                      ;285.5
        mov       DWORD PTR [1060+esp], OFFSET FLAT: __STRLITPACK_23 ;285.5
        mov       DWORD PTR [1064+esp], 7                       ;285.5
        mov       DWORD PTR [1068+esp], OFFSET FLAT: __STRLITPACK_22 ;285.5
        push      32                                            ;285.5
        push      eax                                           ;285.5
        push      OFFSET FLAT: __STRLITPACK_318.0.1             ;285.5
        push      -2088435965                                   ;285.5
        push      999                                           ;285.5
        push      edi                                           ;285.5
        call      _for_open                                     ;285.5
                                ; LOE eax esi edi
.B1.248:                        ; Preds .B1.99
        add       esp, 24                                       ;285.5
                                ; LOE eax esi edi
.B1.100:                        ; Preds .B1.248
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;285.5
        test      eax, eax                                      ;286.11
        jne       .B1.132       ; Prob 5%                       ;286.11
                                ; LOE esi edi
.B1.101:                        ; Preds .B1.264 .B1.100
        mov       DWORD PTR [480+esp], 0                        ;291.5
        lea       eax, DWORD PTR [1072+esp]                     ;291.5
        mov       DWORD PTR [1072+esp], 11                      ;291.5
        mov       DWORD PTR [1076+esp], OFFSET FLAT: __STRLITPACK_19 ;291.5
        mov       DWORD PTR [1080+esp], 7                       ;291.5
        mov       DWORD PTR [1084+esp], OFFSET FLAT: __STRLITPACK_18 ;291.5
        push      32                                            ;291.5
        push      eax                                           ;291.5
        push      OFFSET FLAT: __STRLITPACK_321.0.1             ;291.5
        push      -2088435965                                   ;291.5
        push      989                                           ;291.5
        push      edi                                           ;291.5
        call      _for_open                                     ;291.5
                                ; LOE eax esi edi
.B1.249:                        ; Preds .B1.101
        add       esp, 24                                       ;291.5
                                ; LOE eax esi edi
.B1.102:                        ; Preds .B1.249
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;291.5
        test      eax, eax                                      ;292.11
        jne       .B1.130       ; Prob 5%                       ;292.11
                                ; LOE esi edi
.B1.103:                        ; Preds .B1.263 .B1.102
        mov       DWORD PTR [480+esp], 0                        ;297.5
        lea       eax, DWORD PTR [1088+esp]                     ;297.5
        mov       DWORD PTR [1088+esp], 11                      ;297.5
        mov       DWORD PTR [1092+esp], OFFSET FLAT: __STRLITPACK_15 ;297.5
        mov       DWORD PTR [1096+esp], 7                       ;297.5
        mov       DWORD PTR [1100+esp], OFFSET FLAT: __STRLITPACK_14 ;297.5
        push      32                                            ;297.5
        push      eax                                           ;297.5
        push      OFFSET FLAT: __STRLITPACK_324.0.1             ;297.5
        push      -2088435965                                   ;297.5
        push      988                                           ;297.5
        push      edi                                           ;297.5
        call      _for_open                                     ;297.5
                                ; LOE eax esi edi
.B1.250:                        ; Preds .B1.103
        add       esp, 24                                       ;297.5
                                ; LOE eax esi edi
.B1.104:                        ; Preds .B1.250
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;297.5
        test      eax, eax                                      ;298.11
        jne       .B1.128       ; Prob 5%                       ;298.11
                                ; LOE esi edi
.B1.105:                        ; Preds .B1.262 .B1.104
        mov       DWORD PTR [480+esp], 0                        ;303.5
        lea       eax, DWORD PTR [1104+esp]                     ;303.5
        mov       DWORD PTR [1104+esp], 10                      ;303.5
        mov       DWORD PTR [1108+esp], OFFSET FLAT: __STRLITPACK_11 ;303.5
        mov       DWORD PTR [1112+esp], 7                       ;303.5
        mov       DWORD PTR [1116+esp], OFFSET FLAT: __STRLITPACK_10 ;303.5
        push      32                                            ;303.5
        push      eax                                           ;303.5
        push      OFFSET FLAT: __STRLITPACK_327.0.1             ;303.5
        push      -2088435965                                   ;303.5
        push      990                                           ;303.5
        push      edi                                           ;303.5
        call      _for_open                                     ;303.5
                                ; LOE eax esi edi
.B1.251:                        ; Preds .B1.105
        add       esp, 24                                       ;303.5
                                ; LOE eax esi edi
.B1.106:                        ; Preds .B1.251
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;303.5
        test      eax, eax                                      ;304.11
        jne       .B1.126       ; Prob 5%                       ;304.11
                                ; LOE esi edi
.B1.107:                        ; Preds .B1.261 .B1.106
        mov       DWORD PTR [480+esp], 0                        ;309.5
        lea       eax, DWORD PTR [1120+esp]                     ;309.5
        mov       DWORD PTR [1120+esp], 9                       ;309.5
        mov       DWORD PTR [1124+esp], OFFSET FLAT: __STRLITPACK_7 ;309.5
        mov       DWORD PTR [1128+esp], 7                       ;309.5
        mov       DWORD PTR [1132+esp], OFFSET FLAT: __STRLITPACK_6 ;309.5
        push      32                                            ;309.5
        push      eax                                           ;309.5
        push      OFFSET FLAT: __STRLITPACK_330.0.1             ;309.5
        push      -2088435965                                   ;309.5
        push      6099                                          ;309.5
        push      edi                                           ;309.5
        call      _for_open                                     ;309.5
                                ; LOE eax esi edi
.B1.252:                        ; Preds .B1.107
        add       esp, 24                                       ;309.5
                                ; LOE eax esi edi
.B1.108:                        ; Preds .B1.252
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;309.5
        test      eax, eax                                      ;310.11
        jne       .B1.124       ; Prob 5%                       ;310.11
                                ; LOE esi edi
.B1.109:                        ; Preds .B1.260 .B1.108
        mov       DWORD PTR [480+esp], 0                        ;315.5
        lea       eax, DWORD PTR [1136+esp]                     ;315.5
        mov       DWORD PTR [1136+esp], 6                       ;315.5
        mov       DWORD PTR [1140+esp], OFFSET FLAT: __STRLITPACK_3 ;315.5
        mov       DWORD PTR [1144+esp], 7                       ;315.5
        mov       DWORD PTR [1148+esp], OFFSET FLAT: __STRLITPACK_2 ;315.5
        push      32                                            ;315.5
        push      eax                                           ;315.5
        push      OFFSET FLAT: __STRLITPACK_333.0.1             ;315.5
        push      -2088435965                                   ;315.5
        push      7099                                          ;315.5
        push      edi                                           ;315.5
        call      _for_open                                     ;315.5
                                ; LOE eax esi edi
.B1.253:                        ; Preds .B1.109
        add       esp, 24                                       ;315.5
                                ; LOE eax esi edi
.B1.110:                        ; Preds .B1.253
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;315.5
        test      eax, eax                                      ;316.11
        jne       .B1.122       ; Prob 5%                       ;316.11
                                ; LOE esi edi
.B1.111:                        ; Preds .B1.110
        add       esp, 1236                                     ;321.5
        pop       ebx                                           ;321.5
        pop       edi                                           ;321.5
        pop       esi                                           ;321.5
        mov       esp, ebp                                      ;321.5
        pop       ebp                                           ;321.5
        ret                                                     ;321.5
                                ; LOE
.B1.112:                        ; Preds .B1.5                   ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;37.10
        lea       eax, DWORD PTR [32+esp]                       ;37.10
        mov       DWORD PTR [32+esp], 31                        ;37.10
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_183 ;37.10
        push      32                                            ;37.10
        push      eax                                           ;37.10
        push      OFFSET FLAT: __STRLITPACK_195.0.1             ;37.10
        push      -2088435968                                   ;37.10
        push      911                                           ;37.10
        push      edi                                           ;37.10
        call      _for_write_seq_lis                            ;37.10
                                ; LOE esi edi
.B1.113:                        ; Preds .B1.112                 ; Infreq
        push      32                                            ;38.5
        push      esi                                           ;38.5
        push      esi                                           ;38.5
        push      -2088435968                                   ;38.5
        push      esi                                           ;38.5
        push      OFFSET FLAT: __STRLITPACK_196                 ;38.5
        call      _for_stop_core                                ;38.5
                                ; LOE esi edi
.B1.254:                        ; Preds .B1.113                 ; Infreq
        add       esp, 48                                       ;38.5
        jmp       .B1.6         ; Prob 100%                     ;38.5
                                ; LOE esi edi
.B1.114:                        ; Preds .B1.3                   ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;32.10
        lea       eax, DWORD PTR [24+esp]                       ;32.10
        mov       DWORD PTR [24+esp], 31                        ;32.10
        mov       DWORD PTR [28+esp], OFFSET FLAT: __STRLITPACK_187 ;32.10
        push      32                                            ;32.10
        push      eax                                           ;32.10
        push      OFFSET FLAT: __STRLITPACK_192.0.1             ;32.10
        push      -2088435968                                   ;32.10
        push      911                                           ;32.10
        push      edi                                           ;32.10
        call      _for_write_seq_lis                            ;32.10
                                ; LOE edi
.B1.115:                        ; Preds .B1.114                 ; Infreq
        push      32                                            ;33.5
        xor       esi, esi                                      ;33.5
        push      esi                                           ;33.5
        push      esi                                           ;33.5
        push      -2088435968                                   ;33.5
        push      esi                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_193                 ;33.5
        call      _for_stop_core                                ;33.5
                                ; LOE esi edi
.B1.255:                        ; Preds .B1.115                 ; Infreq
        add       esp, 48                                       ;33.5
        jmp       .B1.4         ; Prob 100%                     ;33.5
                                ; LOE esi edi
.B1.116:                        ; Preds .B1.31                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;112.10
        lea       eax, DWORD PTR [esp]                          ;112.10
        mov       DWORD PTR [esp], 35                           ;112.10
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_134 ;112.10
        push      32                                            ;112.10
        push      eax                                           ;112.10
        push      OFFSET FLAT: __STRLITPACK_232.0.1             ;112.10
        push      -2088435968                                   ;112.10
        push      911                                           ;112.10
        push      edi                                           ;112.10
        call      _for_write_seq_lis                            ;112.10
                                ; LOE ebx esi edi
.B1.117:                        ; Preds .B1.116                 ; Infreq
        push      32                                            ;113.5
        push      esi                                           ;113.5
        push      esi                                           ;113.5
        push      -2088435968                                   ;113.5
        push      esi                                           ;113.5
        push      OFFSET FLAT: __STRLITPACK_233                 ;113.5
        call      _for_stop_core                                ;113.5
                                ; LOE ebx esi edi
.B1.256:                        ; Preds .B1.117                 ; Infreq
        add       esp, 48                                       ;113.5
        jmp       .B1.32        ; Prob 100%                     ;113.5
                                ; LOE ebx esi edi
.B1.118:                        ; Preds .B1.35                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;121.10
        lea       eax, DWORD PTR [8+esp]                        ;121.10
        mov       DWORD PTR [8+esp], 33                         ;121.10
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_129 ;121.10
        push      32                                            ;121.10
        push      eax                                           ;121.10
        push      OFFSET FLAT: __STRLITPACK_236.0.1             ;121.10
        push      -2088435968                                   ;121.10
        push      911                                           ;121.10
        push      edi                                           ;121.10
        call      _for_write_seq_lis                            ;121.10
                                ; LOE ebx esi edi
.B1.119:                        ; Preds .B1.118                 ; Infreq
        push      32                                            ;122.5
        push      esi                                           ;122.5
        push      esi                                           ;122.5
        push      -2088435968                                   ;122.5
        push      esi                                           ;122.5
        push      OFFSET FLAT: __STRLITPACK_237                 ;122.5
        call      _for_stop_core                                ;122.5
                                ; LOE ebx esi edi
.B1.257:                        ; Preds .B1.119                 ; Infreq
        add       esp, 48                                       ;122.5
        jmp       .B1.36        ; Prob 100%                     ;122.5
                                ; LOE ebx esi edi
.B1.120:                        ; Preds .B1.39                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;130.10
        lea       eax, DWORD PTR [16+esp]                       ;130.10
        mov       DWORD PTR [16+esp], 39                        ;130.10
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_124 ;130.10
        push      32                                            ;130.10
        push      eax                                           ;130.10
        push      OFFSET FLAT: __STRLITPACK_240.0.1             ;130.10
        push      -2088435968                                   ;130.10
        push      911                                           ;130.10
        push      edi                                           ;130.10
        call      _for_write_seq_lis                            ;130.10
                                ; LOE esi edi
.B1.121:                        ; Preds .B1.120                 ; Infreq
        push      32                                            ;131.5
        push      esi                                           ;131.5
        push      esi                                           ;131.5
        push      -2088435968                                   ;131.5
        push      esi                                           ;131.5
        push      OFFSET FLAT: __STRLITPACK_241                 ;131.5
        call      _for_stop_core                                ;131.5
                                ; LOE esi edi
.B1.258:                        ; Preds .B1.121                 ; Infreq
        add       esp, 48                                       ;131.5
        jmp       .B1.40        ; Prob 100%                     ;131.5
                                ; LOE esi edi
.B1.122:                        ; Preds .B1.110                 ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;317.10
        lea       eax, DWORD PTR [352+esp]                      ;317.10
        mov       DWORD PTR [352+esp], 25                       ;317.10
        mov       DWORD PTR [356+esp], OFFSET FLAT: __STRLITPACK_0 ;317.10
        push      32                                            ;317.10
        push      eax                                           ;317.10
        push      OFFSET FLAT: __STRLITPACK_334.0.1             ;317.10
        push      -2088435968                                   ;317.10
        push      911                                           ;317.10
        push      edi                                           ;317.10
        call      _for_write_seq_lis                            ;317.10
                                ; LOE esi
.B1.123:                        ; Preds .B1.122                 ; Infreq
        push      32                                            ;318.5
        push      esi                                           ;318.5
        push      esi                                           ;318.5
        push      -2088435968                                   ;318.5
        push      esi                                           ;318.5
        push      OFFSET FLAT: __STRLITPACK_335                 ;318.5
        call      _for_stop_core                                ;318.5
                                ; LOE
.B1.259:                        ; Preds .B1.123                 ; Infreq
        add       esp, 1284                                     ;318.5
        pop       ebx                                           ;318.5
        pop       edi                                           ;318.5
        pop       esi                                           ;318.5
        mov       esp, ebp                                      ;318.5
        pop       ebp                                           ;318.5
        ret                                                     ;318.5
                                ; LOE
.B1.124:                        ; Preds .B1.108                 ; Infreq
        mov       eax, 32                                       ;311.10
        lea       edx, DWORD PTR [344+esp]                      ;311.10
        mov       DWORD PTR [480+esp], 0                        ;311.10
        mov       DWORD PTR [344+esp], eax                      ;311.10
        mov       DWORD PTR [348+esp], OFFSET FLAT: __STRLITPACK_4 ;311.10
        push      eax                                           ;311.10
        push      edx                                           ;311.10
        push      OFFSET FLAT: __STRLITPACK_331.0.1             ;311.10
        push      -2088435968                                   ;311.10
        push      911                                           ;311.10
        push      edi                                           ;311.10
        call      _for_write_seq_lis                            ;311.10
                                ; LOE esi edi
.B1.125:                        ; Preds .B1.124                 ; Infreq
        push      32                                            ;312.5
        push      esi                                           ;312.5
        push      esi                                           ;312.5
        push      -2088435968                                   ;312.5
        push      esi                                           ;312.5
        push      OFFSET FLAT: __STRLITPACK_332                 ;312.5
        call      _for_stop_core                                ;312.5
                                ; LOE esi edi
.B1.260:                        ; Preds .B1.125                 ; Infreq
        add       esp, 48                                       ;312.5
        jmp       .B1.109       ; Prob 100%                     ;312.5
                                ; LOE esi edi
.B1.126:                        ; Preds .B1.106                 ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;305.10
        lea       eax, DWORD PTR [336+esp]                      ;305.10
        mov       DWORD PTR [336+esp], 29                       ;305.10
        mov       DWORD PTR [340+esp], OFFSET FLAT: __STRLITPACK_8 ;305.10
        push      32                                            ;305.10
        push      eax                                           ;305.10
        push      OFFSET FLAT: __STRLITPACK_328.0.1             ;305.10
        push      -2088435968                                   ;305.10
        push      911                                           ;305.10
        push      edi                                           ;305.10
        call      _for_write_seq_lis                            ;305.10
                                ; LOE esi edi
.B1.127:                        ; Preds .B1.126                 ; Infreq
        push      32                                            ;306.5
        push      esi                                           ;306.5
        push      esi                                           ;306.5
        push      -2088435968                                   ;306.5
        push      esi                                           ;306.5
        push      OFFSET FLAT: __STRLITPACK_329                 ;306.5
        call      _for_stop_core                                ;306.5
                                ; LOE esi edi
.B1.261:                        ; Preds .B1.127                 ; Infreq
        add       esp, 48                                       ;306.5
        jmp       .B1.107       ; Prob 100%                     ;306.5
                                ; LOE esi edi
.B1.128:                        ; Preds .B1.104                 ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;299.10
        lea       eax, DWORD PTR [328+esp]                      ;299.10
        mov       DWORD PTR [328+esp], 30                       ;299.10
        mov       DWORD PTR [332+esp], OFFSET FLAT: __STRLITPACK_12 ;299.10
        push      32                                            ;299.10
        push      eax                                           ;299.10
        push      OFFSET FLAT: __STRLITPACK_325.0.1             ;299.10
        push      -2088435968                                   ;299.10
        push      911                                           ;299.10
        push      edi                                           ;299.10
        call      _for_write_seq_lis                            ;299.10
                                ; LOE esi edi
.B1.129:                        ; Preds .B1.128                 ; Infreq
        push      32                                            ;300.5
        push      esi                                           ;300.5
        push      esi                                           ;300.5
        push      -2088435968                                   ;300.5
        push      esi                                           ;300.5
        push      OFFSET FLAT: __STRLITPACK_326                 ;300.5
        call      _for_stop_core                                ;300.5
                                ; LOE esi edi
.B1.262:                        ; Preds .B1.129                 ; Infreq
        add       esp, 48                                       ;300.5
        jmp       .B1.105       ; Prob 100%                     ;300.5
                                ; LOE esi edi
.B1.130:                        ; Preds .B1.102                 ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;293.10
        lea       eax, DWORD PTR [320+esp]                      ;293.10
        mov       DWORD PTR [320+esp], 30                       ;293.10
        mov       DWORD PTR [324+esp], OFFSET FLAT: __STRLITPACK_16 ;293.10
        push      32                                            ;293.10
        push      eax                                           ;293.10
        push      OFFSET FLAT: __STRLITPACK_322.0.1             ;293.10
        push      -2088435968                                   ;293.10
        push      911                                           ;293.10
        push      edi                                           ;293.10
        call      _for_write_seq_lis                            ;293.10
                                ; LOE esi edi
.B1.131:                        ; Preds .B1.130                 ; Infreq
        push      32                                            ;294.5
        push      esi                                           ;294.5
        push      esi                                           ;294.5
        push      -2088435968                                   ;294.5
        push      esi                                           ;294.5
        push      OFFSET FLAT: __STRLITPACK_323                 ;294.5
        call      _for_stop_core                                ;294.5
                                ; LOE esi edi
.B1.263:                        ; Preds .B1.131                 ; Infreq
        add       esp, 48                                       ;294.5
        jmp       .B1.103       ; Prob 100%                     ;294.5
                                ; LOE esi edi
.B1.132:                        ; Preds .B1.100                 ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;287.10
        lea       eax, DWORD PTR [312+esp]                      ;287.10
        mov       DWORD PTR [312+esp], 30                       ;287.10
        mov       DWORD PTR [316+esp], OFFSET FLAT: __STRLITPACK_20 ;287.10
        push      32                                            ;287.10
        push      eax                                           ;287.10
        push      OFFSET FLAT: __STRLITPACK_319.0.1             ;287.10
        push      -2088435968                                   ;287.10
        push      911                                           ;287.10
        push      edi                                           ;287.10
        call      _for_write_seq_lis                            ;287.10
                                ; LOE esi edi
.B1.133:                        ; Preds .B1.132                 ; Infreq
        push      32                                            ;288.5
        push      esi                                           ;288.5
        push      esi                                           ;288.5
        push      -2088435968                                   ;288.5
        push      esi                                           ;288.5
        push      OFFSET FLAT: __STRLITPACK_320                 ;288.5
        call      _for_stop_core                                ;288.5
                                ; LOE esi edi
.B1.264:                        ; Preds .B1.133                 ; Infreq
        add       esp, 48                                       ;288.5
        jmp       .B1.101       ; Prob 100%                     ;288.5
                                ; LOE esi edi
.B1.134:                        ; Preds .B1.98                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;280.10
        lea       eax, DWORD PTR [304+esp]                      ;280.10
        mov       DWORD PTR [304+esp], 35                       ;280.10
        mov       DWORD PTR [308+esp], OFFSET FLAT: __STRLITPACK_24 ;280.10
        push      32                                            ;280.10
        push      eax                                           ;280.10
        push      OFFSET FLAT: __STRLITPACK_316.0.1             ;280.10
        push      -2088435968                                   ;280.10
        push      911                                           ;280.10
        push      edi                                           ;280.10
        call      _for_write_seq_lis                            ;280.10
                                ; LOE esi edi
.B1.135:                        ; Preds .B1.134                 ; Infreq
        push      32                                            ;281.5
        push      esi                                           ;281.5
        push      esi                                           ;281.5
        push      -2088435968                                   ;281.5
        push      esi                                           ;281.5
        push      OFFSET FLAT: __STRLITPACK_317                 ;281.5
        call      _for_stop_core                                ;281.5
                                ; LOE esi edi
.B1.265:                        ; Preds .B1.135                 ; Infreq
        add       esp, 48                                       ;281.5
        jmp       .B1.99        ; Prob 100%                     ;281.5
                                ; LOE esi edi
.B1.136:                        ; Preds .B1.96                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;273.10
        lea       eax, DWORD PTR [296+esp]                      ;273.10
        mov       DWORD PTR [296+esp], 26                       ;273.10
        mov       DWORD PTR [300+esp], OFFSET FLAT: __STRLITPACK_28 ;273.10
        push      32                                            ;273.10
        push      eax                                           ;273.10
        push      OFFSET FLAT: __STRLITPACK_313.0.1             ;273.10
        push      -2088435968                                   ;273.10
        push      911                                           ;273.10
        push      edi                                           ;273.10
        call      _for_write_seq_lis                            ;273.10
                                ; LOE esi edi
.B1.137:                        ; Preds .B1.136                 ; Infreq
        push      32                                            ;274.5
        push      esi                                           ;274.5
        push      esi                                           ;274.5
        push      -2088435968                                   ;274.5
        push      esi                                           ;274.5
        push      OFFSET FLAT: __STRLITPACK_314                 ;274.5
        call      _for_stop_core                                ;274.5
                                ; LOE esi edi
.B1.266:                        ; Preds .B1.137                 ; Infreq
        add       esp, 48                                       ;274.5
        jmp       .B1.97        ; Prob 100%                     ;274.5
                                ; LOE esi edi
.B1.138:                        ; Preds .B1.87                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;262.10
        lea       eax, DWORD PTR [288+esp]                      ;262.10
        mov       DWORD PTR [288+esp], 29                       ;262.10
        mov       DWORD PTR [292+esp], OFFSET FLAT: __STRLITPACK_37 ;262.10
        push      32                                            ;262.10
        push      eax                                           ;262.10
        push      OFFSET FLAT: __STRLITPACK_306.0.1             ;262.10
        push      -2088435968                                   ;262.10
        push      911                                           ;262.10
        push      edi                                           ;262.10
        call      _for_write_seq_lis                            ;262.10
                                ; LOE esi edi
.B1.139:                        ; Preds .B1.138                 ; Infreq
        push      32                                            ;263.5
        push      esi                                           ;263.5
        push      esi                                           ;263.5
        push      -2088435968                                   ;263.5
        push      esi                                           ;263.5
        push      OFFSET FLAT: __STRLITPACK_307                 ;263.5
        call      _for_stop_core                                ;263.5
                                ; LOE esi edi
.B1.267:                        ; Preds .B1.139                 ; Infreq
        add       esp, 48                                       ;263.5
        jmp       .B1.88        ; Prob 100%                     ;263.5
                                ; LOE esi edi
.B1.140:                        ; Preds .B1.85                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;256.10
        lea       eax, DWORD PTR [280+esp]                      ;256.10
        mov       DWORD PTR [280+esp], 27                       ;256.10
        mov       DWORD PTR [284+esp], OFFSET FLAT: __STRLITPACK_41 ;256.10
        push      32                                            ;256.10
        push      eax                                           ;256.10
        push      OFFSET FLAT: __STRLITPACK_303.0.1             ;256.10
        push      -2088435968                                   ;256.10
        push      911                                           ;256.10
        push      edi                                           ;256.10
        call      _for_write_seq_lis                            ;256.10
                                ; LOE esi edi
.B1.141:                        ; Preds .B1.140                 ; Infreq
        push      32                                            ;257.5
        push      esi                                           ;257.5
        push      esi                                           ;257.5
        push      -2088435968                                   ;257.5
        push      esi                                           ;257.5
        push      OFFSET FLAT: __STRLITPACK_304                 ;257.5
        call      _for_stop_core                                ;257.5
                                ; LOE esi edi
.B1.268:                        ; Preds .B1.141                 ; Infreq
        add       esp, 48                                       ;257.5
        jmp       .B1.86        ; Prob 100%                     ;257.5
                                ; LOE esi edi
.B1.142:                        ; Preds .B1.79                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;249.10
        lea       eax, DWORD PTR [272+esp]                      ;249.10
        mov       DWORD PTR [272+esp], 29                       ;249.10
        mov       DWORD PTR [276+esp], OFFSET FLAT: __STRLITPACK_46 ;249.10
        push      32                                            ;249.10
        push      eax                                           ;249.10
        push      OFFSET FLAT: __STRLITPACK_299.0.1             ;249.10
        push      -2088435968                                   ;249.10
        push      911                                           ;249.10
        push      edi                                           ;249.10
        call      _for_write_seq_lis                            ;249.10
                                ; LOE esi edi
.B1.143:                        ; Preds .B1.142                 ; Infreq
        push      32                                            ;250.5
        push      esi                                           ;250.5
        push      esi                                           ;250.5
        push      -2088435968                                   ;250.5
        push      esi                                           ;250.5
        push      OFFSET FLAT: __STRLITPACK_300                 ;250.5
        call      _for_stop_core                                ;250.5
                                ; LOE esi edi
.B1.269:                        ; Preds .B1.143                 ; Infreq
        add       esp, 48                                       ;250.5
        jmp       .B1.80        ; Prob 100%                     ;250.5
                                ; LOE esi edi
.B1.144:                        ; Preds .B1.77                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;243.10
        lea       eax, DWORD PTR [264+esp]                      ;243.10
        mov       DWORD PTR [264+esp], 27                       ;243.10
        mov       DWORD PTR [268+esp], OFFSET FLAT: __STRLITPACK_50 ;243.10
        push      32                                            ;243.10
        push      eax                                           ;243.10
        push      OFFSET FLAT: __STRLITPACK_296.0.1             ;243.10
        push      -2088435968                                   ;243.10
        push      911                                           ;243.10
        push      edi                                           ;243.10
        call      _for_write_seq_lis                            ;243.10
                                ; LOE esi edi
.B1.145:                        ; Preds .B1.144                 ; Infreq
        push      32                                            ;244.5
        push      esi                                           ;244.5
        push      esi                                           ;244.5
        push      -2088435968                                   ;244.5
        push      esi                                           ;244.5
        push      OFFSET FLAT: __STRLITPACK_297                 ;244.5
        call      _for_stop_core                                ;244.5
                                ; LOE esi edi
.B1.270:                        ; Preds .B1.145                 ; Infreq
        add       esp, 48                                       ;244.5
        jmp       .B1.78        ; Prob 100%                     ;244.5
                                ; LOE esi edi
.B1.146:                        ; Preds .B1.75                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;237.10
        lea       eax, DWORD PTR [256+esp]                      ;237.10
        mov       DWORD PTR [256+esp], 27                       ;237.10
        mov       DWORD PTR [260+esp], OFFSET FLAT: __STRLITPACK_54 ;237.10
        push      32                                            ;237.10
        push      eax                                           ;237.10
        push      OFFSET FLAT: __STRLITPACK_293.0.1             ;237.10
        push      -2088435968                                   ;237.10
        push      911                                           ;237.10
        push      edi                                           ;237.10
        call      _for_write_seq_lis                            ;237.10
                                ; LOE esi edi
.B1.147:                        ; Preds .B1.146                 ; Infreq
        push      32                                            ;238.5
        push      esi                                           ;238.5
        push      esi                                           ;238.5
        push      -2088435968                                   ;238.5
        push      esi                                           ;238.5
        push      OFFSET FLAT: __STRLITPACK_294                 ;238.5
        call      _for_stop_core                                ;238.5
                                ; LOE esi edi
.B1.271:                        ; Preds .B1.147                 ; Infreq
        add       esp, 48                                       ;238.5
        jmp       .B1.76        ; Prob 100%                     ;238.5
                                ; LOE esi edi
.B1.148:                        ; Preds .B1.73                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;231.10
        lea       eax, DWORD PTR [248+esp]                      ;231.10
        mov       DWORD PTR [248+esp], 27                       ;231.10
        mov       DWORD PTR [252+esp], OFFSET FLAT: __STRLITPACK_58 ;231.10
        push      32                                            ;231.10
        push      eax                                           ;231.10
        push      OFFSET FLAT: __STRLITPACK_290.0.1             ;231.10
        push      -2088435968                                   ;231.10
        push      911                                           ;231.10
        push      edi                                           ;231.10
        call      _for_write_seq_lis                            ;231.10
                                ; LOE esi edi
.B1.149:                        ; Preds .B1.148                 ; Infreq
        push      32                                            ;232.5
        push      esi                                           ;232.5
        push      esi                                           ;232.5
        push      -2088435968                                   ;232.5
        push      esi                                           ;232.5
        push      OFFSET FLAT: __STRLITPACK_291                 ;232.5
        call      _for_stop_core                                ;232.5
                                ; LOE esi edi
.B1.272:                        ; Preds .B1.149                 ; Infreq
        add       esp, 48                                       ;232.5
        jmp       .B1.74        ; Prob 100%                     ;232.5
                                ; LOE esi edi
.B1.150:                        ; Preds .B1.71                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;225.10
        lea       eax, DWORD PTR [240+esp]                      ;225.10
        mov       DWORD PTR [240+esp], 27                       ;225.10
        mov       DWORD PTR [244+esp], OFFSET FLAT: __STRLITPACK_62 ;225.10
        push      32                                            ;225.10
        push      eax                                           ;225.10
        push      OFFSET FLAT: __STRLITPACK_287.0.1             ;225.10
        push      -2088435968                                   ;225.10
        push      911                                           ;225.10
        push      edi                                           ;225.10
        call      _for_write_seq_lis                            ;225.10
                                ; LOE esi edi
.B1.151:                        ; Preds .B1.150                 ; Infreq
        push      32                                            ;226.5
        push      esi                                           ;226.5
        push      esi                                           ;226.5
        push      -2088435968                                   ;226.5
        push      esi                                           ;226.5
        push      OFFSET FLAT: __STRLITPACK_288                 ;226.5
        call      _for_stop_core                                ;226.5
                                ; LOE esi edi
.B1.273:                        ; Preds .B1.151                 ; Infreq
        add       esp, 48                                       ;226.5
        jmp       .B1.72        ; Prob 100%                     ;226.5
                                ; LOE esi edi
.B1.152:                        ; Preds .B1.69                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;219.10
        lea       eax, DWORD PTR [232+esp]                      ;219.10
        mov       DWORD PTR [232+esp], 29                       ;219.10
        mov       DWORD PTR [236+esp], OFFSET FLAT: __STRLITPACK_66 ;219.10
        push      32                                            ;219.10
        push      eax                                           ;219.10
        push      OFFSET FLAT: __STRLITPACK_284.0.1             ;219.10
        push      -2088435968                                   ;219.10
        push      911                                           ;219.10
        push      edi                                           ;219.10
        call      _for_write_seq_lis                            ;219.10
                                ; LOE esi edi
.B1.153:                        ; Preds .B1.152                 ; Infreq
        push      32                                            ;220.5
        push      esi                                           ;220.5
        push      esi                                           ;220.5
        push      -2088435968                                   ;220.5
        push      esi                                           ;220.5
        push      OFFSET FLAT: __STRLITPACK_285                 ;220.5
        call      _for_stop_core                                ;220.5
                                ; LOE esi edi
.B1.274:                        ; Preds .B1.153                 ; Infreq
        add       esp, 48                                       ;220.5
        jmp       .B1.70        ; Prob 100%                     ;220.5
                                ; LOE esi edi
.B1.154:                        ; Preds .B1.67                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;213.10
        lea       eax, DWORD PTR [224+esp]                      ;213.10
        mov       DWORD PTR [224+esp], 34                       ;213.10
        mov       DWORD PTR [228+esp], OFFSET FLAT: __STRLITPACK_70 ;213.10
        push      32                                            ;213.10
        push      eax                                           ;213.10
        push      OFFSET FLAT: __STRLITPACK_281.0.1             ;213.10
        push      -2088435968                                   ;213.10
        push      911                                           ;213.10
        push      edi                                           ;213.10
        call      _for_write_seq_lis                            ;213.10
                                ; LOE esi edi
.B1.155:                        ; Preds .B1.154                 ; Infreq
        push      32                                            ;214.5
        push      esi                                           ;214.5
        push      esi                                           ;214.5
        push      -2088435968                                   ;214.5
        push      esi                                           ;214.5
        push      OFFSET FLAT: __STRLITPACK_282                 ;214.5
        call      _for_stop_core                                ;214.5
                                ; LOE esi edi
.B1.275:                        ; Preds .B1.155                 ; Infreq
        add       esp, 48                                       ;214.5
        jmp       .B1.68        ; Prob 100%                     ;214.5
                                ; LOE esi edi
.B1.156:                        ; Preds .B1.65                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;207.10
        lea       eax, DWORD PTR [216+esp]                      ;207.10
        mov       DWORD PTR [216+esp], 36                       ;207.10
        mov       DWORD PTR [220+esp], OFFSET FLAT: __STRLITPACK_74 ;207.10
        push      32                                            ;207.10
        push      eax                                           ;207.10
        push      OFFSET FLAT: __STRLITPACK_278.0.1             ;207.10
        push      -2088435968                                   ;207.10
        push      911                                           ;207.10
        push      edi                                           ;207.10
        call      _for_write_seq_lis                            ;207.10
                                ; LOE esi edi
.B1.157:                        ; Preds .B1.156                 ; Infreq
        push      32                                            ;208.5
        push      esi                                           ;208.5
        push      esi                                           ;208.5
        push      -2088435968                                   ;208.5
        push      esi                                           ;208.5
        push      OFFSET FLAT: __STRLITPACK_279                 ;208.5
        call      _for_stop_core                                ;208.5
                                ; LOE esi edi
.B1.276:                        ; Preds .B1.157                 ; Infreq
        add       esp, 48                                       ;208.5
        jmp       .B1.66        ; Prob 100%                     ;208.5
                                ; LOE esi edi
.B1.158:                        ; Preds .B1.63                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;201.10
        lea       eax, DWORD PTR [208+esp]                      ;201.10
        mov       DWORD PTR [208+esp], 36                       ;201.10
        mov       DWORD PTR [212+esp], OFFSET FLAT: __STRLITPACK_78 ;201.10
        push      32                                            ;201.10
        push      eax                                           ;201.10
        push      OFFSET FLAT: __STRLITPACK_275.0.1             ;201.10
        push      -2088435968                                   ;201.10
        push      911                                           ;201.10
        push      edi                                           ;201.10
        call      _for_write_seq_lis                            ;201.10
                                ; LOE esi edi
.B1.159:                        ; Preds .B1.158                 ; Infreq
        push      32                                            ;202.5
        push      esi                                           ;202.5
        push      esi                                           ;202.5
        push      -2088435968                                   ;202.5
        push      esi                                           ;202.5
        push      OFFSET FLAT: __STRLITPACK_276                 ;202.5
        call      _for_stop_core                                ;202.5
                                ; LOE esi edi
.B1.277:                        ; Preds .B1.159                 ; Infreq
        add       esp, 48                                       ;202.5
        jmp       .B1.64        ; Prob 100%                     ;202.5
                                ; LOE esi edi
.B1.160:                        ; Preds .B1.59                  ; Infreq
        mov       eax, 32                                       ;192.10
        lea       edx, DWORD PTR [200+esp]                      ;192.10
        mov       DWORD PTR [480+esp], 0                        ;192.10
        mov       DWORD PTR [200+esp], eax                      ;192.10
        mov       DWORD PTR [204+esp], OFFSET FLAT: __STRLITPACK_84 ;192.10
        push      eax                                           ;192.10
        push      edx                                           ;192.10
        push      OFFSET FLAT: __STRLITPACK_270.0.1             ;192.10
        push      -2088435968                                   ;192.10
        push      911                                           ;192.10
        push      edi                                           ;192.10
        call      _for_write_seq_lis                            ;192.10
                                ; LOE esi edi
.B1.161:                        ; Preds .B1.160                 ; Infreq
        push      32                                            ;193.5
        push      esi                                           ;193.5
        push      esi                                           ;193.5
        push      -2088435968                                   ;193.5
        push      esi                                           ;193.5
        push      OFFSET FLAT: __STRLITPACK_271                 ;193.5
        call      _for_stop_core                                ;193.5
                                ; LOE esi edi
.B1.278:                        ; Preds .B1.161                 ; Infreq
        add       esp, 48                                       ;193.5
        jmp       .B1.60        ; Prob 100%                     ;193.5
                                ; LOE esi edi
.B1.162:                        ; Preds .B1.57                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;186.10
        lea       eax, DWORD PTR [192+esp]                      ;186.10
        mov       DWORD PTR [192+esp], 27                       ;186.10
        mov       DWORD PTR [196+esp], OFFSET FLAT: __STRLITPACK_88 ;186.10
        push      32                                            ;186.10
        push      eax                                           ;186.10
        push      OFFSET FLAT: __STRLITPACK_267.0.1             ;186.10
        push      -2088435968                                   ;186.10
        push      911                                           ;186.10
        push      edi                                           ;186.10
        call      _for_write_seq_lis                            ;186.10
                                ; LOE esi edi
.B1.163:                        ; Preds .B1.162                 ; Infreq
        push      32                                            ;187.5
        push      esi                                           ;187.5
        push      esi                                           ;187.5
        push      -2088435968                                   ;187.5
        push      esi                                           ;187.5
        push      OFFSET FLAT: __STRLITPACK_268                 ;187.5
        call      _for_stop_core                                ;187.5
                                ; LOE esi edi
.B1.279:                        ; Preds .B1.163                 ; Infreq
        add       esp, 48                                       ;187.5
        jmp       .B1.58        ; Prob 100%                     ;187.5
                                ; LOE esi edi
.B1.164:                        ; Preds .B1.55                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;180.10
        lea       eax, DWORD PTR [184+esp]                      ;180.10
        mov       DWORD PTR [184+esp], 30                       ;180.10
        mov       DWORD PTR [188+esp], OFFSET FLAT: __STRLITPACK_92 ;180.10
        push      32                                            ;180.10
        push      eax                                           ;180.10
        push      OFFSET FLAT: __STRLITPACK_264.0.1             ;180.10
        push      -2088435968                                   ;180.10
        push      911                                           ;180.10
        push      edi                                           ;180.10
        call      _for_write_seq_lis                            ;180.10
                                ; LOE esi edi
.B1.165:                        ; Preds .B1.164                 ; Infreq
        push      32                                            ;181.5
        push      esi                                           ;181.5
        push      esi                                           ;181.5
        push      -2088435968                                   ;181.5
        push      esi                                           ;181.5
        push      OFFSET FLAT: __STRLITPACK_265                 ;181.5
        call      _for_stop_core                                ;181.5
                                ; LOE esi edi
.B1.280:                        ; Preds .B1.165                 ; Infreq
        add       esp, 48                                       ;181.5
        jmp       .B1.56        ; Prob 100%                     ;181.5
                                ; LOE esi edi
.B1.166:                        ; Preds .B1.53                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;174.10
        lea       eax, DWORD PTR [176+esp]                      ;174.10
        mov       DWORD PTR [176+esp], 36                       ;174.10
        mov       DWORD PTR [180+esp], OFFSET FLAT: __STRLITPACK_96 ;174.10
        push      32                                            ;174.10
        push      eax                                           ;174.10
        push      OFFSET FLAT: __STRLITPACK_261.0.1             ;174.10
        push      -2088435968                                   ;174.10
        push      911                                           ;174.10
        push      edi                                           ;174.10
        call      _for_write_seq_lis                            ;174.10
                                ; LOE esi edi
.B1.167:                        ; Preds .B1.166                 ; Infreq
        push      32                                            ;175.5
        push      esi                                           ;175.5
        push      esi                                           ;175.5
        push      -2088435968                                   ;175.5
        push      esi                                           ;175.5
        push      OFFSET FLAT: __STRLITPACK_262                 ;175.5
        call      _for_stop_core                                ;175.5
                                ; LOE esi edi
.B1.281:                        ; Preds .B1.167                 ; Infreq
        add       esp, 48                                       ;175.5
        jmp       .B1.54        ; Prob 100%                     ;175.5
                                ; LOE esi edi
.B1.168:                        ; Preds .B1.51                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;168.10
        lea       eax, DWORD PTR [168+esp]                      ;168.10
        mov       DWORD PTR [168+esp], 29                       ;168.10
        mov       DWORD PTR [172+esp], OFFSET FLAT: __STRLITPACK_100 ;168.10
        push      32                                            ;168.10
        push      eax                                           ;168.10
        push      OFFSET FLAT: __STRLITPACK_258.0.1             ;168.10
        push      -2088435968                                   ;168.10
        push      911                                           ;168.10
        push      edi                                           ;168.10
        call      _for_write_seq_lis                            ;168.10
                                ; LOE esi edi
.B1.169:                        ; Preds .B1.168                 ; Infreq
        push      32                                            ;169.5
        push      esi                                           ;169.5
        push      esi                                           ;169.5
        push      -2088435968                                   ;169.5
        push      esi                                           ;169.5
        push      OFFSET FLAT: __STRLITPACK_259                 ;169.5
        call      _for_stop_core                                ;169.5
                                ; LOE esi edi
.B1.282:                        ; Preds .B1.169                 ; Infreq
        add       esp, 48                                       ;169.5
        jmp       .B1.52        ; Prob 100%                     ;169.5
                                ; LOE esi edi
.B1.170:                        ; Preds .B1.49                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;162.10
        lea       eax, DWORD PTR [160+esp]                      ;162.10
        mov       DWORD PTR [160+esp], 37                       ;162.10
        mov       DWORD PTR [164+esp], OFFSET FLAT: __STRLITPACK_104 ;162.10
        push      32                                            ;162.10
        push      eax                                           ;162.10
        push      OFFSET FLAT: __STRLITPACK_255.0.1             ;162.10
        push      -2088435968                                   ;162.10
        push      911                                           ;162.10
        push      edi                                           ;162.10
        call      _for_write_seq_lis                            ;162.10
                                ; LOE esi edi
.B1.171:                        ; Preds .B1.170                 ; Infreq
        push      32                                            ;163.5
        push      esi                                           ;163.5
        push      esi                                           ;163.5
        push      -2088435968                                   ;163.5
        push      esi                                           ;163.5
        push      OFFSET FLAT: __STRLITPACK_256                 ;163.5
        call      _for_stop_core                                ;163.5
                                ; LOE esi edi
.B1.283:                        ; Preds .B1.171                 ; Infreq
        add       esp, 48                                       ;163.5
        jmp       .B1.50        ; Prob 100%                     ;163.5
                                ; LOE esi edi
.B1.172:                        ; Preds .B1.47                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;156.10
        lea       eax, DWORD PTR [152+esp]                      ;156.10
        mov       DWORD PTR [152+esp], 31                       ;156.10
        mov       DWORD PTR [156+esp], OFFSET FLAT: __STRLITPACK_108 ;156.10
        push      32                                            ;156.10
        push      eax                                           ;156.10
        push      OFFSET FLAT: __STRLITPACK_252.0.1             ;156.10
        push      -2088435968                                   ;156.10
        push      911                                           ;156.10
        push      edi                                           ;156.10
        call      _for_write_seq_lis                            ;156.10
                                ; LOE esi edi
.B1.173:                        ; Preds .B1.172                 ; Infreq
        push      32                                            ;157.5
        push      esi                                           ;157.5
        push      esi                                           ;157.5
        push      -2088435968                                   ;157.5
        push      esi                                           ;157.5
        push      OFFSET FLAT: __STRLITPACK_253                 ;157.5
        call      _for_stop_core                                ;157.5
                                ; LOE esi edi
.B1.284:                        ; Preds .B1.173                 ; Infreq
        add       esp, 48                                       ;157.5
        jmp       .B1.48        ; Prob 100%                     ;157.5
                                ; LOE esi edi
.B1.174:                        ; Preds .B1.45                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;149.10
        lea       eax, DWORD PTR [144+esp]                      ;149.10
        mov       DWORD PTR [144+esp], 31                       ;149.10
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_112 ;149.10
        push      32                                            ;149.10
        push      eax                                           ;149.10
        push      OFFSET FLAT: __STRLITPACK_249.0.1             ;149.10
        push      -2088435968                                   ;149.10
        push      911                                           ;149.10
        push      edi                                           ;149.10
        call      _for_write_seq_lis                            ;149.10
                                ; LOE esi edi
.B1.175:                        ; Preds .B1.174                 ; Infreq
        push      32                                            ;150.5
        push      esi                                           ;150.5
        push      esi                                           ;150.5
        push      -2088435968                                   ;150.5
        push      esi                                           ;150.5
        push      OFFSET FLAT: __STRLITPACK_250                 ;150.5
        call      _for_stop_core                                ;150.5
                                ; LOE esi edi
.B1.285:                        ; Preds .B1.175                 ; Infreq
        add       esp, 48                                       ;150.5
        jmp       .B1.46        ; Prob 100%                     ;150.5
                                ; LOE esi edi
.B1.176:                        ; Preds .B1.43                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;143.10
        lea       eax, DWORD PTR [136+esp]                      ;143.10
        mov       DWORD PTR [136+esp], 34                       ;143.10
        mov       DWORD PTR [140+esp], OFFSET FLAT: __STRLITPACK_116 ;143.10
        push      32                                            ;143.10
        push      eax                                           ;143.10
        push      OFFSET FLAT: __STRLITPACK_246.0.1             ;143.10
        push      -2088435968                                   ;143.10
        push      911                                           ;143.10
        push      edi                                           ;143.10
        call      _for_write_seq_lis                            ;143.10
                                ; LOE esi edi
.B1.177:                        ; Preds .B1.176                 ; Infreq
        push      32                                            ;144.5
        push      esi                                           ;144.5
        push      esi                                           ;144.5
        push      -2088435968                                   ;144.5
        push      esi                                           ;144.5
        push      OFFSET FLAT: __STRLITPACK_247                 ;144.5
        call      _for_stop_core                                ;144.5
                                ; LOE esi edi
.B1.286:                        ; Preds .B1.177                 ; Infreq
        add       esp, 48                                       ;144.5
        jmp       .B1.44        ; Prob 100%                     ;144.5
                                ; LOE esi edi
.B1.178:                        ; Preds .B1.41                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;137.10
        lea       eax, DWORD PTR [128+esp]                      ;137.10
        mov       DWORD PTR [128+esp], 34                       ;137.10
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_120 ;137.10
        push      32                                            ;137.10
        push      eax                                           ;137.10
        push      OFFSET FLAT: __STRLITPACK_243.0.1             ;137.10
        push      -2088435968                                   ;137.10
        push      911                                           ;137.10
        push      edi                                           ;137.10
        call      _for_write_seq_lis                            ;137.10
                                ; LOE esi edi
.B1.179:                        ; Preds .B1.178                 ; Infreq
        push      32                                            ;138.5
        push      esi                                           ;138.5
        push      esi                                           ;138.5
        push      -2088435968                                   ;138.5
        push      esi                                           ;138.5
        push      OFFSET FLAT: __STRLITPACK_244                 ;138.5
        call      _for_stop_core                                ;138.5
                                ; LOE esi edi
.B1.287:                        ; Preds .B1.179                 ; Infreq
        add       esp, 48                                       ;138.5
        jmp       .B1.42        ; Prob 100%                     ;138.5
                                ; LOE esi edi
.B1.180:                        ; Preds .B1.27                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;104.10
        lea       eax, DWORD PTR [120+esp]                      ;104.10
        mov       DWORD PTR [120+esp], 34                       ;104.10
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_139 ;104.10
        push      32                                            ;104.10
        push      eax                                           ;104.10
        push      OFFSET FLAT: __STRLITPACK_228.0.1             ;104.10
        push      -2088435968                                   ;104.10
        push      911                                           ;104.10
        push      edi                                           ;104.10
        call      _for_write_seq_lis                            ;104.10
                                ; LOE esi edi
.B1.181:                        ; Preds .B1.180                 ; Infreq
        push      32                                            ;105.5
        push      esi                                           ;105.5
        push      esi                                           ;105.5
        push      -2088435968                                   ;105.5
        push      esi                                           ;105.5
        push      OFFSET FLAT: __STRLITPACK_229                 ;105.5
        call      _for_stop_core                                ;105.5
                                ; LOE esi edi
.B1.288:                        ; Preds .B1.181                 ; Infreq
        add       esp, 48                                       ;105.5
        jmp       .B1.28        ; Prob 100%                     ;105.5
                                ; LOE esi edi
.B1.182:                        ; Preds .B1.25                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;98.10
        lea       eax, DWORD PTR [112+esp]                      ;98.10
        mov       DWORD PTR [112+esp], 29                       ;98.10
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_143 ;98.10
        push      32                                            ;98.10
        push      eax                                           ;98.10
        push      OFFSET FLAT: __STRLITPACK_225.0.1             ;98.10
        push      -2088435968                                   ;98.10
        push      911                                           ;98.10
        push      edi                                           ;98.10
        call      _for_write_seq_lis                            ;98.10
                                ; LOE esi edi
.B1.183:                        ; Preds .B1.182                 ; Infreq
        push      32                                            ;99.5
        push      esi                                           ;99.5
        push      esi                                           ;99.5
        push      -2088435968                                   ;99.5
        push      esi                                           ;99.5
        push      OFFSET FLAT: __STRLITPACK_226                 ;99.5
        call      _for_stop_core                                ;99.5
                                ; LOE esi edi
.B1.289:                        ; Preds .B1.183                 ; Infreq
        add       esp, 48                                       ;99.5
        jmp       .B1.26        ; Prob 100%                     ;99.5
                                ; LOE esi edi
.B1.184:                        ; Preds .B1.23                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;92.10
        lea       eax, DWORD PTR [104+esp]                      ;92.10
        mov       DWORD PTR [104+esp], 26                       ;92.10
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_147 ;92.10
        push      32                                            ;92.10
        push      eax                                           ;92.10
        push      OFFSET FLAT: __STRLITPACK_222.0.1             ;92.10
        push      -2088435968                                   ;92.10
        push      911                                           ;92.10
        push      edi                                           ;92.10
        call      _for_write_seq_lis                            ;92.10
                                ; LOE esi edi
.B1.185:                        ; Preds .B1.184                 ; Infreq
        push      32                                            ;93.5
        push      esi                                           ;93.5
        push      esi                                           ;93.5
        push      -2088435968                                   ;93.5
        push      esi                                           ;93.5
        push      OFFSET FLAT: __STRLITPACK_223                 ;93.5
        call      _for_stop_core                                ;93.5
                                ; LOE esi edi
.B1.290:                        ; Preds .B1.185                 ; Infreq
        add       esp, 48                                       ;93.5
        jmp       .B1.24        ; Prob 100%                     ;93.5
                                ; LOE esi edi
.B1.186:                        ; Preds .B1.21                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;86.10
        lea       eax, DWORD PTR [96+esp]                       ;86.10
        mov       DWORD PTR [96+esp], 30                        ;86.10
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_151 ;86.10
        push      32                                            ;86.10
        push      eax                                           ;86.10
        push      OFFSET FLAT: __STRLITPACK_219.0.1             ;86.10
        push      -2088435968                                   ;86.10
        push      911                                           ;86.10
        push      edi                                           ;86.10
        call      _for_write_seq_lis                            ;86.10
                                ; LOE esi edi
.B1.187:                        ; Preds .B1.186                 ; Infreq
        push      32                                            ;87.5
        push      esi                                           ;87.5
        push      esi                                           ;87.5
        push      -2088435968                                   ;87.5
        push      esi                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_220                 ;87.5
        call      _for_stop_core                                ;87.5
                                ; LOE esi edi
.B1.291:                        ; Preds .B1.187                 ; Infreq
        add       esp, 48                                       ;87.5
        jmp       .B1.22        ; Prob 100%                     ;87.5
                                ; LOE esi edi
.B1.188:                        ; Preds .B1.19                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;80.10
        lea       eax, DWORD PTR [88+esp]                       ;80.10
        mov       DWORD PTR [88+esp], 31                        ;80.10
        mov       DWORD PTR [92+esp], OFFSET FLAT: __STRLITPACK_155 ;80.10
        push      32                                            ;80.10
        push      eax                                           ;80.10
        push      OFFSET FLAT: __STRLITPACK_216.0.1             ;80.10
        push      -2088435968                                   ;80.10
        push      911                                           ;80.10
        push      edi                                           ;80.10
        call      _for_write_seq_lis                            ;80.10
                                ; LOE esi edi
.B1.189:                        ; Preds .B1.188                 ; Infreq
        push      32                                            ;81.5
        push      esi                                           ;81.5
        push      esi                                           ;81.5
        push      -2088435968                                   ;81.5
        push      esi                                           ;81.5
        push      OFFSET FLAT: __STRLITPACK_217                 ;81.5
        call      _for_stop_core                                ;81.5
                                ; LOE esi edi
.B1.292:                        ; Preds .B1.189                 ; Infreq
        add       esp, 48                                       ;81.5
        jmp       .B1.20        ; Prob 100%                     ;81.5
                                ; LOE esi edi
.B1.190:                        ; Preds .B1.17                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;74.10
        lea       eax, DWORD PTR [80+esp]                       ;74.10
        mov       DWORD PTR [80+esp], 31                        ;74.10
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_159 ;74.10
        push      32                                            ;74.10
        push      eax                                           ;74.10
        push      OFFSET FLAT: __STRLITPACK_213.0.1             ;74.10
        push      -2088435968                                   ;74.10
        push      911                                           ;74.10
        push      edi                                           ;74.10
        call      _for_write_seq_lis                            ;74.10
                                ; LOE esi edi
.B1.191:                        ; Preds .B1.190                 ; Infreq
        push      32                                            ;75.5
        push      esi                                           ;75.5
        push      esi                                           ;75.5
        push      -2088435968                                   ;75.5
        push      esi                                           ;75.5
        push      OFFSET FLAT: __STRLITPACK_214                 ;75.5
        call      _for_stop_core                                ;75.5
                                ; LOE esi edi
.B1.293:                        ; Preds .B1.191                 ; Infreq
        add       esp, 48                                       ;75.5
        jmp       .B1.18        ; Prob 100%                     ;75.5
                                ; LOE esi edi
.B1.192:                        ; Preds .B1.15                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;68.10
        lea       eax, DWORD PTR [72+esp]                       ;68.10
        mov       DWORD PTR [72+esp], 27                        ;68.10
        mov       DWORD PTR [76+esp], OFFSET FLAT: __STRLITPACK_163 ;68.10
        push      32                                            ;68.10
        push      eax                                           ;68.10
        push      OFFSET FLAT: __STRLITPACK_210.0.1             ;68.10
        push      -2088435968                                   ;68.10
        push      911                                           ;68.10
        push      edi                                           ;68.10
        call      _for_write_seq_lis                            ;68.10
                                ; LOE esi edi
.B1.193:                        ; Preds .B1.192                 ; Infreq
        push      32                                            ;69.5
        push      esi                                           ;69.5
        push      esi                                           ;69.5
        push      -2088435968                                   ;69.5
        push      esi                                           ;69.5
        push      OFFSET FLAT: __STRLITPACK_211                 ;69.5
        call      _for_stop_core                                ;69.5
                                ; LOE esi edi
.B1.294:                        ; Preds .B1.193                 ; Infreq
        add       esp, 48                                       ;69.5
        jmp       .B1.16        ; Prob 100%                     ;69.5
                                ; LOE esi edi
.B1.194:                        ; Preds .B1.13                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;62.10
        lea       eax, DWORD PTR [64+esp]                       ;62.10
        mov       DWORD PTR [64+esp], 30                        ;62.10
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_167 ;62.10
        push      32                                            ;62.10
        push      eax                                           ;62.10
        push      OFFSET FLAT: __STRLITPACK_207.0.1             ;62.10
        push      -2088435968                                   ;62.10
        push      911                                           ;62.10
        push      edi                                           ;62.10
        call      _for_write_seq_lis                            ;62.10
                                ; LOE esi edi
.B1.195:                        ; Preds .B1.194                 ; Infreq
        push      32                                            ;63.5
        push      esi                                           ;63.5
        push      esi                                           ;63.5
        push      -2088435968                                   ;63.5
        push      esi                                           ;63.5
        push      OFFSET FLAT: __STRLITPACK_208                 ;63.5
        call      _for_stop_core                                ;63.5
                                ; LOE esi edi
.B1.295:                        ; Preds .B1.195                 ; Infreq
        add       esp, 48                                       ;63.5
        jmp       .B1.14        ; Prob 100%                     ;63.5
                                ; LOE esi edi
.B1.196:                        ; Preds .B1.11                  ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;56.10
        lea       eax, DWORD PTR [56+esp]                       ;56.10
        mov       DWORD PTR [56+esp], 31                        ;56.10
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_171 ;56.10
        push      32                                            ;56.10
        push      eax                                           ;56.10
        push      OFFSET FLAT: __STRLITPACK_204.0.1             ;56.10
        push      -2088435968                                   ;56.10
        push      911                                           ;56.10
        push      edi                                           ;56.10
        call      _for_write_seq_lis                            ;56.10
                                ; LOE esi edi
.B1.197:                        ; Preds .B1.196                 ; Infreq
        push      32                                            ;57.5
        push      esi                                           ;57.5
        push      esi                                           ;57.5
        push      -2088435968                                   ;57.5
        push      esi                                           ;57.5
        push      OFFSET FLAT: __STRLITPACK_205                 ;57.5
        call      _for_stop_core                                ;57.5
                                ; LOE esi edi
.B1.296:                        ; Preds .B1.197                 ; Infreq
        add       esp, 48                                       ;57.5
        jmp       .B1.12        ; Prob 100%                     ;57.5
                                ; LOE esi edi
.B1.198:                        ; Preds .B1.9                   ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;50.10
        lea       eax, DWORD PTR [48+esp]                       ;50.10
        mov       DWORD PTR [48+esp], 29                        ;50.10
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_175 ;50.10
        push      32                                            ;50.10
        push      eax                                           ;50.10
        push      OFFSET FLAT: __STRLITPACK_201.0.1             ;50.10
        push      -2088435968                                   ;50.10
        push      911                                           ;50.10
        push      edi                                           ;50.10
        call      _for_write_seq_lis                            ;50.10
                                ; LOE esi edi
.B1.199:                        ; Preds .B1.198                 ; Infreq
        push      32                                            ;51.5
        push      esi                                           ;51.5
        push      esi                                           ;51.5
        push      -2088435968                                   ;51.5
        push      esi                                           ;51.5
        push      OFFSET FLAT: __STRLITPACK_202                 ;51.5
        call      _for_stop_core                                ;51.5
                                ; LOE esi edi
.B1.297:                        ; Preds .B1.199                 ; Infreq
        add       esp, 48                                       ;51.5
        jmp       .B1.10        ; Prob 100%                     ;51.5
                                ; LOE esi edi
.B1.200:                        ; Preds .B1.7                   ; Infreq
        mov       DWORD PTR [480+esp], 0                        ;44.10
        lea       eax, DWORD PTR [40+esp]                       ;44.10
        mov       DWORD PTR [40+esp], 30                        ;44.10
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_179 ;44.10
        push      32                                            ;44.10
        push      eax                                           ;44.10
        push      OFFSET FLAT: __STRLITPACK_198.0.1             ;44.10
        push      -2088435968                                   ;44.10
        push      911                                           ;44.10
        push      edi                                           ;44.10
        call      _for_write_seq_lis                            ;44.10
                                ; LOE esi edi
.B1.201:                        ; Preds .B1.200                 ; Infreq
        push      32                                            ;45.5
        push      esi                                           ;45.5
        push      esi                                           ;45.5
        push      -2088435968                                   ;45.5
        push      esi                                           ;45.5
        push      OFFSET FLAT: __STRLITPACK_199                 ;45.5
        call      _for_stop_core                                ;45.5
                                ; LOE esi edi
.B1.298:                        ; Preds .B1.201                 ; Infreq
        add       esp, 48                                       ;45.5
        jmp       .B1.8         ; Prob 100%                     ;45.5
        ALIGN     16
                                ; LOE esi edi
; mark_end;
_OPENFILE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_191.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_192.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_194.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_195.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_197.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_198.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_200.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_201.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_203.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_204.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_206.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_207.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_209.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_210.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_212.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_213.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_215.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_216.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_218.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_219.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_221.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_222.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_224.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_225.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_227.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_228.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_230.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_231.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_232.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_234.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_235.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_236.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_238.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_239.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_240.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_242.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_243.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_245.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_246.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_248.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_249.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_251.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_252.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_254.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_255.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_257.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_258.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_260.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_261.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_263.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_264.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_266.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_267.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_269.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_270.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_272.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_273.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_274.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_275.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_277.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_278.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_280.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_281.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_283.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_284.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_286.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_287.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_289.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_290.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_292.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_293.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_295.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_296.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_298.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_299.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_301.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_302.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_303.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_305.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_306.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_308.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_309.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_310.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_311.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_312.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_313.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_315.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_316.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_318.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_319.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_321.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_322.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_324.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_325.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_327.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_328.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_330.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_331.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_333.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_334.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _OPENFILE
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_190	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	76
	DB	111
	DB	103
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_189	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_187	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	76
	DB	111
	DB	103
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_193	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_186	DB	69
	DB	112
	DB	111
	DB	99
	DB	104
	DB	79
	DB	117
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_185	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_183	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	69
	DB	112
	DB	111
	DB	99
	DB	104
	DB	79
	DB	117
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_196	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_182	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_181	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_179	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_199	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_178	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_177	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_175	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_202	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_174	DB	115
	DB	99
	DB	101
	DB	110
	DB	97
	DB	114
	DB	105
	DB	111
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_173	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_171	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	115
	DB	99
	DB	101
	DB	110
	DB	97
	DB	114
	DB	105
	DB	111
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_205	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_170	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_169	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_167	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_208	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_166	DB	114
	DB	97
	DB	109
	DB	112
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_165	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_163	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	114
	DB	97
	DB	109
	DB	112
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_211	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_162	DB	105
	DB	110
	DB	99
	DB	105
	DB	100
	DB	101
	DB	110
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_161	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_159	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	105
	DB	110
	DB	99
	DB	105
	DB	100
	DB	101
	DB	110
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_214	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_158	DB	109
	DB	111
	DB	118
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_157	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_155	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	109
	DB	111
	DB	118
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_217	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_154	DB	108
	DB	101
	DB	102
	DB	116
	DB	99
	DB	97
	DB	112
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_153	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_151	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	108
	DB	101
	DB	102
	DB	116
	DB	99
	DB	97
	DB	112
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_220	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_150	DB	118
	DB	109
	DB	115
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_149	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_147	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	118
	DB	109
	DB	115
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_223	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_146	DB	111
	DB	114
	DB	105
	DB	103
	DB	105
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_145	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_143	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	111
	DB	114
	DB	105
	DB	103
	DB	105
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_226	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_142	DB	100
	DB	101
	DB	115
	DB	116
	DB	105
	DB	110
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_141	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_139	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	100
	DB	101
	DB	115
	DB	116
	DB	105
	DB	110
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_229	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_138	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	95
	DB	116
	DB	114
	DB	117
	DB	99
	DB	107
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_137	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	95
	DB	116
	DB	114
	DB	117
	DB	99
	DB	107
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_136	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_134	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	95
	DB	116
	DB	114
	DB	117
	DB	99
	DB	107
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_233	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_133	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	95
	DB	104
	DB	111
	DB	118
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_132	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	95
	DB	104
	DB	111
	DB	118
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_131	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_129	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	95
	DB	104
	DB	111
	DB	118
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_237	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_128	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	95
	DB	115
	DB	117
	DB	112
	DB	101
	DB	114
	DB	122
	DB	111
	DB	110
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_127	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	95
	DB	115
	DB	117
	DB	112
	DB	101
	DB	114
	DB	122
	DB	111
	DB	110
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_126	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_124	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	95
	DB	115
	DB	117
	DB	112
	DB	101
	DB	114
	DB	122
	DB	111
	DB	110
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_241	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_123	DB	83
	DB	84
	DB	79
	DB	80
	DB	67
	DB	97
	DB	112
	DB	52
	DB	87
	DB	97
	DB	121
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_122	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_120	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	67
	DB	97
	DB	112
	DB	52
	DB	87
	DB	97
	DB	121
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_244	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_119	DB	83
	DB	84
	DB	79
	DB	80
	DB	67
	DB	97
	DB	112
	DB	50
	DB	87
	DB	97
	DB	121
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_118	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_116	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	67
	DB	97
	DB	112
	DB	50
	DB	87
	DB	97
	DB	121
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_247	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_115	DB	89
	DB	105
	DB	101
	DB	108
	DB	100
	DB	67
	DB	97
	DB	112
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_114	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_112	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	89
	DB	105
	DB	101
	DB	108
	DB	100
	DB	67
	DB	97
	DB	112
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_250	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_111	DB	87
	DB	111
	DB	114
	DB	107
	DB	90
	DB	111
	DB	110
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_110	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_108	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	87
	DB	111
	DB	114
	DB	107
	DB	90
	DB	111
	DB	110
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_253	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_107	DB	71
	DB	114
	DB	97
	DB	100
	DB	101
	DB	76
	DB	101
	DB	110
	DB	103
	DB	116
	DB	104
	DB	80
	DB	67
	DB	69
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_106	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_104	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	71
	DB	114
	DB	97
	DB	100
	DB	101
	DB	76
	DB	101
	DB	110
	DB	103
	DB	116
	DB	104
	DB	80
	DB	67
	DB	69
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_256	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_103	DB	115
	DB	121
	DB	115
	DB	116
	DB	101
	DB	109
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_102	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_100	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	115
	DB	121
	DB	115
	DB	116
	DB	101
	DB	109
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_259	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_99	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	111
	DB	112
	DB	116
	DB	105
	DB	111
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_98	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_96	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	111
	DB	112
	DB	116
	DB	105
	DB	111
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_262	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_95	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_94	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_92	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_265	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_91	DB	112
	DB	97
	DB	116
	DB	104
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_90	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_88	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	112
	DB	97
	DB	116
	DB	104
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_268	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_87	DB	69
	DB	120
	DB	101
	DB	99
	DB	117
	DB	116
	DB	105
	DB	110
	DB	103
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_86	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_84	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	101
	DB	120
	DB	101
	DB	99
	DB	117
	DB	116
	DB	105
	DB	110
	DB	103
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_271	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_82	DB	68
	DB	89
	DB	78
	DB	85
	DB	83
	DB	84
	DB	32
	DB	73
	DB	83
	DB	32
	DB	82
	DB	85
	DB	78
	DB	78
	DB	73
	DB	78
	DB	71
	DB	46
	DB	46
	DB	46
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_81	DB	86
	DB	101
	DB	104
	DB	84
	DB	114
	DB	97
	DB	106
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	121
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_80	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_78	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	86
	DB	101
	DB	104
	DB	84
	DB	114
	DB	97
	DB	106
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	121
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_276	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_77	DB	66
	DB	117
	DB	115
	DB	84
	DB	114
	DB	97
	DB	106
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	121
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_76	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_74	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	66
	DB	117
	DB	115
	DB	84
	DB	114
	DB	97
	DB	106
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	121
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_279	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_73	DB	83
	DB	117
	DB	109
	DB	109
	DB	97
	DB	114
	DB	121
	DB	83
	DB	116
	DB	97
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_72	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_70	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	83
	DB	117
	DB	109
	DB	109
	DB	97
	DB	114
	DB	121
	DB	83
	DB	116
	DB	97
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_282	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69	DB	79
	DB	117
	DB	116
	DB	77
	DB	85
	DB	67
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_68	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_66	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	79
	DB	117
	DB	116
	DB	109
	DB	116
	DB	99
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_285	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65	DB	102
	DB	111
	DB	114
	DB	116
	DB	46
	DB	54
	DB	48
	DB	48
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_62	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	102
	DB	111
	DB	114
	DB	116
	DB	46
	DB	54
	DB	48
	DB	48
	DB	0
__STRLITPACK_288	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61	DB	102
	DB	111
	DB	114
	DB	116
	DB	46
	DB	55
	DB	48
	DB	48
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_58	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	102
	DB	111
	DB	114
	DB	116
	DB	46
	DB	55
	DB	48
	DB	48
	DB	0
__STRLITPACK_291	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57	DB	102
	DB	111
	DB	114
	DB	116
	DB	46
	DB	56
	DB	48
	DB	48
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_54	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	102
	DB	111
	DB	114
	DB	116
	DB	46
	DB	56
	DB	48
	DB	48
	DB	0
__STRLITPACK_294	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53	DB	102
	DB	111
	DB	114
	DB	116
	DB	46
	DB	57
	DB	48
	DB	48
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_52	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_50	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	102
	DB	111
	DB	114
	DB	116
	DB	46
	DB	57
	DB	48
	DB	48
	DB	0
__STRLITPACK_297	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49	DB	104
	DB	97
	DB	122
	DB	109
	DB	97
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_48	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_46	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	104
	DB	97
	DB	122
	DB	109
	DB	97
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_300	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45	DB	116
	DB	111
	DB	108
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_44	DB	116
	DB	111
	DB	108
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_43	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_41	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	116
	DB	111
	DB	108
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_304	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_40	DB	86
	DB	101
	DB	104
	DB	80
	DB	111
	DB	115
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_39	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_37	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	86
	DB	101
	DB	104
	DB	80
	DB	111
	DB	115
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_307	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_36	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_35	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_34	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	112
	DB	97
	DB	116
	DB	104
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_33	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_32	DB	83
	DB	73
	DB	82
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_31	DB	83
	DB	73
	DB	82
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_30	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_28	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	83
	DB	73
	DB	82
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_314	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27	DB	83
	DB	112
	DB	97
	DB	99
	DB	101
	DB	84
	DB	105
	DB	109
	DB	101
	DB	80
	DB	97
	DB	114
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_24	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	83
	DB	112
	DB	97
	DB	99
	DB	101
	DB	84
	DB	105
	DB	109
	DB	101
	DB	80
	DB	97
	DB	114
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_317	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23	DB	69
	DB	120
	DB	101
	DB	99
	DB	76
	DB	111
	DB	103
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_22	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_20	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	101
	DB	120
	DB	101
	DB	99
	DB	108
	DB	111
	DB	103
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_320	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19	DB	65
	DB	108
	DB	116
	DB	80
	DB	97
	DB	116
	DB	104
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_18	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_16	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	65
	DB	108
	DB	116
	DB	80
	DB	97
	DB	116
	DB	104
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_323	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15	DB	65
	DB	108
	DB	116
	DB	84
	DB	105
	DB	109
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_14	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_12	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	65
	DB	108
	DB	116
	DB	84
	DB	105
	DB	109
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_326	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_11	DB	65
	DB	108
	DB	116
	DB	69
	DB	110
	DB	81
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_10	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_8	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	65
	DB	108
	DB	116
	DB	69
	DB	110
	DB	113
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_329	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7	DB	109
	DB	116
	DB	99
	DB	112
	DB	104
	DB	46
	DB	105
	DB	116
	DB	102
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_6	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_4	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	109
	DB	116
	DB	99
	DB	112
	DB	104
	DB	46
	DB	105
	DB	102
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_332	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_3	DB	120
	DB	121
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_2	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_0	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	120
	DB	121
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_335	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_TOLLEXIST:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ERROR:BYTE
_DATA	ENDS
EXTRN	_for_close:PROC
EXTRN	_for_inquire:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_open:PROC
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
