; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _WINSETUP
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _WINSETUP
_WINSETUP	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        push      ebx                                           ;1.12
        mov       ebx, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      ebp                                           ;1.12
        push      ebp                                           ;1.12
        mov       ebp, DWORD PTR [4+ebx]                        ;1.12
        mov       DWORD PTR [4+esp], ebp                        ;1.12
        mov       ebp, esp                                      ;1.12
        sub       esp, 472                                      ;1.12
        xor       eax, eax                                      ;40.33
        mov       DWORD PTR [-8+ebp], edi                       ;1.12
        mov       DWORD PTR [-12+ebp], esi                      ;1.12
        mov       DWORD PTR [-128+ebp], eax                     ;40.33
        mov       DWORD PTR [-124+ebp], eax                     ;40.33
        mov       DWORD PTR [-120+ebp], eax                     ;40.33
        mov       DWORD PTR [-116+ebp], 128                     ;40.33
        mov       DWORD PTR [-112+ebp], 1                       ;40.33
        mov       DWORD PTR [-108+ebp], eax                     ;40.33
        mov       DWORD PTR [-104+ebp], eax                     ;40.33
        mov       DWORD PTR [-100+ebp], eax                     ;40.33
        mov       DWORD PTR [-96+ebp], eax                      ;40.33
        call      _INITIALSETTINGS                              ;49.10
                                ; LOE
.B1.2:                          ; Preds .B1.1
        call      __initializefonts                             ;50.10
                                ; LOE
.B1.3:                          ; Preds .B1.2
        push      200                                           ;51.9
        push      OFFSET FLAT: _WINSETUP$STRPATH.0.1            ;51.9
        call      _GETCWD                                       ;51.9
                                ; LOE f1
.B1.61:                         ; Preds .B1.3
        fstp      st(0)                                         ;
        add       esp, 8                                        ;51.9
                                ; LOE
.B1.4:                          ; Preds .B1.61
        mov       edx, 200                                      ;52.10
        mov       edi, OFFSET FLAT: _WINSETUP$STRPATH.0.1       ;52.10
        push      edx                                           ;52.10
        push      edi                                           ;52.10
        call      _for_len_trim                                 ;52.10
                                ; LOE eax edi
.B1.63:                         ; Preds .B1.4
        mov       edx, 200                                      ;
        add       esp, 8                                        ;52.10
        push      edx                                           ;53.11
        push      edi                                           ;53.11
        push      edx                                           ;53.11
        mov       esi, eax                                      ;52.10
        lea       eax, DWORD PTR [-472+ebp]                     ;53.11
        push      eax                                           ;53.11
        call      _for_trim                                     ;53.11
                                ; LOE eax esi
.B1.62:                         ; Preds .B1.63
        add       esp, 16                                       ;53.11
                                ; LOE eax esi
.B1.5:                          ; Preds .B1.62
        xor       edi, edi                                      ;53.1
        lea       ecx, DWORD PTR [-472+ebp]                     ;53.1
        cdq                                                     ;53.1
        push      edi                                           ;53.1
        push      edx                                           ;53.1
        push      eax                                           ;53.1
        push      ecx                                           ;53.1
        push      edi                                           ;53.1
        push      200                                           ;53.1
        push      OFFSET FLAT: _WINSETUP$STRPATH.0.1            ;53.1
        call      _for_cpystr                                   ;53.1
                                ; LOE esi
.B1.64:                         ; Preds .B1.5
        add       esp, 28                                       ;53.1
                                ; LOE esi
.B1.6:                          ; Preds .B1.64
        mov       eax, 1                                        ;55.1
        lea       edx, DWORD PTR [-68+ebp]                      ;55.1
        push      eax                                           ;55.1
        push      esi                                           ;55.1
        push      2                                             ;55.1
        push      edx                                           ;55.1
        mov       DWORD PTR [-116+ebp], 133                     ;55.1
        mov       DWORD PTR [-124+ebp], esi                     ;55.1
        mov       DWORD PTR [-112+ebp], eax                     ;55.1
        mov       DWORD PTR [-120+ebp], 0                       ;55.1
        mov       DWORD PTR [-96+ebp], eax                      ;55.1
        mov       DWORD PTR [-104+ebp], eax                     ;55.1
        mov       DWORD PTR [-100+ebp], esi                     ;55.1
        call      _for_check_mult_overflow                      ;55.1
                                ; LOE eax esi
.B1.65:                         ; Preds .B1.6
        add       esp, 16                                       ;55.1
                                ; LOE eax esi
.B1.7:                          ; Preds .B1.65
        mov       edx, DWORD PTR [-116+ebp]                     ;55.1
        and       eax, 1                                        ;55.1
        and       edx, 1                                        ;55.1
        lea       ecx, DWORD PTR [-128+ebp]                     ;55.1
        shl       eax, 4                                        ;55.1
        add       edx, edx                                      ;55.1
        or        edx, eax                                      ;55.1
        or        edx, 262144                                   ;55.1
        push      edx                                           ;55.1
        push      ecx                                           ;55.1
        push      DWORD PTR [-68+ebp]                           ;55.1
        call      _for_alloc_allocatable                        ;55.1
                                ; LOE esi
.B1.66:                         ; Preds .B1.7
        add       esp, 12                                       ;55.1
                                ; LOE esi
.B1.8:                          ; Preds .B1.66
        mov       edi, 1                                        ;57.15
        lea       ecx, DWORD PTR [-30+esi]                      ;56.20
        test      ecx, ecx                                      ;57.15
        mov       eax, DWORD PTR [-124+ebp]                     ;57.1
        cmovle    ecx, edi                                      ;57.15
        mov       edi, DWORD PTR [-96+ebp]                      ;57.1
        imul      edi, eax                                      ;57.1
        push      0                                             ;57.1
        lea       edx, DWORD PTR [-1+ecx]                       ;57.15
        mov       DWORD PTR [-272+ebp], edx                     ;57.15
        mov       edx, DWORD PTR [-128+ebp]                     ;57.1
        lea       ecx, DWORD PTR [_WINSETUP$STRPATH.0.1-1+ecx]  ;57.1
        add       edx, eax                                      ;57.1
        sub       edx, edi                                      ;57.1
        sub       esi, DWORD PTR [-272+ebp]                     ;57.1
        mov       DWORD PTR [-268+ebp], edx                     ;57.1
        cdq                                                     ;57.1
        mov       eax, 0                                        ;57.1
        mov       edi, edx                                      ;57.1
        cmovs     esi, eax                                      ;57.1
        mov       eax, esi                                      ;57.1
        cdq                                                     ;57.1
        push      edx                                           ;57.1
        push      esi                                           ;57.1
        push      ecx                                           ;57.1
        push      edi                                           ;57.1
        push      DWORD PTR [-124+ebp]                          ;57.1
        push      DWORD PTR [-268+ebp]                          ;57.1
        call      _for_cpystr                                   ;57.1
                                ; LOE
.B1.67:                         ; Preds .B1.8
        add       esp, 28                                       ;57.1
                                ; LOE
.B1.9:                          ; Preds .B1.67
        push      OFFSET FLAT: _WINSETUP$WC.0.1                 ;59.10
        call      __f_getwindowconfig                           ;59.10
                                ; LOE
.B1.68:                         ; Preds .B1.9
        add       esp, 4                                        ;59.10
                                ; LOE
.B1.10:                         ; Preds .B1.68
        movzx     eax, BYTE PTR [__STRLITPACK_13+10]            ;60.1
        push      1                                             ;61.1
        pop       edi                                           ;61.1
        movzx     edx, BYTE PTR [__STRLITPACK_13+11]            ;60.1
        mov       BYTE PTR [_WINSETUP$WC.0.1+118], al           ;60.1
        mov       BYTE PTR [_WINSETUP$WC.0.1+119], dl           ;60.1
        movzx     eax, BYTE PTR [__STRLITPACK_13+13]            ;60.1
        movzx     edx, BYTE PTR [__STRLITPACK_13+14]            ;60.1
        mov       BYTE PTR [_WINSETUP$WC.0.1+121], al           ;60.1
        mov       eax, 256                                      ;62.1
        mov       BYTE PTR [_WINSETUP$WC.0.1+122], dl           ;60.1
        mov       WORD PTR [_WINSETUP$WC.0.1+8], ax             ;62.1
        mov       eax, DWORD PTR [-124+ebp]                     ;63.12
        mov       edx, DWORD PTR [-96+ebp]                      ;63.1
        imul      edx, eax                                      ;63.1
        movq      xmm0, QWORD PTR [__STRLITPACK_13+1]           ;60.1
        movdqa    xmm1, XMMWORD PTR [_2il0floatpacket.9]        ;60.1
        movdqu    XMMWORD PTR [_WINSETUP$WC.0.1+123], xmm1      ;60.1
        mov       BYTE PTR [_WINSETUP$WC.0.1+108], 84           ;60.1
        movzx     ecx, BYTE PTR [__STRLITPACK_13+9]             ;60.1
        mov       BYTE PTR [_WINSETUP$WC.0.1+117], cl           ;60.1
        movzx     ecx, BYTE PTR [__STRLITPACK_13+12]            ;60.1
        mov       esi, DWORD PTR [-128+ebp]                     ;63.1
        add       esi, eax                                      ;63.1
        mov       BYTE PTR [_WINSETUP$WC.0.1+120], cl           ;60.1
        xor       ecx, ecx                                      ;63.1
        sub       esi, edx                                      ;63.1
        cdq                                                     ;63.1
        push      ecx                                           ;63.1
        push      edx                                           ;63.1
        push      eax                                           ;63.1
        push      esi                                           ;63.1
        push      ecx                                           ;63.1
        push      80                                            ;63.1
        push      OFFSET FLAT: _WINSETUP$WC.0.1+14              ;63.1
        movq      QWORD PTR [_WINSETUP$WC.0.1+109], xmm0        ;60.1
        mov       BYTE PTR [_WINSETUP$WC.0.1+139], 32           ;60.1
        mov       WORD PTR [_WINSETUP$WC.0.1+98], di            ;61.1
        call      _for_cpystr                                   ;63.1
                                ; LOE
.B1.69:                         ; Preds .B1.10
        add       esp, 28                                       ;63.1
                                ; LOE
.B1.11:                         ; Preds .B1.69
        push      OFFSET FLAT: _WINSETUP$WC.0.1                 ;64.10
        call      __f_setwindowconfig                           ;64.10
                                ; LOE
.B1.70:                         ; Preds .B1.11
        add       esp, 4                                        ;64.10
                                ; LOE
.B1.12:                         ; Preds .B1.70
        push      OFFSET FLAT: _WINSETUP$WINFO.0.1              ;67.10
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;67.10
        mov       esi, 2                                        ;66.1
        mov       WORD PTR [_WINSETUP$WINFO.0.1], si            ;66.1
        call      _SETWSIZEQQ                                   ;67.10
                                ; LOE esi
.B1.71:                         ; Preds .B1.12
        add       esp, 8                                        ;67.10
                                ; LOE esi
.B1.13:                         ; Preds .B1.71
        push      OFFSET FLAT: _WINSETUP$WINFO.0.1              ;74.10
        mov       eax, 4                                        ;69.1
        mov       edx, 300                                      ;70.1
        mov       WORD PTR [_WINSETUP$WINFO.0.1], ax            ;69.1
        mov       ecx, 100                                      ;71.1
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;74.10
        mov       edi, 500                                      ;72.1
        mov       eax, 460                                      ;73.1
        mov       WORD PTR [_WINSETUP$WINFO.0.1+2], dx          ;70.1
        mov       WORD PTR [_WINSETUP$WINFO.0.1+4], cx          ;71.1
        mov       WORD PTR [_WINSETUP$WINFO.0.1+6], di          ;72.1
        mov       WORD PTR [_WINSETUP$WINFO.0.1+8], ax          ;73.1
        call      _SETWSIZEQQ                                   ;74.10
                                ; LOE esi
.B1.72:                         ; Preds .B1.13
        add       esp, 8                                        ;74.10
                                ; LOE esi
.B1.14:                         ; Preds .B1.72
        push      10485760                                      ;75.10
        call      __setbkcolorrgb                               ;75.10
                                ; LOE esi
.B1.73:                         ; Preds .B1.14
        add       esp, 4                                        ;75.10
                                ; LOE esi
.B1.15:                         ; Preds .B1.73
        push      OFFSET FLAT: _WINSETUP$WINFO.0.1              ;78.10
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;78.10
        mov       WORD PTR [_WINSETUP$WINFO.0.1], si            ;77.1
        call      _SETWSIZEQQ                                   ;78.10
                                ; LOE
.B1.74:                         ; Preds .B1.15
        add       esp, 8                                        ;78.10
                                ; LOE
.B1.16:                         ; Preds .B1.74
        push      0                                             ;79.6
        call      __FQclearscreen                               ;79.6
                                ; LOE
.B1.75:                         ; Preds .B1.16
        add       esp, 4                                        ;79.6
                                ; LOE
.B1.17:                         ; Preds .B1.75
        push      32                                            ;82.10
        mov       DWORD PTR [-56+ebp], 0                        ;82.10
        lea       eax, DWORD PTR [-24+ebp]                      ;82.10
        push      eax                                           ;82.10
        push      OFFSET FLAT: __STRLITPACK_14.0.1              ;82.10
        push      -2088435968                                   ;82.10
        push      -1                                            ;82.10
        mov       DWORD PTR [-24+ebp], 14                       ;82.10
        lea       edi, DWORD PTR [-4+ebp]                       ;82.10
        mov       DWORD PTR [-16+ebp], edi                      ;82.10
        lea       edi, DWORD PTR [-56+ebp]                      ;82.10
        push      edi                                           ;82.10
        mov       DWORD PTR [-20+ebp], OFFSET FLAT: __STRLITPACK_12 ;82.10
        call      _for_inquire                                  ;82.10
                                ; LOE edi
.B1.76:                         ; Preds .B1.17
        add       esp, 24                                       ;82.10
                                ; LOE edi
.B1.18:                         ; Preds .B1.76
        test      BYTE PTR [-4+ebp], 1                          ;84.4
        je        .B1.44        ; Prob 30%                      ;84.4
                                ; LOE edi
.B1.19:                         ; Preds .B1.18
        push      32                                            ;85.1
        mov       DWORD PTR [-56+ebp], 0                        ;85.1
        lea       eax, DWORD PTR [-88+ebp]                      ;85.1
        push      eax                                           ;85.1
        push      OFFSET FLAT: __STRLITPACK_15.0.1              ;85.1
        push      -2088435968                                   ;85.1
        push      599                                           ;85.1
        push      edi                                           ;85.1
        mov       DWORD PTR [-88+ebp], 14                       ;85.1
        mov       DWORD PTR [-84+ebp], OFFSET FLAT: __STRLITPACK_11 ;85.1
        mov       DWORD PTR [-80+ebp], 10                       ;85.1
        mov       DWORD PTR [-76+ebp], OFFSET FLAT: __STRLITPACK_10 ;85.1
        mov       DWORD PTR [-72+ebp], 4                        ;85.1
        call      _for_open                                     ;85.1
                                ; LOE edi
.B1.77:                         ; Preds .B1.19
        add       esp, 24                                       ;85.1
                                ; LOE edi
.B1.20:                         ; Preds .B1.77
        push      32                                            ;87.1
        push      OFFSET FLAT: WINSETUP$format_pack.0.1         ;87.1
        mov       DWORD PTR [-56+ebp], 0                        ;87.1
        lea       eax, DWORD PTR [-272+ebp]                     ;87.1
        push      eax                                           ;87.1
        push      OFFSET FLAT: __STRLITPACK_16.0.1              ;87.1
        push      -2085814525                                   ;87.1
        push      599                                           ;87.1
        push      edi                                           ;87.1
        mov       DWORD PTR [-272+ebp], 2                       ;87.1
        mov       DWORD PTR [-268+ebp], OFFSET FLAT: __STRLITPACK_8 ;87.1
        call      _for_read_seq_fmt                             ;87.1
                                ; LOE eax edi
.B1.78:                         ; Preds .B1.20
        add       esp, 28                                       ;87.1
                                ; LOE eax edi
.B1.21:                         ; Preds .B1.78
        test      eax, eax                                      ;87.1
        jne       .B1.28        ; Prob 50%                      ;87.1
                                ; LOE eax edi
.B1.22:                         ; Preds .B1.21
        mov       eax, 1                                        ;87.1
        mov       esi, OFFSET FLAT: _WINSETUP$PATHTEST.0.1      ;87.59
        mov       edi, eax                                      ;87.59
                                ; LOE esi edi
.B1.23:                         ; Preds .B1.25 .B1.22
        mov       DWORD PTR [-64+ebp], 1                        ;87.1
        lea       edx, DWORD PTR [-64+ebp]                      ;87.1
        push      edx                                           ;87.1
        push      OFFSET FLAT: __STRLITPACK_17.0.1              ;87.1
        mov       DWORD PTR [-60+ebp], esi                      ;87.1
        lea       ecx, DWORD PTR [-56+ebp]                      ;87.1
        push      ecx                                           ;87.1
        call      _for_read_seq_fmt_xmit                        ;87.1
                                ; LOE eax esi edi
.B1.79:                         ; Preds .B1.23
        add       esp, 12                                       ;87.1
                                ; LOE eax esi edi
.B1.24:                         ; Preds .B1.79
        test      eax, eax                                      ;87.1
        jne       .B1.56        ; Prob 20%                      ;87.1
                                ; LOE eax esi edi
.B1.25:                         ; Preds .B1.24
        inc       edi                                           ;87.1
        inc       esi                                           ;87.1
        cmp       edi, 100                                      ;87.1
        jle       .B1.23        ; Prob 99%                      ;87.1
                                ; LOE esi edi
.B1.26:                         ; Preds .B1.25
        push      0                                             ;87.1
        push      OFFSET FLAT: __STRLITPACK_18.0.1              ;87.1
        lea       edi, DWORD PTR [-56+ebp]                      ;
        push      edi                                           ;87.1
        call      _for_read_seq_fmt_xmit                        ;87.1
                                ; LOE eax edi
.B1.80:                         ; Preds .B1.26
        add       esp, 12                                       ;87.1
                                ; LOE eax edi
.B1.28:                         ; Preds .B1.80 .B1.56 .B1.21
        cmp       eax, -2                                       ;87.1
        je        .B1.30        ; Prob 16%                      ;87.1
                                ; LOE eax edi
.B1.29:                         ; Preds .B1.28
        test      eax, eax                                      ;87.1
        jle       .B1.54        ; Prob 16%                      ;87.1
                                ; LOE eax edi
.B1.30:                         ; Preds .B1.89 .B1.28 .B1.29
        mov       eax, 100                                      ;91.16
        lea       esi, DWORD PTR [-264+ebp]                     ;91.16
        push      eax                                           ;91.16
        push      OFFSET FLAT: _WINSETUP$PATHTEST.0.1           ;91.16
        push      eax                                           ;91.16
        push      esi                                           ;91.16
        call      _for_trim                                     ;91.16
                                ; LOE eax esi
.B1.81:                         ; Preds .B1.30
        add       esp, 16                                       ;91.16
                                ; LOE eax esi
.B1.31:                         ; Preds .B1.81
        xor       ecx, ecx                                      ;91.5
        cdq                                                     ;91.5
        push      ecx                                           ;91.5
        push      edx                                           ;91.5
        push      eax                                           ;91.5
        push      esi                                           ;91.5
        push      ecx                                           ;91.5
        push      100                                           ;91.5
        push      OFFSET FLAT: _WINSETUP$PATHTEST.0.1           ;91.5
        call      _for_cpystr                                   ;91.5
                                ; LOE
.B1.82:                         ; Preds .B1.31
        add       esp, 28                                       ;91.5
                                ; LOE
.B1.32:                         ; Preds .B1.82
        mov       ecx, 100                                      ;92.1
                                ; LOE ecx
.B1.33:                         ; Preds .B1.34 .B1.32
        cmp       BYTE PTR [_WINSETUP$PATHTEST.0.1-1+ecx], 92   ;93.19
        je        .B1.49        ; Prob 20%                      ;93.19
                                ; LOE ecx
.B1.34:                         ; Preds .B1.33
        dec       ecx                                           ;97.1
        jg        .B1.33        ; Prob 99%                      ;97.1
                                ; LOE ecx
.B1.35:                         ; Preds .B1.34
        xor       ecx, ecx                                      ;
                                ; LOE ecx
.B1.36:                         ; Preds .B1.52 .B1.35 .B1.49
        mov       DWORD PTR [-92+ebp], esp                      ;103.6
        lea       eax, DWORD PTR [10+ecx]                       ;103.6
        mov       DWORD PTR [.T167_.0.2], ecx                   ;103.6
        call      __alloca_probe                                ;103.6
        and       esp, -16                                      ;103.6
        mov       eax, esp                                      ;103.6
                                ; LOE eax ecx
.B1.84:                         ; Preds .B1.36
        test      ecx, ecx                                      ;103.6
        mov       esi, eax                                      ;103.6
        mov       eax, 0                                        ;103.6
        cmovg     eax, ecx                                      ;103.6
        call      __alloca_probe                                ;103.6
        and       esp, -16                                      ;103.6
        mov       eax, esp                                      ;103.6
                                ; LOE eax ecx esi
.B1.83:                         ; Preds .B1.84
        test      ecx, ecx                                      ;103.6
        jle       .B1.41        ; Prob 2%                       ;103.6
                                ; LOE eax ecx esi
.B1.37:                         ; Preds .B1.83
        mov       DWORD PTR [-156+ebp], eax                     ;
        mov       edi, 1                                        ;
        mov       DWORD PTR [-160+ebp], esi                     ;
        mov       edx, OFFSET FLAT: _WINSETUP$PATHARRAY.0.1     ;103.6
        mov       DWORD PTR [-164+ebp], eax                     ;
        mov       esi, DWORD PTR [-156+ebp]                     ;
                                ; LOE edx ecx esi edi
.B1.38:                         ; Preds .B1.39 .B1.37
        mov       al, BYTE PTR [edx]                            ;103.6
        mov       BYTE PTR [esi], al                            ;103.6
                                ; LOE edx ecx esi edi
.B1.39:                         ; Preds .B1.38
        inc       edi                                           ;103.6
        inc       edx                                           ;103.6
        inc       esi                                           ;103.6
        cmp       edi, ecx                                      ;103.6
        jle       .B1.38        ; Prob 82%                      ;103.6
                                ; LOE edx ecx esi edi
.B1.40:                         ; Preds .B1.39
        mov       eax, DWORD PTR [-164+ebp]                     ;
        mov       esi, DWORD PTR [-160+ebp]                     ;
        mov       ecx, DWORD PTR [.T167_.0.2]                   ;103.6
                                ; LOE eax ecx esi
.B1.41:                         ; Preds .B1.40 .B1.83
        xor       edi, edi                                      ;103.6
        test      ecx, ecx                                      ;103.6
        mov       DWORD PTR [-160+ebp], eax                     ;103.6
        mov       eax, edi                                      ;103.6
        cmovg     eax, ecx                                      ;103.6
        add       ecx, 10                                       ;103.6
        cdq                                                     ;103.6
        cmovs     ecx, edi                                      ;103.6
        mov       DWORD PTR [-152+ebp], eax                     ;103.6
        mov       eax, ecx                                      ;103.6
        mov       DWORD PTR [-148+ebp], edx                     ;103.6
        cdq                                                     ;103.6
        push      edx                                           ;103.6
        push      ecx                                           ;103.6
        push      esi                                           ;103.6
        push      edi                                           ;103.6
        push      2                                             ;103.6
        mov       DWORD PTR [-144+ebp], OFFSET FLAT: __STRLITPACK_21 ;103.6
        lea       ecx, DWORD PTR [-160+ebp]                     ;103.6
        push      ecx                                           ;103.6
        mov       DWORD PTR [-136+ebp], 10                      ;103.6
        mov       DWORD PTR [-132+ebp], edi                     ;103.6
        call      _for_concat                                   ;103.6
                                ; LOE esi
.B1.85:                         ; Preds .B1.41
        add       esp, 24                                       ;103.6
                                ; LOE esi
.B1.42:                         ; Preds .B1.85
        mov       edx, DWORD PTR [.T167_.0.2]                   ;103.6
        xor       eax, eax                                      ;103.6
        add       edx, 10                                       ;103.6
        cmovns    eax, edx                                      ;103.6
        push      eax                                           ;103.6
        push      OFFSET FLAT: __NLITPACK_5.0.2                 ;103.6
        push      OFFSET FLAT: __NLITPACK_4.0.2                 ;103.6
        push      esi                                           ;103.6
        call      __f_loadimage                                 ;103.6
                                ; LOE
.B1.86:                         ; Preds .B1.42
        add       esp, 16                                       ;103.6
                                ; LOE
.B1.43:                         ; Preds .B1.86
        mov       eax, DWORD PTR [-92+ebp]                      ;103.6
        mov       esp, eax                                      ;103.6
        jmp       .B1.46        ; Prob 100%                     ;103.6
                                ; LOE
.B1.44:                         ; Preds .B1.18
        xor       eax, eax                                      ;107.1
        push      eax                                           ;107.1
        push      eax                                           ;107.1
        push      48                                            ;107.1
        push      OFFSET FLAT: __STRLITPACK_6                   ;107.1
        push      eax                                           ;107.1
        push      80                                            ;107.1
        push      OFFSET FLAT: _WINSETUP$PRINTSTR1.0.1          ;107.1
        call      _for_cpystr                                   ;107.1
                                ; LOE
.B1.87:                         ; Preds .B1.44
        add       esp, 28                                       ;107.1
                                ; LOE
.B1.45:                         ; Preds .B1.54 .B1.87
        push      80                                            ;108.11
        mov       eax, OFFSET FLAT: __NLITPACK_1.0.1            ;108.11
        push      eax                                           ;108.11
        push      eax                                           ;108.11
        push      eax                                           ;108.11
        push      eax                                           ;108.11
        push      eax                                           ;108.11
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;108.11
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;108.11
        push      OFFSET FLAT: _WINSETUP$PRINTSTR1.0.1          ;108.11
        call      _PRINTSCREEN                                  ;108.11
                                ; LOE
.B1.88:                         ; Preds .B1.45
        add       esp, 36                                       ;108.11
                                ; LOE
.B1.46:                         ; Preds .B1.43 .B1.88
        call      __initializefonts                             ;112.12
                                ; LOE
.B1.47:                         ; Preds .B1.46
        mov       esi, DWORD PTR [-116+ebp]                     ;114.1
        test      esi, 1                                        ;114.1
        jne       .B1.57        ; Prob 3%                       ;114.1
                                ; LOE esi
.B1.48:                         ; Preds .B1.47 .B1.58
        mov       esi, DWORD PTR [-12+ebp]                      ;114.1
        mov       edi, DWORD PTR [-8+ebp]                       ;114.1
        mov       esp, ebp                                      ;114.1
        pop       ebp                                           ;114.1
        mov       esp, ebx                                      ;114.1
        pop       ebx                                           ;114.1
        ret                                                     ;114.1
                                ; LOE
.B1.49:                         ; Preds .B1.33                  ; Infreq
        test      ecx, ecx                                      ;99.1
        jle       .B1.36        ; Prob 2%                       ;99.1
                                ; LOE ecx
.B1.50:                         ; Preds .B1.49                  ; Infreq
        mov       esi, 1                                        ;
        xor       edx, edx                                      ;
                                ; LOE edx ecx esi
.B1.51:                         ; Preds .B1.52 .B1.50           ; Infreq
        mov       al, BYTE PTR [_WINSETUP$PATHTEST.0.1+edx]     ;100.2
        mov       BYTE PTR [_WINSETUP$PATHARRAY.0.1+edx], al    ;100.2
                                ; LOE edx ecx esi
.B1.52:                         ; Preds .B1.51                  ; Infreq
        inc       esi                                           ;101.1
        inc       edx                                           ;101.1
        cmp       esi, ecx                                      ;101.1
        jle       .B1.51        ; Prob 82%                      ;101.1
        jmp       .B1.36        ; Prob 100%                     ;101.1
                                ; LOE edx ecx esi
.B1.54:                         ; Preds .B1.29                  ; Infreq
        jne       .B1.45        ; Prob 50%                      ;88.10
                                ; LOE edi
.B1.55:                         ; Preds .B1.54                  ; Infreq
        push      32                                            ;89.7
        xor       eax, eax                                      ;89.7
        push      eax                                           ;89.7
        push      OFFSET FLAT: __STRLITPACK_19.0.1              ;89.7
        push      -2088435968                                   ;89.7
        push      599                                           ;89.7
        push      edi                                           ;89.7
        mov       DWORD PTR [-56+ebp], eax                      ;89.7
        call      _for_close                                    ;89.7
                                ; LOE
.B1.89:                         ; Preds .B1.55                  ; Infreq
        add       esp, 24                                       ;89.7
        jmp       .B1.30        ; Prob 100%                     ;89.7
                                ; LOE
.B1.56:                         ; Preds .B1.24                  ; Infreq
        lea       edi, DWORD PTR [-56+ebp]                      ;
        jmp       .B1.28        ; Prob 100%                     ;
                                ; LOE eax edi
.B1.57:                         ; Preds .B1.47                  ; Infreq
        mov       edx, esi                                      ;114.1
        mov       eax, esi                                      ;114.1
        shr       edx, 1                                        ;114.1
        and       eax, 1                                        ;114.1
        and       edx, 1                                        ;114.1
        add       eax, eax                                      ;114.1
        shl       edx, 2                                        ;114.1
        or        edx, eax                                      ;114.1
        or        edx, 262144                                   ;114.1
        push      edx                                           ;114.1
        push      DWORD PTR [-128+ebp]                          ;114.1
        call      _for_dealloc_allocatable                      ;114.1
                                ; LOE esi
.B1.90:                         ; Preds .B1.57                  ; Infreq
        add       esp, 8                                        ;114.1
                                ; LOE esi
.B1.58:                         ; Preds .B1.90                  ; Infreq
        and       esi, -2                                       ;114.1
        mov       DWORD PTR [-128+ebp], 0                       ;114.1
        mov       DWORD PTR [-116+ebp], esi                     ;114.1
        jmp       .B1.48        ; Prob 100%                     ;114.1
        ALIGN     16
                                ; LOE
; mark_end;
_WINSETUP ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_WINSETUP$STRPATH.0.1	DB ?	; pad
	ORG $+198	; pad
	DB ?	; pad
	DD 6 DUP (0H)	; pad
_WINSETUP$WC.0.1	DB ?	; pad
	ORG $+146	; pad
	DB ?	; pad
	DD 3 DUP (0H)	; pad
_WINSETUP$PATHARRAY.0.1	DB ?	; pad
	ORG $+98	; pad
	DB ?	; pad
	DD 7 DUP (0H)	; pad
_WINSETUP$PRINTSTR1.0.1	DB ?	; pad
	ORG $+78	; pad
	DB ?	; pad
	DD 4 DUP (0H)	; pad
_WINSETUP$PATHTEST.0.1	DB ?	; pad
	ORG $+98	; pad
	DB ?	; pad
	DD 1 DUP (0H)	; pad
_WINSETUP$WINFO.0.1	DD 2 DUP (0H)	; pad
	DB 2 DUP ( 0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
WINSETUP$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	29
	DB	0
	DB	0
	DB	0
	DB	100
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	-2147483648
__NLITPACK_1.0.1	DD	0
__STRLITPACK_14.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	2
	DB	0
	DB	9
	DB	1
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_19.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_3.0.1	DD	200
__NLITPACK_2.0.1	DD	20
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WINSETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _BMPLOAD
; mark_begin;
       ALIGN     16
	PUBLIC _BMPLOAD
_BMPLOAD	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;118.12
        mov       ebp, esp                                      ;118.12
        sub       esp, 48                                       ;118.12
        mov       edx, DWORD PTR [8+ebp]                        ;118.12
        mov       DWORD PTR [-4+ebp], ebx                       ;118.12
        mov       DWORD PTR [-8+ebp], edi                       ;118.12
        mov       ebx, DWORD PTR [edx]                          ;118.12
        mov       eax, ebx                                      ;118.12
        xor       edx, edx                                      ;118.12
        add       eax, 10                                       ;118.12
        mov       DWORD PTR [-12+ebp], esi                      ;118.12
        cmovs     eax, edx                                      ;118.12
        mov       ecx, DWORD PTR [12+ebp]                       ;118.12
        mov       DWORD PTR [.T167_.0.2], ebx                   ;118.12
        mov       DWORD PTR [-16+ebp], esp                      ;118.12
        call      __alloca_probe                                ;118.12
        and       esp, -16                                      ;118.12
        mov       eax, esp                                      ;118.12
                                ; LOE eax ecx ebx
.B2.12:                         ; Preds .B2.1
        test      ebx, ebx                                      ;118.12
        mov       esi, eax                                      ;118.12
        mov       eax, 0                                        ;118.12
        cmovg     eax, ebx                                      ;118.12
        call      __alloca_probe                                ;118.12
        and       esp, -16                                      ;118.12
        mov       eax, esp                                      ;118.12
                                ; LOE eax ecx ebx esi
.B2.11:                         ; Preds .B2.12
        test      ebx, ebx                                      ;125.1
        jle       .B2.6         ; Prob 2%                       ;125.1
                                ; LOE eax ecx ebx esi
.B2.2:                          ; Preds .B2.11
        mov       edx, 1                                        ;
        mov       edi, eax                                      ;
        mov       DWORD PTR [-48+ebp], esi                      ;
        mov       esi, edx                                      ;
                                ; LOE eax ecx ebx esi edi
.B2.3:                          ; Preds .B2.4 .B2.2
        mov       dl, BYTE PTR [ecx]                            ;126.3
        mov       BYTE PTR [edi], dl                            ;126.3
                                ; LOE eax ecx ebx esi edi
.B2.4:                          ; Preds .B2.3
        inc       esi                                           ;127.1
        inc       ecx                                           ;127.1
        inc       edi                                           ;127.1
        cmp       esi, ebx                                      ;127.1
        jle       .B2.3         ; Prob 82%                      ;127.1
                                ; LOE eax ecx ebx esi edi
.B2.5:                          ; Preds .B2.4
        mov       esi, DWORD PTR [-48+ebp]                      ;
        mov       ebx, DWORD PTR [.T167_.0.2]                   ;129.1
                                ; LOE eax ebx esi
.B2.6:                          ; Preds .B2.5 .B2.11
        xor       edi, edi                                      ;129.12
        test      ebx, ebx                                      ;129.12
        mov       DWORD PTR [-48+ebp], eax                      ;129.12
        mov       eax, edi                                      ;129.12
        cmovg     eax, ebx                                      ;129.12
        add       ebx, 10                                       ;129.1
        cdq                                                     ;129.12
        cmovs     ebx, edi                                      ;129.1
        lea       ecx, DWORD PTR [-48+ebp]                      ;129.1
        mov       DWORD PTR [-40+ebp], eax                      ;129.12
        mov       eax, ebx                                      ;129.1
        mov       DWORD PTR [-36+ebp], edx                      ;129.12
        cdq                                                     ;129.1
        push      edx                                           ;129.1
        push      ebx                                           ;129.1
        push      esi                                           ;129.1
        push      edi                                           ;129.1
        push      2                                             ;129.1
        push      ecx                                           ;129.1
        mov       DWORD PTR [-32+ebp], OFFSET FLAT: __STRLITPACK_21 ;129.12
        mov       DWORD PTR [-24+ebp], 10                       ;129.12
        mov       DWORD PTR [-20+ebp], edi                      ;129.12
        call      _for_concat                                   ;129.1
                                ; LOE esi
.B2.13:                         ; Preds .B2.6
        add       esp, 24                                       ;129.1
                                ; LOE esi
.B2.7:                          ; Preds .B2.13
        mov       edx, DWORD PTR [.T167_.0.2]                   ;130.10
        xor       eax, eax                                      ;130.10
        add       edx, 10                                       ;130.10
        cmovns    eax, edx                                      ;130.10
        push      eax                                           ;130.10
        push      OFFSET FLAT: __NLITPACK_5.0.2                 ;130.10
        push      OFFSET FLAT: __NLITPACK_4.0.2                 ;130.10
        push      esi                                           ;130.10
        call      __f_loadimage                                 ;130.10
                                ; LOE
.B2.14:                         ; Preds .B2.7
        add       esp, 16                                       ;130.10
                                ; LOE
.B2.8:                          ; Preds .B2.14
        mov       eax, DWORD PTR [-16+ebp]                      ;131.1
        mov       esp, eax                                      ;131.1
                                ; LOE
.B2.15:                         ; Preds .B2.8
        mov       ebx, DWORD PTR [-4+ebp]                       ;131.1
        mov       esi, DWORD PTR [-12+ebp]                      ;131.1
        mov       edi, DWORD PTR [-8+ebp]                       ;131.1
        mov       esp, ebp                                      ;131.1
        pop       ebp                                           ;131.1
        ret                                                     ;131.1
        ALIGN     16
                                ; LOE
; mark_end;
_BMPLOAD ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _BMPLOAD
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _COPY_A2S
; mark_begin;
       ALIGN     16
	PUBLIC _COPY_A2S
_COPY_A2S	PROC NEAR 
; parameter 1: 16 + esp
; parameter 2: 20 + esp
; parameter 3: 24 + esp
.B3.1:                          ; Preds .B3.0
        push      esi                                           ;134.15
        push      edi                                           ;134.15
        push      esi                                           ;134.15
        xor       eax, eax                                      ;138.1
        mov       esi, DWORD PTR [24+esp]                       ;134.15
        mov       edx, DWORD PTR [24+esi]                       ;138.1
        test      edx, edx                                      ;138.1
        cmovge    eax, edx                                      ;138.1
        test      eax, eax                                      ;138.1
        jle       .B3.6         ; Prob 2%                       ;138.1
                                ; LOE eax ebx ebp esi
.B3.2:                          ; Preds .B3.1
        mov       ecx, DWORD PTR [esi]                          ;139.13
        mov       edx, 1                                        ;
        mov       esi, DWORD PTR [28+esi]                       ;139.13
        mov       edi, DWORD PTR [16+esp]                       ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, eax                                      ;
                                ; LOE edx ecx ebx ebp esi edi
.B3.3:                          ; Preds .B3.4 .B3.2
        mov       al, BYTE PTR [ecx]                            ;139.4
        mov       BYTE PTR [edi], al                            ;139.4
                                ; LOE edx ecx ebx ebp esi edi
.B3.4:                          ; Preds .B3.3
        inc       edx                                           ;140.1
        add       ecx, esi                                      ;140.1
        inc       edi                                           ;140.1
        cmp       edx, ebx                                      ;140.1
        jle       .B3.3         ; Prob 82%                      ;140.1
                                ; LOE edx ecx ebx ebp esi edi
.B3.5:                          ; Preds .B3.4
        mov       ebx, DWORD PTR [esp]                          ;
                                ; LOE ebx ebp
.B3.6:                          ; Preds .B3.5 .B3.1
        mov       eax, DWORD PTR [16+esp]                       ;141.1
        pop       ecx                                           ;141.1
        pop       edi                                           ;141.1
        pop       esi                                           ;141.1
        ret                                                     ;141.1
        ALIGN     16
                                ; LOE
; mark_end;
_COPY_A2S ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _COPY_A2S
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _COPY_S2A
; mark_begin;
       ALIGN     16
	PUBLIC _COPY_S2A
_COPY_S2A	PROC NEAR 
; parameter 1: 20 + esp
; parameter 2: 24 + esp
; parameter 3: 28 + esp
.B4.1:                          ; Preds .B4.0
        sub       esp, 16                                       ;144.15
        mov       edx, DWORD PTR [28+esp]                       ;144.15
        test      edx, edx                                      ;148.1
        mov       ecx, DWORD PTR [24+esp]                       ;144.15
        jle       .B4.6         ; Prob 2%                       ;148.1
                                ; LOE edx ecx ebx ebp esi edi
.B4.2:                          ; Preds .B4.1
        mov       DWORD PTR [8+esp], edi                        ;
        mov       eax, 1                                        ;
        mov       DWORD PTR [12+esp], esi                       ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [esp], ebp                          ;
        mov       edi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx edi
.B4.3:                          ; Preds .B4.4 .B4.2
        mov       ebp, DWORD PTR [28+edi]                       ;149.4
        mov       esi, DWORD PTR [edi]                          ;149.4
        sub       esi, ebp                                      ;149.4
        imul      ebp, eax                                      ;149.4
        mov       bl, BYTE PTR [ecx]                            ;149.4
        mov       BYTE PTR [ebp+esi], bl                        ;149.4
                                ; LOE eax edx ecx edi
.B4.4:                          ; Preds .B4.3
        inc       eax                                           ;150.1
        inc       ecx                                           ;150.1
        cmp       eax, edx                                      ;150.1
        jle       .B4.3         ; Prob 82%                      ;150.1
                                ; LOE eax edx ecx edi
.B4.5:                          ; Preds .B4.4
        mov       esi, DWORD PTR [12+esp]                       ;
        mov       edi, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       ebp, DWORD PTR [esp]                          ;
                                ; LOE ebx ebp esi edi
.B4.6:                          ; Preds .B4.5 .B4.1
        mov       eax, DWORD PTR [20+esp]                       ;151.1
        add       esp, 16                                       ;151.1
        ret                                                     ;151.1
        ALIGN     16
                                ; LOE
; mark_end;
_COPY_S2A ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _COPY_S2A
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	DB 2 DUP ( 0H)	; pad
.T167_.0.2	DD 1 DUP (0H)	; pad
_BSS	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
_2il0floatpacket.9	DD	020202020H,020202020H,020202020H,020202020H
__STRLITPACK_13	DB	84
	DB	105
	DB	109
	DB	101
	DB	115
	DB	32
	DB	78
	DB	101
	DB	119
	DB	32
	DB	82
	DB	111
	DB	109
	DB	97
	DB	110
	DB	0
__STRLITPACK_12	DB	119
	DB	111
	DB	114
	DB	107
	DB	105
	DB	110
	DB	103
	DB	100
	DB	105
	DB	114
	DB	46
	DB	105
	DB	110
	DB	105
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_6	DB	78
	DB	111
	DB	32
	DB	73
	DB	109
	DB	97
	DB	103
	DB	101
	DB	32
	DB	65
	DB	118
	DB	97
	DB	105
	DB	108
	DB	97
	DB	98
	DB	108
	DB	101
	DB	46
	DB	32
	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	114
	DB	117
	DB	110
	DB	32
	DB	68
	DB	121
	DB	110
	DB	117
	DB	115
	DB	84
	DB	32
	DB	102
	DB	114
	DB	111
	DB	109
	DB	32
	DB	78
	DB	69
	DB	88
	DB	84
	DB	65
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_11	DB	119
	DB	111
	DB	114
	DB	107
	DB	105
	DB	110
	DB	103
	DB	100
	DB	105
	DB	114
	DB	46
	DB	105
	DB	110
	DB	105
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_10	DB	115
	DB	101
	DB	113
	DB	117
	DB	101
	DB	110
	DB	116
	DB	105
	DB	97
	DB	108
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_8	DB	78
	DB	79
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_21	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	46
	DB	98
	DB	109
	DB	112
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_5.0.2	DD	30
__NLITPACK_4.0.2	DD	20
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
EXTRN	__f_loadimage:PROC
EXTRN	_for_concat:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_close:PROC
EXTRN	_for_read_seq_fmt_xmit:PROC
EXTRN	_for_read_seq_fmt:PROC
EXTRN	_for_open:PROC
EXTRN	_for_inquire:PROC
EXTRN	__FQclearscreen:PROC
EXTRN	__setbkcolorrgb:PROC
EXTRN	_SETWSIZEQQ:PROC
EXTRN	__f_setwindowconfig:PROC
EXTRN	__f_getwindowconfig:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_for_cpystr:PROC
EXTRN	_for_trim:PROC
EXTRN	_for_len_trim:PROC
EXTRN	__initializefonts:PROC
EXTRN	_PRINTSCREEN:PROC
EXTRN	_INITIALSETTINGS:PROC
EXTRN	_GETCWD:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
