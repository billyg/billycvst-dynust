     SUBROUTINE READ_INTERSECTION_CONTROLS()  
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

     USE DYNUST_MAIN_MODULE
     USE DYNUST_NETWORK_MODULE
	 USE DYNUST_SIGNAL_MODULE

     INTEGER kgpointmp(noofarcs+1,2)
     INTEGER CheckSignal(noofnodes,2)
     INTEGER ntmp,TmpSigData(10)
     INTEGER j
     INTEGER TmpMnAph(100), TmpMJAph(100)
     LOGICAL eofflag,eofflag2,eofflag3,nmvmflag
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

     nmvmflag = .false.
     CheckSignal(:,:) = 0
     kgpoint(:)=0
	 kgpointmp(:,:)=0
     m_dynust_network_arc_de(:)%gcratio=0
     m_dynust_network_arcmove_de(:,:)%SignalPreventFor=1 ! default all prevented
     m_dynust_network_arcmove_de(:,:)%SignalPreventBack=1 ! default all prevented

     kg=1

	 
	 iflag = 0
	 DO i = 1,noofnodes-noof_master_destinations
	 	   READ(44,*,iostat=error) (m_dynust_network_node(i)%node(j),j=1,4)
		   IF(m_dynust_network_node(i)%node(2) > 1) THEN
		     iflag=1
           ENDIF
	 ENDDO 
	 IF(iflag == 0) THEN ! all types are found to be no control
	    DO is = 1, noofarcs
           IF(m_dynust_network_arc_nde(is)%link_iden == 5) THEN
             DO it = 1, noofnodes-noof_master_destinations
                IF(m_dynust_network_arc_nde(is)%idnod == OutToInNodeNum(m_dynust_network_node(it)%node(1))) THEN
				   m_dynust_network_node(it)%node(2) = 5
				ENDIF
			 ENDDO
		   ENDIF
	    ENDDO
	 
	 ENDIF
     close(678)
	 rewind(44)
	 READ(44,*)
	 READ(44,*)


     CALL DeclareSignal(noofnodes-noof_master_destinations)

	 icount = 0
     DO 10 i=1,noofnodes-noof_master_destinations
         kgpoint(i)=kg

	     READ(44,*,iostat=error) (m_dynust_network_node(i)%node(j),j=1,4)
	     IF((m_dynust_network_node(i)%node(2) == 4.or.m_dynust_network_node(i)%node(2) == 5).and.m_dynust_network_node(i)%node(3) < 1) THEN
	        WRITE(911,*) "Error in control.dat"
	        WRITE(911,*) "Please check node", m_dynust_network_node(i)%node(1)
	        STOP
	     ENDIF
		 TmpSigData(1:4) = m_dynust_network_node(i)%node(1:4)

		 CALL insertSignalPar(TmpSigData,10)
		 IF(TmpSigData(2) == 5.or.TmpSigData(2) == 4) THEN
		    CALL DeclarePhase(OutToInNodeNum(TmpSigData(1)),TmpSigData(3))
         ENDIF

! check input errors	
	     IF(error /= 0) THEN
           WRITE(911,*) 'Error when reading control.dat'
	       STOP
	     ENDIF
         IF(m_dynust_network_node(i)%node(2) < 1) THEN ! check signal type input error
           WRITE(911,*) "Error in control.dat"
           WRITE(911,*) "node", m_dynust_network_node(i)%node(1),"signal type invalid"
		   STOP
		 ELSEIF (m_dynust_network_node(i)%node(2) == 4.or.m_dynust_network_node(i)%node(2) == 5) THEN
		   IF(m_dynust_network_node(i)%node(3) < 1.or.m_dynust_network_node(i)%node(4) < 1) THEN
           WRITE(911,*) "Error in phasing and cycle setting"
		   WRITE(911,*) "Please check node ",m_dynust_network_node(i)%node(1)
		   STOP
           ENDIF
		 ENDIF


	     m_dynust_network_node(i)%node(1) = OutToInNodeNum(m_dynust_network_node(i)%node(1)) ! change to internal node number
         IF(m_dynust_network_node(i)%node(2) == 4.or.m_dynust_network_node(i)%node(2) == 5) THEN 
	       icount=icount+1
	       CheckSignal(icount,1) = m_dynust_network_node(i)%node(1)
	       CheckSignal(icount,2) = m_dynust_network_node(i)%node(3)
	       kgpointmp(icount,1)= i
	       kgpointmp(icount,2)= kg
           kg=kg+m_dynust_network_node(i)%node(3)
	     ELSE !STOP, no control, yield, relax the SignalPreventFor to be 0
           DO KM = backpointr(m_dynust_network_node(i)%node(1)),backpointr(m_dynust_network_node(i)%node(1)+1)-1 ! for all inbound links (back)
             DO JM = 1, m_dynust_network_arc_nde(m_dynust_network_arc_nde(KM)%BackToForLink)%llink%no ! for all outbound links of the inbound link
                m_dynust_network_arcmove_de(m_dynust_network_arc_nde(m_dynust_network_arc_nde(KM)%BackToForLink)%llink%p(JM),:)%SignalPreventFor = 0
!               SignalPreventBack(ForToBackLink(llink(BackToForLink(KM),JM)),:)=0
                m_dynust_network_arcmove_de(m_dynust_network_arc_nde(m_dynust_network_arc_nde(m_dynust_network_arc_nde(KM)%BackToForLink)%llink%p(JM))%ForToBackLink,:)%SignalPreventBack=0
	         ENDDO
	       ENDDO
           IF(m_dynust_network_node(i)%node(2) == 2.or.m_dynust_network_node(i)%node(2) == 6) THEN 
		        SignCount=SignCount+1 !yield or two-way STOP
           ENDIF
         ENDIF
10 CONTINUE
     kgpoint(noofnodes+1)=kg
     icountfinal=icount
     kg=kg-1
! --
! -- Read the signal phasing information for each phase.
! -- 
     IF(kg > 0) THEN  !FBA Block
	 icount = 1
	 icount2 = 1
	 icount3 = 0
	  
	  
	  DO 192 i=1,kg
        READ(44,*,iostat=error) itmp,(nsign(i,j),j=1,9)

        TmpSigData(:) = 0 
		TmpSigData(1) = itmp
		TmpSigData(2:10) = nsign(i,1:9)
		CALL InsertSignalPhase(TmpSigData,10)
		CALL DeclarePhaseMove(TmpSigData,10)

		IF(error /= 0) THEN
          WRITE(911,*) 'Error when reading signal.dat'
	      STOP
	    ENDIF

	    IF(OutToInNodeNum(itmp) == CheckSignal(icount2,1)) THEN
	      icount3 = icount3 + 1
	    ELSE
	      IF(icount3 /= CheckSignal(icount2,2)) THEN
			WRITE(911,*) 'error in matching signals'
            WRITE(911,*) 'node', itmp, 'phase',nsign(i,1)
	        WRITE(911,*) "icount3, CheckSignal(icount2,2)",icount3,CheckSignal(icount2,2)
			WRITE(911,*) "possible reason: missing phasing for previous signalized node"
			WRITE(911,*) "check signal types in the first block"
			STOP
	      ENDIF
	      icount3 = 1
	      icount2 = icount2 + 1
	    ENDIF

	    icount=0
		DO ii = 6, 9						!
	      IF (nsign(i,ii) /= 0) THEN		!
	        icount=icount+1					!
	      ENDIF								!
	    ENDDO								!
	    IF(icount /= nsign(i,5)) THEN		!
		  WRITE(911,*) "Number of inbound links for node", itmp
          WRITE(911,*) "is not specified correctly"
	      WRITE(911,*) "Phase", nsign(i,1)
		  WRITE(911,*) "Read as ", nsign(i,5), "counted as", icount
	     STOP
	    ENDIF

! --  convert to internal numbering system	    
	    IF(nsign(i,6) > 0) THEN
		  nsign(i,6) = OutToInNodeNum(nsign(i,6)) !G
          IF(nsign(i,6) < 1) THEN
		   WRITE(911,*) 'error in node number for phase #6', nsign(i,1), 'for node ', itmp
		   STOP
          ENDIF
        ENDIF  
	    IF(nsign(i,7) > 0) THEN
		  nsign(i,7) = OutToInNodeNum(nsign(i,7)) !G
          IF(nsign(i,7) < 1) THEN
		     WRITE(911,*) 'error in node number for phase #7', nsign(i,1), 'for node ', itmp
		     STOP
          ENDIF
        ENDIF
 	    IF(nsign(i,8) > 0) THEN
		  nsign(i,8) = OutToInNodeNum(nsign(i,8)) !G
          IF(nsign(i,8) < 1) THEN
	        WRITE(911,*) 'error in node number for phase #8', nsign(i,1), 'for node ', itmp
		    STOP
          ENDIF
        ENDIF

	    IF(nsign(i,9) > 0) THEN
		  nsign(i,9) = OutToInNodeNum(nsign(i,9)) !G
          IF(nsign(i,9) < 1) THEN 
		   WRITE(911,*) 'error in node number for phase #9', nsign(i,1), 'for node ', itmp
		   STOP
          ENDIF
        ENDIF

          IF(isigcount == 1) THEN
            DO j=12,14
              nsign(i,j)=strtsig(isigcount)*60
            end do
          ENDIF

       DO ii=6,nsign(i,5)+5
	     ntmp = nsign(i,ii)
	     IF(GetFLinkFromNode(ntmp,OutToInNodeNum(itmp),13) == 0) THEN
	       WRITE(911,*) 'error in signal file, link doesnt exist'
		   WRITE(911,*) 'inbound',m_dynust_network_node_nde(ntmp)%IntoOutNodeNum,'signal node',itmp
	       WRITE(911,'("signal node",i7," Phase",i3)')itmp,nsign(i,1)
	       IF(nsign(i,6) > 0) WRITE(911,'(" inbound nodes",i10)') m_dynust_network_node_nde(nsign(i,6))%IntoOutNodeNum
             IF(nsign(i,7) > 0) WRITE(911,'(" inbound nodes",i10)') m_dynust_network_node_nde(nsign(i,7))%IntoOutNodeNum
             IF(nsign(i,8) > 0) WRITE(911,'(" inbound nodes",i10)') m_dynust_network_node_nde(nsign(i,8))%IntoOutNodeNum
	         STOP
           ELSE
             nsign(i,ii) = GetFLinkFromNode(ntmp,OutToInNodeNum(itmp),13)
	     ENDIF 
       end do

      DO i1=1,nsign(i,5) !# of inbound links
       READ(44,66) ifrom,ito,iphasek,nmvm,(almov(i1,iin),iin=1,nmvm)
		IF(nmvm < 1) THEN
         WRITE(911,*) "error in phase movement for node",ito, "phase ", iphasek
		 nmvmflag=.true.
		ENDIF

		TmpSigData(1) = ifrom 
		TmpSigData(2) = ito
		TmpSigData(3) = iphasek
		TmpSigData(4) = nmvm
		TmpSigData(5:5+nmvm) = almov(i1,1:nmvm)
		CALL DeclareOBLink(TmpSigData,10,i1)
        CALL InsertPhaseMove(TmpSigData,10,i1) 

	    IF(error /= 0) THEN
         WRITE(911,*) 'Error when reading phasing in control.dat'
         WRITE(911,*)'from,to,phasek number,# of outbound links, outbound node number...' 
		 WRITE(911,*) ifrom,ito,iphasek,nmvm,(almov(i1,iin),iin=1,nmvm)
	     STOP
	    ENDIF

        iflag=0
	    DO jp = 1, nmvm
		  IF(OutToInNodeNum(almov(i1,jp)) < 1) THEN
            WRITE(911,*) 'node does not exist, from ', ifrom, 'to ', ito, 'phase',iphasek,'outbound node', almov(i1,jp)
            STOP
		  ENDIF
          IF(GetFLinkFromNode(OutToInNodeNum(ito),OutToInNodeNum(almov(i1,jp)),13) > 0) THEN
            iflag = 1
			exit
		  ELSE
            WRITE(911,*) 'from ', ifrom, 'to ', ito, 'phase', iphasek
			WRITE(911,*) 'link UpNode -> DNode does not exit', ito,almov(i1,jp)
			STOP
		  ENDIF
        ENDDO
     

	  IF(OutToInNodeNum(ifrom) /= m_dynust_network_arc_nde(nsign(i,5+i1))%iunod) THEN
		 WRITE(911,'("inbound node",i5," for node",i5," at phase",i5," is not correct")')ifrom,ito,iphasek
	     STOP
	  ENDIF
	  DO mm = 1, nmvm						                   !G
	    IF(almov(i1,mm) > 0) almov(i1,mm)=OutToInNodeNum(almov(i1,mm)) !G
	  ENDDO								                       !G

        IF(m_dynust_network_node(OutToInNodeNum(ito))%iConZone(1) > 0) THEN
	    DO mpp= 1, m_dynust_network_node(OutToInNodeNum(ito))%iConZone(1)
           nmvm = nmvm + 1
           almov(i1,nmvm) = destination(MasterDest(m_dynust_network_node(OutToInNodeNum(ito))%iConZone(mpp+1)))
	    ENDDO
        ENDIF


        
66      FORMAT(15i7)
         j=nsign(i,i1+5)
           DO ka=1,m_dynust_network_arc_nde(j)%llink%no
            DO ia=1,nmvm
              IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(ka))%idnod == almov(i1,ia)) THEN
!			   movement(j,iphasek,ka)=.true.
	           MVPF = MoveNoForLink(j,m_dynust_network_arc_nde(j)%llink%p(ka))
	           m_dynust_network_arcmove_de(m_dynust_network_arc_nde(j)%llink%p(ka),MVPF)%SignalPreventFor = 0  ! set the value to 1 is this is a permissive movement
!               MVPB = MoveNoBackLink(j,llink(j)%p(ka))
               MVPB = getBstMove(j,m_dynust_network_arc_nde(j)%llink%p(ka))
               m_dynust_network_arcmove_de(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(ka))%ForToBackLink,MVPB)%SignalPreventBack=0
	        ENDIF
            ENDDO 
	   
! -- make uturn movements allowed IF it is specified in the signal 
! --
!              IF(iunod(llink(j)%p(ka)) == idnod(j).and.idnod(llink(j)%p(ka)) == iunod(j)) THEN
!                 movement(j,iphasek,ka)=1
!	             MVPF = MoveNoForLink(j,llink(j)%p(ka))
!	             SignalPreventFor(llink(j)%p(ka),MVPF) = 0
!                 MVPB = getBstMove(j,llink(j)%p(ka))
!                 SignalPreventBack(ForToBackLink(llink(j)%p(ka)),MVPB)=0
!              ENDIF

        ENDDO
      ENDDO
      

! --
      
192   CONTINUE
     ENDIF !FBA Block

      IF(nmvmflag) STOP
     IF(SignCount > 0) THEN
     READ(44,*,iostat=error)
     IF(error /= 0) THEN !error = 24 is eof
       WRITE(911,*) 'Error when reading 2-way/yield in control.dat'
	   STOP
	 ENDIF

      ALLOCATE(SignData(SignCount), stat=error)
	  SignData(:)%node = 0
	  SignData(:)%NofMajor = 0
	  SignData(:)%NofMinor = 0
      

	  ALLOCATE(SignApprh(SignCount), stat=error) 
      SignApprh(:)%major(1) = 0 ! max 4 major 4 minor approaches
      SignApprh(:)%major(2) = 0
	  SignApprh(:)%major(3) = 0
      SignApprh(:)%major(4) = 0
      SignApprh(:)%minor(1) = 0
      SignApprh(:)%minor(2) = 0
	  SignApprh(:)%minor(3) = 0
      SignApprh(:)%minor(4) = 0
 
      DO i = 1, SignCount
        READ(44,*,iostat=error) SignData(i)%node, SignData(i)%NofMajor, SignData(i)%NofMinor
        SignData(i)%node = OutToInNodeNum(SignData(i)%node)
		IF(error /= 0) THEN
          WRITE(911,*) 'Error when reading 2-way/yield in control.dat'
	      STOP
	    ENDIF

        READ(44,*,iostat=error) (TmpMJAph(j),j=1,2*SignData(i)%NofMajor) 
        IF(error /= 0) THEN
          WRITE(911,*) 'Error when reading 2-way/yield in control.dat'
	      STOP
	    ENDIF
		DO IMp= 1, 2*SignData(i)%NofMajor
         IF(mod(IMP,2) == 0) THEN
           IF(OutToInNodeNum(TmpMJAph(IMP)) /= SignData(i)%node) THEN
		    WRITE(911,*) "Error in two-way signal major st. spec."
			WRITE(911,*) " check node ", m_dynust_network_node_nde(SignData(i)%node)%IntoOutNodeNum
			STOP
		   ENDIF
		 ENDIF
		ENDDO
	    
		DO k = 1, SignData(i)%NofMajor !get link number for major approach
        ienter1 = OutToInNodeNum(TmpMJAph(k*2-1))
		ienter2 = OutToInNodeNum(TmpMJAph(k*2))
		IF(ienter1 < 1.or.ienter2 < 1) THEN
           WRITE(911,*) "Error in two-way STOP"
		   WRITE(911,*) "check major st spec for node", m_dynust_network_node_nde(SignData(i)%node)%IntoOutNodeNum
		   STOP
		ENDIF
         ienter3 = GetFLinkFromNode(ienter1,ienter2,13)
		 IF(ienter3 < 1) THEN
		    WRITE(911,*) "error in two-twy STOP"
			WRITE(911,*) "check major spec for node ", m_dynust_network_node_nde(SignData(i)%node)%IntoOutNodeNum
			STOP
		 ELSE
           SignApprh(i)%major(k) = ienter3
		 ENDIF
	    ENDDO


	    READ(44,*,iostat=error) (TmpMnAph(j),j=1,2*SignData(i)%NofMinor)
        IF(error /= 0) THEN
          WRITE(911,*) 'Error when reading 2-way/yield in control.dat'
	      STOP
	    ENDIF

		DO IMp= 1, 2*SignData(i)%NofMinor
         IF(mod(IMP,2) == 0) THEN
           IF(OutToInNodeNum(TmpMnAph(IMP)) /= SignData(i)%node) THEN
		    WRITE(911,*) "Error in two-way signal minor st. spec"
			WRITE(911,*) " check node ", m_dynust_network_node_nde(SignData(i)%node)%IntoOutNodeNum
			STOP
		   ENDIF
		 ENDIF
		ENDDO

	    DO k = 1, SignData(i)%NofMinor ! get link number for minor approach

        ienter1 = OutToInNodeNum(TmpMnAph(k*2-1))
		ienter2 = OutToInNodeNum(TmpMnAph(k*2))
		IF(ienter1 < 1.or.ienter2 < 1) THEN
           WRITE(911,*) "Error in two-way STOP"
		   WRITE(911,*) "check minor st spec for node", m_dynust_network_node_nde(SignData(i)%node)%IntoOutNodeNum
		   STOP
		ENDIF
          ienter3 = GetFLinkFromNode(ienter1, ienter2,13)
		  IF(ienter3 < 1) THEN
		    WRITE(911,*) "error in two-twy STOP"
			WRITE(911,*) "check minor spec for node ", m_dynust_network_node_nde(SignData(i)%node)%IntoOutNodeNum
			STOP
		  ELSE
	        SignApprh(i)%minor(k) = ienter3
		  ENDIF
	    ENDDO
      ENDDO
 
 	 ENDIF !SignCount > 0


         DO i=1,noofarcs
           DO kk=1,maxmove
             IF(m_dynust_network_arcmove_de(i,kk)%SignalPreventFor == 1) THEN
	         m_dynust_network_arcmove_de(i,kk)%openalty=PenForPreventMove
	       ENDIF

             IF(m_dynust_network_arcmove_de(m_dynust_network_arc_nde(i)%ForToBackLink,kk)%SignalPreventBack == 1) THEN
                m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(i)%ForToBackLink,kk)%penalty=PenForPreventMove  ! defined in DYNUST_MAIN_MODULE
             ENDIF

           ENDDO
         ENDDO

        DO i=1,noofarcs
          IF(m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2) == 4) THEN
            DO k=kgpoint(m_dynust_network_arc_nde(i)%idnod),kgpoint(m_dynust_network_arc_nde(i)%idnod+1)-1
               DO m=6,nsign(k,5)+5
                  IF(i == nsign(k,m)) THEN
                     m_dynust_network_arc_de(i)%gcratio=m_dynust_network_arc_de(i)%gcratio + (float(nsign(k,3))/m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(4))
                   ENDIF
                end do
            end do
          ENDIF
! --
! --
          IF(m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2) == 5) THEN
             DO k=kgpoint(m_dynust_network_arc_nde(i)%idnod),kgpoint(m_dynust_network_arc_nde(i)%idnod+1)-1
                DO m=6,nsign(k,5)+5
                   IF(i == nsign(k,m)) THEN
                     m_dynust_network_arc_de(i)%gcratio=m_dynust_network_arc_de(i)%gcratio + (float(nsign(k,2))/m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(4))
                   ENDIF
                end do
             end do
          ENDIF
        ENDDO 


      DO i = 1, noofarcs
        eofflag2=.false.
        eofflag3=.false.

        IF(m_dynust_network_arc_nde(i)%link_iden == 1.or.m_dynust_network_arc_nde(i)%link_iden == 2.or.m_dynust_network_arc_nde(i)%link_iden == 7.or.m_dynust_network_arc_nde(i)%link_iden == 9.or.m_dynust_network_arc_nde(i)%link_iden == 10) THEN
		  IF(m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2) /= 2.and.m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2) /= 1) THEN
		    DO ks = 1, m_dynust_network_arc_nde(i)%llink%no
              IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden == 1.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden == 2.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden == 3.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden == 4.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden == 9.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden == 10) eofflag2=.true.
			ENDDO
		    IF(eofflag2) WRITE(511,'("Signs/Signals found on freeway section from",i7,"  to",i7)') m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%idnod)%IntoOutNodeNum
		  ENDIF
		  IF(m_dynust_network_node(m_dynust_network_arc_nde(i)%iunod)%node(2) /= 2.and.m_dynust_network_node(m_dynust_network_arc_nde(i)%iunod)%node(2) /= 1) THEN
		    DO ks = 1, m_dynust_network_arc_nde(i)%inlink%no
              IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden == 1.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden == 2.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden == 3.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden == 4.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden == 9.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden == 10) eofflag3=.true.
			ENDDO
            IF(eofflag3) WRITE(511,'("Signs/Signals found on freeway section from",i7,"  to",i7)') m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%idnod)%IntoOutNodeNum
          ENDIF
		ENDIF
	  ENDDO

193 FORMAT(11i4)
 19 FORMAT(i5,i2,i2,i4)

	END SUBROUTINE
