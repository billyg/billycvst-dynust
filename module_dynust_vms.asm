; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _VARIABLE_MESSAGE_SIGN_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _VARIABLE_MESSAGE_SIGN_MODULE$
_VARIABLE_MESSAGE_SIGN_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_VARIABLE_MESSAGE_SIGN_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _VARIABLE_MESSAGE_SIGN_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_READINPUT
; mark_begin;
       ALIGN     16
	PUBLIC _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_READINPUT
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_READINPUT	PROC NEAR 
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;29.12
        mov       ebp, esp                                      ;29.12
        and       esp, -16                                      ;29.12
        sub       esp, 528                                      ;29.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_NUM] ;31.2
        test      ecx, ecx                                      ;31.2
        jle       .B2.31        ; Prob 2%                       ;31.2
                                ; LOE ecx ebx esi edi
.B2.2:                          ; Preds .B2.1
        mov       edx, 1                                        ;
        mov       eax, 4                                        ;
        mov       DWORD PTR [500+esp], edx                      ;
        mov       DWORD PTR [448+esp], ecx                      ;
        mov       DWORD PTR [216+esp], esi                      ;
        mov       DWORD PTR [220+esp], edi                      ;
        mov       DWORD PTR [224+esp], ebx                      ;
        mov       ebx, eax                                      ;
                                ; LOE ebx
.B2.3:                          ; Preds .B2.29 .B2.2
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE+32] ;32.7
        shl       edx, 2                                        ;32.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE] ;32.7
        sub       ecx, edx                                      ;32.7
        add       ecx, ebx                                      ;32.7
        mov       DWORD PTR [64+esp], 0                         ;32.7
        mov       DWORD PTR [328+esp], 4                        ;32.7
        mov       DWORD PTR [332+esp], ecx                      ;32.7
        push      32                                            ;32.7
        lea       esi, DWORD PTR [332+esp]                      ;32.7
        push      esi                                           ;32.7
        push      OFFSET FLAT: __STRLITPACK_57.0.2              ;32.7
        push      -2088435968                                   ;32.7
        push      49                                            ;32.7
        lea       edi, DWORD PTR [84+esp]                       ;32.7
        push      edi                                           ;32.7
        call      _for_read_seq_lis                             ;32.7
                                ; LOE ebx
.B2.4:                          ; Preds .B2.3
        lea       ecx, DWORD PTR [504+esp]                      ;32.7
        lea       edx, DWORD PTR [464+esp]                      ;32.7
        mov       DWORD PTR [504+esp], edx                      ;32.7
        push      ecx                                           ;32.7
        push      OFFSET FLAT: __STRLITPACK_58.0.2              ;32.7
        lea       esi, DWORD PTR [96+esp]                       ;32.7
        push      esi                                           ;32.7
        call      _for_read_seq_lis_xmit                        ;32.7
                                ; LOE ebx
.B2.5:                          ; Preds .B2.4
        lea       ecx, DWORD PTR [524+esp]                      ;32.7
        lea       edx, DWORD PTR [480+esp]                      ;32.7
        mov       DWORD PTR [524+esp], edx                      ;32.7
        push      ecx                                           ;32.7
        push      OFFSET FLAT: __STRLITPACK_59.0.2              ;32.7
        lea       esi, DWORD PTR [108+esp]                      ;32.7
        push      esi                                           ;32.7
        call      _for_read_seq_lis_xmit                        ;32.7
                                ; LOE ebx
.B2.6:                          ; Preds .B2.5
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;32.35
        lea       edi, DWORD PTR [384+esp]                      ;32.7
        neg       esi                                           ;32.7
        add       esi, 2                                        ;32.7
        imul      esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;32.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;32.7
        shl       edx, 2                                        ;32.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;32.7
        sub       ecx, edx                                      ;32.7
        add       ecx, ebx                                      ;32.7
        add       esi, ecx                                      ;32.7
        mov       DWORD PTR [384+esp], 4                        ;32.7
        mov       DWORD PTR [388+esp], esi                      ;32.7
        push      edi                                           ;32.7
        push      OFFSET FLAT: __STRLITPACK_60.0.2              ;32.7
        lea       edx, DWORD PTR [120+esp]                      ;32.7
        push      edx                                           ;32.7
        call      _for_read_seq_lis_xmit                        ;32.7
                                ; LOE ebx
.B2.7:                          ; Preds .B2.6
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;32.44
        lea       edi, DWORD PTR [404+esp]                      ;32.7
        neg       esi                                           ;32.7
        add       esi, 3                                        ;32.7
        imul      esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;32.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;32.7
        shl       edx, 2                                        ;32.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;32.7
        sub       ecx, edx                                      ;32.7
        add       ecx, ebx                                      ;32.7
        add       esi, ecx                                      ;32.7
        mov       DWORD PTR [404+esp], 4                        ;32.7
        mov       DWORD PTR [408+esp], esi                      ;32.7
        push      edi                                           ;32.7
        push      OFFSET FLAT: __STRLITPACK_61.0.2              ;32.7
        lea       edx, DWORD PTR [132+esp]                      ;32.7
        push      edx                                           ;32.7
        call      _for_read_seq_lis_xmit                        ;32.7
                                ; LOE ebx
.B2.8:                          ; Preds .B2.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_START+32] ;32.7
        lea       esi, DWORD PTR [424+esp]                      ;32.7
        shl       edx, 2                                        ;32.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_START] ;32.7
        sub       ecx, edx                                      ;32.7
        add       ecx, ebx                                      ;32.7
        mov       DWORD PTR [424+esp], 4                        ;32.7
        mov       DWORD PTR [428+esp], ecx                      ;32.7
        push      esi                                           ;32.7
        push      OFFSET FLAT: __STRLITPACK_62.0.2              ;32.7
        lea       edi, DWORD PTR [144+esp]                      ;32.7
        push      edi                                           ;32.7
        call      _for_read_seq_lis_xmit                        ;32.7
                                ; LOE ebx
.B2.9:                          ; Preds .B2.8
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_END+32] ;32.7
        lea       esi, DWORD PTR [444+esp]                      ;32.7
        shl       edx, 2                                        ;32.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_END] ;32.7
        sub       ecx, edx                                      ;32.7
        add       ecx, ebx                                      ;32.7
        mov       DWORD PTR [444+esp], 4                        ;32.7
        mov       DWORD PTR [448+esp], ecx                      ;32.7
        push      esi                                           ;32.7
        push      OFFSET FLAT: __STRLITPACK_63.0.2              ;32.7
        lea       edi, DWORD PTR [156+esp]                      ;32.7
        push      edi                                           ;32.7
        call      _for_read_seq_lis_xmit                        ;32.7
                                ; LOE ebx
.B2.130:                        ; Preds .B2.9
        add       esp, 96                                       ;32.7
                                ; LOE ebx
.B2.10:                         ; Preds .B2.130
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE+32] ;33.7
        mov       ecx, ebx                                      ;33.45
        shl       edx, 2                                        ;33.45
        neg       edx                                           ;33.45
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE] ;33.45
        mov       edx, DWORD PTR [edx+ecx]                      ;33.10
        cmp       edx, 3                                        ;33.21
        je        .B2.17        ; Prob 16%                      ;33.21
                                ; LOE edx ebx
.B2.11:                         ; Preds .B2.10
        cmp       edx, 2                                        ;33.41
        je        .B2.17        ; Prob 16%                      ;33.41
                                ; LOE edx ebx
.B2.12:                         ; Preds .B2.11
        cmp       edx, 5                                        ;33.61
        je        .B2.17        ; Prob 16%                      ;33.61
                                ; LOE ebx
.B2.13:                         ; Preds .B2.12
        mov       DWORD PTR [64+esp], 0                         ;34.10
        lea       edx, DWORD PTR [272+esp]                      ;34.10
        mov       DWORD PTR [272+esp], 57                       ;34.10
        mov       DWORD PTR [276+esp], OFFSET FLAT: __STRLITPACK_55 ;34.10
        push      32                                            ;34.10
        push      edx                                           ;34.10
        push      OFFSET FLAT: __STRLITPACK_64.0.2              ;34.10
        push      -2088435968                                   ;34.10
        push      911                                           ;34.10
        lea       ecx, DWORD PTR [84+esp]                       ;34.10
        push      ecx                                           ;34.10
        call      _for_write_seq_lis                            ;34.10
                                ; LOE ebx
.B2.14:                         ; Preds .B2.13
        mov       edx, 32                                       ;35.10
        lea       ecx, DWORD PTR [304+esp]                      ;35.10
        mov       DWORD PTR [88+esp], 0                         ;35.10
        mov       DWORD PTR [304+esp], edx                      ;35.10
        mov       DWORD PTR [308+esp], OFFSET FLAT: __STRLITPACK_53 ;35.10
        push      edx                                           ;35.10
        push      ecx                                           ;35.10
        push      OFFSET FLAT: __STRLITPACK_65.0.2              ;35.10
        push      -2088435968                                   ;35.10
        push      911                                           ;35.10
        lea       esi, DWORD PTR [108+esp]                      ;35.10
        push      esi                                           ;35.10
        call      _for_write_seq_lis                            ;35.10
                                ; LOE ebx
.B2.15:                         ; Preds .B2.14
        mov       DWORD PTR [112+esp], 0                        ;36.10
        lea       edx, DWORD PTR [336+esp]                      ;36.10
        mov       DWORD PTR [336+esp], 38                       ;36.10
        mov       DWORD PTR [340+esp], OFFSET FLAT: __STRLITPACK_51 ;36.10
        push      32                                            ;36.10
        push      edx                                           ;36.10
        push      OFFSET FLAT: __STRLITPACK_66.0.2              ;36.10
        push      -2088435968                                   ;36.10
        push      911                                           ;36.10
        lea       ecx, DWORD PTR [132+esp]                      ;36.10
        push      ecx                                           ;36.10
        call      _for_write_seq_lis                            ;36.10
                                ; LOE ebx
.B2.16:                         ; Preds .B2.15
        push      32                                            ;37.10
        xor       edx, edx                                      ;37.10
        push      edx                                           ;37.10
        push      edx                                           ;37.10
        push      -2088435968                                   ;37.10
        push      edx                                           ;37.10
        push      OFFSET FLAT: __STRLITPACK_67                  ;37.10
        call      _for_stop_core                                ;37.10
                                ; LOE ebx
.B2.131:                        ; Preds .B2.16
        add       esp, 96                                       ;37.10
                                ; LOE ebx
.B2.17:                         ; Preds .B2.131 .B2.10 .B2.11 .B2.12
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;40.16
        mov       esi, DWORD PTR [444+esp]                      ;40.16
        mov       edx, DWORD PTR [440+esp]                      ;40.16
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;40.16
        shl       eax, 2                                        ;40.16
        lea       edi, DWORD PTR [ecx+esi*4]                    ;40.16
        push      OFFSET FLAT: __NLITPACK_0.0.2                 ;40.16
        sub       edi, eax                                      ;40.16
        lea       edx, DWORD PTR [ecx+edx*4]                    ;40.16
        push      edi                                           ;40.16
        sub       edx, eax                                      ;40.16
        push      edx                                           ;40.16
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;40.16
                                ; LOE eax ebx
.B2.132:                        ; Preds .B2.17
        add       esp, 12                                       ;40.16
                                ; LOE eax ebx
.B2.18:                         ; Preds .B2.132
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;40.7
        mov       edx, edi                                      ;40.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;40.7
        imul      edx, ecx                                      ;40.7
        mov       DWORD PTR [456+esp], ecx                      ;40.7
        sub       edx, ecx                                      ;40.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;40.7
        neg       edx                                           ;40.7
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;40.16
        add       esi, ebx                                      ;40.7
        mov       DWORD PTR [452+esp], esi                      ;40.7
        add       edx, esi                                      ;40.7
        lea       esi, DWORD PTR [ecx*4]                        ;40.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;41.7
        sub       edx, esi                                      ;40.7
        mov       esi, DWORD PTR [444+esp]                      ;41.7
        mov       DWORD PTR [464+esp], ecx                      ;41.7
        mov       DWORD PTR [edx], eax                          ;40.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;41.7
        sub       esi, edx                                      ;41.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODEWVMS+32] ;41.7
        neg       eax                                           ;41.7
        mov       DWORD PTR [472+esp], edx                      ;41.7
        add       eax, DWORD PTR [ecx+esi*4]                    ;41.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODEWVMS] ;41.7
        mov       edx, DWORD PTR [500+esp]                      ;41.7
        mov       DWORD PTR [ecx+eax*4], edx                    ;41.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE+32] ;43.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE] ;43.7
        mov       DWORD PTR [468+esp], edx                      ;43.7
        lea       esi, DWORD PTR [edx*4]                        ;43.21
        neg       esi                                           ;43.21
        add       esi, eax                                      ;43.21
        cmp       DWORD PTR [esi+ebx], 2                        ;43.21
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;43.21
        je        .B2.96        ; Prob 16%                      ;43.21
                                ; LOE eax ebx esi edi
.B2.19:                         ; Preds .B2.121 .B2.119 .B2.18
        mov       edx, DWORD PTR [468+esp]                      ;59.17
        add       eax, ebx                                      ;59.17
        shl       edx, 2                                        ;59.17
        neg       edx                                           ;59.17
        imul      edi, DWORD PTR [456+esp]                      ;
        mov       ecx, DWORD PTR [edx+eax]                      ;59.6
        shl       esi, 2                                        ;
        mov       DWORD PTR [460+esp], ecx                      ;59.6
        cmp       ecx, 3                                        ;59.17
        je        .B2.86        ; Prob 16%                      ;59.17
                                ; LOE ebx esi edi
.B2.20:                         ; Preds .B2.19 .B2.144 .B2.92 .B2.91
        mov       ecx, DWORD PTR [440+esp]                      ;71.10
        mov       edx, DWORD PTR [464+esp]                      ;71.32
        mov       eax, DWORD PTR [472+esp]                      ;71.32
        shl       eax, 2                                        ;71.32
        mov       DWORD PTR [472+esp], eax                      ;71.32
        lea       edx, DWORD PTR [edx+ecx*4]                    ;71.32
        sub       edx, eax                                      ;71.32
        cmp       DWORD PTR [edx], 0                            ;71.29
        jle       .B2.82        ; Prob 16%                      ;71.29
                                ; LOE ebx esi edi
.B2.21:                         ; Preds .B2.20
        mov       ecx, DWORD PTR [444+esp]                      ;71.36
        mov       edx, DWORD PTR [464+esp]                      ;71.32
        lea       edx, DWORD PTR [edx+ecx*4]                    ;71.32
        sub       edx, DWORD PTR [472+esp]                      ;71.32
        cmp       DWORD PTR [edx], 0                            ;71.55
        jle       .B2.82        ; Prob 16%                      ;71.55
                                ; LOE ebx esi edi
.B2.22:                         ; Preds .B2.142 .B2.21
        cmp       DWORD PTR [460+esp], 5                        ;77.21
        je        .B2.32        ; Prob 16%                      ;77.21
                                ; LOE ebx esi edi
.B2.23:                         ; Preds .B2.73 .B2.140 .B2.22
        sub       edi, DWORD PTR [456+esp]                      ;103.20
        mov       edx, DWORD PTR [452+esp]                      ;103.20
        sub       edx, edi                                      ;103.20
        sub       edx, esi                                      ;103.20
        cmp       DWORD PTR [edx], 0                            ;103.20
        jne       .B2.29        ; Prob 50%                      ;103.20
                                ; LOE ebx
.B2.24:                         ; Preds .B2.23
        mov       DWORD PTR [64+esp], 0                         ;104.10
        lea       edx, DWORD PTR [240+esp]                      ;104.10
        mov       DWORD PTR [240+esp], 27                       ;104.10
        mov       DWORD PTR [244+esp], OFFSET FLAT: __STRLITPACK_27 ;104.10
        push      32                                            ;104.10
        push      edx                                           ;104.10
        push      OFFSET FLAT: __STRLITPACK_94.0.2              ;104.10
        push      -2088435968                                   ;104.10
        push      911                                           ;104.10
        lea       ecx, DWORD PTR [84+esp]                       ;104.10
        push      ecx                                           ;104.10
        call      _for_write_seq_lis                            ;104.10
                                ; LOE ebx
.B2.25:                         ; Preds .B2.24
        mov       DWORD PTR [88+esp], 0                         ;105.10
        lea       edx, DWORD PTR [272+esp]                      ;105.10
        mov       DWORD PTR [272+esp], 18                       ;105.10
        mov       DWORD PTR [276+esp], OFFSET FLAT: __STRLITPACK_25 ;105.10
        push      32                                            ;105.10
        push      edx                                           ;105.10
        push      OFFSET FLAT: __STRLITPACK_95.0.2              ;105.10
        push      -2088435968                                   ;105.10
        push      911                                           ;105.10
        lea       ecx, DWORD PTR [108+esp]                      ;105.10
        push      ecx                                           ;105.10
        call      _for_write_seq_lis                            ;105.10
                                ; LOE ebx
.B2.26:                         ; Preds .B2.25
        mov       DWORD PTR [112+esp], 0                        ;106.10
        lea       edx, DWORD PTR [304+esp]                      ;106.10
        mov       DWORD PTR [304+esp], 14                       ;106.10
        mov       DWORD PTR [308+esp], OFFSET FLAT: __STRLITPACK_23 ;106.10
        push      32                                            ;106.10
        push      edx                                           ;106.10
        push      OFFSET FLAT: __STRLITPACK_96.0.2              ;106.10
        push      -2088435968                                   ;106.10
        push      911                                           ;106.10
        lea       ecx, DWORD PTR [132+esp]                      ;106.10
        push      ecx                                           ;106.10
        call      _for_write_seq_lis                            ;106.10
                                ; LOE ebx
.B2.27:                         ; Preds .B2.26
        mov       edx, DWORD PTR [572+esp]                      ;106.10
        lea       ecx, DWORD PTR [392+esp]                      ;106.10
        mov       DWORD PTR [392+esp], edx                      ;106.10
        push      ecx                                           ;106.10
        push      OFFSET FLAT: __STRLITPACK_97.0.2              ;106.10
        lea       esi, DWORD PTR [144+esp]                      ;106.10
        push      esi                                           ;106.10
        call      _for_write_seq_lis_xmit                       ;106.10
                                ; LOE ebx
.B2.28:                         ; Preds .B2.27
        push      32                                            ;107.10
        xor       edx, edx                                      ;107.10
        push      edx                                           ;107.10
        push      edx                                           ;107.10
        push      -2088435968                                   ;107.10
        push      edx                                           ;107.10
        push      OFFSET FLAT: __STRLITPACK_98                  ;107.10
        call      _for_stop_core                                ;107.10
                                ; LOE ebx
.B2.133:                        ; Preds .B2.28
        add       esp, 108                                      ;107.10
                                ; LOE ebx
.B2.29:                         ; Preds .B2.133 .B2.23
        mov       edx, DWORD PTR [500+esp]                      ;111.9
        add       ebx, 4                                        ;111.9
        inc       edx                                           ;111.9
        mov       DWORD PTR [500+esp], edx                      ;111.9
        cmp       edx, DWORD PTR [448+esp]                      ;111.9
        jle       .B2.3         ; Prob 82%                      ;111.9
                                ; LOE ebx
.B2.30:                         ; Preds .B2.29
        mov       esi, DWORD PTR [216+esp]                      ;
        mov       edi, DWORD PTR [220+esp]                      ;
        mov       ebx, DWORD PTR [224+esp]                      ;
                                ; LOE ebx esi edi
.B2.31:                         ; Preds .B2.30 .B2.1
        mov       esp, ebp                                      ;113.1
        pop       ebp                                           ;113.1
        ret                                                     ;113.1
                                ; LOE
.B2.32:                         ; Preds .B2.22                  ; Infreq
        mov       edx, DWORD PTR [456+esp]                      ;78.17
        lea       ecx, DWORD PTR [edx+edx*2]                    ;78.17
        neg       ecx                                           ;78.17
        add       ecx, edi                                      ;78.6
        neg       ecx                                           ;78.6
        add       ecx, DWORD PTR [452+esp]                      ;78.6
        sub       ecx, esi                                      ;78.6
        mov       edx, DWORD PTR [ecx]                          ;78.6
        test      edx, edx                                      ;78.6
        mov       DWORD PTR [436+esp], edx                      ;78.6
        jle       .B2.65        ; Prob 2%                       ;78.6
                                ; LOE edx ebx esi edi dl dh
.B2.33:                         ; Preds .B2.32                  ; Infreq
        mov       ecx, 1                                        ;
        mov       DWORD PTR [492+esp], ecx                      ;
        mov       DWORD PTR [380+esp], ebx                      ;
                                ; LOE
.B2.34:                         ; Preds .B2.63 .B2.33           ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+44] ;79.31
        neg       ecx                                           ;79.7
        add       ecx, DWORD PTR [492+esp]                      ;79.7
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+40] ;79.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+32] ;79.7
        shl       eax, 2                                        ;79.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE] ;79.7
        sub       edx, eax                                      ;79.7
        add       edx, DWORD PTR [380+esp]                      ;79.7
        add       ecx, edx                                      ;79.7
        mov       DWORD PTR [64+esp], 0                         ;79.7
        mov       DWORD PTR [312+esp], 4                        ;79.7
        mov       DWORD PTR [316+esp], ecx                      ;79.7
        push      32                                            ;79.7
        lea       ebx, DWORD PTR [316+esp]                      ;79.7
        push      ebx                                           ;79.7
        push      OFFSET FLAT: __STRLITPACK_85.0.2              ;79.7
        push      -2088435965                                   ;79.7
        push      49                                            ;79.7
        lea       esi, DWORD PTR [84+esp]                       ;79.7
        push      esi                                           ;79.7
        call      _for_read_seq_lis                             ;79.7
                                ; LOE eax
.B2.134:                        ; Preds .B2.34                  ; Infreq
        add       esp, 24                                       ;79.7
                                ; LOE eax
.B2.35:                         ; Preds .B2.134                 ; Infreq
        test      eax, eax                                      ;79.7
        jne       .B2.46        ; Prob 50%                      ;79.7
                                ; LOE eax
.B2.36:                         ; Preds .B2.35                  ; Infreq
        push      0                                             ;79.7
        push      OFFSET FLAT: __STRLITPACK_86.0.2              ;79.7
        lea       eax, DWORD PTR [72+esp]                       ;79.7
        push      eax                                           ;79.7
        call      _for_read_seq_lis_xmit                        ;79.7
                                ; LOE eax
.B2.135:                        ; Preds .B2.36                  ; Infreq
        add       esp, 12                                       ;79.7
                                ; LOE eax
.B2.37:                         ; Preds .B2.135                 ; Infreq
        test      eax, eax                                      ;79.7
        jne       .B2.46        ; Prob 50%                      ;79.7
                                ; LOE eax
.B2.38:                         ; Preds .B2.37                  ; Infreq
        mov       ecx, DWORD PTR [492+esp]                      ;79.7
        sub       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+44] ;79.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+32] ;79.7
        shl       eax, 2                                        ;79.7
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+40] ;79.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE] ;79.7
        sub       edx, eax                                      ;79.7
        add       edx, DWORD PTR [380+esp]                      ;79.7
        mov       esi, DWORD PTR [edx+ecx]                      ;79.7
        test      esi, esi                                      ;79.7
        jle       .B2.80        ; Prob 16%                      ;79.7
                                ; LOE esi
.B2.39:                         ; Preds .B2.38                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;79.48
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;79.48
        imul      edi, ecx                                      ;79.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;79.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;79.48
        sub       edx, edi                                      ;79.7
        neg       eax                                           ;79.7
        add       edx, ecx                                      ;79.7
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;79.7
        shl       ebx, 3                                        ;79.7
        add       eax, DWORD PTR [492+esp]                      ;79.7
        sub       edx, ebx                                      ;79.7
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;79.7
        mov       edi, DWORD PTR [500+esp]                      ;79.7
        lea       edx, DWORD PTR [edx+edi*8]                    ;79.7
        add       eax, edx                                      ;79.7
        lea       edi, DWORD PTR [296+esp]                      ;79.7
        mov       DWORD PTR [296+esp], eax                      ;79.7
        push      edi                                           ;79.7
        push      OFFSET FLAT: __STRLITPACK_87.0.2              ;79.7
        lea       eax, DWORD PTR [72+esp]                       ;79.7
        push      eax                                           ;79.7
        call      _for_read_seq_lis_xmit                        ;79.7
                                ; LOE eax esi edi
.B2.136:                        ; Preds .B2.39                  ; Infreq
        add       esp, 12                                       ;79.7
                                ; LOE eax esi edi
.B2.40:                         ; Preds .B2.136                 ; Infreq
        test      eax, eax                                      ;79.7
        jne       .B2.46        ; Prob 10%                      ;79.7
                                ; LOE eax esi edi
.B2.41:                         ; Preds .B2.40                  ; Infreq
        mov       ebx, 1                                        ;
                                ; LOE ebx esi
.B2.42:                         ; Preds .B2.44 .B2.41           ; Infreq
        inc       ebx                                           ;79.7
        cmp       ebx, esi                                      ;79.7
        jg        .B2.80        ; Prob 20%                      ;79.7
                                ; LOE ebx esi
.B2.43:                         ; Preds .B2.42                  ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;79.48
        neg       edx                                           ;79.7
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;79.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;79.48
        shl       edi, 3                                        ;79.7
        neg       eax                                           ;79.7
        add       edx, DWORD PTR [492+esp]                      ;79.7
        add       eax, ebx                                      ;79.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;79.7
        sub       ecx, edi                                      ;79.7
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;79.7
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;79.7
        mov       edi, DWORD PTR [500+esp]                      ;79.7
        lea       ecx, DWORD PTR [ecx+edi*8]                    ;79.7
        add       edx, ecx                                      ;79.7
        add       eax, edx                                      ;79.7
        mov       DWORD PTR [296+esp], eax                      ;79.7
        lea       eax, DWORD PTR [296+esp]                      ;79.7
        push      eax                                           ;79.7
        push      OFFSET FLAT: __STRLITPACK_87.0.2              ;79.7
        lea       edx, DWORD PTR [72+esp]                       ;79.7
        push      edx                                           ;79.7
        call      _for_read_seq_lis_xmit                        ;79.7
                                ; LOE eax ebx esi
.B2.137:                        ; Preds .B2.43                  ; Infreq
        add       esp, 12                                       ;79.7
                                ; LOE eax ebx esi
.B2.44:                         ; Preds .B2.137                 ; Infreq
        test      eax, eax                                      ;79.7
        je        .B2.42        ; Prob 82%                      ;79.7
                                ; LOE eax ebx esi
.B2.46:                         ; Preds .B2.141 .B2.44 .B2.40 .B2.37 .B2.35
                                ;                               ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;80.10
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;80.10
        mov       DWORD PTR [520+esp], ecx                      ;80.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;79.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;80.7
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;80.10
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;80.10
        mov       DWORD PTR [384+esp], ebx                      ;80.10
        imul      ebx, edi                                      ;80.43
        mov       DWORD PTR [388+esp], eax                      ;80.10
        mov       edx, DWORD PTR [492+esp]                      ;80.43
        imul      edx, eax                                      ;80.43
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;80.43
        mov       DWORD PTR [392+esp], ecx                      ;80.7
        shl       ecx, 3                                        ;80.43
        mov       DWORD PTR [516+esp], ecx                      ;80.43
        neg       eax                                           ;80.43
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;80.7
        add       eax, edx                                      ;80.43
        mov       ecx, DWORD PTR [500+esp]                      ;80.43
        mov       DWORD PTR [396+esp], ebx                      ;80.43
        sub       ebx, edi                                      ;80.43
        mov       DWORD PTR [408+esp], esi                      ;80.7
        neg       ebx                                           ;80.43
        mov       DWORD PTR [476+esp], edi                      ;80.10
        lea       esi, DWORD PTR [esi+ecx*8]                    ;80.43
        add       ebx, esi                                      ;80.43
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+44] ;85.7
        neg       ecx                                           ;85.7
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+32] ;85.7
        mov       DWORD PTR [512+esp], edx                      ;80.43
        mov       edx, DWORD PTR [380+esp]                      ;85.7
        shl       edi, 2                                        ;85.7
        add       ecx, DWORD PTR [492+esp]                      ;85.7
        neg       edi                                           ;85.7
        mov       DWORD PTR [400+esp], eax                      ;80.43
        add       eax, ebx                                      ;80.43
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE] ;85.7
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+40] ;85.7
        add       edx, edi                                      ;85.7
        sub       eax, DWORD PTR [516+esp]                      ;80.43
        mov       edi, DWORD PTR [edx+ecx]                      ;85.7
        mov       eax, DWORD PTR [eax]                          ;80.10
        cmp       eax, DWORD PTR [444+esp]                      ;80.38
        je        .B2.51        ; Prob 50%                      ;80.38
                                ; LOE ebx esi edi
.B2.47:                         ; Preds .B2.46 .B2.51           ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;81.8
        lea       eax, DWORD PTR [232+esp]                      ;81.8
        mov       DWORD PTR [232+esp], 23                       ;81.8
        mov       DWORD PTR [236+esp], OFFSET FLAT: __STRLITPACK_33 ;81.8
        push      32                                            ;81.8
        push      eax                                           ;81.8
        push      OFFSET FLAT: __STRLITPACK_89.0.2              ;81.8
        push      -2088435968                                   ;81.8
        push      911                                           ;81.8
        lea       edx, DWORD PTR [84+esp]                       ;81.8
        push      edx                                           ;81.8
        call      _for_write_seq_lis                            ;81.8
                                ; LOE ebx esi edi
.B2.48:                         ; Preds .B2.47                  ; Infreq
        mov       eax, DWORD PTR [524+esp]                      ;82.8
        lea       edx, DWORD PTR [328+esp]                      ;82.8
        mov       DWORD PTR [88+esp], 0                         ;82.8
        mov       DWORD PTR [328+esp], eax                      ;82.8
        push      32                                            ;82.8
        push      OFFSET FLAT: VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_READINPUT$format_pack.0.2+80 ;82.8
        push      edx                                           ;82.8
        push      OFFSET FLAT: __STRLITPACK_90.0.2              ;82.8
        push      -2088435968                                   ;82.8
        push      911                                           ;82.8
        lea       ecx, DWORD PTR [112+esp]                      ;82.8
        push      ecx                                           ;82.8
        call      _for_write_seq_fmt                            ;82.8
                                ; LOE ebx esi edi
.B2.49:                         ; Preds .B2.48                  ; Infreq
        push      32                                            ;83.8
        xor       eax, eax                                      ;83.8
        push      eax                                           ;83.8
        push      eax                                           ;83.8
        push      -2088435968                                   ;83.8
        push      eax                                           ;83.8
        push      OFFSET FLAT: __STRLITPACK_91                  ;83.8
        call      _for_stop_core                                ;83.8
                                ; LOE ebx esi edi
.B2.138:                        ; Preds .B2.49                  ; Infreq
        add       esp, 76                                       ;83.8
                                ; LOE ebx esi edi
.B2.50:                         ; Preds .B2.138                 ; Infreq
        test      edi, edi                                      ;85.7
        jg        .B2.52        ; Prob 50%                      ;85.7
        jmp       .B2.58        ; Prob 100%                     ;85.7
                                ; LOE ebx esi edi
.B2.51:                         ; Preds .B2.46                  ; Infreq
        test      edi, edi                                      ;80.62
        jle       .B2.47        ; Prob 1%                       ;80.62
                                ; LOE ebx esi edi
.B2.52:                         ; Preds .B2.51 .B2.50           ; Infreq
        mov       eax, edi                                      ;85.7
        shr       eax, 31                                       ;85.7
        add       eax, edi                                      ;85.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;86.9
        sar       eax, 1                                        ;85.7
        mov       DWORD PTR [404+esp], edx                      ;86.9
        test      eax, eax                                      ;85.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;86.9
        mov       DWORD PTR [484+esp], eax                      ;85.7
        jbe       .B2.78        ; Prob 3%                       ;85.7
                                ; LOE edx ebx esi edi
.B2.53:                         ; Preds .B2.52                  ; Infreq
        mov       DWORD PTR [496+esp], edi                      ;
        lea       ecx, DWORD PTR [edx*4]                        ;
        neg       ecx                                           ;
        xor       eax, eax                                      ;
        add       ecx, DWORD PTR [404+esp]                      ;
        mov       edi, DWORD PTR [400+esp]                      ;
        add       ebx, edi                                      ;
        mov       DWORD PTR [324+esp], ecx                      ;
        mov       ecx, DWORD PTR [516+esp]                      ;
        sub       ebx, ecx                                      ;
        mov       DWORD PTR [372+esp], ebx                      ;
        mov       ebx, DWORD PTR [476+esp]                      ;
        mov       DWORD PTR [308+esp], edx                      ;
        mov       DWORD PTR [300+esp], esi                      ;
        add       ebx, ebx                                      ;
        neg       ebx                                           ;
        add       ebx, DWORD PTR [396+esp]                      ;
        neg       ebx                                           ;
        add       ebx, esi                                      ;
        add       ebx, edi                                      ;
        sub       ebx, ecx                                      ;
        mov       DWORD PTR [368+esp], ebx                      ;
        mov       ebx, DWORD PTR [372+esp]                      ;
        mov       esi, eax                                      ;
        mov       edx, DWORD PTR [368+esp]                      ;
        mov       ecx, DWORD PTR [324+esp]                      ;
                                ; LOE eax edx ecx ebx esi
.B2.54:                         ; Preds .B2.54 .B2.53           ; Infreq
        mov       edi, DWORD PTR [ebx+esi*2]                    ;86.40
        inc       eax                                           ;85.7
        mov       edi, DWORD PTR [ecx+edi*4]                    ;86.9
        mov       DWORD PTR [ebx+esi*2], edi                    ;86.9
        mov       edi, DWORD PTR [edx+esi*2]                    ;86.40
        mov       edi, DWORD PTR [ecx+edi*4]                    ;86.9
        mov       DWORD PTR [edx+esi*2], edi                    ;86.9
        add       esi, DWORD PTR [476+esp]                      ;85.7
        cmp       eax, DWORD PTR [484+esp]                      ;85.7
        jb        .B2.54        ; Prob 63%                      ;85.7
                                ; LOE eax edx ecx ebx esi
.B2.55:                         ; Preds .B2.54                  ; Infreq
        mov       edi, DWORD PTR [496+esp]                      ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;85.7
        mov       edx, DWORD PTR [308+esp]                      ;
        mov       esi, DWORD PTR [300+esp]                      ;
                                ; LOE eax edx esi edi
.B2.56:                         ; Preds .B2.55 .B2.78           ; Infreq
        lea       ecx, DWORD PTR [-1+eax]                       ;85.7
        cmp       edi, ecx                                      ;85.7
        jbe       .B2.58        ; Prob 3%                       ;85.7
                                ; LOE eax edx esi edi
.B2.57:                         ; Preds .B2.56                  ; Infreq
        sub       esi, DWORD PTR [396+esp]                      ;86.9
        imul      eax, DWORD PTR [476+esp]                      ;86.9
        add       esi, DWORD PTR [400+esp]                      ;86.9
        sub       esi, DWORD PTR [516+esp]                      ;86.9
        mov       ecx, DWORD PTR [eax+esi]                      ;86.40
        sub       ecx, edx                                      ;86.9
        mov       edx, DWORD PTR [404+esp]                      ;86.9
        mov       ebx, DWORD PTR [edx+ecx*4]                    ;86.9
        mov       DWORD PTR [eax+esi], ebx                      ;86.9
                                ; LOE edi
.B2.58:                         ; Preds .B2.56 .B2.50 .B2.57    ; Infreq
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;88.10
        dec       edi                                           ;90.10
        neg       ebx                                           ;88.10
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;88.10
        add       ebx, 2                                        ;88.10
        shl       edx, 2                                        ;88.10
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;88.10
        sub       ecx, edx                                      ;88.10
        imul      ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;88.10
        mov       eax, DWORD PTR [380+esp]                      ;88.10
        add       ecx, eax                                      ;88.10
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE+32] ;89.10
        shl       esi, 2                                        ;89.10
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE] ;89.10
        mov       DWORD PTR [ecx+ebx], 100                      ;88.10
        sub       edx, esi                                      ;89.10
        mov       ecx, DWORD PTR [492+esp]                      ;89.10
        add       edx, eax                                      ;89.10
        sub       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE+44] ;89.10
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE+40] ;89.10
        mov       eax, DWORD PTR [376+esp]                      ;89.10
        test      edi, edi                                      ;90.10
        mov       DWORD PTR [edx+ecx], eax                      ;89.10
        jle       .B2.63        ; Prob 2%                       ;90.10
                                ; LOE edi
.B2.59:                         ; Preds .B2.58                  ; Infreq
        mov       ecx, DWORD PTR [384+esp]                      ;
        mov       edx, 1                                        ;
        mov       ebx, DWORD PTR [520+esp]                      ;
        imul      ecx, DWORD PTR [476+esp]                      ;
        imul      ebx, DWORD PTR [388+esp]                      ;
        mov       esi, DWORD PTR [392+esp]                      ;
        mov       eax, DWORD PTR [408+esp]                      ;
        sub       eax, ecx                                      ;
        mov       DWORD PTR [408+esp], eax                      ;
        lea       ecx, DWORD PTR [ebx+esi*8]                    ;
        neg       ecx                                           ;
        add       ecx, eax                                      ;
        mov       eax, DWORD PTR [500+esp]                      ;
        mov       DWORD PTR [520+esp], ebx                      ;
        mov       DWORD PTR [504+esp], edx                      ;
        mov       DWORD PTR [496+esp], edi                      ;
        lea       eax, DWORD PTR [ecx+eax*8]                    ;
        add       eax, DWORD PTR [512+esp]                      ;
        mov       ecx, DWORD PTR [408+esp]                      ;
        mov       ebx, DWORD PTR [476+esp]                      ;
                                ; LOE eax ecx ebx
.B2.60:                         ; Preds .B2.61 .B2.59           ; Infreq
        mov       esi, DWORD PTR [516+esp]                      ;91.42
        sub       ecx, DWORD PTR [520+esp]                      ;91.42
        sub       esi, ebx                                      ;91.42
        sub       ecx, esi                                      ;91.42
        mov       edx, DWORD PTR [504+esp]                      ;91.89
        imul      edx, ebx                                      ;91.89
        mov       ebx, DWORD PTR [500+esp]                      ;91.42
        add       eax, edx                                      ;91.42
        lea       ecx, DWORD PTR [ecx+ebx*8]                    ;91.42
        add       ecx, DWORD PTR [512+esp]                      ;91.42
        push      OFFSET FLAT: __NLITPACK_0.0.2                 ;91.42
        add       ecx, edx                                      ;91.42
        push      ecx                                           ;91.42
        push      eax                                           ;91.42
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;91.42
                                ; LOE eax
.B2.139:                        ; Preds .B2.60                  ; Infreq
        add       esp, 12                                       ;91.42
        mov       DWORD PTR [508+esp], eax                      ;91.42
                                ; LOE
.B2.61:                         ; Preds .B2.139                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;91.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;91.12
        imul      eax, edi                                      ;91.12
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;91.12
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;91.12
        imul      esi, ebx                                      ;91.12
        mov       edx, DWORD PTR [492+esp]                      ;91.12
        imul      edx, edi                                      ;91.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;91.42
        mov       DWORD PTR [520+esp], eax                      ;91.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;91.42
        sub       ecx, esi                                      ;91.12
        lea       eax, DWORD PTR [eax+edi*8]                    ;91.12
        neg       eax                                           ;91.12
        lea       esi, DWORD PTR [edi*8]                        ;91.12
        add       eax, ecx                                      ;91.12
        mov       DWORD PTR [516+esp], esi                      ;91.12
        mov       esi, DWORD PTR [500+esp]                      ;91.12
        mov       edi, DWORD PTR [504+esp]                      ;91.12
        mov       DWORD PTR [512+esp], edx                      ;91.12
        lea       eax, DWORD PTR [eax+esi*8]                    ;91.12
        mov       esi, edi                                      ;91.12
        imul      esi, ebx                                      ;91.12
        add       eax, edx                                      ;91.12
        inc       edi                                           ;92.10
        mov       edx, DWORD PTR [508+esp]                      ;91.12
        mov       DWORD PTR [4+eax+esi], edx                    ;91.12
        mov       DWORD PTR [504+esp], edi                      ;92.10
        cmp       edi, DWORD PTR [496+esp]                      ;92.10
        jle       .B2.60        ; Prob 82%                      ;92.10
                                ; LOE eax ecx ebx
.B2.63:                         ; Preds .B2.61 .B2.58           ; Infreq
        mov       eax, DWORD PTR [492+esp]                      ;93.9
        inc       eax                                           ;93.9
        mov       DWORD PTR [492+esp], eax                      ;93.9
        cmp       eax, DWORD PTR [436+esp]                      ;93.9
        jle       .B2.34        ; Prob 82%                      ;93.9
                                ; LOE
.B2.64:                         ; Preds .B2.63                  ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;94.20
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;94.20
        imul      edi, edx                                      ;
        mov       DWORD PTR [456+esp], edx                      ;94.20
        lea       edx, DWORD PTR [edx+edx*2]                    ;94.20
        neg       edx                                           ;94.20
        mov       ebx, DWORD PTR [380+esp]                      ;
        mov       ecx, ebx                                      ;
        add       edx, edi                                      ;94.20
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;
        neg       edx                                           ;94.20
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;94.9
        add       edx, ecx                                      ;94.20
        shl       esi, 2                                        ;
        sub       edx, esi                                      ;94.20
        mov       DWORD PTR [452+esp], ecx                      ;
        mov       edx, DWORD PTR [edx]                          ;94.20
        mov       DWORD PTR [436+esp], edx                      ;94.20
                                ; LOE edx ebx esi edi dl dh
.B2.65:                         ; Preds .B2.64 .B2.32           ; Infreq
        mov       ecx, edx                                      ;94.9
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE] ;97.9
        lea       edx, DWORD PTR [-1+ecx]                       ;94.9
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE+40] ;97.12
        mov       DWORD PTR [200+esp], ecx                      ;97.12
        test      edx, edx                                      ;94.9
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE+32] ;97.9
        mov       DWORD PTR [184+esp], edx                      ;94.9
        mov       DWORD PTR [180+esp], ecx                      ;97.9
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE+44] ;94.9
        jle       .B2.77        ; Prob 1%                       ;94.9
                                ; LOE eax edx ecx ebx esi edi cl ch
.B2.66:                         ; Preds .B2.65                  ; Infreq
        mov       DWORD PTR [100+esp], esi                      ;
        add       eax, ebx                                      ;
        mov       esi, DWORD PTR [184+esp]                      ;95.29
        mov       ecx, esi                                      ;95.29
        shr       ecx, 31                                       ;95.29
        add       ecx, esi                                      ;95.29
        sar       ecx, 1                                        ;95.29
        mov       esi, DWORD PTR [180+esp]                      ;
        imul      edx, DWORD PTR [200+esp]                      ;
        shl       esi, 2                                        ;
        mov       DWORD PTR [180+esp], esi                      ;
        test      ecx, ecx                                      ;95.29
        mov       DWORD PTR [188+esp], ecx                      ;95.29
        mov       esi, DWORD PTR [100+esp]                      ;95.29
        jbe       .B2.76        ; Prob 0%                       ;95.29
                                ; LOE eax edx ebx esi edi
.B2.67:                         ; Preds .B2.66                  ; Infreq
        mov       DWORD PTR [100+esp], esi                      ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [108+esp], edi                      ;
        mov       edi, edx                                      ;95.29
        mov       esi, DWORD PTR [200+esp]                      ;95.29
        sub       edi, esi                                      ;95.29
        neg       edi                                           ;95.29
        add       edi, eax                                      ;95.29
        mov       DWORD PTR [380+esp], ebx                      ;
        mov       ebx, DWORD PTR [180+esp]                      ;95.29
        sub       edi, ebx                                      ;95.29
        mov       DWORD PTR [208+esp], ecx                      ;
        mov       DWORD PTR [172+esp], edx                      ;
        movss     xmm0, DWORD PTR [edi]                         ;95.45
        lea       edi, DWORD PTR [esi+esi]                      ;95.45
        neg       edi                                           ;
        lea       esi, DWORD PTR [esi+esi*2]                    ;95.45
        neg       esi                                           ;95.45
        add       edi, edx                                      ;
        add       esi, edx                                      ;
        neg       edi                                           ;
        neg       esi                                           ;
        add       edi, eax                                      ;
        add       esi, eax                                      ;
        sub       edi, ebx                                      ;
        sub       esi, ebx                                      ;
        mov       DWORD PTR [192+esp], edi                      ;
        mov       DWORD PTR [196+esp], esi                      ;
        mov       DWORD PTR [176+esp], eax                      ;
        mov       eax, ecx                                      ;
        mov       edx, DWORD PTR [196+esp]                      ;
        mov       ecx, DWORD PTR [192+esp]                      ;
        mov       ebx, DWORD PTR [208+esp]                      ;
        mov       esi, DWORD PTR [188+esp]                      ;
        mov       edi, DWORD PTR [200+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.68:                         ; Preds .B2.68 .B2.67           ; Infreq
        inc       ebx                                           ;95.29
        addss     xmm0, DWORD PTR [ecx+eax*2]                   ;95.11
        movss     DWORD PTR [ecx+eax*2], xmm0                   ;95.11
        movss     xmm1, DWORD PTR [edx+eax*2]                   ;95.45
        addss     xmm0, xmm1                                    ;95.11
        movss     DWORD PTR [edx+eax*2], xmm0                   ;95.11
        add       eax, edi                                      ;95.29
        cmp       ebx, esi                                      ;95.29
        jb        .B2.68        ; Prob 64%                      ;95.29
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.69:                         ; Preds .B2.68                  ; Infreq
        mov       DWORD PTR [208+esp], ebx                      ;
        mov       ecx, ebx                                      ;95.29
        mov       edx, DWORD PTR [172+esp]                      ;
        mov       eax, DWORD PTR [176+esp]                      ;
        mov       esi, DWORD PTR [100+esp]                      ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;95.29
        mov       edi, DWORD PTR [108+esp]                      ;
        mov       ebx, DWORD PTR [380+esp]                      ;
        mov       DWORD PTR [204+esp], ecx                      ;95.29
                                ; LOE eax edx ebx esi edi
.B2.70:                         ; Preds .B2.69 .B2.76           ; Infreq
        mov       ecx, DWORD PTR [204+esp]                      ;95.29
        lea       ecx, DWORD PTR [-1+ecx]                       ;95.29
        cmp       ecx, DWORD PTR [184+esp]                      ;95.29
        jae       .B2.72        ; Prob 0%                       ;95.29
                                ; LOE eax edx ebx esi edi
.B2.71:                         ; Preds .B2.70                  ; Infreq
        mov       ecx, DWORD PTR [204+esp]                      ;95.11
        mov       DWORD PTR [172+esp], edx                      ;
        mov       DWORD PTR [176+esp], eax                      ;
        sub       eax, edx                                      ;95.11
        mov       edx, DWORD PTR [200+esp]                      ;95.45
        mov       DWORD PTR [100+esp], esi                      ;
        lea       esi, DWORD PTR [1+ecx]                        ;95.11
        imul      ecx, edx                                      ;95.11
        imul      esi, edx                                      ;95.45
        sub       eax, DWORD PTR [180+esp]                      ;95.11
        mov       edx, DWORD PTR [172+esp]                      ;95.11
        movss     xmm0, DWORD PTR [ecx+eax]                     ;95.29
        addss     xmm0, DWORD PTR [esi+eax]                     ;95.11
        movss     DWORD PTR [esi+eax], xmm0                     ;95.11
        mov       eax, DWORD PTR [176+esp]                      ;95.11
        mov       esi, DWORD PTR [100+esp]                      ;95.11
                                ; LOE eax edx ebx esi edi
.B2.72:                         ; Preds .B2.77 .B2.71 .B2.70    ; Infreq
        mov       ecx, DWORD PTR [200+esp]                      ;97.12
        imul      ecx, DWORD PTR [436+esp]                      ;97.12
        add       edx, DWORD PTR [180+esp]                      ;97.40
        add       eax, ecx                                      ;97.40
        sub       eax, edx                                      ;97.40
        movss     xmm1, DWORD PTR [eax]                         ;97.12
        comiss    xmm1, DWORD PTR [_2il0floatpacket.0]          ;97.32
        ja        .B2.74        ; Prob 5%                       ;97.32
                                ; LOE ebx esi edi xmm1
.B2.73:                         ; Preds .B2.72                  ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.1]          ;97.64
        comiss    xmm0, xmm1                                    ;97.64
        jbe       .B2.23        ; Prob 95%                      ;97.64
                                ; LOE ebx esi edi
.B2.74:                         ; Preds .B2.73 .B2.72           ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;98.11
        lea       edx, DWORD PTR [8+esp]                        ;98.11
        mov       DWORD PTR [8+esp], 33                         ;98.11
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_29 ;98.11
        push      32                                            ;98.11
        push      edx                                           ;98.11
        push      OFFSET FLAT: __STRLITPACK_92.0.2              ;98.11
        push      -2088435968                                   ;98.11
        push      911                                           ;98.11
        lea       ecx, DWORD PTR [84+esp]                       ;98.11
        push      ecx                                           ;98.11
        call      _for_write_seq_lis                            ;98.11
                                ; LOE ebx esi edi
.B2.75:                         ; Preds .B2.74                  ; Infreq
        push      32                                            ;99.11
        xor       edx, edx                                      ;99.11
        push      edx                                           ;99.11
        push      edx                                           ;99.11
        push      -2088435968                                   ;99.11
        push      edx                                           ;99.11
        push      OFFSET FLAT: __STRLITPACK_93                  ;99.11
        call      _for_stop_core                                ;99.11
                                ; LOE ebx esi edi
.B2.140:                        ; Preds .B2.75                  ; Infreq
        add       esp, 48                                       ;99.11
        jmp       .B2.23        ; Prob 100%                     ;99.11
                                ; LOE ebx esi edi
.B2.76:                         ; Preds .B2.66                  ; Infreq
        mov       DWORD PTR [204+esp], 1                        ;
        jmp       .B2.70        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi
.B2.77:                         ; Preds .B2.65                  ; Infreq
        add       eax, ebx                                      ;
        shl       ecx, 2                                        ;
        imul      edx, DWORD PTR [200+esp]                      ;
        mov       DWORD PTR [180+esp], ecx                      ;
        jmp       .B2.72        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi
.B2.78:                         ; Preds .B2.52                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.56        ; Prob 100%                     ;
                                ; LOE eax edx esi edi
.B2.80:                         ; Preds .B2.42 .B2.38           ; Infreq
        lea       edx, DWORD PTR [96+esp]                       ;79.7
        lea       eax, DWORD PTR [376+esp]                      ;79.7
        mov       DWORD PTR [96+esp], eax                       ;79.7
        push      edx                                           ;79.7
        push      OFFSET FLAT: __STRLITPACK_88.0.2              ;79.7
        lea       ecx, DWORD PTR [72+esp]                       ;79.7
        push      ecx                                           ;79.7
        call      _for_read_seq_lis_xmit                        ;79.7
                                ; LOE eax
.B2.141:                        ; Preds .B2.80                  ; Infreq
        add       esp, 12                                       ;79.7
        jmp       .B2.46        ; Prob 100%                     ;79.7
                                ; LOE eax
.B2.82:                         ; Preds .B2.20 .B2.21           ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;72.10
        lea       edx, DWORD PTR [112+esp]                      ;72.10
        mov       DWORD PTR [112+esp], 16                       ;72.10
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_37 ;72.10
        push      32                                            ;72.10
        push      edx                                           ;72.10
        push      OFFSET FLAT: __STRLITPACK_81.0.2              ;72.10
        push      -2088435968                                   ;72.10
        push      911                                           ;72.10
        lea       ecx, DWORD PTR [84+esp]                       ;72.10
        push      ecx                                           ;72.10
        call      _for_write_seq_lis                            ;72.10
                                ; LOE ebx esi edi
.B2.83:                         ; Preds .B2.82                  ; Infreq
        mov       DWORD PTR [88+esp], 0                         ;73.10
        lea       edx, DWORD PTR [144+esp]                      ;73.10
        mov       DWORD PTR [144+esp], 20                       ;73.10
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_35 ;73.10
        push      32                                            ;73.10
        push      edx                                           ;73.10
        push      OFFSET FLAT: __STRLITPACK_82.0.2              ;73.10
        push      -2088435968                                   ;73.10
        push      911                                           ;73.10
        lea       ecx, DWORD PTR [108+esp]                      ;73.10
        push      ecx                                           ;73.10
        call      _for_write_seq_lis                            ;73.10
                                ; LOE ebx esi edi
.B2.84:                         ; Preds .B2.83                  ; Infreq
        mov       edx, DWORD PTR [548+esp]                      ;73.10
        lea       ecx, DWORD PTR [312+esp]                      ;73.10
        mov       DWORD PTR [312+esp], edx                      ;73.10
        push      ecx                                           ;73.10
        push      OFFSET FLAT: __STRLITPACK_83.0.2              ;73.10
        lea       edx, DWORD PTR [120+esp]                      ;73.10
        push      edx                                           ;73.10
        call      _for_write_seq_lis_xmit                       ;73.10
                                ; LOE ebx esi edi
.B2.85:                         ; Preds .B2.84                  ; Infreq
        push      32                                            ;74.10
        xor       edx, edx                                      ;74.10
        push      edx                                           ;74.10
        push      edx                                           ;74.10
        push      -2088435968                                   ;74.10
        push      edx                                           ;74.10
        push      OFFSET FLAT: __STRLITPACK_84                  ;74.10
        call      _for_stop_core                                ;74.10
                                ; LOE ebx esi edi
.B2.142:                        ; Preds .B2.85                  ; Infreq
        add       esp, 84                                       ;74.10
        jmp       .B2.22        ; Prob 100%                     ;74.10
                                ; LOE ebx esi edi
.B2.86:                         ; Preds .B2.19                  ; Infreq
        mov       edx, DWORD PTR [456+esp]                      ;60.7
        lea       ecx, DWORD PTR [edx+edx]                      ;60.7
        neg       ecx                                           ;60.21
        add       ecx, edi                                      ;60.21
        neg       ecx                                           ;60.21
        add       ecx, DWORD PTR [452+esp]                      ;60.21
        sub       ecx, esi                                      ;60.21
        cmp       DWORD PTR [ecx], 100                          ;60.21
        jbe       .B2.91        ; Prob 50%                      ;60.21
                                ; LOE ebx esi edi
.B2.87:                         ; Preds .B2.86                  ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;61.5
        lea       edx, DWORD PTR [40+esp]                       ;61.5
        mov       DWORD PTR [40+esp], 23                        ;61.5
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_45 ;61.5
        push      32                                            ;61.5
        push      edx                                           ;61.5
        push      OFFSET FLAT: __STRLITPACK_74.0.2              ;61.5
        push      -2088435968                                   ;61.5
        push      911                                           ;61.5
        lea       ecx, DWORD PTR [84+esp]                       ;61.5
        push      ecx                                           ;61.5
        call      _for_write_seq_lis                            ;61.5
                                ; LOE ebx esi edi
.B2.88:                         ; Preds .B2.87                  ; Infreq
        mov       DWORD PTR [88+esp], 0                         ;62.5
        lea       edx, DWORD PTR [72+esp]                       ;62.5
        mov       DWORD PTR [72+esp], 38                        ;62.5
        mov       DWORD PTR [76+esp], OFFSET FLAT: __STRLITPACK_43 ;62.5
        push      32                                            ;62.5
        push      edx                                           ;62.5
        push      OFFSET FLAT: __STRLITPACK_75.0.2              ;62.5
        push      -2088435968                                   ;62.5
        push      911                                           ;62.5
        lea       ecx, DWORD PTR [108+esp]                      ;62.5
        push      ecx                                           ;62.5
        call      _for_write_seq_lis                            ;62.5
                                ; LOE ebx esi edi
.B2.89:                         ; Preds .B2.88                  ; Infreq
        mov       edx, DWORD PTR [548+esp]                      ;62.5
        lea       ecx, DWORD PTR [104+esp]                      ;62.5
        mov       DWORD PTR [104+esp], edx                      ;62.5
        push      ecx                                           ;62.5
        push      OFFSET FLAT: __STRLITPACK_76.0.2              ;62.5
        lea       edx, DWORD PTR [120+esp]                      ;62.5
        push      edx                                           ;62.5
        call      _for_write_seq_lis_xmit                       ;62.5
                                ; LOE ebx esi edi
.B2.90:                         ; Preds .B2.89                  ; Infreq
        push      32                                            ;63.5
        xor       edx, edx                                      ;63.5
        push      edx                                           ;63.5
        push      edx                                           ;63.5
        push      -2088435968                                   ;63.5
        push      edx                                           ;63.5
        push      OFFSET FLAT: __STRLITPACK_77                  ;63.5
        call      _for_stop_core                                ;63.5
                                ; LOE ebx esi edi
.B2.143:                        ; Preds .B2.90                  ; Infreq
        add       esp, 84                                       ;63.5
                                ; LOE ebx esi edi
.B2.91:                         ; Preds .B2.143 .B2.86          ; Infreq
        mov       edx, DWORD PTR [456+esp]                      ;65.7
        lea       ecx, DWORD PTR [edx+edx*2]                    ;65.7
        neg       ecx                                           ;65.7
        add       ecx, edi                                      ;65.20
        neg       ecx                                           ;65.20
        add       ecx, DWORD PTR [452+esp]                      ;65.20
        sub       ecx, esi                                      ;65.20
        mov       edx, DWORD PTR [ecx]                          ;65.7
        test      edx, edx                                      ;65.16
        je        .B2.20        ; Prob 50%                      ;65.16
                                ; LOE edx ebx esi edi
.B2.92:                         ; Preds .B2.91                  ; Infreq
        cmp       edx, 1                                        ;65.34
        je        .B2.20        ; Prob 79%                      ;65.34
                                ; LOE ebx esi edi
.B2.93:                         ; Preds .B2.92                  ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;66.7
        lea       edx, DWORD PTR [24+esp]                       ;66.7
        mov       DWORD PTR [24+esp], 23                        ;66.7
        mov       DWORD PTR [28+esp], OFFSET FLAT: __STRLITPACK_41 ;66.7
        push      32                                            ;66.7
        push      edx                                           ;66.7
        push      OFFSET FLAT: __STRLITPACK_78.0.2              ;66.7
        push      -2088435968                                   ;66.7
        push      911                                           ;66.7
        lea       ecx, DWORD PTR [84+esp]                       ;66.7
        push      ecx                                           ;66.7
        call      _for_write_seq_lis                            ;66.7
                                ; LOE ebx esi edi
.B2.94:                         ; Preds .B2.93                  ; Infreq
        mov       DWORD PTR [88+esp], 0                         ;67.10
        lea       edx, DWORD PTR [56+esp]                       ;67.10
        mov       DWORD PTR [56+esp], 38                        ;67.10
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_39 ;67.10
        push      32                                            ;67.10
        push      edx                                           ;67.10
        push      OFFSET FLAT: __STRLITPACK_79.0.2              ;67.10
        push      -2088435968                                   ;67.10
        push      911                                           ;67.10
        lea       ecx, DWORD PTR [108+esp]                      ;67.10
        push      ecx                                           ;67.10
        call      _for_write_seq_lis                            ;67.10
                                ; LOE ebx esi edi
.B2.95:                         ; Preds .B2.94                  ; Infreq
        push      32                                            ;68.7
        xor       edx, edx                                      ;68.7
        push      edx                                           ;68.7
        push      edx                                           ;68.7
        push      -2088435968                                   ;68.7
        push      edx                                           ;68.7
        push      OFFSET FLAT: __STRLITPACK_80                  ;68.7
        call      _for_stop_core                                ;68.7
                                ; LOE ebx esi edi
.B2.144:                        ; Preds .B2.95                  ; Infreq
        add       esp, 72                                       ;68.7
        jmp       .B2.20        ; Prob 100%                     ;68.7
                                ; LOE ebx esi edi
.B2.96:                         ; Preds .B2.18                  ; Infreq
        xor       edx, edx                                      ;44.5
        mov       DWORD PTR [64+esp], edx                       ;44.5
        push      32                                            ;44.5
        push      edx                                           ;44.5
        push      OFFSET FLAT: __STRLITPACK_68.0.2              ;44.5
        push      -2088435965                                   ;44.5
        push      49                                            ;44.5
        lea       ecx, DWORD PTR [84+esp]                       ;44.5
        push      ecx                                           ;44.5
        call      _for_read_seq_lis                             ;44.5
                                ; LOE eax ebx
.B2.145:                        ; Preds .B2.96                  ; Infreq
        add       esp, 24                                       ;44.5
                                ; LOE eax ebx
.B2.97:                         ; Preds .B2.145                 ; Infreq
        test      eax, eax                                      ;44.5
        je        .B2.99        ; Prob 50%                      ;44.5
                                ; LOE eax ebx
.B2.98:                         ; Preds .B2.97                  ; Infreq
        mov       edx, DWORD PTR [500+esp]                      ;
        lea       ecx, DWORD PTR [edx*8]                        ;
        mov       DWORD PTR [420+esp], ecx                      ;
        jmp       .B2.107       ; Prob 100%                     ;
                                ; LOE eax ebx
.B2.99:                         ; Preds .B2.97                  ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;44.61
        neg       edx                                           ;44.5
        add       edx, 3                                        ;44.5
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;44.5
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;44.5
        mov       esi, DWORD PTR [500+esp]                      ;44.5
        neg       ecx                                           ;44.5
        add       ecx, esi                                      ;44.5
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;44.5
        lea       edi, DWORD PTR [esi*8]                        ;
        mov       DWORD PTR [420+esp], edi                      ;
        mov       edx, DWORD PTR [edx+ecx*4]                    ;44.5
        test      edx, edx                                      ;44.5
        mov       DWORD PTR [212+esp], edx                      ;44.5
        jle       .B2.126       ; Prob 16%                      ;44.5
                                ; LOE ebx
.B2.100:                        ; Preds .B2.99                  ; Infreq
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;44.30
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;44.30
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;44.30
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;44.30
        imul      edx, edi                                      ;44.5
        imul      ecx, eax                                      ;44.5
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;44.5
        sub       edi, ecx                                      ;44.5
        sub       esi, edx                                      ;44.5
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;44.5
        add       esi, edi                                      ;44.5
        shl       edx, 3                                        ;44.5
        sub       eax, edx                                      ;44.5
        add       esi, eax                                      ;44.5
        mov       ecx, DWORD PTR [500+esp]                      ;44.5
        lea       esi, DWORD PTR [esi+ecx*8]                    ;44.5
        mov       DWORD PTR [104+esp], esi                      ;44.5
        lea       esi, DWORD PTR [104+esp]                      ;44.5
        push      esi                                           ;44.5
        push      OFFSET FLAT: __STRLITPACK_69.0.2              ;44.5
        lea       edi, DWORD PTR [72+esp]                       ;44.5
        push      edi                                           ;44.5
        call      _for_read_seq_lis_xmit                        ;44.5
                                ; LOE eax ebx esi
.B2.146:                        ; Preds .B2.100                 ; Infreq
        add       esp, 12                                       ;44.5
                                ; LOE eax ebx esi
.B2.101:                        ; Preds .B2.146                 ; Infreq
        test      eax, eax                                      ;44.5
        jne       .B2.107       ; Prob 10%                      ;44.5
                                ; LOE eax ebx esi
.B2.102:                        ; Preds .B2.101                 ; Infreq
        mov       DWORD PTR [380+esp], ebx                      ;
        mov       edi, 1                                        ;
        mov       ebx, DWORD PTR [500+esp]                      ;
                                ; LOE ebx esi edi
.B2.103:                        ; Preds .B2.105 .B2.102         ; Infreq
        inc       edi                                           ;44.5
        cmp       edi, DWORD PTR [212+esp]                      ;44.5
        jg        .B2.125       ; Prob 20%                      ;44.5
                                ; LOE ebx esi edi
.B2.104:                        ; Preds .B2.103                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;44.30
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;44.30
        imul      ecx, eax                                      ;44.5
        sub       ecx, eax                                      ;44.5
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;44.30
        neg       edx                                           ;44.5
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;44.5
        add       edx, edi                                      ;44.5
        sub       eax, ecx                                      ;44.5
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;44.5
        shl       ecx, 3                                        ;44.5
        sub       eax, ecx                                      ;44.5
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;44.5
        lea       eax, DWORD PTR [eax+ebx*8]                    ;44.5
        add       edx, eax                                      ;44.5
        mov       DWORD PTR [104+esp], edx                      ;44.5
        push      esi                                           ;44.5
        push      OFFSET FLAT: __STRLITPACK_69.0.2              ;44.5
        lea       edx, DWORD PTR [72+esp]                       ;44.5
        push      edx                                           ;44.5
        call      _for_read_seq_lis_xmit                        ;44.5
                                ; LOE eax ebx esi edi
.B2.147:                        ; Preds .B2.104                 ; Infreq
        add       esp, 12                                       ;44.5
                                ; LOE eax ebx esi edi
.B2.105:                        ; Preds .B2.147                 ; Infreq
        test      eax, eax                                      ;44.5
        je        .B2.103       ; Prob 82%                      ;44.5
                                ; LOE eax ebx esi edi
.B2.106:                        ; Preds .B2.105                 ; Infreq
        mov       DWORD PTR [500+esp], ebx                      ;
        mov       ebx, DWORD PTR [380+esp]                      ;
                                ; LOE eax ebx
.B2.107:                        ; Preds .B2.150 .B2.101 .B2.106 .B2.98 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;45.8
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;45.8
        mov       DWORD PTR [168+esp], esi                      ;45.8
        imul      esi, edi                                      ;45.35
        mov       DWORD PTR [380+esp], ebx                      ;
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;44.5
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;45.8
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;45.35
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;45.35
        imul      ebx, ecx                                      ;45.35
        shl       eax, 3                                        ;45.35
        mov       DWORD PTR [412+esp], eax                      ;45.35
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;45.5
        mov       eax, DWORD PTR [420+esp]                      ;45.35
        mov       DWORD PTR [156+esp], esi                      ;45.35
        sub       esi, edi                                      ;45.35
        neg       esi                                           ;45.35
        mov       DWORD PTR [416+esp], ebx                      ;45.35
        add       eax, edx                                      ;45.35
        add       esi, eax                                      ;45.35
        sub       ebx, ecx                                      ;45.35
        sub       esi, ebx                                      ;45.35
        sub       esi, DWORD PTR [412+esp]                      ;45.35
        mov       DWORD PTR [128+esp], edx                      ;45.5
        mov       DWORD PTR [228+esp], edi                      ;45.8
        mov       edx, DWORD PTR [esi]                          ;45.8
        mov       DWORD PTR [164+esp], ecx                      ;45.8
        mov       DWORD PTR [160+esp], eax                      ;45.35
        mov       ebx, DWORD PTR [380+esp]                      ;45.35
        cmp       edx, DWORD PTR [444+esp]                      ;45.35
        jne       .B2.122       ; Prob 5%                       ;45.35
                                ; LOE ebx bl bh
.B2.108:                        ; Preds .B2.149 .B2.107         ; Infreq
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;50.16
        mov       esi, edi                                      ;50.5
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;50.16
        mov       ecx, ebx                                      ;50.5
        imul      esi, edx                                      ;50.5
        mov       DWORD PTR [456+esp], edx                      ;50.16
        lea       edx, DWORD PTR [edx+edx*2]                    ;50.16
        neg       edx                                           ;50.16
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;50.5
        add       edx, esi                                      ;50.5
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;50.5
        neg       edx                                           ;50.5
        add       edx, ecx                                      ;50.5
        mov       DWORD PTR [452+esp], ecx                      ;50.5
        shl       eax, 2                                        ;50.5
        sub       edx, eax                                      ;50.5
        mov       DWORD PTR [152+esp], esi                      ;50.5
        mov       DWORD PTR [148+esp], eax                      ;50.5
        mov       DWORD PTR [144+esp], edx                      ;50.5
        mov       ecx, DWORD PTR [edx]                          ;50.5
        test      ecx, ecx                                      ;50.5
        mov       DWORD PTR [140+esp], ecx                      ;50.5
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;50.5
        jle       .B2.115       ; Prob 50%                      ;50.5
                                ; LOE ebx esi edi bl bh
.B2.109:                        ; Preds .B2.108                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;51.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;51.7
        mov       DWORD PTR [136+esp], edx                      ;51.7
        mov       edx, DWORD PTR [140+esp]                      ;50.5
        mov       DWORD PTR [132+esp], ecx                      ;51.7
        mov       ecx, edx                                      ;50.5
        shr       ecx, 31                                       ;50.5
        add       ecx, edx                                      ;50.5
        sar       ecx, 1                                        ;50.5
        mov       DWORD PTR [268+esp], ecx                      ;50.5
        test      ecx, ecx                                      ;50.5
        jbe       .B2.120       ; Prob 3%                       ;50.5
                                ; LOE ebx esi edi bl bh
.B2.110:                        ; Preds .B2.109                 ; Infreq
        mov       edx, DWORD PTR [132+esp]                      ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [100+esp], esi                      ;
        mov       DWORD PTR [108+esp], edi                      ;
        mov       ecx, DWORD PTR [164+esp]                      ;
        lea       esi, DWORD PTR [edx*4]                        ;
        neg       esi                                           ;
        add       esi, DWORD PTR [136+esp]                      ;
        mov       DWORD PTR [20+esp], esi                       ;
        mov       esi, DWORD PTR [228+esp]                      ;
        mov       edi, esi                                      ;
        mov       DWORD PTR [380+esp], ebx                      ;
        mov       ebx, DWORD PTR [416+esp]                      ;
        sub       edi, ebx                                      ;
        sub       ecx, DWORD PTR [412+esp]                      ;
        mov       edx, DWORD PTR [160+esp]                      ;
        add       edi, ecx                                      ;
        sub       edx, DWORD PTR [156+esp]                      ;
        add       edi, edx                                      ;
        mov       DWORD PTR [60+esp], edi                       ;
        lea       edi, DWORD PTR [esi+esi]                      ;
        sub       edi, ebx                                      ;
        add       ecx, edi                                      ;
        add       edx, ecx                                      ;
        mov       ecx, DWORD PTR [60+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       ebx, eax                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.111:                        ; Preds .B2.111 .B2.110         ; Infreq
        mov       edi, DWORD PTR [ecx+eax*2]                    ;51.37
        inc       ebx                                           ;50.5
        mov       edi, DWORD PTR [esi+edi*4]                    ;51.7
        mov       DWORD PTR [ecx+eax*2], edi                    ;51.7
        mov       edi, DWORD PTR [edx+eax*2]                    ;51.37
        mov       edi, DWORD PTR [esi+edi*4]                    ;51.7
        mov       DWORD PTR [edx+eax*2], edi                    ;51.7
        add       eax, DWORD PTR [228+esp]                      ;50.5
        cmp       ebx, DWORD PTR [268+esp]                      ;50.5
        jb        .B2.111       ; Prob 64%                      ;50.5
                                ; LOE eax edx ecx ebx esi
.B2.112:                        ; Preds .B2.111                 ; Infreq
        mov       edx, ebx                                      ;50.5
        mov       esi, DWORD PTR [100+esp]                      ;
        mov       edi, DWORD PTR [108+esp]                      ;
        mov       ebx, DWORD PTR [380+esp]                      ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;50.5
                                ; LOE edx ebx esi edi bl bh
.B2.113:                        ; Preds .B2.112 .B2.120         ; Infreq
        lea       ecx, DWORD PTR [-1+edx]                       ;50.5
        cmp       ecx, DWORD PTR [140+esp]                      ;50.5
        jae       .B2.115       ; Prob 3%                       ;50.5
                                ; LOE edx ebx esi edi bl bh
.B2.114:                        ; Preds .B2.113                 ; Infreq
        mov       ecx, DWORD PTR [156+esp]                      ;51.7
        mov       eax, DWORD PTR [164+esp]                      ;51.7
        add       ecx, DWORD PTR [416+esp]                      ;51.7
        mov       DWORD PTR [100+esp], esi                      ;
        mov       esi, DWORD PTR [160+esp]                      ;51.7
        sub       eax, DWORD PTR [412+esp]                      ;51.7
        sub       esi, ecx                                      ;51.7
        imul      edx, DWORD PTR [228+esp]                      ;51.7
        add       eax, esi                                      ;51.7
        mov       esi, DWORD PTR [edx+eax]                      ;51.37
        sub       esi, DWORD PTR [132+esp]                      ;51.7
        mov       ecx, DWORD PTR [136+esp]                      ;51.7
        mov       ecx, DWORD PTR [ecx+esi*4]                    ;51.7
        mov       DWORD PTR [edx+eax], ecx                      ;51.7
        mov       esi, DWORD PTR [100+esp]                      ;51.7
                                ; LOE ebx esi edi bl bh
.B2.115:                        ; Preds .B2.108 .B2.113 .B2.114 ; Infreq
        mov       edx, DWORD PTR [456+esp]                      ;53.8
        lea       ecx, DWORD PTR [edx+edx]                      ;53.8
        mov       edx, DWORD PTR [152+esp]                      ;53.8
        sub       edx, ecx                                      ;53.8
        neg       edx                                           ;53.8
        add       edx, DWORD PTR [452+esp]                      ;53.8
        sub       edx, DWORD PTR [148+esp]                      ;53.8
        mov       DWORD PTR [edx], 100                          ;53.8
        mov       edx, DWORD PTR [144+esp]                      ;54.20
        mov       eax, DWORD PTR [edx]                          ;54.20
        dec       eax                                           ;54.8
        test      eax, eax                                      ;54.8
        jle       .B2.121       ; Prob 2%                       ;54.8
                                ; LOE eax ebx esi edi bl bh
.B2.116:                        ; Preds .B2.115                 ; Infreq
        mov       edx, DWORD PTR [168+esp]                      ;
        mov       esi, 1                                        ;
        imul      edx, DWORD PTR [228+esp]                      ;
        mov       ecx, DWORD PTR [128+esp]                      ;
        sub       ecx, edx                                      ;
        mov       edx, DWORD PTR [416+esp]                      ;
        sub       edx, DWORD PTR [164+esp]                      ;
        neg       edx                                           ;
        add       edx, ecx                                      ;
        sub       edx, DWORD PTR [412+esp]                      ;
        mov       DWORD PTR [128+esp], ecx                      ;
        mov       DWORD PTR [428+esp], esi                      ;
        mov       DWORD PTR [424+esp], eax                      ;
        add       edx, DWORD PTR [420+esp]                      ;
        mov       esi, DWORD PTR [164+esp]                      ;
        mov       eax, DWORD PTR [228+esp]                      ;
        mov       DWORD PTR [380+esp], ebx                      ;
                                ; LOE eax edx ecx esi
.B2.117:                        ; Preds .B2.118 .B2.116         ; Infreq
        mov       ebx, DWORD PTR [428+esp]                      ;55.85
        imul      ebx, eax                                      ;55.85
        sub       ecx, DWORD PTR [416+esp]                      ;55.39
        add       edx, ebx                                      ;55.39
        sub       esi, DWORD PTR [412+esp]                      ;55.39
        add       ecx, esi                                      ;55.39
        add       ecx, eax                                      ;55.39
        add       ecx, DWORD PTR [420+esp]                      ;55.39
        push      OFFSET FLAT: __NLITPACK_0.0.2                 ;55.39
        add       ecx, ebx                                      ;55.39
        push      ecx                                           ;55.39
        push      edx                                           ;55.39
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;55.39
                                ; LOE eax
.B2.148:                        ; Preds .B2.117                 ; Infreq
        add       esp, 12                                       ;55.39
        mov       DWORD PTR [432+esp], eax                      ;55.39
                                ; LOE
.B2.118:                        ; Preds .B2.148                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;55.10
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;55.10
        imul      edx, eax                                      ;55.10
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;55.39
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;55.10
        sub       ecx, edx                                      ;55.10
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;55.10
        imul      edx, ebx                                      ;55.10
        mov       DWORD PTR [416+esp], edx                      ;55.10
        sub       edx, ebx                                      ;55.10
        neg       edx                                           ;55.10
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;55.39
        add       edx, ecx                                      ;55.10
        shl       esi, 3                                        ;55.10
        mov       DWORD PTR [412+esp], esi                      ;55.10
        sub       edx, esi                                      ;55.10
        mov       esi, DWORD PTR [428+esp]                      ;55.10
        mov       ebx, esi                                      ;55.10
        imul      ebx, eax                                      ;55.10
        inc       esi                                           ;56.8
        add       edx, DWORD PTR [420+esp]                      ;55.10
        mov       edi, DWORD PTR [432+esp]                      ;55.10
        mov       DWORD PTR [428+esp], esi                      ;56.8
        mov       DWORD PTR [4+edx+ebx], edi                    ;55.10
        cmp       esi, DWORD PTR [424+esp]                      ;56.8
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;56.8
        jle       .B2.117       ; Prob 82%                      ;56.8
                                ; LOE eax edx ecx esi
.B2.119:                        ; Preds .B2.118                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;71.7
        mov       ebx, DWORD PTR [380+esp]                      ;
        mov       DWORD PTR [464+esp], edx                      ;71.7
        mov       edx, ebx                                      ;
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE+32] ;59.3
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;60.7
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;71.7
        mov       DWORD PTR [468+esp], edi                      ;59.3
        mov       DWORD PTR [456+esp], esi                      ;60.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE] ;59.3
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;60.7
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;60.4
        mov       DWORD PTR [472+esp], ecx                      ;71.7
        mov       DWORD PTR [452+esp], edx                      ;
        jmp       .B2.19        ; Prob 100%                     ;
                                ; LOE eax ebx esi edi
.B2.120:                        ; Preds .B2.109                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.113       ; Prob 100%                     ;
                                ; LOE edx ebx esi edi bl bh
.B2.121:                        ; Preds .B2.115                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE+32] ;59.3
        mov       DWORD PTR [468+esp], edx                      ;59.3
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;71.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;71.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE] ;59.3
        mov       DWORD PTR [464+esp], ecx                      ;71.7
        mov       DWORD PTR [472+esp], edx                      ;71.7
        jmp       .B2.19        ; Prob 100%                     ;71.7
                                ; LOE eax ebx esi edi
.B2.122:                        ; Preds .B2.107                 ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;46.6
        lea       edx, DWORD PTR [esp]                          ;46.6
        mov       DWORD PTR [esp], 23                           ;46.6
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_49 ;46.6
        push      32                                            ;46.6
        push      edx                                           ;46.6
        push      OFFSET FLAT: __STRLITPACK_71.0.2              ;46.6
        push      -2088435968                                   ;46.6
        push      911                                           ;46.6
        lea       ecx, DWORD PTR [84+esp]                       ;46.6
        push      ecx                                           ;46.6
        call      _for_write_seq_lis                            ;46.6
                                ; LOE ebx bl bh
.B2.123:                        ; Preds .B2.122                 ; Infreq
        mov       edx, DWORD PTR [524+esp]                      ;47.6
        lea       ecx, DWORD PTR [40+esp]                       ;47.6
        mov       DWORD PTR [88+esp], 0                         ;47.6
        mov       DWORD PTR [40+esp], edx                       ;47.6
        push      32                                            ;47.6
        push      OFFSET FLAT: VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_READINPUT$format_pack.0.2 ;47.6
        push      ecx                                           ;47.6
        push      OFFSET FLAT: __STRLITPACK_72.0.2              ;47.6
        push      -2088435968                                   ;47.6
        push      911                                           ;47.6
        lea       esi, DWORD PTR [112+esp]                      ;47.6
        push      esi                                           ;47.6
        call      _for_write_seq_fmt                            ;47.6
                                ; LOE ebx bl bh
.B2.124:                        ; Preds .B2.123                 ; Infreq
        push      32                                            ;48.6
        xor       edx, edx                                      ;48.6
        push      edx                                           ;48.6
        push      edx                                           ;48.6
        push      -2088435968                                   ;48.6
        push      edx                                           ;48.6
        push      OFFSET FLAT: __STRLITPACK_73                  ;48.6
        call      _for_stop_core                                ;48.6
                                ; LOE ebx bl bh
.B2.149:                        ; Preds .B2.124                 ; Infreq
        add       esp, 76                                       ;48.6
        jmp       .B2.108       ; Prob 100%                     ;48.6
                                ; LOE ebx bl bh
.B2.125:                        ; Preds .B2.103                 ; Infreq
        mov       DWORD PTR [500+esp], ebx                      ;
        mov       ebx, DWORD PTR [380+esp]                      ;
                                ; LOE ebx
.B2.126:                        ; Preds .B2.99 .B2.125          ; Infreq
        push      0                                             ;44.5
        push      OFFSET FLAT: __STRLITPACK_70.0.2              ;44.5
        lea       edx, DWORD PTR [72+esp]                       ;44.5
        push      edx                                           ;44.5
        call      _for_read_seq_lis_xmit                        ;44.5
                                ; LOE eax ebx
.B2.150:                        ; Preds .B2.126                 ; Infreq
        add       esp, 12                                       ;44.5
        jmp       .B2.107       ; Prob 100%                     ;44.5
        ALIGN     16
                                ; LOE eax ebx
; mark_end;
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_READINPUT ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_READINPUT$format_pack.0.2	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	23
	DB	0
	DB	83
	DB	117
	DB	98
	DB	112
	DB	97
	DB	116
	DB	104
	DB	115
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	86
	DB	77
	DB	83
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	3
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	32
	DB	32
	DB	105
	DB	115
	DB	32
	DB	105
	DB	110
	DB	99
	DB	111
	DB	114
	DB	114
	DB	101
	DB	99
	DB	116
	DB	44
	DB	32
	DB	112
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	23
	DB	0
	DB	83
	DB	117
	DB	98
	DB	112
	DB	97
	DB	116
	DB	104
	DB	115
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	86
	DB	77
	DB	83
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	3
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	32
	DB	32
	DB	105
	DB	115
	DB	32
	DB	105
	DB	110
	DB	99
	DB	111
	DB	114
	DB	114
	DB	101
	DB	99
	DB	116
	DB	44
	DB	32
	DB	112
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_57.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_62.0.2	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63.0.2	DB	26
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.2	DD	9
__STRLITPACK_68.0.2	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_69.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_71.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_74.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_75.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_76.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_78.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_81.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_82.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_83.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_85.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_86.0.2	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_87.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_88.0.2	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_89.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_90.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_92.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_94.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_95.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_96.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_97.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_READINPUT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MAIN
; mark_begin;
       ALIGN     16
	PUBLIC _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MAIN
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MAIN	PROC NEAR 
; parameter 1: 48 + esp
.B3.1:                          ; Preds .B3.0
        push      esi                                           ;114.12
        push      edi                                           ;114.12
        push      ebx                                           ;114.12
        push      ebp                                           ;114.12
        sub       esp, 28                                       ;114.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_NUM] ;115.7
        test      eax, eax                                      ;115.18
        mov       DWORD PTR [8+esp], eax                        ;115.7
        jle       .B3.8         ; Prob 16%                      ;115.18
                                ; LOE
.B3.2:                          ; Preds .B3.1
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;119.13
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;119.13
        imul      edi, esi                                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_START+32] ;117.11
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_START] ;
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;119.13
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;119.13
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       ecx, esi                                      ;
        shl       ebp, 2                                        ;
        sub       ebx, edi                                      ;
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        sub       ecx, ebp                                      ;
        mov       eax, DWORD PTR [48+esp]                       ;114.12
        add       ecx, ebx                                      ;
        mov       DWORD PTR [24+esp], ecx                       ;
        lea       ecx, DWORD PTR [esi+esi]                      ;119.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_END+32] ;117.22
        lea       esi, DWORD PTR [esi+esi*2]                    ;119.13
        movss     xmm0, DWORD PTR [eax]                         ;117.11
        sub       ecx, ebp                                      ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE+32] ;118.13
        sub       esi, ebp                                      ;
        shl       edx, 2                                        ;
        add       ecx, ebx                                      ;
        shl       eax, 2                                        ;
        add       ebx, esi                                      ;
        mov       DWORD PTR [esp], 1                            ;
        neg       edx                                           ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        neg       eax                                           ;
        mov       DWORD PTR [12+esp], ebx                       ;
        mov       DWORD PTR [16+esp], ecx                       ;
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_END] ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE] ;
        mov       DWORD PTR [20+esp], edi                       ;
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       ebp, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx ebp xmm0
.B3.3:                          ; Preds .B3.6 .B3.2
        comiss    xmm0, DWORD PTR [ebx+ebp*4]                   ;117.22
        jb        .B3.6         ; Prob 50%                      ;117.22
                                ; LOE eax edx ecx ebx ebp xmm0
.B3.4:                          ; Preds .B3.3
        movss     xmm1, DWORD PTR [edx+ebp*4]                   ;117.52
        comiss    xmm1, xmm0                                    ;117.50
        jbe       .B3.6         ; Prob 50%                      ;117.50
                                ; LOE eax edx ecx ebx ebp xmm0
.B3.5:                          ; Preds .B3.4
        cmp       DWORD PTR [eax+ebp*4], 1                      ;118.27
        je        .B3.9         ; Prob 5%                       ;118.27
                                ; LOE eax edx ecx ebx ebp xmm0
.B3.6:                          ; Preds .B3.10 .B3.12 .B3.5 .B3.3 .B3.4
                                ;       .B3.11 .B3.13
        inc       ebp                                           ;122.9
        cmp       ebp, ecx                                      ;122.9
        jle       .B3.3         ; Prob 82%                      ;122.9
                                ; LOE eax edx ecx ebx ebp xmm0
.B3.8:                          ; Preds .B3.6 .B3.1
        add       esp, 28                                       ;124.1
        pop       ebp                                           ;124.1
        pop       ebx                                           ;124.1
        pop       edi                                           ;124.1
        pop       esi                                           ;124.1
        ret                                                     ;124.1
                                ; LOE
.B3.9:                          ; Preds .B3.5                   ; Infreq
        mov       esi, DWORD PTR [16+esp]                       ;119.13
        mov       edi, DWORD PTR [24+esp]                       ;119.13
        mov       DWORD PTR [8+esp], eax                        ;
        mov       eax, DWORD PTR [esi+ebp*4]                    ;119.13
        imul      esi, DWORD PTR [edi+ebp*4], 900               ;119.13
        mov       edi, DWORD PTR [20+esp]                       ;119.13
        test      eax, eax                                      ;119.13
        mov       DWORD PTR [4+esp], eax                        ;119.13
        mov       DWORD PTR [esp], esi                          ;119.13
        movss     xmm3, DWORD PTR [652+esi+edi]                 ;119.13
        mov       eax, DWORD PTR [8+esp]                        ;119.13
        jle       .B3.12        ; Prob 16%                      ;119.13
                                ; LOE eax edx ecx ebx ebp al ah xmm0 xmm3
.B3.10:                         ; Preds .B3.9                   ; Infreq
        cvtsi2ss  xmm1, DWORD PTR [4+esp]                       ;119.13
        divss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;119.13
        comiss    xmm1, xmm3                                    ;119.13
        jbe       .B3.6         ; Prob 50%                      ;119.13
                                ; LOE eax edx ecx ebx ebp al ah xmm0 xmm3
.B3.11:                         ; Preds .B3.10                  ; Infreq
        mov       esi, DWORD PTR [12+esp]                       ;119.13
        mov       edi, DWORD PTR [esp]                          ;119.13
        cvtsi2ss  xmm1, DWORD PTR [esi+ebp*4]                   ;119.13
        divss     xmm1, DWORD PTR [_2il0floatpacket.6]          ;119.13
        mov       esi, DWORD PTR [20+esp]                       ;119.13
        addss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;119.13
        mulss     xmm1, xmm3                                    ;119.13
        movss     DWORD PTR [652+edi+esi], xmm1                 ;119.13
        jmp       .B3.6         ; Prob 100%                     ;119.13
                                ; LOE eax edx ecx ebx ebp xmm0
.B3.12:                         ; Preds .B3.9                   ; Infreq
        cvtsi2ss  xmm1, DWORD PTR [4+esp]                       ;119.13
        divss     xmm1, DWORD PTR [_2il0floatpacket.5]          ;119.13
        comiss    xmm3, xmm1                                    ;119.13
        jbe       .B3.6         ; Prob 50%                      ;119.13
                                ; LOE eax edx ecx ebx ebp al ah xmm0 xmm3
.B3.13:                         ; Preds .B3.12                  ; Infreq
        mov       esi, DWORD PTR [12+esp]                       ;119.13
        mov       edi, 99                                       ;119.13
        movss     xmm2, DWORD PTR [_2il0floatpacket.8]          ;119.13
        cmp       DWORD PTR [esi+ebp*4], 99                     ;119.13
        cmovl     edi, DWORD PTR [esi+ebp*4]                    ;119.13
        cvtsi2ss  xmm1, edi                                     ;119.13
        divss     xmm1, DWORD PTR [_2il0floatpacket.6]          ;119.13
        mov       DWORD PTR [esi+ebp*4], edi                    ;119.13
        subss     xmm2, xmm1                                    ;119.13
        mulss     xmm2, xmm3                                    ;119.13
        mov       edi, DWORD PTR [20+esp]                       ;119.13
        mov       esi, DWORD PTR [esp]                          ;119.13
        movss     DWORD PTR [652+esi+edi], xmm2                 ;119.13
        jmp       .B3.6         ; Prob 100%                     ;119.13
        ALIGN     16
                                ; LOE eax edx ecx ebx ebp xmm0
; mark_end;
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MAIN ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MAIN
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DIVERSION
; mark_begin;
       ALIGN     16
	PUBLIC _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DIVERSION
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DIVERSION	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
.B4.1:                          ; Preds .B4.0
        push      ebp                                           ;127.12
        mov       ebp, esp                                      ;127.12
        and       esp, -16                                      ;127.12
        push      esi                                           ;127.12
        push      edi                                           ;127.12
        push      ebx                                           ;127.12
        sub       esp, 84                                       ;127.12
        push      OFFSET FLAT: __NLITPACK_1.0.4                 ;133.7
        call      _RANXY                                        ;133.7
                                ; LOE f1
.B4.13:                         ; Preds .B4.1
        fstp      DWORD PTR [4+esp]                             ;133.7
        movss     xmm1, DWORD PTR [4+esp]                       ;133.7
        add       esp, 4                                        ;133.7
                                ; LOE xmm1
.B4.2:                          ; Preds .B4.13
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;135.11
        mov       ebx, DWORD PTR [20+ebp]                       ;127.12
        neg       esi                                           ;135.8
        add       esi, 2                                        ;135.8
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;135.2
        neg       edx                                           ;135.8
        imul      esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;135.8
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;135.2
        add       edx, DWORD PTR [ebx]                          ;135.8
        lea       ecx, DWORD PTR [eax+edx*4]                    ;135.8
        cvtsi2ss  xmm0, DWORD PTR [ecx+esi]                     ;135.11
        mulss     xmm0, DWORD PTR [_2il0floatpacket.13]         ;135.22
        comiss    xmm0, xmm1                                    ;135.8
        jb        .B4.10        ; Prob 50%                      ;135.8
                                ; LOE ebx
.B4.3:                          ; Preds .B4.2
        mov       eax, DWORD PTR [8+ebp]                        ;127.12
        movss     xmm0, DWORD PTR [_2il0floatpacket.14]         ;142.9
        movss     xmm1, DWORD PTR [_2il0floatpacket.15]         ;142.9
        movss     xmm3, DWORD PTR [eax]                         ;142.9
        lea       eax, DWORD PTR [72+esp]                       ;143.9
        divss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;142.9
        andps     xmm0, xmm3                                    ;142.9
        pxor      xmm3, xmm0                                    ;142.9
        movaps    xmm2, xmm3                                    ;142.9
        movss     xmm4, DWORD PTR [_2il0floatpacket.16]         ;142.9
        cmpltss   xmm2, xmm1                                    ;142.9
        andps     xmm1, xmm2                                    ;142.9
        movaps    xmm2, xmm3                                    ;142.9
        movaps    xmm6, xmm4                                    ;142.9
        addss     xmm2, xmm1                                    ;142.9
        addss     xmm6, xmm4                                    ;142.9
        subss     xmm2, xmm1                                    ;142.9
        movaps    xmm7, xmm2                                    ;142.9
        mov       esi, DWORD PTR [16+ebp]                       ;127.12
        subss     xmm7, xmm3                                    ;142.9
        movaps    xmm5, xmm7                                    ;142.9
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.17]         ;142.9
        cmpnless  xmm5, xmm4                                    ;142.9
        andps     xmm5, xmm6                                    ;142.9
        andps     xmm7, xmm6                                    ;142.9
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;137.4
        subss     xmm2, xmm5                                    ;142.9
        mov       edi, DWORD PTR [esi]                          ;137.13
        neg       ecx                                           ;137.4
        mov       DWORD PTR [76+esp], edi                       ;141.4
        add       ecx, edi                                      ;137.4
        shl       ecx, 6                                        ;137.4
        addss     xmm2, xmm7                                    ;142.9
        orps      xmm2, xmm0                                    ;142.9
        cvtss2si  edi, xmm2                                     ;142.4
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;137.4
        mov       DWORD PTR [80+esp], edi                       ;142.4
        lea       edi, DWORD PTR [68+esp]                       ;143.9
        movsx     edx, WORD PTR [12+edx+ecx]                    ;137.13
        xor       ecx, ecx                                      ;138.4
        mov       DWORD PTR [60+esp], edx                       ;137.4
        lea       edx, DWORD PTR [64+esp]                       ;143.9
        mov       DWORD PTR [64+esp], ecx                       ;138.4
        mov       DWORD PTR [68+esp], ecx                       ;139.4
        mov       DWORD PTR [72+esp], ecx                       ;140.4
        lea       ecx, DWORD PTR [80+esp]                       ;143.9
        push      eax                                           ;143.9
        push      edi                                           ;143.9
        push      edx                                           ;143.9
        push      ecx                                           ;143.9
        mov       edi, DWORD PTR [12+ebp]                       ;143.9
        lea       eax, DWORD PTR [76+esp]                       ;143.9
        push      eax                                           ;143.9
        push      edi                                           ;143.9
        lea       eax, DWORD PTR [100+esp]                      ;143.9
        push      eax                                           ;143.9
        call      _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME ;143.9
                                ; LOE ebx esi edi
.B4.14:                         ; Preds .B4.3
        add       esp, 28                                       ;143.9
                                ; LOE ebx esi edi
.B4.4:                          ; Preds .B4.14
        test      BYTE PTR [72+esp], 1                          ;149.8
        je        .B4.10        ; Prob 60%                      ;149.8
                                ; LOE ebx esi edi
.B4.5:                          ; Preds .B4.4
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;150.6
        neg       ecx                                           ;150.6
        add       ecx, DWORD PTR [esi]                          ;150.6
        shl       ecx, 5                                        ;150.6
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;150.6
        movsx     eax, WORD PTR [20+edx+ecx]                    ;150.12
        mov       DWORD PTR [40+esp], eax                       ;150.6
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;151.6
        neg       eax                                           ;151.6
        add       eax, DWORD PTR [esi]                          ;151.6
        shl       eax, 6                                        ;151.6
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;151.6
        movsx     ecx, WORD PTR [12+edx+eax]                    ;151.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;153.11
        neg       eax                                           ;153.11
        add       eax, 3                                        ;153.11
        mov       DWORD PTR [44+esp], ecx                       ;151.6
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;153.11
        neg       ecx                                           ;153.11
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;153.11
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;153.11
        add       ecx, DWORD PTR [ebx]                          ;153.11
        lea       edx, DWORD PTR [edx+ecx*4]                    ;153.11
        add       eax, edx                                      ;153.11
        lea       ecx, DWORD PTR [48+esp]                       ;153.11
        lea       edx, DWORD PTR [40+esp]                       ;153.11
        push      ecx                                           ;153.11
        push      OFFSET FLAT: __NLITPACK_2.0.4                 ;153.11
        push      edx                                           ;153.11
        lea       ecx, DWORD PTR [56+esp]                       ;153.11
        push      ecx                                           ;153.11
        push      eax                                           ;153.11
        push      edi                                           ;153.11
        push      esi                                           ;153.11
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;153.11
                                ; LOE ebx esi edi
.B4.15:                         ; Preds .B4.5
        add       esp, 28                                       ;153.11
                                ; LOE ebx esi edi
.B4.6:                          ; Preds .B4.15
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;154.10
        neg       eax                                           ;154.27
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;154.6
        add       eax, 3                                        ;154.27
        shl       edx, 2                                        ;154.27
        neg       edx                                           ;154.27
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;154.27
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;154.27
        mov       ecx, DWORD PTR [ebx]                          ;154.10
        lea       ebx, DWORD PTR [edx+ecx*4]                    ;154.27
        mov       eax, DWORD PTR [ebx+eax]                      ;154.10
        test      eax, eax                                      ;154.23
        je        .B4.9         ; Prob 50%                      ;154.23
                                ; LOE eax esi edi
.B4.7:                          ; Preds .B4.6
        cmp       eax, 1                                        ;154.44
        je        .B4.9         ; Prob 16%                      ;154.44
                                ; LOE esi edi
.B4.8:                          ; Preds .B4.7
        mov       DWORD PTR [esp], 0                            ;155.8
        lea       edx, DWORD PTR [esp]                          ;155.8
        mov       DWORD PTR [32+esp], 9                         ;155.8
        lea       eax, DWORD PTR [32+esp]                       ;155.8
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_22 ;155.8
        push      32                                            ;155.8
        push      eax                                           ;155.8
        push      OFFSET FLAT: __STRLITPACK_99.0.4              ;155.8
        push      -2088435968                                   ;155.8
        push      -1                                            ;155.8
        push      edx                                           ;155.8
        call      _for_write_seq_lis                            ;155.8
                                ; LOE esi edi
.B4.16:                         ; Preds .B4.8
        add       esp, 24                                       ;155.8
                                ; LOE esi edi
.B4.9:                          ; Preds .B4.16 .B4.7 .B4.6
        lea       eax, DWORD PTR [56+esp]                       ;157.11
        lea       edx, DWORD PTR [52+esp]                       ;157.11
        push      eax                                           ;157.11
        push      edx                                           ;157.11
        lea       ecx, DWORD PTR [52+esp]                       ;157.11
        push      ecx                                           ;157.11
        push      edi                                           ;157.11
        push      esi                                           ;157.11
        push      DWORD PTR [8+ebp]                             ;157.11
        call      _RETRIEVE_NEXT_LINK                           ;157.11
                                ; LOE
.B4.17:                         ; Preds .B4.9
        add       esp, 24                                       ;157.11
                                ; LOE
.B4.10:                         ; Preds .B4.17 .B4.4 .B4.2
        add       esp, 84                                       ;160.1
        pop       ebx                                           ;160.1
        pop       edi                                           ;160.1
        pop       esi                                           ;160.1
        mov       esp, ebp                                      ;160.1
        pop       ebp                                           ;160.1
        ret                                                     ;160.1
        ALIGN     16
                                ; LOE
; mark_end;
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DIVERSION ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__NLITPACK_1.0.4	DD	29
__NLITPACK_2.0.4	DD	0
__STRLITPACK_99.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DIVERSION
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DETOUR
; mark_begin;
       ALIGN     16
	PUBLIC _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DETOUR
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DETOUR	PROC NEAR 
; parameter 1: 52 + esp
; parameter 2: 56 + esp
; parameter 3: 60 + esp
; parameter 4: 64 + esp
.B5.1:                          ; Preds .B5.0
        push      esi                                           ;163.12
        push      edi                                           ;163.12
        push      ebx                                           ;163.12
        push      ebp                                           ;163.12
        sub       esp, 32                                       ;163.12
        mov       ebp, DWORD PTR [64+esp]                       ;163.12
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;170.18
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;170.7
        mov       esi, DWORD PTR [ebp]                          ;170.18
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;170.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;170.18
        mov       DWORD PTR [8+esp], esi                        ;170.18
        lea       ebx, DWORD PTR [esi*4]                        ;170.7
        mov       DWORD PTR [esp], ebx                          ;170.7
        mov       ebx, edx                                      ;170.7
        neg       ebx                                           ;170.7
        lea       esi, DWORD PTR [ecx+esi*4]                    ;170.7
        add       ebx, 3                                        ;170.7
        lea       edi, DWORD PTR [ebp*4]                        ;170.7
        imul      ebx, eax                                      ;170.7
        sub       esi, edi                                      ;170.7
        mov       edi, DWORD PTR [esi+ebx]                      ;170.7
        test      edi, edi                                      ;170.7
        mov       ebx, DWORD PTR [esp]                          ;170.7
        jle       .B5.6         ; Prob 2%                       ;170.7
                                ; LOE eax edx ecx ebx ebp edi
.B5.2:                          ; Preds .B5.1
        mov       DWORD PTR [esp], edi                          ;
        mov       esi, 1                                        ;
        mov       ebp, DWORD PTR [64+esp]                       ;
        mov       ebx, DWORD PTR [60+esp]                       ;
                                ; LOE ebx ebp esi
.B5.3:                          ; Preds .B5.4 .B5.2
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;171.24
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;171.24
        imul      eax, edi                                      ;171.9
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;171.24
        sub       eax, edi                                      ;171.9
        neg       ecx                                           ;171.9
        add       ecx, esi                                      ;171.9
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;171.9
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;171.9
        neg       edx                                           ;171.9
        sub       edi, eax                                      ;171.9
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;171.9
        add       edx, DWORD PTR [ebp]                          ;171.9
        lea       eax, DWORD PTR [edi+edx*8]                    ;171.9
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;172.9
        cvtsi2ss  xmm0, DWORD PTR [eax+ecx]                     ;171.9
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;172.9
        neg       ecx                                           ;172.9
        add       ecx, DWORD PTR [ebx]                          ;172.9
        shl       ecx, 6                                        ;172.9
        movss     DWORD PTR [24+esp], xmm0                      ;171.9
        movzx     eax, WORD PTR [12+edx+ecx]                    ;172.15
        lea       ecx, DWORD PTR [24+esp]                       ;173.11
        lea       edx, DWORD PTR [-1+esi+eax]                   ;172.9
        mov       WORD PTR [28+esp], dx                         ;172.9
        push      ecx                                           ;173.11
        push      OFFSET FLAT: __NLITPACK_3.0.5                 ;173.11
        lea       edi, DWORD PTR [36+esp]                       ;173.11
        push      edi                                           ;173.11
        push      ebx                                           ;173.11
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;173.11
                                ; LOE ebx ebp esi
.B5.14:                         ; Preds .B5.3
        add       esp, 16                                       ;173.11
                                ; LOE ebx ebp esi
.B5.4:                          ; Preds .B5.14
        inc       esi                                           ;174.4
        cmp       esi, DWORD PTR [esp]                          ;174.4
        jle       .B5.3         ; Prob 82%                      ;174.4
                                ; LOE ebx ebp esi
.B5.5:                          ; Preds .B5.4
        mov       ebp, DWORD PTR [64+esp]                       ;176.46
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;176.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;176.46
        mov       ebx, DWORD PTR [ebp]                          ;176.46
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;176.46
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;176.13
        mov       DWORD PTR [8+esp], ebx                        ;176.46
        shl       ebx, 2                                        ;
                                ; LOE eax edx ecx ebx ebp
.B5.6:                          ; Preds .B5.5 .B5.1
        neg       edx                                           ;176.7
        add       ecx, ebx                                      ;176.7
        shl       ebp, 2                                        ;176.7
        add       edx, 3                                        ;176.7
        neg       ebp                                           ;176.7
        imul      edx, eax                                      ;176.7
        add       ecx, ebp                                      ;176.7
        mov       edi, DWORD PTR [60+esp]                       ;176.13
        mov       eax, DWORD PTR [ecx+edx]                      ;176.46
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;176.7
        neg       edx                                           ;176.7
        mov       edi, DWORD PTR [edi]                          ;176.13
        add       edx, edi                                      ;176.7
        shl       edx, 6                                        ;176.7
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;176.7
        cmp       eax, 1                                        ;178.22
        movsx     ecx, WORD PTR [12+esi+edx]                    ;176.13
        lea       ebx, DWORD PTR [-1+eax+ecx]                   ;176.7
        mov       DWORD PTR [esp], ebx                          ;176.7
        jle       .B5.8         ; Prob 16%                      ;178.22
                                ; LOE eax edi
.B5.7:                          ; Preds .B5.6
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;179.17
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;179.17
        imul      ebx, edx                                      ;179.6
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;179.17
        neg       ecx                                           ;179.6
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;179.6
        sub       ebp, ebx                                      ;179.6
        lea       eax, DWORD PTR [-1+eax+ecx]                   ;179.6
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;179.6
        neg       ecx                                           ;179.6
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;179.6
        lea       esi, DWORD PTR [4+ebp+edx]                    ;179.6
        add       ecx, DWORD PTR [8+esp]                        ;179.6
        lea       edx, DWORD PTR [esi+ecx*8]                    ;179.6
        mov       eax, DWORD PTR [edx+eax]                      ;179.6
        mov       DWORD PTR [4+esp], eax                        ;179.6
        jmp       .B5.9         ; Prob 100%                     ;179.6
                                ; LOE edi
.B5.8:                          ; Preds .B5.6
        mov       eax, DWORD PTR [56+esp]                       ;181.6
        mov       edx, DWORD PTR [eax]                          ;181.6
        mov       DWORD PTR [4+esp], edx                        ;181.6
                                ; LOE edi
.B5.9:                          ; Preds .B5.7 .B5.8
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;183.7
        lea       ecx, DWORD PTR [12+esp]                       ;185.12
        neg       eax                                           ;183.7
        lea       esi, DWORD PTR [4+esp]                        ;185.12
        add       eax, edi                                      ;183.7
        lea       ebp, DWORD PTR [esp]                          ;185.12
        shl       eax, 5                                        ;183.7
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;183.7
        movsx     edx, WORD PTR [20+ebx+eax]                    ;183.13
        lea       ebx, DWORD PTR [8+esp]                        ;185.12
        mov       DWORD PTR [8+esp], edx                        ;183.7
        push      ecx                                           ;185.12
        push      OFFSET FLAT: __NLITPACK_4.0.5                 ;185.12
        push      ebx                                           ;185.12
        push      ebp                                           ;185.12
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_IPINIT    ;185.12
        push      esi                                           ;185.12
        push      DWORD PTR [84+esp]                            ;185.12
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;185.12
                                ; LOE ebx
.B5.10:                         ; Preds .B5.9
        mov       edi, DWORD PTR [88+esp]                       ;186.7
        lea       ebp, DWORD PTR [48+esp]                       ;187.12
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;186.7
        lea       esi, DWORD PTR [44+esp]                       ;187.12
        neg       edx                                           ;186.7
        add       edx, DWORD PTR [edi]                          ;186.7
        shl       edx, 6                                        ;186.7
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;186.7
        movsx     ecx, WORD PTR [12+eax+edx]                    ;186.13
        mov       DWORD PTR [36+esp], ecx                       ;186.7
        push      ebp                                           ;187.12
        push      esi                                           ;187.12
        push      ebx                                           ;187.12
        push      DWORD PTR [96+esp]                            ;187.12
        push      edi                                           ;187.12
        push      DWORD PTR [100+esp]                           ;187.12
        call      _RETRIEVE_NEXT_LINK                           ;187.12
                                ; LOE
.B5.11:                         ; Preds .B5.10
        add       esp, 84                                       ;189.1
        pop       ebp                                           ;189.1
        pop       ebx                                           ;189.1
        pop       edi                                           ;189.1
        pop       esi                                           ;189.1
        ret                                                     ;189.1
        ALIGN     16
                                ; LOE
; mark_end;
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DETOUR ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__NLITPACK_3.0.5	DD	1
__NLITPACK_4.0.5	DD	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DETOUR
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MULTI
; mark_begin;
       ALIGN     16
	PUBLIC _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MULTI
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MULTI	PROC NEAR 
; parameter 1: 52 + esp
; parameter 2: 56 + esp
; parameter 3: 60 + esp
; parameter 4: 64 + esp
.B6.1:                          ; Preds .B6.0
        push      esi                                           ;192.12
        push      edi                                           ;192.12
        push      ebx                                           ;192.12
        push      ebp                                           ;192.12
        sub       esp, 32                                       ;192.12
        push      OFFSET FLAT: __NLITPACK_5.0.6                 ;197.12
        call      _RANXY                                        ;197.12
                                ; LOE f1
.B6.18:                         ; Preds .B6.1
        fstp      DWORD PTR [4+esp]                             ;197.12
        movss     xmm0, DWORD PTR [4+esp]                       ;197.12
        add       esp, 4                                        ;197.12
                                ; LOE xmm0
.B6.2:                          ; Preds .B6.18
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;199.18
        mov       ebp, DWORD PTR [64+esp]                       ;192.12
        neg       ecx                                           ;199.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;199.7
        add       ecx, 3                                        ;199.7
        shl       eax, 2                                        ;199.7
        neg       eax                                           ;199.7
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;199.7
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;199.7
        mov       ebp, DWORD PTR [ebp]                          ;199.18
        mov       DWORD PTR [16+esp], 1                         ;
        lea       edx, DWORD PTR [eax+ebp*4]                    ;199.7
        mov       ecx, DWORD PTR [edx+ecx]                      ;199.7
        test      ecx, ecx                                      ;199.7
        jle       .B6.7         ; Prob 2%                       ;199.7
                                ; LOE ecx ebp xmm0
.B6.3:                          ; Preds .B6.2
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE+44] ;200.12
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE+40] ;200.12
        mov       ebx, edx                                      ;
        imul      esi, edx                                      ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE] ;200.9
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSRATE+32] ;200.9
        shl       edi, 2                                        ;
        neg       edi                                           ;
        lea       eax, DWORD PTR [eax+ebp*4]                    ;
        sub       eax, esi                                      ;
        mov       esi, DWORD PTR [16+esp]                       ;
        add       eax, edi                                      ;
                                ; LOE eax edx ecx ebx ebp esi xmm0
.B6.4:                          ; Preds .B6.5 .B6.3
        movss     xmm1, DWORD PTR [ebx+eax]                     ;200.12
        comiss    xmm1, xmm0                                    ;200.29
        jae       .B6.6         ; Prob 20%                      ;200.29
                                ; LOE eax edx ecx ebx ebp esi xmm0
.B6.5:                          ; Preds .B6.4
        inc       esi                                           ;201.7
        add       ebx, edx                                      ;201.7
        cmp       esi, ecx                                      ;201.7
        jle       .B6.4         ; Prob 82%                      ;201.7
                                ; LOE eax edx ecx ebx ebp esi xmm0
.B6.6:                          ; Preds .B6.4 .B6.5
        mov       DWORD PTR [16+esp], esi                       ;
                                ; LOE ebp
.B6.7:                          ; Preds .B6.2 .B6.6
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE] ;203.7
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+32] ;203.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+44] ;203.18
        mov       ebx, DWORD PTR [16+esp]                       ;203.7
        lea       esi, DWORD PTR [ecx+ebp*4]                    ;203.7
        sub       ebx, edx                                      ;203.7
        shl       edi, 2                                        ;203.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+40] ;203.18
        sub       esi, edi                                      ;203.7
        imul      ebx, eax                                      ;203.7
        mov       ebx, DWORD PTR [esi+ebx]                      ;203.7
        test      ebx, ebx                                      ;203.7
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+32] ;203.7
        jle       .B6.12        ; Prob 2%                       ;203.7
                                ; LOE eax edx ecx ebx ebp esi
.B6.8:                          ; Preds .B6.7
        mov       DWORD PTR [esp], ebx                          ;
        mov       esi, 1                                        ;
        mov       ebx, DWORD PTR [60+esp]                       ;
        mov       ebp, DWORD PTR [64+esp]                       ;
                                ; LOE ebx ebp esi
.B6.9:                          ; Preds .B6.10 .B6.8
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;204.24
        neg       edx                                           ;204.9
        add       edx, DWORD PTR [16+esp]                       ;204.9
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;204.9
        neg       edi                                           ;204.9
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;204.9
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;204.9
        add       edi, DWORD PTR [ebp]                          ;204.9
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;204.24
        neg       eax                                           ;204.9
        add       eax, esi                                      ;204.9
        lea       ecx, DWORD PTR [ecx+edi*8]                    ;204.9
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;204.9
        add       edx, ecx                                      ;204.9
        cvtsi2ss  xmm0, DWORD PTR [edx+eax]                     ;204.9
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;205.9
        neg       edx                                           ;205.9
        add       edx, DWORD PTR [ebx]                          ;205.9
        shl       edx, 6                                        ;205.9
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;205.9
        movss     DWORD PTR [24+esp], xmm0                      ;204.9
        movzx     edi, WORD PTR [12+eax+edx]                    ;205.15
        lea       edx, DWORD PTR [24+esp]                       ;206.11
        lea       eax, DWORD PTR [-1+esi+edi]                   ;205.9
        mov       WORD PTR [28+esp], ax                         ;205.9
        push      edx                                           ;206.11
        push      OFFSET FLAT: __NLITPACK_6.0.6                 ;206.11
        lea       ecx, DWORD PTR [36+esp]                       ;206.11
        push      ecx                                           ;206.11
        push      ebx                                           ;206.11
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;206.11
                                ; LOE ebx ebp esi
.B6.19:                         ; Preds .B6.9
        add       esp, 16                                       ;206.11
                                ; LOE ebx ebp esi
.B6.10:                         ; Preds .B6.19
        inc       esi                                           ;207.4
        cmp       esi, DWORD PTR [esp]                          ;207.4
        jle       .B6.9         ; Prob 82%                      ;207.4
                                ; LOE ebx ebp esi
.B6.11:                         ; Preds .B6.10
        mov       eax, DWORD PTR [64+esp]                       ;209.48
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE] ;209.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+44] ;209.48
        mov       ebp, DWORD PTR [eax]                          ;209.48
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+40] ;209.48
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSNNODE+32] ;209.13
                                ; LOE eax edx ecx ebp esi
.B6.12:                         ; Preds .B6.11 .B6.7
        neg       edx                                           ;209.7
        neg       esi                                           ;209.7
        mov       ebx, DWORD PTR [16+esp]                       ;209.7
        add       edx, ebx                                      ;209.7
        imul      edx, eax                                      ;209.7
        add       esi, ebp                                      ;209.7
        add       edx, ecx                                      ;209.7
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;209.7
        neg       eax                                           ;209.7
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;209.7
        mov       ecx, DWORD PTR [edx+esi*4]                    ;209.48
        mov       esi, DWORD PTR [60+esp]                       ;209.7
        add       eax, DWORD PTR [esi]                          ;209.7
        shl       eax, 6                                        ;209.7
        movsx     edx, WORD PTR [12+edi+eax]                    ;209.13
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+56] ;211.15
        neg       eax                                           ;211.4
        lea       edi, DWORD PTR [-1+ecx+edx]                   ;209.7
        mov       DWORD PTR [esp], edi                          ;209.7
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;212.4
        neg       edi                                           ;212.4
        add       edi, DWORD PTR [esi]                          ;212.4
        lea       edx, DWORD PTR [-1+ecx+eax]                   ;211.4
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+44] ;211.15
        neg       eax                                           ;211.4
        add       eax, ebx                                      ;211.4
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+32] ;211.4
        neg       ebx                                           ;211.4
        add       ebx, ebp                                      ;211.4
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+40] ;211.4
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH+52] ;211.4
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH] ;211.4
        shl       edi, 5                                        ;212.4
        lea       ecx, DWORD PTR [4+ebp+ebx*8]                  ;211.4
        add       eax, ecx                                      ;211.4
        lea       ecx, DWORD PTR [4+esp]                        ;214.12
        lea       ebx, DWORD PTR [8+esp]                        ;214.12
        mov       eax, DWORD PTR [eax+edx]                      ;211.4
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;212.4
        mov       DWORD PTR [4+esp], eax                        ;211.4
        lea       eax, DWORD PTR [12+esp]                       ;214.12
        movsx     ebp, WORD PTR [20+edx+edi]                    ;212.10
        lea       edx, DWORD PTR [esp]                          ;214.12
        mov       DWORD PTR [8+esp], ebp                        ;212.4
        push      eax                                           ;214.12
        push      OFFSET FLAT: __NLITPACK_7.0.6                 ;214.12
        push      ebx                                           ;214.12
        push      edx                                           ;214.12
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_IPINIT    ;214.12
        push      ecx                                           ;214.12
        push      esi                                           ;214.12
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;214.12
                                ; LOE ebx
.B6.13:                         ; Preds .B6.12
        mov       edi, DWORD PTR [88+esp]                       ;215.7
        lea       ebp, DWORD PTR [48+esp]                       ;216.12
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;215.7
        lea       esi, DWORD PTR [44+esp]                       ;216.12
        neg       edx                                           ;215.7
        add       edx, DWORD PTR [edi]                          ;215.7
        shl       edx, 6                                        ;215.7
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;215.7
        movsx     ecx, WORD PTR [12+eax+edx]                    ;215.13
        mov       DWORD PTR [36+esp], ecx                       ;215.7
        push      ebp                                           ;216.12
        push      esi                                           ;216.12
        push      ebx                                           ;216.12
        push      DWORD PTR [96+esp]                            ;216.12
        push      edi                                           ;216.12
        push      DWORD PTR [100+esp]                           ;216.12
        call      _RETRIEVE_NEXT_LINK                           ;216.12
                                ; LOE
.B6.14:                         ; Preds .B6.13
        add       esp, 84                                       ;218.1
        pop       ebp                                           ;218.1
        pop       ebx                                           ;218.1
        pop       edi                                           ;218.1
        pop       esi                                           ;218.1
        ret                                                     ;218.1
        ALIGN     16
                                ; LOE
; mark_end;
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MULTI ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__NLITPACK_5.0.6	DD	30
__NLITPACK_6.0.6	DD	1
__NLITPACK_7.0.6	DD	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MULTI
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_PATH
; mark_begin;
       ALIGN     16
	PUBLIC _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_PATH
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_PATH	PROC NEAR 
; parameter 1: 24 + esp
; parameter 2: 28 + esp
; parameter 3: 32 + esp
.B7.1:                          ; Preds .B7.0
        push      ebx                                           ;221.12
        sub       esp, 16                                       ;221.12
        mov       ebx, DWORD PTR [32+esp]                       ;221.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;223.9
        neg       eax                                           ;223.9
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;223.15
        neg       edx                                           ;223.9
        add       eax, DWORD PTR [ebx]                          ;223.9
        add       edx, 2                                        ;223.9
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;223.9
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;223.9
        mov       ecx, DWORD PTR [28+esp]                       ;221.12
        lea       eax, DWORD PTR [ebx+eax*4]                    ;223.9
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;224.9
        neg       ebx                                           ;224.9
        add       ebx, DWORD PTR [ecx]                          ;224.9
        shl       ebx, 6                                        ;224.9
        mov       edx, DWORD PTR [eax+edx]                      ;223.9
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;224.9
        mov       DWORD PTR [esp], edx                          ;223.9
        movsx     edx, WORD PTR [12+eax+ebx]                    ;224.15
        mov       DWORD PTR [4+esp], edx                        ;224.9
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;225.9
        neg       edx                                           ;225.9
        add       edx, DWORD PTR [ecx]                          ;225.9
        shl       edx, 5                                        ;225.9
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;225.9
        movsx     ebx, WORD PTR [20+eax+edx]                    ;225.15
        lea       eax, DWORD PTR [12+esp]                       ;227.14
        mov       DWORD PTR [8+esp], ebx                        ;225.9
        lea       ebx, DWORD PTR [4+esp]                        ;227.14
        lea       edx, DWORD PTR [8+esp]                        ;227.14
        push      eax                                           ;227.14
        push      OFFSET FLAT: __NLITPACK_8.0.7                 ;227.14
        push      edx                                           ;227.14
        push      ebx                                           ;227.14
        lea       eax, DWORD PTR [16+esp]                       ;227.14
        push      eax                                           ;227.14
        push      DWORD PTR [44+esp]                            ;227.14
        push      ecx                                           ;227.14
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;227.14
                                ; LOE ebp esi edi
.B7.2:                          ; Preds .B7.1
        add       esp, 44                                       ;228.1
        pop       ebx                                           ;228.1
        ret                                                     ;228.1
        ALIGN     16
                                ; LOE
; mark_end;
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_PATH ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__NLITPACK_8.0.7	DD	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_PATH
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_SPEED
; mark_begin;
       ALIGN     16
	PUBLIC _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_SPEED
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_SPEED	PROC NEAR 
; parameter 1: 20 + esp
.B8.1:                          ; Preds .B8.0
        push      esi                                           ;231.12
        push      edi                                           ;231.12
        push      ebx                                           ;231.12
        push      ebp                                           ;231.12
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;232.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;232.13
        mov       edi, ebx                                      ;232.7
        imul      edx, ebx                                      ;232.7
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;232.7
        lea       ebp, DWORD PTR [ebx+ebx]                      ;233.10
        sub       ecx, edx                                      ;232.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;232.7
        shl       edx, 2                                        ;232.7
        mov       eax, DWORD PTR [20+esp]                       ;231.12
        sub       edi, edx                                      ;232.7
        add       edi, ecx                                      ;232.7
        sub       ebp, edx                                      ;233.19
        add       ebp, ecx                                      ;233.19
        mov       eax, DWORD PTR [eax]                          ;232.13
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        imul      edi, DWORD PTR [edi+eax*4], 900               ;232.7
        mov       ebp, DWORD PTR [ebp+eax*4]                    ;233.10
        test      ebp, ebp                                      ;233.19
        cvtsi2ss  xmm0, ebp                                     ;234.48
        movss     xmm1, DWORD PTR [652+edi+esi]                 ;234.13
        jle       .B8.5         ; Prob 16%                      ;233.19
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B8.2:                          ; Preds .B8.1
        divss     xmm0, DWORD PTR [_2il0floatpacket.30]         ;234.56
        comiss    xmm0, xmm1                                    ;234.46
        jbe       .B8.4         ; Prob 50%                      ;234.46
                                ; LOE eax edx ecx ebx esi edi xmm1
.B8.3:                          ; Preds .B8.2
        lea       ebx, DWORD PTR [ebx+ebx*2]                    ;235.49
        sub       ebx, edx                                      ;235.12
        add       ecx, ebx                                      ;235.12
        cvtsi2ss  xmm0, DWORD PTR [ecx+eax*4]                   ;235.49
        divss     xmm0, DWORD PTR [_2il0floatpacket.32]         ;235.57
        addss     xmm0, DWORD PTR [_2il0floatpacket.33]         ;235.47
        mulss     xmm0, xmm1                                    ;235.12
        movss     DWORD PTR [652+edi+esi], xmm0                 ;235.12
                                ; LOE
.B8.4:                          ; Preds .B8.2 .B8.5 .B8.3
        pop       ebp                                           ;244.1
        pop       ebx                                           ;244.1
        pop       edi                                           ;244.1
        pop       esi                                           ;244.1
        ret                                                     ;244.1
                                ; LOE
.B8.5:                          ; Preds .B8.1                   ; Infreq
        divss     xmm0, DWORD PTR [_2il0floatpacket.31]         ;238.58
        comiss    xmm1, xmm0                                    ;238.48
        jbe       .B8.4         ; Prob 50%                      ;238.48
                                ; LOE eax edx ecx ebx esi edi xmm1
.B8.6:                          ; Preds .B8.5                   ; Infreq
        movss     xmm2, DWORD PTR [_2il0floatpacket.33]         ;240.6
        lea       ebx, DWORD PTR [ebx+ebx*2]                    ;239.17
        sub       ebx, edx                                      ;239.26
        mov       edx, 99                                       ;239.26
        add       ecx, ebx                                      ;239.26
        cmp       DWORD PTR [ecx+eax*4], 99                     ;239.26
        cmovl     edx, DWORD PTR [ecx+eax*4]                    ;239.26
        cvtsi2ss  xmm0, edx                                     ;240.43
        divss     xmm0, DWORD PTR [_2il0floatpacket.32]         ;240.51
        mov       DWORD PTR [ecx+eax*4], edx                    ;239.33
        subss     xmm2, xmm0                                    ;240.41
        mulss     xmm2, xmm1                                    ;240.6
        movss     DWORD PTR [652+edi+esi], xmm2                 ;240.6
        pop       ebp                                           ;240.6
        pop       ebx                                           ;240.6
        pop       edi                                           ;240.6
        pop       esi                                           ;240.6
        ret                                                     ;240.6
        ALIGN     16
                                ; LOE
; mark_end;
_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_SPEED ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_SPEED
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN
; mark_begin;
       ALIGN     16
	PUBLIC _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN
_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN	PROC NEAR 
.B9.1:                          ; Preds .B9.0
        push      ebx                                           ;248.12
        mov       ebx, esp                                      ;248.12
        and       esp, -16                                      ;248.12
        push      ebp                                           ;248.12
        push      ebp                                           ;248.12
        mov       ebp, DWORD PTR [4+ebx]                        ;248.12
        mov       DWORD PTR [4+esp], ebp                        ;248.12
        mov       ebp, esp                                      ;248.12
        sub       esp, 200                                      ;248.12
        push      0                                             ;255.7
        mov       DWORD PTR [-108+ebp], edi                     ;248.12
        mov       DWORD PTR [-112+ebp], esi                     ;248.12
        mov       DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$SEED.0.9], 12345 ;254.2
        call      _for_random_seed_put                          ;255.7
                                ; LOE
.B9.151:                        ; Preds .B9.1
        add       esp, 4                                        ;255.7
                                ; LOE
.B9.2:                          ; Preds .B9.151
        call      _for_random_seed_bit_size                     ;256.7
                                ; LOE eax
.B9.3:                          ; Preds .B9.2
        mov       esi, eax                                      ;256.7
        lea       edx, DWORD PTR [-104+ebp]                     ;257.2
        sar       esi, 4                                        ;256.7
        shr       esi, 27                                       ;256.7
        push      32                                            ;257.2
        add       esi, eax                                      ;256.7
        lea       eax, DWORD PTR [-200+ebp]                     ;257.2
        push      eax                                           ;257.2
        push      OFFSET FLAT: __STRLITPACK_100.0.9             ;257.2
        push      -2088435968                                   ;257.2
        push      6767                                          ;257.2
        push      edx                                           ;257.2
        sar       esi, 5                                        ;256.7
        mov       DWORD PTR [-104+ebp], 0                       ;257.2
        mov       DWORD PTR [-200+ebp], 41                      ;257.2
        mov       DWORD PTR [-196+ebp], OFFSET FLAT: __STRLITPACK_20 ;257.2
        call      _for_write_seq_lis                            ;257.2
                                ; LOE esi
.B9.153:                        ; Preds .B9.3
        add       esp, 24                                       ;257.2
                                ; LOE esi
.B9.4:                          ; Preds .B9.153
        mov       DWORD PTR [-72+ebp], esi                      ;257.2
        lea       eax, DWORD PTR [-72+ebp]                      ;257.2
        push      eax                                           ;257.2
        push      OFFSET FLAT: __STRLITPACK_101.0.9             ;257.2
        lea       edx, DWORD PTR [-104+ebp]                     ;257.2
        push      edx                                           ;257.2
        call      _for_write_seq_lis_xmit                       ;257.2
                                ; LOE esi
.B9.154:                        ; Preds .B9.4
        add       esp, 12                                       ;257.2
                                ; LOE esi
.B9.5:                          ; Preds .B9.154
        xor       edx, edx                                      ;258.7
        test      esi, esi                                      ;258.7
        mov       eax, esi                                      ;258.7
        cmovle    eax, edx                                      ;258.7
        shl       eax, 2                                        ;258.7
        mov       DWORD PTR [-184+ebp], esp                     ;258.7
        mov       DWORD PTR [-68+ebp], eax                      ;258.7
        call      __alloca_probe                                ;258.7
        and       esp, -16                                      ;258.7
        mov       eax, esp                                      ;258.7
                                ; LOE eax esi
.B9.155:                        ; Preds .B9.5
        mov       edi, eax                                      ;258.7
        test      esi, esi                                      ;258.7
        jle       .B9.8         ; Prob 50%                      ;258.7
                                ; LOE esi edi
.B9.6:                          ; Preds .B9.155
        cmp       esi, 24                                       ;258.7
        jle       .B9.68        ; Prob 0%                       ;258.7
                                ; LOE esi edi
.B9.7:                          ; Preds .B9.6
        lea       eax, DWORD PTR [esi*4]                        ;258.7
        push      eax                                           ;258.7
        push      OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;258.7
        push      edi                                           ;258.7
        call      __intel_fast_memcpy                           ;258.7
                                ; LOE esi edi
.B9.156:                        ; Preds .B9.7
        add       esp, 12                                       ;258.7
                                ; LOE esi edi
.B9.8:                          ; Preds .B9.74 .B9.155 .B9.72 .B9.156
        push      edi                                           ;258.7
        call      _for_random_seed_get                          ;258.7
                                ; LOE esi edi
.B9.157:                        ; Preds .B9.8
        add       esp, 4                                        ;258.7
                                ; LOE esi edi
.B9.9:                          ; Preds .B9.157
        test      esi, esi                                      ;258.23
        jle       .B9.12        ; Prob 50%                      ;258.23
                                ; LOE esi edi
.B9.10:                         ; Preds .B9.9
        cmp       esi, 24                                       ;258.23
        jle       .B9.77        ; Prob 0%                       ;258.23
                                ; LOE esi edi
.B9.11:                         ; Preds .B9.10
        lea       eax, DWORD PTR [esi*4]                        ;258.23
        push      eax                                           ;258.23
        push      edi                                           ;258.23
        push      OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;258.23
        call      __intel_fast_memcpy                           ;258.23
                                ; LOE esi
.B9.158:                        ; Preds .B9.11
        add       esp, 12                                       ;258.23
                                ; LOE esi
.B9.12:                         ; Preds .B9.83 .B9.9 .B9.81 .B9.158
        mov       eax, DWORD PTR [-184+ebp]                     ;258.7
        mov       esp, eax                                      ;258.7
                                ; LOE esi
.B9.160:                        ; Preds .B9.12
        push      32                                            ;259.2
        mov       DWORD PTR [-104+ebp], 0                       ;259.2
        lea       eax, DWORD PTR [-192+ebp]                     ;259.2
        push      eax                                           ;259.2
        push      OFFSET FLAT: __STRLITPACK_102.0.9             ;259.2
        push      -2088435968                                   ;259.2
        push      6767                                          ;259.2
        mov       DWORD PTR [-192+ebp], 22                      ;259.2
        lea       edx, DWORD PTR [-104+ebp]                     ;259.2
        push      edx                                           ;259.2
        mov       DWORD PTR [-188+ebp], OFFSET FLAT: __STRLITPACK_18 ;259.2
        call      _for_write_seq_lis                            ;259.2
                                ; LOE esi
.B9.159:                        ; Preds .B9.160
        add       esp, 24                                       ;259.2
                                ; LOE esi
.B9.13:                         ; Preds .B9.159
        mov       DWORD PTR [-184+ebp], 8                       ;259.2
        lea       eax, DWORD PTR [-184+ebp]                     ;259.2
        push      eax                                           ;259.2
        push      OFFSET FLAT: __STRLITPACK_103.0.9             ;259.2
        mov       DWORD PTR [-180+ebp], OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;259.2
        lea       edx, DWORD PTR [-104+ebp]                     ;259.2
        push      edx                                           ;259.2
        call      _for_write_seq_lis_xmit                       ;259.2
                                ; LOE esi
.B9.161:                        ; Preds .B9.13
        add       esp, 12                                       ;259.2
                                ; LOE esi
.B9.14:                         ; Preds .B9.161
        call      _for_random_number_single                     ;260.7
                                ; LOE esi f1
.B9.15:                         ; Preds .B9.14
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9] ;260.7
        call      _for_random_number_single                     ;260.7
                                ; LOE esi f1
.B9.16:                         ; Preds .B9.15
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9+4] ;260.7
        call      _for_random_number_single                     ;260.7
                                ; LOE esi f1
.B9.17:                         ; Preds .B9.16
        push      32                                            ;261.2
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9+8] ;260.7
        lea       eax, DWORD PTR [-176+ebp]                     ;261.2
        push      eax                                           ;261.2
        push      OFFSET FLAT: __STRLITPACK_104.0.9             ;261.2
        push      -2088435968                                   ;261.2
        push      6767                                          ;261.2
        mov       DWORD PTR [-104+ebp], 0                       ;261.2
        lea       edx, DWORD PTR [-104+ebp]                     ;261.2
        push      edx                                           ;261.2
        mov       DWORD PTR [-176+ebp], 18                      ;261.2
        mov       DWORD PTR [-172+ebp], OFFSET FLAT: __STRLITPACK_16 ;261.2
        call      _for_write_seq_lis                            ;261.2
                                ; LOE esi
.B9.165:                        ; Preds .B9.17
        add       esp, 24                                       ;261.2
                                ; LOE esi
.B9.18:                         ; Preds .B9.165
        mov       DWORD PTR [-168+ebp], 12                      ;261.2
        lea       eax, DWORD PTR [-168+ebp]                     ;261.2
        push      eax                                           ;261.2
        push      OFFSET FLAT: __STRLITPACK_105.0.9             ;261.2
        mov       DWORD PTR [-164+ebp], OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9 ;261.2
        lea       edx, DWORD PTR [-104+ebp]                     ;261.2
        push      edx                                           ;261.2
        call      _for_write_seq_lis_xmit                       ;261.2
                                ; LOE esi
.B9.166:                        ; Preds .B9.18
        add       esp, 12                                       ;261.2
                                ; LOE esi
.B9.19:                         ; Preds .B9.166
        mov       eax, DWORD PTR [-68+ebp]                      ;262.7
        mov       DWORD PTR [-152+ebp], esp                     ;262.7
        call      __alloca_probe                                ;262.7
        and       esp, -16                                      ;262.7
        mov       eax, esp                                      ;262.7
                                ; LOE eax esi
.B9.167:                        ; Preds .B9.19
        mov       edi, eax                                      ;262.7
        test      esi, esi                                      ;262.7
        jle       .B9.22        ; Prob 50%                      ;262.7
                                ; LOE esi edi
.B9.20:                         ; Preds .B9.167
        cmp       esi, 24                                       ;262.7
        jle       .B9.86        ; Prob 0%                       ;262.7
                                ; LOE esi edi
.B9.21:                         ; Preds .B9.20
        lea       eax, DWORD PTR [esi*4]                        ;262.7
        push      eax                                           ;262.7
        push      OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;262.7
        push      edi                                           ;262.7
        call      __intel_fast_memcpy                           ;262.7
                                ; LOE esi edi
.B9.168:                        ; Preds .B9.21
        add       esp, 12                                       ;262.7
                                ; LOE esi edi
.B9.22:                         ; Preds .B9.92 .B9.167 .B9.90 .B9.168
        push      edi                                           ;262.7
        call      _for_random_seed_get                          ;262.7
                                ; LOE esi edi
.B9.169:                        ; Preds .B9.22
        add       esp, 4                                        ;262.7
                                ; LOE esi edi
.B9.23:                         ; Preds .B9.169
        test      esi, esi                                      ;262.23
        jle       .B9.26        ; Prob 50%                      ;262.23
                                ; LOE esi edi
.B9.24:                         ; Preds .B9.23
        cmp       esi, 24                                       ;262.23
        jle       .B9.95        ; Prob 0%                       ;262.23
                                ; LOE esi edi
.B9.25:                         ; Preds .B9.24
        lea       eax, DWORD PTR [esi*4]                        ;262.23
        push      eax                                           ;262.23
        push      edi                                           ;262.23
        push      OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;262.23
        call      __intel_fast_memcpy                           ;262.23
                                ; LOE esi
.B9.170:                        ; Preds .B9.25
        add       esp, 12                                       ;262.23
                                ; LOE esi
.B9.26:                         ; Preds .B9.101 .B9.23 .B9.99 .B9.170
        mov       eax, DWORD PTR [-152+ebp]                     ;262.7
        mov       esp, eax                                      ;262.7
                                ; LOE esi
.B9.172:                        ; Preds .B9.26
        push      32                                            ;263.2
        mov       DWORD PTR [-104+ebp], 0                       ;263.2
        lea       eax, DWORD PTR [-160+ebp]                     ;263.2
        push      eax                                           ;263.2
        push      OFFSET FLAT: __STRLITPACK_106.0.9             ;263.2
        push      -2088435968                                   ;263.2
        push      6767                                          ;263.2
        mov       DWORD PTR [-160+ebp], 26                      ;263.2
        lea       edx, DWORD PTR [-104+ebp]                     ;263.2
        push      edx                                           ;263.2
        mov       DWORD PTR [-156+ebp], OFFSET FLAT: __STRLITPACK_14 ;263.2
        call      _for_write_seq_lis                            ;263.2
                                ; LOE esi
.B9.171:                        ; Preds .B9.172
        add       esp, 24                                       ;263.2
                                ; LOE esi
.B9.27:                         ; Preds .B9.171
        mov       DWORD PTR [-152+ebp], 8                       ;263.2
        lea       eax, DWORD PTR [-152+ebp]                     ;263.2
        push      eax                                           ;263.2
        push      OFFSET FLAT: __STRLITPACK_107.0.9             ;263.2
        mov       DWORD PTR [-148+ebp], OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;263.2
        lea       edx, DWORD PTR [-104+ebp]                     ;263.2
        push      edx                                           ;263.2
        call      _for_write_seq_lis_xmit                       ;263.2
                                ; LOE esi
.B9.173:                        ; Preds .B9.27
        add       esp, 12                                       ;263.2
                                ; LOE esi
.B9.28:                         ; Preds .B9.173
        mov       eax, DWORD PTR [-68+ebp]                      ;264.7
        mov       DWORD PTR [-144+ebp], esp                     ;264.7
        call      __alloca_probe                                ;264.7
        and       esp, -16                                      ;264.7
        mov       eax, esp                                      ;264.7
                                ; LOE eax esi
.B9.174:                        ; Preds .B9.28
        mov       edi, eax                                      ;264.7
        test      esi, esi                                      ;264.7
        jle       .B9.31        ; Prob 50%                      ;264.7
                                ; LOE esi edi
.B9.29:                         ; Preds .B9.174
        cmp       esi, 24                                       ;264.7
        jle       .B9.104       ; Prob 0%                       ;264.7
                                ; LOE esi edi
.B9.30:                         ; Preds .B9.29
        lea       eax, DWORD PTR [esi*4]                        ;264.7
        push      eax                                           ;264.7
        push      OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$SEED.0.9 ;264.7
        push      edi                                           ;264.7
        call      __intel_fast_memcpy                           ;264.7
                                ; LOE esi edi
.B9.175:                        ; Preds .B9.30
        add       esp, 12                                       ;264.7
                                ; LOE esi edi
.B9.31:                         ; Preds .B9.110 .B9.174 .B9.108 .B9.175
        push      edi                                           ;264.7
        call      _for_random_seed_put                          ;264.7
                                ; LOE esi
.B9.176:                        ; Preds .B9.31
        add       esp, 4                                        ;264.7
                                ; LOE esi
.B9.32:                         ; Preds .B9.176
        mov       eax, DWORD PTR [-144+ebp]                     ;264.7
        mov       esp, eax                                      ;264.7
                                ; LOE esi
.B9.178:                        ; Preds .B9.32
        mov       eax, DWORD PTR [-68+ebp]                      ;265.7
        mov       DWORD PTR [-136+ebp], esp                     ;265.7
        call      __alloca_probe                                ;265.7
        and       esp, -16                                      ;265.7
        mov       eax, esp                                      ;265.7
                                ; LOE eax esi
.B9.177:                        ; Preds .B9.178
        mov       edi, eax                                      ;265.7
        test      esi, esi                                      ;265.7
        jle       .B9.35        ; Prob 50%                      ;265.7
                                ; LOE esi edi
.B9.33:                         ; Preds .B9.177
        cmp       esi, 24                                       ;265.7
        jle       .B9.113       ; Prob 0%                       ;265.7
                                ; LOE esi edi
.B9.34:                         ; Preds .B9.33
        lea       eax, DWORD PTR [esi*4]                        ;265.7
        push      eax                                           ;265.7
        push      OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;265.7
        push      edi                                           ;265.7
        call      __intel_fast_memcpy                           ;265.7
                                ; LOE esi edi
.B9.179:                        ; Preds .B9.34
        add       esp, 12                                       ;265.7
                                ; LOE esi edi
.B9.35:                         ; Preds .B9.119 .B9.177 .B9.117 .B9.179
        push      edi                                           ;265.7
        call      _for_random_seed_get                          ;265.7
                                ; LOE esi edi
.B9.180:                        ; Preds .B9.35
        add       esp, 4                                        ;265.7
                                ; LOE esi edi
.B9.36:                         ; Preds .B9.180
        test      esi, esi                                      ;265.23
        jle       .B9.39        ; Prob 50%                      ;265.23
                                ; LOE esi edi
.B9.37:                         ; Preds .B9.36
        cmp       esi, 24                                       ;265.23
        jle       .B9.122       ; Prob 0%                       ;265.23
                                ; LOE esi edi
.B9.38:                         ; Preds .B9.37
        lea       eax, DWORD PTR [esi*4]                        ;265.23
        push      eax                                           ;265.23
        push      edi                                           ;265.23
        push      OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;265.23
        call      __intel_fast_memcpy                           ;265.23
                                ; LOE esi
.B9.181:                        ; Preds .B9.38
        add       esp, 12                                       ;265.23
                                ; LOE esi
.B9.39:                         ; Preds .B9.128 .B9.36 .B9.126 .B9.181
        mov       eax, DWORD PTR [-136+ebp]                     ;265.7
        mov       esp, eax                                      ;265.7
                                ; LOE esi
.B9.183:                        ; Preds .B9.39
        push      32                                            ;266.2
        mov       DWORD PTR [-104+ebp], 0                       ;266.2
        lea       eax, DWORD PTR [-144+ebp]                     ;266.2
        push      eax                                           ;266.2
        push      OFFSET FLAT: __STRLITPACK_108.0.9             ;266.2
        push      -2088435968                                   ;266.2
        push      6767                                          ;266.2
        mov       DWORD PTR [-144+ebp], 22                      ;266.2
        lea       edx, DWORD PTR [-104+ebp]                     ;266.2
        push      edx                                           ;266.2
        mov       DWORD PTR [-140+ebp], OFFSET FLAT: __STRLITPACK_12 ;266.2
        call      _for_write_seq_lis                            ;266.2
                                ; LOE esi
.B9.182:                        ; Preds .B9.183
        add       esp, 24                                       ;266.2
                                ; LOE esi
.B9.40:                         ; Preds .B9.182
        mov       DWORD PTR [-136+ebp], 8                       ;266.2
        lea       eax, DWORD PTR [-136+ebp]                     ;266.2
        push      eax                                           ;266.2
        push      OFFSET FLAT: __STRLITPACK_109.0.9             ;266.2
        mov       DWORD PTR [-132+ebp], OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;266.2
        lea       edx, DWORD PTR [-104+ebp]                     ;266.2
        push      edx                                           ;266.2
        call      _for_write_seq_lis_xmit                       ;266.2
                                ; LOE esi
.B9.184:                        ; Preds .B9.40
        add       esp, 12                                       ;266.2
                                ; LOE esi
.B9.41:                         ; Preds .B9.184
        call      _for_random_number_single                     ;267.7
                                ; LOE esi f1
.B9.42:                         ; Preds .B9.41
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9] ;267.7
        call      _for_random_number_single                     ;267.7
                                ; LOE esi f1
.B9.43:                         ; Preds .B9.42
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9+4] ;267.7
        call      _for_random_number_single                     ;267.7
                                ; LOE esi f1
.B9.44:                         ; Preds .B9.43
        push      32                                            ;268.2
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9+8] ;267.7
        lea       eax, DWORD PTR [-128+ebp]                     ;268.2
        push      eax                                           ;268.2
        push      OFFSET FLAT: __STRLITPACK_110.0.9             ;268.2
        push      -2088435968                                   ;268.2
        push      6767                                          ;268.2
        mov       DWORD PTR [-104+ebp], 0                       ;268.2
        lea       edx, DWORD PTR [-104+ebp]                     ;268.2
        push      edx                                           ;268.2
        mov       DWORD PTR [-128+ebp], 18                      ;268.2
        mov       DWORD PTR [-124+ebp], OFFSET FLAT: __STRLITPACK_10 ;268.2
        call      _for_write_seq_lis                            ;268.2
                                ; LOE esi
.B9.188:                        ; Preds .B9.44
        add       esp, 24                                       ;268.2
                                ; LOE esi
.B9.45:                         ; Preds .B9.188
        mov       DWORD PTR [-120+ebp], 12                      ;268.2
        lea       eax, DWORD PTR [-120+ebp]                     ;268.2
        push      eax                                           ;268.2
        push      OFFSET FLAT: __STRLITPACK_111.0.9             ;268.2
        mov       DWORD PTR [-116+ebp], OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9 ;268.2
        lea       edx, DWORD PTR [-104+ebp]                     ;268.2
        push      edx                                           ;268.2
        call      _for_write_seq_lis_xmit                       ;268.2
                                ; LOE esi
.B9.189:                        ; Preds .B9.45
        add       esp, 12                                       ;268.2
                                ; LOE esi
.B9.46:                         ; Preds .B9.189
        mov       ecx, esi                                      ;270.8
        mov       edi, 1                                        ;269.2
        and       ecx, -4                                       ;270.8
        lea       edx, DWORD PTR [esi*4]                        ;
        mov       DWORD PTR [-12+ebp], edx                      ;
        mov       DWORD PTR [-4+ebp], ecx                       ;
        mov       DWORD PTR [-8+ebp], edi                       ;
                                ; LOE esi
.B9.47:                         ; Preds .B9.66 .B9.46
        mov       eax, DWORD PTR [-68+ebp]                      ;270.8
        mov       DWORD PTR [-16+ebp], esp                      ;270.8
        call      __alloca_probe                                ;270.8
        and       esp, -16                                      ;270.8
        mov       eax, esp                                      ;270.8
                                ; LOE eax esi
.B9.190:                        ; Preds .B9.47
        mov       edi, eax                                      ;270.8
        test      esi, esi                                      ;270.8
        jle       .B9.50        ; Prob 50%                      ;270.8
                                ; LOE esi edi
.B9.48:                         ; Preds .B9.190
        cmp       esi, 24                                       ;270.8
        jle       .B9.131       ; Prob 0%                       ;270.8
                                ; LOE esi edi
.B9.49:                         ; Preds .B9.48
        push      DWORD PTR [-12+ebp]                           ;270.8
        push      OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;270.8
        push      edi                                           ;270.8
        call      __intel_fast_memcpy                           ;270.8
                                ; LOE esi edi
.B9.191:                        ; Preds .B9.49
        add       esp, 12                                       ;270.8
                                ; LOE esi edi
.B9.50:                         ; Preds .B9.137 .B9.190 .B9.191 .B9.135
        push      edi                                           ;270.8
        call      _for_random_seed_get                          ;270.8
                                ; LOE esi edi
.B9.192:                        ; Preds .B9.50
        add       esp, 4                                        ;270.8
                                ; LOE esi edi
.B9.51:                         ; Preds .B9.192
        test      esi, esi                                      ;270.24
        jle       .B9.54        ; Prob 50%                      ;270.24
                                ; LOE esi edi
.B9.52:                         ; Preds .B9.51
        cmp       esi, 24                                       ;270.24
        jle       .B9.140       ; Prob 0%                       ;270.24
                                ; LOE esi edi
.B9.53:                         ; Preds .B9.52
        push      DWORD PTR [-12+ebp]                           ;270.24
        push      edi                                           ;270.24
        push      OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;270.24
        call      __intel_fast_memcpy                           ;270.24
                                ; LOE esi
.B9.193:                        ; Preds .B9.53
        add       esp, 12                                       ;270.24
                                ; LOE esi
.B9.54:                         ; Preds .B9.146 .B9.193 .B9.51 .B9.144
        mov       eax, DWORD PTR [-16+ebp]                      ;270.8
        mov       esp, eax                                      ;270.8
                                ; LOE esi
.B9.195:                        ; Preds .B9.54
        push      32                                            ;271.3
        mov       DWORD PTR [-104+ebp], 0                       ;271.3
        lea       eax, DWORD PTR [-64+ebp]                      ;271.3
        push      eax                                           ;271.3
        push      OFFSET FLAT: __STRLITPACK_112.0.9             ;271.3
        push      -2088435968                                   ;271.3
        push      6767                                          ;271.3
        mov       DWORD PTR [-64+ebp], 26                       ;271.3
        lea       edx, DWORD PTR [-104+ebp]                     ;271.3
        push      edx                                           ;271.3
        mov       DWORD PTR [-60+ebp], OFFSET FLAT: __STRLITPACK_8 ;271.3
        call      _for_write_seq_lis                            ;271.3
                                ; LOE esi
.B9.194:                        ; Preds .B9.195
        add       esp, 24                                       ;271.3
                                ; LOE esi
.B9.55:                         ; Preds .B9.194
        mov       DWORD PTR [-56+ebp], 8                        ;271.3
        lea       eax, DWORD PTR [-56+ebp]                      ;271.3
        push      eax                                           ;271.3
        push      OFFSET FLAT: __STRLITPACK_113.0.9             ;271.3
        mov       DWORD PTR [-52+ebp], OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9 ;271.3
        lea       edx, DWORD PTR [-104+ebp]                     ;271.3
        push      edx                                           ;271.3
        call      _for_write_seq_lis_xmit                       ;271.3
                                ; LOE esi
.B9.196:                        ; Preds .B9.55
        add       esp, 12                                       ;271.3
                                ; LOE esi
.B9.56:                         ; Preds .B9.196
        call      _for_random_number_single                     ;272.8
                                ; LOE esi f1
.B9.57:                         ; Preds .B9.56
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9] ;272.8
        call      _for_random_number_single                     ;272.8
                                ; LOE esi f1
.B9.58:                         ; Preds .B9.57
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9+4] ;272.8
        call      _for_random_number_single                     ;272.8
                                ; LOE esi f1
.B9.59:                         ; Preds .B9.58
        push      32                                            ;273.3
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9+8] ;272.8
        lea       eax, DWORD PTR [-48+ebp]                      ;273.3
        push      eax                                           ;273.3
        push      OFFSET FLAT: __STRLITPACK_114.0.9             ;273.3
        push      -2088435968                                   ;273.3
        push      6767                                          ;273.3
        mov       DWORD PTR [-104+ebp], 0                       ;273.3
        lea       edx, DWORD PTR [-104+ebp]                     ;273.3
        push      edx                                           ;273.3
        mov       DWORD PTR [-48+ebp], 18                       ;273.3
        mov       DWORD PTR [-44+ebp], OFFSET FLAT: __STRLITPACK_6 ;273.3
        call      _for_write_seq_lis                            ;273.3
                                ; LOE esi
.B9.200:                        ; Preds .B9.59
        add       esp, 24                                       ;273.3
                                ; LOE esi
.B9.60:                         ; Preds .B9.200
        mov       DWORD PTR [-40+ebp], 12                       ;273.3
        lea       eax, DWORD PTR [-40+ebp]                      ;273.3
        push      eax                                           ;273.3
        push      OFFSET FLAT: __STRLITPACK_115.0.9             ;273.3
        mov       DWORD PTR [-36+ebp], OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9 ;273.3
        lea       edx, DWORD PTR [-104+ebp]                     ;273.3
        push      edx                                           ;273.3
        call      _for_write_seq_lis_xmit                       ;273.3
                                ; LOE esi
.B9.201:                        ; Preds .B9.60
        add       esp, 12                                       ;273.3
                                ; LOE esi
.B9.61:                         ; Preds .B9.201
        call      _for_random_number_single                     ;274.8
                                ; LOE esi f1
.B9.62:                         ; Preds .B9.61
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9] ;274.8
        call      _for_random_number_single                     ;274.8
                                ; LOE esi f1
.B9.63:                         ; Preds .B9.62
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9+4] ;274.8
        call      _for_random_number_single                     ;274.8
                                ; LOE esi f1
.B9.64:                         ; Preds .B9.63
        push      32                                            ;275.3
        fstp      DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9+8] ;274.8
        lea       eax, DWORD PTR [-32+ebp]                      ;275.3
        push      eax                                           ;275.3
        push      OFFSET FLAT: __STRLITPACK_116.0.9             ;275.3
        push      -2088435968                                   ;275.3
        push      6767                                          ;275.3
        mov       DWORD PTR [-104+ebp], 0                       ;275.3
        lea       edx, DWORD PTR [-104+ebp]                     ;275.3
        push      edx                                           ;275.3
        mov       DWORD PTR [-32+ebp], 18                       ;275.3
        mov       DWORD PTR [-28+ebp], OFFSET FLAT: __STRLITPACK_4 ;275.3
        call      _for_write_seq_lis                            ;275.3
                                ; LOE esi
.B9.205:                        ; Preds .B9.64
        add       esp, 24                                       ;275.3
                                ; LOE esi
.B9.65:                         ; Preds .B9.205
        mov       DWORD PTR [-24+ebp], 12                       ;275.3
        lea       eax, DWORD PTR [-24+ebp]                      ;275.3
        push      eax                                           ;275.3
        push      OFFSET FLAT: __STRLITPACK_117.0.9             ;275.3
        mov       DWORD PTR [-20+ebp], OFFSET FLAT: _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9 ;275.3
        lea       edx, DWORD PTR [-104+ebp]                     ;275.3
        push      edx                                           ;275.3
        call      _for_write_seq_lis_xmit                       ;275.3
                                ; LOE esi
.B9.206:                        ; Preds .B9.65
        add       esp, 12                                       ;275.3
                                ; LOE esi
.B9.66:                         ; Preds .B9.206
        mov       eax, DWORD PTR [-8+ebp]                       ;276.2
        inc       eax                                           ;276.2
        mov       DWORD PTR [-8+ebp], eax                       ;276.2
        cmp       eax, 3                                        ;276.2
        jle       .B9.47        ; Prob 82%                      ;276.2
                                ; LOE esi
.B9.67:                         ; Preds .B9.66
        mov       esi, DWORD PTR [-112+ebp]                     ;277.1
        mov       edi, DWORD PTR [-108+ebp]                     ;277.1
        mov       esp, ebp                                      ;277.1
        pop       ebp                                           ;277.1
        mov       esp, ebx                                      ;277.1
        pop       ebx                                           ;277.1
        ret                                                     ;277.1
                                ; LOE
.B9.68:                         ; Preds .B9.6                   ; Infreq
        cmp       esi, 4                                        ;258.7
        jl        .B9.76        ; Prob 10%                      ;258.7
                                ; LOE esi edi
.B9.69:                         ; Preds .B9.68                  ; Infreq
        mov       eax, esi                                      ;258.7
        xor       edx, edx                                      ;258.7
        and       eax, -4                                       ;258.7
                                ; LOE eax edx esi edi
.B9.70:                         ; Preds .B9.70 .B9.69           ; Infreq
        movdqa    xmm0, XMMWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+edx*4] ;258.23
        movdqu    XMMWORD PTR [edi+edx*4], xmm0                 ;258.23
        add       edx, 4                                        ;258.7
        cmp       edx, eax                                      ;258.7
        jb        .B9.70        ; Prob 50%                      ;258.7
                                ; LOE eax edx esi edi
.B9.72:                         ; Preds .B9.70 .B9.76           ; Infreq
        cmp       eax, esi                                      ;258.7
        jae       .B9.8         ; Prob 10%                      ;258.7
                                ; LOE eax esi edi
.B9.74:                         ; Preds .B9.72 .B9.74           ; Infreq
        mov       edx, DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+eax*4] ;258.23
        mov       DWORD PTR [edi+eax*4], edx                    ;258.23
        inc       eax                                           ;258.7
        cmp       eax, esi                                      ;258.7
        jb        .B9.74        ; Prob 50%                      ;258.7
        jmp       .B9.8         ; Prob 100%                     ;258.7
                                ; LOE eax esi edi
.B9.76:                         ; Preds .B9.68                  ; Infreq
        xor       eax, eax                                      ;258.7
        jmp       .B9.72        ; Prob 100%                     ;258.7
                                ; LOE eax esi edi
.B9.77:                         ; Preds .B9.10                  ; Infreq
        cmp       esi, 4                                        ;258.23
        jl        .B9.85        ; Prob 10%                      ;258.23
                                ; LOE esi edi
.B9.78:                         ; Preds .B9.77                  ; Infreq
        mov       eax, esi                                      ;258.23
        xor       edx, edx                                      ;258.23
        and       eax, -4                                       ;258.23
                                ; LOE eax edx esi edi
.B9.79:                         ; Preds .B9.79 .B9.78           ; Infreq
        movdqu    xmm0, XMMWORD PTR [edi+edx*4]                 ;258.23
        movdqa    XMMWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+edx*4], xmm0 ;258.23
        add       edx, 4                                        ;258.23
        cmp       edx, eax                                      ;258.23
        jb        .B9.79        ; Prob 50%                      ;258.23
                                ; LOE eax edx esi edi
.B9.81:                         ; Preds .B9.79 .B9.85           ; Infreq
        cmp       eax, esi                                      ;258.23
        jae       .B9.12        ; Prob 10%                      ;258.23
                                ; LOE eax esi edi
.B9.83:                         ; Preds .B9.81 .B9.83           ; Infreq
        mov       edx, DWORD PTR [edi+eax*4]                    ;258.23
        mov       DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+eax*4], edx ;258.23
        inc       eax                                           ;258.23
        cmp       eax, esi                                      ;258.23
        jb        .B9.83        ; Prob 50%                      ;258.23
        jmp       .B9.12        ; Prob 100%                     ;258.23
                                ; LOE eax esi edi
.B9.85:                         ; Preds .B9.77                  ; Infreq
        xor       eax, eax                                      ;258.23
        jmp       .B9.81        ; Prob 100%                     ;258.23
                                ; LOE eax esi edi
.B9.86:                         ; Preds .B9.20                  ; Infreq
        cmp       esi, 4                                        ;262.7
        jl        .B9.94        ; Prob 10%                      ;262.7
                                ; LOE esi edi
.B9.87:                         ; Preds .B9.86                  ; Infreq
        mov       eax, esi                                      ;262.7
        xor       edx, edx                                      ;262.7
        and       eax, -4                                       ;262.7
                                ; LOE eax edx esi edi
.B9.88:                         ; Preds .B9.88 .B9.87           ; Infreq
        movdqa    xmm0, XMMWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+edx*4] ;262.23
        movdqu    XMMWORD PTR [edi+edx*4], xmm0                 ;262.23
        add       edx, 4                                        ;262.7
        cmp       edx, eax                                      ;262.7
        jb        .B9.88        ; Prob 50%                      ;262.7
                                ; LOE eax edx esi edi
.B9.90:                         ; Preds .B9.88 .B9.94           ; Infreq
        cmp       eax, esi                                      ;262.7
        jae       .B9.22        ; Prob 10%                      ;262.7
                                ; LOE eax esi edi
.B9.92:                         ; Preds .B9.90 .B9.92           ; Infreq
        mov       edx, DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+eax*4] ;262.23
        mov       DWORD PTR [edi+eax*4], edx                    ;262.23
        inc       eax                                           ;262.7
        cmp       eax, esi                                      ;262.7
        jb        .B9.92        ; Prob 50%                      ;262.7
        jmp       .B9.22        ; Prob 100%                     ;262.7
                                ; LOE eax esi edi
.B9.94:                         ; Preds .B9.86                  ; Infreq
        xor       eax, eax                                      ;262.7
        jmp       .B9.90        ; Prob 100%                     ;262.7
                                ; LOE eax esi edi
.B9.95:                         ; Preds .B9.24                  ; Infreq
        cmp       esi, 4                                        ;262.23
        jl        .B9.103       ; Prob 10%                      ;262.23
                                ; LOE esi edi
.B9.96:                         ; Preds .B9.95                  ; Infreq
        mov       eax, esi                                      ;262.23
        xor       edx, edx                                      ;262.23
        and       eax, -4                                       ;262.23
                                ; LOE eax edx esi edi
.B9.97:                         ; Preds .B9.97 .B9.96           ; Infreq
        movdqu    xmm0, XMMWORD PTR [edi+edx*4]                 ;262.23
        movdqa    XMMWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+edx*4], xmm0 ;262.23
        add       edx, 4                                        ;262.23
        cmp       edx, eax                                      ;262.23
        jb        .B9.97        ; Prob 50%                      ;262.23
                                ; LOE eax edx esi edi
.B9.99:                         ; Preds .B9.97 .B9.103          ; Infreq
        cmp       eax, esi                                      ;262.23
        jae       .B9.26        ; Prob 10%                      ;262.23
                                ; LOE eax esi edi
.B9.101:                        ; Preds .B9.99 .B9.101          ; Infreq
        mov       edx, DWORD PTR [edi+eax*4]                    ;262.23
        mov       DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+eax*4], edx ;262.23
        inc       eax                                           ;262.23
        cmp       eax, esi                                      ;262.23
        jb        .B9.101       ; Prob 50%                      ;262.23
        jmp       .B9.26        ; Prob 100%                     ;262.23
                                ; LOE eax esi edi
.B9.103:                        ; Preds .B9.95                  ; Infreq
        xor       eax, eax                                      ;262.23
        jmp       .B9.99        ; Prob 100%                     ;262.23
                                ; LOE eax esi edi
.B9.104:                        ; Preds .B9.29                  ; Infreq
        cmp       esi, 4                                        ;264.7
        jl        .B9.112       ; Prob 10%                      ;264.7
                                ; LOE esi edi
.B9.105:                        ; Preds .B9.104                 ; Infreq
        mov       eax, esi                                      ;264.7
        xor       edx, edx                                      ;264.7
        and       eax, -4                                       ;264.7
                                ; LOE eax edx esi edi
.B9.106:                        ; Preds .B9.106 .B9.105         ; Infreq
        movdqa    xmm0, XMMWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$SEED.0.9+edx*4] ;264.23
        movdqu    XMMWORD PTR [edi+edx*4], xmm0                 ;264.23
        add       edx, 4                                        ;264.7
        cmp       edx, eax                                      ;264.7
        jb        .B9.106       ; Prob 50%                      ;264.7
                                ; LOE eax edx esi edi
.B9.108:                        ; Preds .B9.106 .B9.112         ; Infreq
        cmp       eax, esi                                      ;264.7
        jae       .B9.31        ; Prob 10%                      ;264.7
                                ; LOE eax esi edi
.B9.110:                        ; Preds .B9.108 .B9.110         ; Infreq
        mov       edx, DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$SEED.0.9+eax*4] ;264.23
        mov       DWORD PTR [edi+eax*4], edx                    ;264.23
        inc       eax                                           ;264.7
        cmp       eax, esi                                      ;264.7
        jb        .B9.110       ; Prob 50%                      ;264.7
        jmp       .B9.31        ; Prob 100%                     ;264.7
                                ; LOE eax esi edi
.B9.112:                        ; Preds .B9.104                 ; Infreq
        xor       eax, eax                                      ;264.7
        jmp       .B9.108       ; Prob 100%                     ;264.7
                                ; LOE eax esi edi
.B9.113:                        ; Preds .B9.33                  ; Infreq
        cmp       esi, 4                                        ;265.7
        jl        .B9.121       ; Prob 10%                      ;265.7
                                ; LOE esi edi
.B9.114:                        ; Preds .B9.113                 ; Infreq
        mov       eax, esi                                      ;265.7
        xor       edx, edx                                      ;265.7
        and       eax, -4                                       ;265.7
                                ; LOE eax edx esi edi
.B9.115:                        ; Preds .B9.115 .B9.114         ; Infreq
        movdqa    xmm0, XMMWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+edx*4] ;265.23
        movdqu    XMMWORD PTR [edi+edx*4], xmm0                 ;265.23
        add       edx, 4                                        ;265.7
        cmp       edx, eax                                      ;265.7
        jb        .B9.115       ; Prob 50%                      ;265.7
                                ; LOE eax edx esi edi
.B9.117:                        ; Preds .B9.115 .B9.121         ; Infreq
        cmp       eax, esi                                      ;265.7
        jae       .B9.35        ; Prob 10%                      ;265.7
                                ; LOE eax esi edi
.B9.119:                        ; Preds .B9.117 .B9.119         ; Infreq
        mov       edx, DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+eax*4] ;265.23
        mov       DWORD PTR [edi+eax*4], edx                    ;265.23
        inc       eax                                           ;265.7
        cmp       eax, esi                                      ;265.7
        jb        .B9.119       ; Prob 50%                      ;265.7
        jmp       .B9.35        ; Prob 100%                     ;265.7
                                ; LOE eax esi edi
.B9.121:                        ; Preds .B9.113                 ; Infreq
        xor       eax, eax                                      ;265.7
        jmp       .B9.117       ; Prob 100%                     ;265.7
                                ; LOE eax esi edi
.B9.122:                        ; Preds .B9.37                  ; Infreq
        cmp       esi, 4                                        ;265.23
        jl        .B9.130       ; Prob 10%                      ;265.23
                                ; LOE esi edi
.B9.123:                        ; Preds .B9.122                 ; Infreq
        mov       eax, esi                                      ;265.23
        xor       edx, edx                                      ;265.23
        and       eax, -4                                       ;265.23
                                ; LOE eax edx esi edi
.B9.124:                        ; Preds .B9.124 .B9.123         ; Infreq
        movdqu    xmm0, XMMWORD PTR [edi+edx*4]                 ;265.23
        movdqa    XMMWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+edx*4], xmm0 ;265.23
        add       edx, 4                                        ;265.23
        cmp       edx, eax                                      ;265.23
        jb        .B9.124       ; Prob 50%                      ;265.23
                                ; LOE eax edx esi edi
.B9.126:                        ; Preds .B9.124 .B9.130         ; Infreq
        cmp       eax, esi                                      ;265.23
        jae       .B9.39        ; Prob 10%                      ;265.23
                                ; LOE eax esi edi
.B9.128:                        ; Preds .B9.126 .B9.128         ; Infreq
        mov       edx, DWORD PTR [edi+eax*4]                    ;265.23
        mov       DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+eax*4], edx ;265.23
        inc       eax                                           ;265.23
        cmp       eax, esi                                      ;265.23
        jb        .B9.128       ; Prob 50%                      ;265.23
        jmp       .B9.39        ; Prob 100%                     ;265.23
                                ; LOE eax esi edi
.B9.130:                        ; Preds .B9.122                 ; Infreq
        xor       eax, eax                                      ;265.23
        jmp       .B9.126       ; Prob 100%                     ;265.23
                                ; LOE eax esi edi
.B9.131:                        ; Preds .B9.48                  ; Infreq
        cmp       esi, 4                                        ;270.8
        jl        .B9.139       ; Prob 10%                      ;270.8
                                ; LOE esi edi
.B9.132:                        ; Preds .B9.131                 ; Infreq
        mov       eax, DWORD PTR [-4+ebp]                       ;270.8
        xor       edx, edx                                      ;270.8
        mov       ecx, eax                                      ;270.8
                                ; LOE eax edx ecx esi edi
.B9.133:                        ; Preds .B9.133 .B9.132         ; Infreq
        movdqa    xmm0, XMMWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+edx*4] ;270.24
        movdqu    XMMWORD PTR [edi+edx*4], xmm0                 ;270.24
        add       edx, 4                                        ;270.8
        cmp       edx, ecx                                      ;270.8
        jb        .B9.133       ; Prob 50%                      ;270.8
                                ; LOE eax edx ecx esi edi
.B9.135:                        ; Preds .B9.133 .B9.139         ; Infreq
        cmp       eax, esi                                      ;270.8
        jae       .B9.50        ; Prob 10%                      ;270.8
                                ; LOE eax esi edi
.B9.137:                        ; Preds .B9.135 .B9.137         ; Infreq
        mov       edx, DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+eax*4] ;270.24
        mov       DWORD PTR [edi+eax*4], edx                    ;270.24
        inc       eax                                           ;270.8
        cmp       eax, esi                                      ;270.8
        jb        .B9.137       ; Prob 50%                      ;270.8
        jmp       .B9.50        ; Prob 100%                     ;270.8
                                ; LOE eax esi edi
.B9.139:                        ; Preds .B9.131                 ; Infreq
        xor       eax, eax                                      ;270.8
        jmp       .B9.135       ; Prob 100%                     ;270.8
                                ; LOE eax esi edi
.B9.140:                        ; Preds .B9.52                  ; Infreq
        cmp       esi, 4                                        ;270.24
        jl        .B9.148       ; Prob 10%                      ;270.24
                                ; LOE esi edi
.B9.141:                        ; Preds .B9.140                 ; Infreq
        mov       eax, DWORD PTR [-4+ebp]                       ;270.24
        xor       edx, edx                                      ;270.24
        mov       ecx, eax                                      ;270.24
                                ; LOE eax edx ecx esi edi
.B9.142:                        ; Preds .B9.142 .B9.141         ; Infreq
        movdqu    xmm0, XMMWORD PTR [edi+edx*4]                 ;270.24
        movdqa    XMMWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+edx*4], xmm0 ;270.24
        add       edx, 4                                        ;270.24
        cmp       edx, ecx                                      ;270.24
        jb        .B9.142       ; Prob 50%                      ;270.24
                                ; LOE eax edx ecx esi edi
.B9.144:                        ; Preds .B9.142 .B9.148         ; Infreq
        cmp       eax, esi                                      ;270.24
        jae       .B9.54        ; Prob 10%                      ;270.24
                                ; LOE eax esi edi
.B9.146:                        ; Preds .B9.144 .B9.146         ; Infreq
        mov       edx, DWORD PTR [edi+eax*4]                    ;270.24
        mov       DWORD PTR [_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9+eax*4], edx ;270.24
        inc       eax                                           ;270.24
        cmp       eax, esi                                      ;270.24
        jb        .B9.146       ; Prob 50%                      ;270.24
        jmp       .B9.54        ; Prob 100%                     ;270.24
                                ; LOE eax esi edi
.B9.148:                        ; Preds .B9.140                 ; Infreq
        xor       eax, eax                                      ;270.24
        jmp       .B9.144       ; Prob 100%                     ;270.24
        ALIGN     16
                                ; LOE eax esi edi
; mark_end;
_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$SEED.0.9	DD 2 DUP (0H)	; pad
	DD 2 DUP (0H)	; pad
_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$OLD.0.9	DD 2 DUP (0H)	; pad
	DD 2 DUP (0H)	; pad
_VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN$HARVEST.0.9	DD 3 DUP (0H)	; pad
_BSS	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__STRLITPACK_100.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_101.0.9	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_102.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_103.0.9	DB	9
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_104.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_105.0.9	DB	26
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_106.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_107.0.9	DB	9
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_108.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_109.0.9	DB	9
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_110.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_111.0.9	DB	26
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_112.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_113.0.9	DB	9
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_114.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_115.0.9	DB	26
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_116.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_117.0.9	DB	26
	DB	5
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _VARIABLE_MESSAGE_SIGN_MODULE_mp_TESTRAN
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55	DB	79
	DB	110
	DB	108
	DB	121
	DB	32
	DB	67
	DB	111
	DB	110
	DB	103
	DB	101
	DB	115
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	87
	DB	97
	DB	114
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	111
	DB	114
	DB	32
	DB	109
	DB	97
	DB	110
	DB	100
	DB	97
	DB	116
	DB	111
	DB	114
	DB	121
	DB	32
	DB	100
	DB	101
	DB	116
	DB	111
	DB	117
	DB	114
	DB	32
	DB	97
	DB	114
	DB	101
	DB	32
	DB	97
	DB	99
	DB	116
	DB	105
	DB	118
	DB	97
	DB	116
	DB	101
	DB	100
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_53	DB	79
	DB	116
	DB	104
	DB	101
	DB	114
	DB	32
	DB	102
	DB	101
	DB	97
	DB	116
	DB	117
	DB	114
	DB	101
	DB	115
	DB	32
	DB	97
	DB	114
	DB	101
	DB	32
	DB	117
	DB	110
	DB	100
	DB	101
	DB	114
	DB	32
	DB	116
	DB	101
	DB	115
	DB	116
	DB	105
	DB	110
	DB	103
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51	DB	79
	DB	112
	DB	116
	DB	105
	DB	111
	DB	110
	DB	97
	DB	108
	DB	32
	DB	100
	DB	101
	DB	116
	DB	111
	DB	117
	DB	114
	DB	32
	DB	105
	DB	115
	DB	32
	DB	112
	DB	101
	DB	114
	DB	109
	DB	97
	DB	110
	DB	101
	DB	110
	DB	116
	DB	108
	DB	121
	DB	32
	DB	114
	DB	101
	DB	109
	DB	111
	DB	118
	DB	101
	DB	100
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_67	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	105
	DB	102
	DB	121
	DB	105
	DB	110
	DB	103
	DB	32
	DB	86
	DB	77
	DB	83
	DB	0
__STRLITPACK_73	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	118
	DB	109
	DB	115
	DB	46
	DB	100
	DB	97
	DB	116
	DB	32
	DB	116
	DB	121
	DB	112
	DB	101
	DB	32
	DB	51
	DB	0
__STRLITPACK_43	DB	67
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	118
	DB	109
	DB	115
	DB	40
	DB	105
	DB	44
	DB	50
	DB	41
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	114
	DB	101
	DB	115
	DB	112
	DB	111
	DB	110
	DB	115
	DB	101
	DB	32
	DB	112
	DB	101
	DB	114
	DB	99
	DB	101
	DB	110
	DB	116
	DB	97
	DB	103
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_77	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	118
	DB	109
	DB	115
	DB	46
	DB	100
	DB	97
	DB	116
	DB	32
	DB	116
	DB	121
	DB	112
	DB	101
	DB	32
	DB	51
	DB	0
__STRLITPACK_39	DB	68
	DB	105
	DB	118
	DB	101
	DB	114
	DB	115
	DB	105
	DB	111
	DB	110
	DB	32
	DB	77
	DB	111
	DB	100
	DB	101
	DB	32
	DB	115
	DB	104
	DB	111
	DB	117
	DB	108
	DB	101
	DB	32
	DB	98
	DB	101
	DB	32
	DB	101
	DB	105
	DB	116
	DB	104
	DB	101
	DB	114
	DB	32
	DB	49
	DB	32
	DB	111
	DB	114
	DB	32
	DB	48
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_80	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_37	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	118
	DB	109
	DB	115
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_35	DB	112
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_84	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_33	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	105
	DB	102
	DB	121
	DB	105
	DB	110
	DB	103
	DB	32
	DB	86
	DB	77
	DB	83
	DB	0
__STRLITPACK_91	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29	DB	86
	DB	77
	DB	83
	DB	32
	DB	84
	DB	121
	DB	112
	DB	101
	DB	32
	DB	53
	DB	32
	DB	115
	DB	117
	DB	98
	DB	112
	DB	97
	DB	116
	DB	104
	DB	32
	DB	112
	DB	114
	DB	111
	DB	98
	DB	32
	DB	105
	DB	110
	DB	99
	DB	111
	DB	114
	DB	114
	DB	101
	DB	99
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_93	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27	DB	73
	DB	78
	DB	80
	DB	85
	DB	84
	DB	32
	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	58
	DB	32
	DB	86
	DB	77
	DB	83
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	0
__STRLITPACK_25	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	86
	DB	77
	DB	83
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_23	DB	102
	DB	111
	DB	114
	DB	32
	DB	86
	DB	77
	DB	83
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_98	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.0	DD	03f800347H
_2il0floatpacket.1	DD	03f7ff972H
_2il0floatpacket.5	DD	0c2700000H
_2il0floatpacket.6	DD	042c80000H
_2il0floatpacket.7	DD	042700000H
_2il0floatpacket.8	DD	03f800000H
__STRLITPACK_22	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	118
	DB	109
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
_2il0floatpacket.13	DD	03c23d70aH
_2il0floatpacket.14	DD	080000000H
_2il0floatpacket.15	DD	04b000000H
_2il0floatpacket.16	DD	03f000000H
_2il0floatpacket.17	DD	0bf000000H
_2il0floatpacket.30	DD	042700000H
_2il0floatpacket.31	DD	0c2700000H
_2il0floatpacket.32	DD	042c80000H
_2il0floatpacket.33	DD	03f800000H
__STRLITPACK_20	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	105
	DB	110
	DB	116
	DB	101
	DB	103
	DB	101
	DB	114
	DB	115
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	115
	DB	116
	DB	97
	DB	114
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	118
	DB	97
	DB	108
	DB	117
	DB	101
	DB	32
	DB	61
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_18	DB	32
	DB	79
	DB	108
	DB	100
	DB	32
	DB	115
	DB	116
	DB	97
	DB	114
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	118
	DB	97
	DB	108
	DB	117
	DB	101
	DB	32
	DB	61
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_16	DB	32
	DB	82
	DB	97
	DB	110
	DB	100
	DB	111
	DB	109
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	115
	DB	32
	DB	58
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_14	DB	32
	DB	80
	DB	114
	DB	101
	DB	115
	DB	101
	DB	110
	DB	116
	DB	32
	DB	115
	DB	116
	DB	97
	DB	114
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	118
	DB	97
	DB	108
	DB	117
	DB	101
	DB	32
	DB	61
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_12	DB	32
	DB	78
	DB	101
	DB	119
	DB	32
	DB	115
	DB	116
	DB	97
	DB	114
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	118
	DB	97
	DB	108
	DB	117
	DB	101
	DB	32
	DB	61
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_10	DB	32
	DB	82
	DB	97
	DB	110
	DB	100
	DB	111
	DB	109
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	115
	DB	32
	DB	58
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_8	DB	32
	DB	80
	DB	114
	DB	101
	DB	115
	DB	101
	DB	110
	DB	116
	DB	32
	DB	115
	DB	116
	DB	97
	DB	114
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	118
	DB	97
	DB	108
	DB	117
	DB	101
	DB	32
	DB	61
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_6	DB	32
	DB	82
	DB	97
	DB	110
	DB	100
	DB	111
	DB	109
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	115
	DB	32
	DB	58
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_4	DB	32
	DB	82
	DB	97
	DB	110
	DB	100
	DB	111
	DB	109
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	115
	DB	32
	DB	58
	DB	32
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IPINIT:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMSRATE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMSNNODE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMSTYPETWOPATH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NODEWVMS:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS_END:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS_START:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMSTYPE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS_NUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ERROR:BYTE
_DATA	ENDS
EXTRN	_for_random_number_single:PROC
EXTRN	_for_random_seed_get:PROC
EXTRN	_for_random_seed_bit_size:PROC
EXTRN	_for_random_seed_put:PROC
EXTRN	_DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME:PROC
EXTRN	_RANXY:PROC
EXTRN	_RETRIEVE_VEH_PATH_ASTAR:PROC
EXTRN	_RETRIEVE_NEXT_LINK:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_read_seq_lis_xmit:PROC
EXTRN	_for_read_seq_lis:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT:PROC
EXTRN	__intel_fast_memcpy:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
