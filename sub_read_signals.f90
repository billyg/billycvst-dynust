     SUBROUTINE READ_CONTROLS()  
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

     USE DYNUST_MAIN_MODULE
     USE DYNUST_NETWORK_MODULE
	 USE DYNUST_SIGNAL_MODULE

!     INTEGER error
     INTEGER kgpointmp(noofarcs+1,2)
     INTEGER CheckSignal(noofnodes,2)
     INTEGER ntmp,TmpSigData(10)
     INTEGER j
     INTEGER TmpMnAph(100), TmpMJAph(100)
     LOGICAL eofflag,eofflag2,eofflag3,nmvmflag

!     ALLOCATE(CheckSignal(noofnodes,2))
!     ALLOCATE(kgpointmp(noofarcs+1,2),stat=error)

! --
! -- Initialization of relevent arrays.
! --
     nmvmflag = .false.
     CheckSignal(:,:) = 0
     kgpoint(:)=0
	 kgpointmp(:,:)=0
     m_dynust_network_arc_de(:)%gcratio=0
!     movement(:,:,:)=.false.
     m_dynust_network_arcmove_de(:,:)%SignalPreventFor=1 ! default all prevented
     m_dynust_network_arcmove_de(:,:)%SignalPreventBack=1 ! default all prevented
! --
! -- kg is a counter for the number of phases in the whole network.
! --
     kg=1
! --
! -- read the control type data from the input file.
! --
! -- THEN, sort all phases in the network into one list (the counter kg).
! -- kgpoint is the pointer for the staring phase for each node (out of
! -- the kg phases).
! --
     
! -- this is to automatically generate the control type for those need default setting	 
	 iflag = 0
	 DO i = 1,noofnodes-noof_master_destinations
	 	   READ(44,*,iostat=error) (m_dynust_network_node(i)%node(j),j=1,4)
		   IF(m_dynust_network_node(i)%node(2).gt.1) THEN
		     iflag=1
           ENDIF
	 ENDDO 
	 IF(iflag.eq.0) THEN ! all types are found to be no control
	    DO is = 1, noofarcs
           IF(m_dynust_network_arc_nde(is)%link_iden.eq.5) THEN
             DO it = 1, noofnodes-noof_master_destinations
                IF(m_dynust_network_arc_nde(is)%idnod.eq.idnum(m_dynust_network_node(it)%node(1))) THEN
				   m_dynust_network_node(it)%node(2) = 5
				ENDIF
			 ENDDO
		   ENDIF
	    ENDDO
	 
	 ENDIF
     close(678)
	 rewind(44)
	 READ(44,*)
	 READ(44,*)

!@@@@@@@@@@@@@@
     CALL DeclareSignal(noofnodes-noof_master_destinations)
!@@@@@@@@@@@@@@	         
	 icount = 0
     DO 10 i=1,noofnodes-noof_master_destinations
         kgpoint(i)=kg

	     READ(44,*,iostat=error) (m_dynust_network_node(i)%node(j),j=1,4)
	     IF((m_dynust_network_node(i)%node(2).eq.4.or.m_dynust_network_node(i)%node(2).eq.5).and.m_dynust_network_node(i)%node(3).lt.1) THEN
	        WRITE(911,*) "Error in control.dat"
	        WRITE(911,*) "Please check node", m_dynust_network_node(i)%node(1)
	        STOP
	     ENDIF
		 TmpSigData(1:4) = m_dynust_network_node(i)%node(1:4)
!@@@@@@@@@@@@@@         
		 CALL insertSignalPar(TmpSigData,10)
		 IF(TmpSigData(2).eq.5.or.TmpSigData(2).eq.4) THEN
		    CALL DeclarePhase(idnum(TmpSigData(1)),TmpSigData(3))
         ENDIF
!@@@@@@@@@@@@@@	    
! check input errors	
	     IF(error.ne.0) THEN
           WRITE(911,*) 'Error when reading control.dat'
	       STOP
	     ENDIF
         IF(m_dynust_network_node(i)%node(2).lt.1) THEN ! check signal type input error
           WRITE(911,*) "Error in control.dat"
           WRITE(911,*) "node", m_dynust_network_node(i)%node(1),"signal type invalid"
		   STOP
		 ELSEIF (m_dynust_network_node(i)%node(2).eq.4.or.m_dynust_network_node(i)%node(2).eq.5) THEN
		   IF(m_dynust_network_node(i)%node(3).lt.1.or.m_dynust_network_node(i)%node(4).lt.1) THEN
           WRITE(911,*) "Error in phasing and cycle setting"
		   WRITE(911,*) "Please check node ",m_dynust_network_node(i)%node(1)
		   STOP
           ENDIF
		 ENDIF


	     m_dynust_network_node(i)%node(1) = idnum(m_dynust_network_node(i)%node(1)) ! change to internal node number
         IF(m_dynust_network_node(i)%node(2).eq.4.or.m_dynust_network_node(i)%node(2).eq.5) THEN 
	       icount=icount+1
	       CheckSignal(icount,1) = m_dynust_network_node(i)%node(1)
	       CheckSignal(icount,2) = m_dynust_network_node(i)%node(3)
	       kgpointmp(icount,1)= i
	       kgpointmp(icount,2)= kg
           kg=kg+m_dynust_network_node(i)%node(3)
	     ELSE !STOP, no control, yield, relax the SignalPreventFor to be 0
           DO KM = backpointr(m_dynust_network_node(i)%node(1)),backpointr(m_dynust_network_node(i)%node(1)+1)-1 ! for all inbound links (back)
             DO JM = 1, m_dynust_network_arc_nde(m_dynust_network_arc_de(KM)%BackToForLink)%llink%no ! for all outbound links of the inbound link
                m_dynust_network_arcmove_de(m_dynust_network_arc_nde(m_dynust_network_arc_de(KM)%BackToForLink)%llink%p(JM),:)%SignalPreventFor = 0
!               SignalPreventBack(ForToBackLink(llink(BackToForLink(KM),JM)),:)=0
                m_dynust_network_arcmove_de(m_dynust_network_arc_nde(m_dynust_network_arc_nde(m_dynust_network_arc_de(KM)%BackToForLink)%llink%p(JM))%ForToBackLink,:)%SignalPreventBack=0
	         ENDDO
	       ENDDO
           IF(m_dynust_network_node(i)%node(2).eq.2.or.m_dynust_network_node(i)%node(2).eq.6) THEN 
		        SignCount=SignCount+1 !yield or two-way STOP
           ENDIF
         ENDIF
10 continue
     kgpoint(noofnodes+1)=kg
     icountfinal=icount
     kg=kg-1
! --
! -- Read the signal phasing information for each phase.
! -- 
     IF(kg.gt.0) THEN  !FBA Block
	 icount = 1
	 icount2 = 1
	 icount3 = 0
	  
	  
	  DO 192 i=1,kg
        READ(44,*,iostat=error) itmp,(nsign(i,j),j=1,9)
!@@@@@@@@@@@@@@	   
        TmpSigData(:) = 0 
		TmpSigData(1) = itmp
		TmpSigData(2:10) = nsign(i,1:9)
		CALL InsertSignalPhase(TmpSigData,10)
		CALL DeclarePhaseMove(TmpSigData,10)
!@@@@@@@@@@@@@@
		IF(error.ne.0) THEN
          WRITE(911,*) 'Error when reading signal.dat'
	      STOP
	    ENDIF

	    IF(idnum(itmp).eq.CheckSignal(icount2,1)) THEN
	      icount3 = icount3 + 1
	    ELSE
	      IF(icount3.ne.CheckSignal(icount2,2)) THEN
			WRITE(911,*) 'error in matching signals'
            WRITE(911,*) 'node', itmp, 'phase',nsign(i,1)
	        WRITE(911,*) "icount3, CheckSignal(icount2,2)",icount3,CheckSignal(icount2,2)
			WRITE(911,*) "possible reason: missing phasing for previous signalized node"
			WRITE(911,*) "check signal types in the first block"
			STOP
	      ENDIF
	      icount3 = 1
	      icount2 = icount2 + 1
	    ENDIF

	    icount=0
		DO ii = 6, 9						!
	      IF (nsign(i,ii).ne.0) THEN		!
	        icount=icount+1					!
	      ENDIF								!
	    ENDDO								!
	    IF(icount.ne.nsign(i,5)) THEN		!
		  WRITE(911,*) "Number of inbound links for node", itmp
          WRITE(911,*) "is not specified correctly"
	      WRITE(911,*) "Phase", nsign(i,1)
		  WRITE(911,*) "Read as ", nsign(i,5), "counted as", icount
	     STOP
	    ENDIF
! --  convert to internal numbering system	    
	    IF(nsign(i,6).gt.0) THEN
		  nsign(i,6) = idnum(nsign(i,6)) !G
          IF(nsign(i,6).lt.1) THEN
		   WRITE(911,*) 'error in node number for phase #6', nsign(i,1), 'for node ', itmp
		   STOP
          ENDIF
        ENDIF  
	    IF(nsign(i,7).gt.0) THEN
		  nsign(i,7) = idnum(nsign(i,7)) !G
          IF(nsign(i,7).lt.1) THEN
		     WRITE(911,*) 'error in node number for phase #7', nsign(i,1), 'for node ', itmp
		     STOP
          ENDIF
        ENDIF
 	    IF(nsign(i,8).gt.0) THEN
		  nsign(i,8) = idnum(nsign(i,8)) !G
          IF(nsign(i,8).lt.1) THEN
	        WRITE(911,*) 'error in node number for phase #8', nsign(i,1), 'for node ', itmp
		    STOP
          ENDIF
        ENDIF

	    IF(nsign(i,9).gt.0) THEN
		  nsign(i,9) = idnum(nsign(i,9)) !G
          IF(nsign(i,9).lt.1) THEN 
		   WRITE(911,*) 'error in node number for phase #9', nsign(i,1), 'for node ', itmp
		   STOP
          ENDIF
        ENDIF
! -- 
! -- for the first signal timing plan, set the starting and ending times for
! -- the green and yellow times for each phase.
! -- Note : for the subsequent signal timing plans, the starting and ending
! -- times are alreay set based on the preceeding signal setting plan.
! -- Therefore, there is no need to set them again.
! -- 
          IF(isigcount.eq.1) THEN
            DO j=12,14
              nsign(i,j)=strtsig(isigcount)*60
            end do
          ENDIF

! -- convert the node (in the nsign array) to link by recognizing
! -- the upstream node and the downstream nodes.
       DO ii=6,nsign(i,5)+5
	     ntmp = nsign(i,ii)
	     IF(GetFLinkFromNode(ntmp,idnum(itmp),14).eq.0) THEN
	       WRITE(911,*) 'error in signal file, link doesnt exist'
		   WRITE(911,*) 'inbound',m_dynust_network_node_nde(ntmp)%nodenum,'signal node',itmp
	       WRITE(911,'("signal node",i7," Phase",i3)')itmp,nsign(i,1)
	       IF(nsign(i,6).gt.0) WRITE(911,'(" inbound nodes",i10)') m_dynust_network_node_nde(nsign(i,6))%nodenum
             IF(nsign(i,7).gt.0) WRITE(911,'(" inbound nodes",i10)') m_dynust_network_node_nde(nsign(i,7))%nodenum
             IF(nsign(i,8).gt.0) WRITE(911,'(" inbound nodes",i10)') m_dynust_network_node_nde(nsign(i,8))%nodenum
	         STOP
           ELSE
             nsign(i,ii) = GetFLinkFromNode(ntmp,idnum(itmp),14)
	     ENDIF 
       end do
! --
! -- Read the movement data for each phase and each link which is allowed
! -- to utilize the intersection during each phase.
! -- nsign(i,5) : number of approaches which are allowed to move during phase i
! --
! -- 
! -- ifrom : the upstream node of the current approach
! -- ito : the downstream node of the current approach
! -- iphase : the current phase number
! -- nmvm : number of movements allowed from the current approach
! -- almov : a list of nodes to which the movements from the current
! --         approach are allowed.
! --

      DO i1=1,nsign(i,5) !# of inbound links
       READ(44,66) ifrom,ito,iphasek,nmvm,(almov(i1,iin),iin=1,nmvm)
!        READ(44,*,iostat=error)ifrom,ito,iphasek,nmvm,(almov(i1,iin),iin=1,nmvm)
		IF(nmvm.lt.1) THEN
         WRITE(911,*) "error in phase movement for node",ito, "phase ", iphasek
		 nmvmflag=.true.
		ENDIF
!@@@@@@@@@@@@@b
		TmpSigData(1) = ifrom 
		TmpSigData(2) = ito
		TmpSigData(3) = iphasek
		TmpSigData(4) = nmvm
		TmpSigData(5:5+nmvm) = almov(i1,1:nmvm)
		CALL DeclareOBLink(TmpSigData,10,i1)
        CALL InsertPhaseMove(TmpSigData,10,i1) 

!@@@@@@@@@@@@@
!        READ(44,*,iostat=error)ifrom,ito,iphasek,nmvm,(almov(i1,iin),iin=1,6)
	    IF(error.ne.0) THEN
         WRITE(911,*) 'Error when reading phasing in control.dat'
         WRITE(911,*)'from,to,phasek number,# of outbound links, outbound node number...' 
		 WRITE(911,*) ifrom,ito,iphasek,nmvm,(almov(i1,iin),iin=1,nmvm)
	     STOP
	    ENDIF

        iflag=0
	    DO jp = 1, nmvm
		  IF(idnum(almov(i1,jp)).lt.1) THEN
            WRITE(911,*) 'node does not exist, from ', ifrom, 'to ', ito, 'phase',iphasek,'outbound node', almov(i1,jp)
            STOP
		  ENDIF
          IF(GetFLinkFromNode(idnum(ito),idnum(almov(i1,jp)),14).gt.0) THEN
            iflag = 1
			exit
		  ELSE
            WRITE(911,*) 'from ', ifrom, 'to ', ito, 'phase', iphasek
			WRITE(911,*) 'link UpNode -> DNode does not exit', ito,almov(i1,jp)
			STOP
		  ENDIF
        ENDDO
     

	  IF(idnum(ifrom).ne.m_dynust_network_arc_nde(nsign(i,5+i1))%iunod) THEN
		 WRITE(911,'("inbound node",i5," for node",i5," at phase",i5," is not correct")')ifrom,ito,iphasek
	     STOP
	  ENDIF
	  DO mm = 1, nmvm						                   !G
	    IF(almov(i1,mm).gt.0) almov(i1,mm)=idnum(almov(i1,mm)) !G
	  ENDDO								                       !G
! -- make movement to the centroid allowed for all phases
        IF(m_dynust_network_node(idnum(ito))%iConZone(1).gt.0) THEN
	    DO mpp= 1, m_dynust_network_node(idnum(ito))%iConZone(1)
           nmvm = nmvm + 1
           almov(i1,nmvm) = destination(MasterDest(m_dynust_network_node(idnum(ito))%iConZone(mpp+1)))
	    ENDDO
        ENDIF


        
66      FORMAT(15i7)
         j=nsign(i,i1+5)
           DO ka=1,m_dynust_network_arc_nde(j)%llink%no
            DO ia=1,nmvm
              IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(ka))%idnod.eq.almov(i1,ia)) THEN
!			   movement(j,iphasek,ka)=.true.
	           MVPF = MoveNoForLink(j,m_dynust_network_arc_nde(j)%llink%p(ka))
	           m_dynust_network_arcmove_de(m_dynust_network_arc_nde(j)%llink%p(ka),MVPF)%SignalPreventFor = 0  ! set the value to 1 is this is a permissive movement
!               MVPB = MoveNoBackLink(j,llink(j)%p(ka))
               MVPB = getBstMove(j,m_dynust_network_arc_nde(j)%llink%p(ka))
               m_dynust_network_arcmove_de(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(ka))%ForToBackLink,MVPB)%SignalPreventBack=0
	        ENDIF
            ENDDO 
	   
! -- make uturn movements allowed IF it is specified in the signal 
! --
!              IF(iunod(llink(j)%p(ka)).eq.idnod(j).and.idnod(llink(j)%p(ka)).eq.iunod(j)) THEN
!                 movement(j,iphasek,ka)=1
!	             MVPF = MoveNoForLink(j,llink(j)%p(ka))
!	             SignalPreventFor(llink(j)%p(ka),MVPF) = 0
!                 MVPB = getBstMove(j,llink(j)%p(ka))
!                 SignalPreventBack(ForToBackLink(llink(j)%p(ka)),MVPB)=0
!              ENDIF

        ENDDO
      ENDDO
      

! --
      
192   continue
     ENDIF !FBA Block

      IF(nmvmflag) STOP
!==========================================================
! start reading Yield sign or two-way STOP
     IF(SignCount.gt.0) THEN
!     IF(EOF(44)) THEN
!        WRITE(911,*) 'end of file when reading 2-way/yield control.dat'
!        STOP
!     ENDIF
     READ(44,*,iostat=error)
     IF(error.ne.0) THEN !error = 24 is eof
       WRITE(911,*) 'Error when reading 2-way/yield in control.dat'
	   STOP
	 ENDIF

      ALLOCATE(SignData(SignCount), stat=error)
	  SignData(:)%node = 0
	  SignData(:)%NofMajor = 0
	  SignData(:)%NofMinor = 0
      

	  ALLOCATE(SignApprh(SignCount), stat=error) 
      SignApprh(:)%major(1) = 0 ! max 4 major 4 minor approaches
      SignApprh(:)%major(2) = 0
	  SignApprh(:)%major(3) = 0
      SignApprh(:)%major(4) = 0
      SignApprh(:)%minor(1) = 0
      SignApprh(:)%minor(2) = 0
	  SignApprh(:)%minor(3) = 0
      SignApprh(:)%minor(4) = 0
 
      DO i = 1, SignCount
!        IF(EOF(44)) THEN
!          WRITE(911,*) 'end of file when reading 2-way/yield control.dat'
!          STOP
    !    ENDIF
        READ(44,*,iostat=error) SignData(i)%node, SignData(i)%NofMajor, SignData(i)%NofMinor
        SignData(i)%node = idnum(SignData(i)%node)
		IF(error.ne.0) THEN
          WRITE(911,*) 'Error when reading 2-way/yield in control.dat'
	      STOP
	    ENDIF
!        IF(EOF(44)) THEN
!        WRITE(911,*) 'end of file when reading 2-way/yield control.dat'
!      STOP
!        ENDIF

! -------read major links
        READ(44,*,iostat=error) (TmpMJAph(j),j=1,2*SignData(i)%NofMajor) 
        IF(error.ne.0) THEN
          WRITE(911,*) 'Error when reading 2-way/yield in control.dat'
	      STOP
	    ENDIF
		DO IMp= 1, 2*SignData(i)%NofMajor
         IF(mod(IMP,2).eq.0) THEN
           IF(idnum(TmpMJAph(IMP)).ne.SignData(i)%node) THEN
		    WRITE(911,*) "Error in two-way signal major st. spec."
			WRITE(911,*) " check node ", m_dynust_network_node_nde(SignData(i)%node)%nodenum
			STOP
		   ENDIF
		 ENDIF
		ENDDO
	    
		DO k = 1, SignData(i)%NofMajor !get link number for major approach
        ienter1 = idnum(TmpMJAph(k*2-1))
		ienter2 = idnum(TmpMJAph(k*2))
		IF(ienter1.lt.1.or.ienter2.lt.1) THEN
           WRITE(911,*) "Error in two-way STOP"
		   WRITE(911,*) "check major st spec for node", m_dynust_network_node_nde(SignData(i)%node)%nodenum
		   STOP
		ENDIF
         ienter3 = GetFLinkFromNode(ienter1,ienter2,14)
		 IF(ienter3.lt.1) THEN
		    WRITE(911,*) "error in two-twy STOP"
			WRITE(911,*) "check major spec for node ", m_dynust_network_node_nde(SignData(i)%node)%nodenum
			STOP
		 ELSE
           SignApprh(i)%major(k) = ienter3
		 ENDIF
	    ENDDO

! -------read minor links
	    READ(44,*,iostat=error) (TmpMnAph(j),j=1,2*SignData(i)%NofMinor)
        IF(error.ne.0) THEN
          WRITE(911,*) 'Error when reading 2-way/yield in control.dat'
	      STOP
	    ENDIF

		DO IMp= 1, 2*SignData(i)%NofMinor
         IF(mod(IMP,2).eq.0) THEN
           IF(idnum(TmpMnAph(IMP)).ne.SignData(i)%node) THEN
		    WRITE(911,*) "Error in two-way signal minor st. spec"
			WRITE(911,*) " check node ", m_dynust_network_node_nde(SignData(i)%node)%nodenum
			STOP
		   ENDIF
		 ENDIF
		ENDDO

	    DO k = 1, SignData(i)%NofMinor ! get link number for minor approach

        ienter1 = idnum(TmpMnAph(k*2-1))
		ienter2 = idnum(TmpMnAph(k*2))
		IF(ienter1.lt.1.or.ienter2.lt.1) THEN
           WRITE(911,*) "Error in two-way STOP"
		   WRITE(911,*) "check minor st spec for node", m_dynust_network_node_nde(SignData(i)%node)%nodenum
		   STOP
		ENDIF
          ienter3 = GetFLinkFromNode(ienter1, ienter2,14)
		  IF(ienter3.lt.1) THEN
		    WRITE(911,*) "error in two-twy STOP"
			WRITE(911,*) "check minor spec for node ", m_dynust_network_node_nde(SignData(i)%node)%nodenum
			STOP
		  ELSE
	        SignApprh(i)%minor(k) = ienter3
		  ENDIF
	    ENDDO
      ENDDO
 
 	 ENDIF !SignCount.gt.0


! --
! --
! -- The next block is to get the prevented movements due to signal phasing
! -- SignalPreventBack(i,j)=1 means that the movement to link i from movement j is 
! -- prevented and 0 otherwise
! -- The difference between prevent and prevent1 is in the definition of i.
! -- SignalPreventFor carries the information in the forward star representation and
! -- SignalPreventBack for the backward-star representation.
! --
! --
! -- for the prevented movements, consider a very high penalty.
! --
         DO i=1,noofarcs
           DO kk=1,maxmove
             IF(m_dynust_network_arcmove_de(i,kk)%SignalPreventFor.eq.1) THEN
	         m_dynust_network_arcmove_de(i,kk)%openalty=PenForPreventMove
	       ENDIF

             IF(m_dynust_network_arcmove_de(m_dynust_network_arc_nde(i)%ForToBackLink,kk)%SignalPreventBack.eq.1) THEN
                m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(i)%ForToBackLink,kk)%penalty=PenForPreventMove  ! defined in DYNUST_MAIN_MODULE
             ENDIF

           ENDDO
         ENDDO
! --
! -- calculate the green time ratio (gcratio)
! -- consider different cases
! -- pretimed, actuated signal control
! --
        DO i=1,noofarcs
          IF(m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2).eq.4) THEN
            DO k=kgpoint(m_dynust_network_arc_nde(i)%idnod),kgpoint(m_dynust_network_arc_nde(i)%idnod+1)-1
               DO m=6,nsign(k,5)+5
                  IF(i.eq.nsign(k,m)) THEN
                     m_dynust_network_arc_de(i)%gcratio=m_dynust_network_arc_de(i)%gcratio + (float(nsign(k,3))/m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(4))
                   ENDIF
                end do
            end do
          ENDIF
! --
! --
          IF(m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2).eq.5) THEN
             DO k=kgpoint(m_dynust_network_arc_nde(i)%idnod),kgpoint(m_dynust_network_arc_nde(i)%idnod+1)-1
                DO m=6,nsign(k,5)+5
                   IF(i.eq.nsign(k,m)) THEN
                     m_dynust_network_arc_de(i)%gcratio=m_dynust_network_arc_de(i)%gcratio + (float(nsign(k,2))/m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(4))
                   ENDIF
                end do
             end do
          ENDIF
        ENDDO 
!
! --

! -- Check IF any control is found on the freeway.
      DO i = 1, noofarcs
        eofflag2=.false.
        eofflag3=.false.

        IF(m_dynust_network_arc_nde(i)%link_iden.eq.1.or.m_dynust_network_arc_nde(i)%link_iden.eq.2.or.m_dynust_network_arc_nde(i)%link_iden.eq.7.or.m_dynust_network_arc_nde(i)%link_iden.eq.9.or.m_dynust_network_arc_nde(i)%link_iden.eq.10) THEN
		  IF(m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2).ne.2.and.m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2).ne.1) THEN
		    DO ks = 1, m_dynust_network_arc_nde(i)%llink%no
              IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden.eq.1.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden.eq.2.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden.eq.3.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden.eq.4.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden.eq.9.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ks))%link_iden.eq.10) eofflag2=.true.
			ENDDO
		    IF(eofflag2) WRITE(511,'("Signs/Signals found on freeway section from",i7,"  to",i7)') m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%iunod)%nodenum,m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%idnod)%nodenum
		  ENDIF
		  IF(m_dynust_network_node(m_dynust_network_arc_nde(i)%iunod)%node(2).ne.2.and.m_dynust_network_node(m_dynust_network_arc_nde(i)%iunod)%node(2).ne.1) THEN
		    DO ks = 1, m_dynust_network_arc_nde(i)%inlink%no
              IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden.eq.1.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden.eq.2.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden.eq.3.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden.eq.4.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden.eq.9.or.m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ks))%link_iden.eq.10) eofflag3=.true.
			ENDDO
            IF(eofflag3) WRITE(511,'("Signs/Signals found on freeway section from",i7,"  to",i7)') m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%iunod)%nodenum,m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%idnod)%nodenum
          ENDIF
!          IF(eofflag2.and.eofflag3) THEN
!		    WRITE(511,'("Signs/Signals found on freeway section from",i7,"  to",i7)') nodenum(iunod(i)),nodenum(idnod(i))
!          ENDIF
		ENDIF
	  ENDDO
!	  close(911)
!	  IF(eofflag2.or.eofflag3) STOP

193 FORMAT(11i4)
 19 FORMAT(i5,i2,i2,i4)
!    DEALLOCATE(CheckSignal)
!    DEALLOCATE(kgpointmp)

	END SUBROUTINE
