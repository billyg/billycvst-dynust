; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_VEH_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_VEH_MODULE$
_DYNUST_VEH_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_VEH_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH
_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH	PROC NEAR 
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;109.12
        mov       ebp, esp                                      ;109.12
        and       esp, -16                                      ;109.12
        push      esi                                           ;109.12
        push      edi                                           ;109.12
        push      ebx                                           ;109.12
        sub       esp, 148                                      ;109.12
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;113.13
        cmp       edx, 1                                        ;113.24
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_NOOFBUSES] ;114.29
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NV_VEBUFFER] ;114.35
        je        .B2.428       ; Prob 16%                      ;113.24
                                ; LOE eax edx xmm0
.B2.2:                          ; Preds .B2.1
        test      edx, -3                                       ;115.32
        jne       .B2.4         ; Prob 50%                      ;115.32
                                ; LOE eax xmm0
.B2.3:                          ; Preds .B2.2
        cvtsi2ss  xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE] ;116.26
        mulss     xmm3, xmm0                                    ;116.26
        mov       ecx, 10                                       ;116.11
        movss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;116.26
        andps     xmm0, xmm3                                    ;116.26
        pxor      xmm3, xmm0                                    ;116.26
        movss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;116.26
        movaps    xmm2, xmm3                                    ;116.26
        movss     xmm4, DWORD PTR [_2il0floatpacket.4]          ;116.26
        cmpltss   xmm2, xmm1                                    ;116.26
        andps     xmm1, xmm2                                    ;116.26
        movaps    xmm2, xmm3                                    ;116.26
        movaps    xmm6, xmm4                                    ;116.26
        addss     xmm2, xmm1                                    ;116.26
        addss     xmm6, xmm4                                    ;116.26
        subss     xmm2, xmm1                                    ;116.26
        movaps    xmm7, xmm2                                    ;116.26
        subss     xmm7, xmm3                                    ;116.26
        movaps    xmm5, xmm7                                    ;116.26
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.5]          ;116.26
        cmpnless  xmm5, xmm4                                    ;116.26
        andps     xmm5, xmm6                                    ;116.26
        andps     xmm7, xmm6                                    ;116.26
        subss     xmm2, xmm5                                    ;116.26
        addss     xmm2, xmm7                                    ;116.26
        orps      xmm2, xmm0                                    ;116.26
        cvtss2si  edx, xmm2                                     ;116.26
        add       eax, edx                                      ;116.61
        cmp       eax, 10                                       ;116.11
        cmovge    ecx, eax                                      ;116.11
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH], ecx ;116.11
        jmp       .B2.5         ; Prob 100%                     ;116.11
                                ; LOE ecx
.B2.4:                          ; Preds .B2.2
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE] ;118.26
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESTABLE] ;118.26
        cvtsi2ss  xmm3, edx                                     ;118.26
        mulss     xmm3, xmm0                                    ;118.26
        movss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;118.26
        andps     xmm0, xmm3                                    ;118.26
        pxor      xmm3, xmm0                                    ;118.26
        movss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;118.26
        movaps    xmm2, xmm3                                    ;118.26
        movss     xmm4, DWORD PTR [_2il0floatpacket.4]          ;118.26
        cmpltss   xmm2, xmm1                                    ;118.26
        andps     xmm1, xmm2                                    ;118.26
        movaps    xmm2, xmm3                                    ;118.26
        movaps    xmm6, xmm4                                    ;118.26
        addss     xmm2, xmm1                                    ;118.26
        addss     xmm6, xmm4                                    ;118.26
        subss     xmm2, xmm1                                    ;118.26
        movaps    xmm7, xmm2                                    ;118.26
        subss     xmm7, xmm3                                    ;118.26
        movaps    xmm5, xmm7                                    ;118.26
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.5]          ;118.26
        cmpnless  xmm5, xmm4                                    ;118.26
        andps     xmm5, xmm6                                    ;118.26
        andps     xmm7, xmm6                                    ;118.26
        subss     xmm2, xmm5                                    ;118.26
        addss     xmm2, xmm7                                    ;118.26
        orps      xmm2, xmm0                                    ;118.26
        cvtss2si  ecx, xmm2                                     ;118.26
        add       eax, ecx                                      ;118.78
        mov       ecx, 10                                       ;118.11
        cmp       eax, 10                                       ;118.11
        cmovge    ecx, eax                                      ;118.11
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH], ecx ;118.11
                                ; LOE ecx
.B2.5:                          ; Preds .B2.428 .B2.3 .B2.4
        xor       eax, eax                                      ;122.13
        test      ecx, ecx                                      ;122.13
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;121.13
        lea       edx, DWORD PTR [96+esp]                       ;122.13
        push      256                                           ;122.13
        cmovle    ecx, eax                                      ;122.13
        push      ecx                                           ;122.13
        push      2                                             ;122.13
        push      edx                                           ;122.13
        call      _for_check_mult_overflow                      ;122.13
                                ; LOE eax
.B2.6:                          ; Preds .B2.5
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+12] ;122.13
        and       eax, 1                                        ;122.13
        and       edx, 1                                        ;122.13
        add       edx, edx                                      ;122.13
        shl       eax, 4                                        ;122.13
        or        edx, 1                                        ;122.13
        or        edx, eax                                      ;122.13
        or        edx, 262144                                   ;122.13
        push      edx                                           ;122.13
        push      OFFSET FLAT: _DYNUST_VEH_MODULE_mp_M_DYNUST_VEH ;122.13
        push      DWORD PTR [120+esp]                           ;122.13
        call      _for_alloc_allocatable                        ;122.13
                                ; LOE eax
.B2.441:                        ; Preds .B2.6
        add       esp, 28                                       ;122.13
        mov       ebx, eax                                      ;122.13
                                ; LOE ebx
.B2.7:                          ; Preds .B2.441
        test      ebx, ebx                                      ;122.13
        je        .B2.10        ; Prob 50%                      ;122.13
                                ; LOE ebx
.B2.8:                          ; Preds .B2.7
        mov       DWORD PTR [64+esp], 0                         ;124.17
        lea       edx, DWORD PTR [64+esp]                       ;124.17
        mov       DWORD PTR [56+esp], 49                        ;124.17
        lea       eax, DWORD PTR [56+esp]                       ;124.17
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_14 ;124.17
        push      32                                            ;124.17
        push      eax                                           ;124.17
        push      OFFSET FLAT: __STRLITPACK_16.0.2              ;124.17
        push      -2088435968                                   ;124.17
        push      911                                           ;124.17
        push      edx                                           ;124.17
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;122.13
        call      _for_write_seq_lis                            ;124.17
                                ; LOE
.B2.9:                          ; Preds .B2.8
        push      32                                            ;125.14
        xor       eax, eax                                      ;125.14
        push      eax                                           ;125.14
        push      eax                                           ;125.14
        push      -2088435968                                   ;125.14
        push      eax                                           ;125.14
        push      OFFSET FLAT: __STRLITPACK_17                  ;125.14
        call      _for_stop_core                                ;125.14
                                ; LOE
.B2.442:                        ; Preds .B2.9
        add       esp, 48                                       ;125.14
        jmp       .B2.17        ; Prob 100%                     ;125.14
                                ; LOE
.B2.10:                         ; Preds .B2.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;122.13
        xor       eax, eax                                      ;122.13
        test      ecx, ecx                                      ;122.13
        cmovle    ecx, eax                                      ;122.13
        mov       esi, 256                                      ;122.13
        lea       edi, DWORD PTR [52+esp]                       ;122.13
        push      esi                                           ;122.13
        push      ecx                                           ;122.13
        push      2                                             ;122.13
        push      edi                                           ;122.13
        mov       edx, 1                                        ;122.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+12], 133 ;122.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+4], esi ;122.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+16], edx ;122.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+8], eax ;122.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32], edx ;122.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+24], ecx ;122.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+28], esi ;122.13
        call      _for_check_mult_overflow                      ;122.13
                                ; LOE ebx esi
.B2.443:                        ; Preds .B2.10
        add       esp, 16                                       ;122.13
                                ; LOE ebx esi
.B2.11:                         ; Preds .B2.443
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;122.13
        mov       ecx, edx                                      ;122.13
        mov       eax, DWORD PTR [52+esp]                       ;122.13
        add       eax, edx                                      ;122.13
        cmp       edx, eax                                      ;122.13
        jae       .B2.16        ; Prob 67%                      ;122.13
                                ; LOE edx ecx ebx esi
.B2.13:                         ; Preds .B2.11
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.14:                         ; Preds .B2.15 .B2.13
        mov       edi, eax                                      ;122.13
        shl       edi, 8                                        ;122.13
        add       edi, edx                                      ;122.13
        mov       edx, esi                                      ;122.13
                                ; LOE eax edx ecx ebx esi edi
.B2.433:                        ; Preds .B2.433 .B2.14
        movsd     xmm0, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T205_-8+edx] ;122.13
        movsd     QWORD PTR [-8+edi+edx], xmm0                  ;122.13
        movsd     xmm1, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T205_-16+edx] ;122.13
        movsd     QWORD PTR [-16+edi+edx], xmm1                 ;122.13
        movsd     xmm2, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T205_-24+edx] ;122.13
        movsd     QWORD PTR [-24+edi+edx], xmm2                 ;122.13
        movsd     xmm3, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T205_-32+edx] ;122.13
        movsd     QWORD PTR [-32+edi+edx], xmm3                 ;122.13
        sub       edx, 32                                       ;122.13
        jne       .B2.433       ; Prob 87%                      ;122.13
                                ; LOE eax edx ecx ebx esi edi
.B2.15:                         ; Preds .B2.433
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;122.13
        add       ecx, 256                                      ;122.13
        mov       edi, DWORD PTR [52+esp]                       ;122.13
        inc       eax                                           ;122.13
        add       edi, edx                                      ;122.13
        cmp       ecx, edi                                      ;122.13
        jb        .B2.14        ; Prob 82%                      ;122.13
                                ; LOE eax edx ecx ebx esi
.B2.16:                         ; Preds .B2.11 .B2.15
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;122.13
                                ; LOE
.B2.17:                         ; Preds .B2.442 .B2.16
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;128.10
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+24] ;128.10
        test      eax, eax                                      ;128.10
        mov       DWORD PTR [esp], ebx                          ;128.10
        jle       .B2.66        ; Prob 50%                      ;128.10
                                ; LOE eax ebx bl bh
.B2.18:                         ; Preds .B2.17
        mov       esi, eax                                      ;128.10
        shr       esi, 31                                       ;128.10
        add       esi, eax                                      ;128.10
        sar       esi, 1                                        ;128.10
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;128.10
        test      esi, esi                                      ;128.10
        jbe       .B2.287       ; Prob 10%                      ;128.10
                                ; LOE eax ecx ebx esi bl bh
.B2.19:                         ; Preds .B2.18
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        mov       ebx, 1065353216                               ;
                                ; LOE eax edx ecx ebx esi edi
.B2.20:                         ; Preds .B2.20 .B2.19
        inc       edx                                           ;128.10
        mov       DWORD PTR [12+edi+ecx], ebx                   ;128.10
        mov       DWORD PTR [268+edi+ecx], ebx                  ;128.10
        add       edi, 512                                      ;128.10
        cmp       edx, esi                                      ;128.10
        jb        .B2.20        ; Prob 63%                      ;128.10
                                ; LOE eax edx ecx ebx esi edi
.B2.21:                         ; Preds .B2.20
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;128.10
                                ; LOE eax edx ecx ebx esi
.B2.22:                         ; Preds .B2.21 .B2.287
        lea       edi, DWORD PTR [-1+edx]                       ;128.10
        cmp       eax, edi                                      ;128.10
        jbe       .B2.24        ; Prob 10%                      ;128.10
                                ; LOE eax edx ecx ebx esi
.B2.23:                         ; Preds .B2.22
        mov       edi, ebx                                      ;128.10
        add       edx, ebx                                      ;128.10
        neg       edi                                           ;128.10
        add       edi, edx                                      ;128.10
        shl       edi, 8                                        ;128.10
        mov       DWORD PTR [-244+ecx+edi], 1065353216          ;128.10
                                ; LOE eax ecx ebx esi
.B2.24:                         ; Preds .B2.22 .B2.23
        test      esi, esi                                      ;129.10
        jbe       .B2.286       ; Prob 10%                      ;129.10
                                ; LOE eax ecx ebx esi
.B2.25:                         ; Preds .B2.24
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.26:                         ; Preds .B2.26 .B2.25
        inc       edx                                           ;129.10
        mov       DWORD PTR [16+edi+ecx], ebx                   ;129.10
        mov       DWORD PTR [272+edi+ecx], ebx                  ;129.10
        add       edi, 512                                      ;129.10
        cmp       edx, esi                                      ;129.10
        jb        .B2.26        ; Prob 63%                      ;129.10
                                ; LOE eax edx ecx ebx esi edi
.B2.27:                         ; Preds .B2.26
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;129.10
                                ; LOE eax edx ecx ebx esi
.B2.28:                         ; Preds .B2.27 .B2.286
        lea       edi, DWORD PTR [-1+edx]                       ;129.10
        cmp       eax, edi                                      ;129.10
        jbe       .B2.30        ; Prob 10%                      ;129.10
                                ; LOE eax edx ecx ebx esi
.B2.29:                         ; Preds .B2.28
        mov       edi, ebx                                      ;129.10
        add       edx, ebx                                      ;129.10
        neg       edi                                           ;129.10
        add       edi, edx                                      ;129.10
        shl       edi, 8                                        ;129.10
        mov       DWORD PTR [-240+ecx+edi], 0                   ;129.10
                                ; LOE eax ecx ebx esi
.B2.30:                         ; Preds .B2.28 .B2.29
        test      esi, esi                                      ;130.10
        jbe       .B2.285       ; Prob 10%                      ;130.10
                                ; LOE eax ecx ebx esi
.B2.31:                         ; Preds .B2.30
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.32:                         ; Preds .B2.32 .B2.31
        mov       edi, edx                                      ;130.10
        inc       edx                                           ;130.10
        shl       edi, 9                                        ;130.10
        cmp       edx, esi                                      ;130.10
        mov       DWORD PTR [20+ecx+edi], ebx                   ;130.10
        mov       DWORD PTR [276+ecx+edi], ebx                  ;130.10
        jb        .B2.32        ; Prob 63%                      ;130.10
                                ; LOE eax edx ecx ebx esi
.B2.33:                         ; Preds .B2.32
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;130.10
                                ; LOE eax edx ecx ebx esi
.B2.34:                         ; Preds .B2.33 .B2.285
        lea       edi, DWORD PTR [-1+edx]                       ;130.10
        cmp       eax, edi                                      ;130.10
        jbe       .B2.36        ; Prob 10%                      ;130.10
                                ; LOE eax edx ecx ebx esi
.B2.35:                         ; Preds .B2.34
        mov       edi, ebx                                      ;130.10
        add       edx, ebx                                      ;130.10
        neg       edi                                           ;130.10
        add       edi, edx                                      ;130.10
        shl       edi, 8                                        ;130.10
        mov       DWORD PTR [-236+ecx+edi], 0                   ;130.10
                                ; LOE eax ecx ebx esi
.B2.36:                         ; Preds .B2.34 .B2.35
        test      esi, esi                                      ;131.10
        jbe       .B2.284       ; Prob 10%                      ;131.10
                                ; LOE eax ecx ebx esi
.B2.37:                         ; Preds .B2.36
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.38:                         ; Preds .B2.38 .B2.37
        mov       edi, edx                                      ;131.10
        inc       edx                                           ;131.10
        shl       edi, 9                                        ;131.10
        cmp       edx, esi                                      ;131.10
        mov       BYTE PTR [24+ecx+edi], bl                     ;131.10
        mov       BYTE PTR [280+ecx+edi], bl                    ;131.10
        jb        .B2.38        ; Prob 63%                      ;131.10
                                ; LOE eax edx ecx ebx esi
.B2.39:                         ; Preds .B2.38
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;131.10
                                ; LOE eax edx ecx ebx esi
.B2.40:                         ; Preds .B2.39 .B2.284
        lea       edi, DWORD PTR [-1+edx]                       ;131.10
        cmp       eax, edi                                      ;131.10
        jbe       .B2.42        ; Prob 10%                      ;131.10
                                ; LOE eax edx ecx ebx esi
.B2.41:                         ; Preds .B2.40
        mov       edi, ebx                                      ;131.10
        add       edx, ebx                                      ;131.10
        neg       edi                                           ;131.10
        add       edi, edx                                      ;131.10
        shl       edi, 8                                        ;131.10
        mov       BYTE PTR [-232+ecx+edi], 0                    ;131.10
                                ; LOE eax ecx ebx esi
.B2.42:                         ; Preds .B2.40 .B2.41
        test      esi, esi                                      ;132.10
        jbe       .B2.283       ; Prob 10%                      ;132.10
                                ; LOE eax ecx ebx esi
.B2.43:                         ; Preds .B2.42
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.44:                         ; Preds .B2.44 .B2.43
        inc       edx                                           ;132.10
        mov       DWORD PTR [28+edi+ecx], ebx                   ;132.10
        mov       DWORD PTR [284+edi+ecx], ebx                  ;132.10
        add       edi, 512                                      ;132.10
        cmp       edx, esi                                      ;132.10
        jb        .B2.44        ; Prob 63%                      ;132.10
                                ; LOE eax edx ecx ebx esi edi
.B2.45:                         ; Preds .B2.44
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;132.10
                                ; LOE eax edx ecx ebx esi
.B2.46:                         ; Preds .B2.45 .B2.283
        lea       edi, DWORD PTR [-1+edx]                       ;132.10
        cmp       eax, edi                                      ;132.10
        jbe       .B2.48        ; Prob 10%                      ;132.10
                                ; LOE eax edx ecx ebx esi
.B2.47:                         ; Preds .B2.46
        mov       edi, ebx                                      ;132.10
        add       edx, ebx                                      ;132.10
        neg       edi                                           ;132.10
        add       edi, edx                                      ;132.10
        shl       edi, 8                                        ;132.10
        mov       DWORD PTR [-228+ecx+edi], 0                   ;132.10
                                ; LOE eax ecx ebx esi
.B2.48:                         ; Preds .B2.46 .B2.47
        test      esi, esi                                      ;133.10
        jbe       .B2.282       ; Prob 10%                      ;133.10
                                ; LOE eax ecx ebx esi
.B2.49:                         ; Preds .B2.48
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.50:                         ; Preds .B2.50 .B2.49
        inc       edx                                           ;133.10
        mov       DWORD PTR [32+edi+ecx], ebx                   ;133.10
        mov       DWORD PTR [288+edi+ecx], ebx                  ;133.10
        add       edi, 512                                      ;133.10
        cmp       edx, esi                                      ;133.10
        jb        .B2.50        ; Prob 63%                      ;133.10
                                ; LOE eax edx ecx ebx esi edi
.B2.51:                         ; Preds .B2.50
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;133.10
                                ; LOE eax edx ecx ebx esi
.B2.52:                         ; Preds .B2.51 .B2.282
        lea       edi, DWORD PTR [-1+edx]                       ;133.10
        cmp       eax, edi                                      ;133.10
        jbe       .B2.54        ; Prob 10%                      ;133.10
                                ; LOE eax edx ecx ebx esi
.B2.53:                         ; Preds .B2.52
        mov       edi, ebx                                      ;133.10
        add       edx, ebx                                      ;133.10
        neg       edi                                           ;133.10
        add       edi, edx                                      ;133.10
        shl       edi, 8                                        ;133.10
        mov       DWORD PTR [-224+ecx+edi], 0                   ;133.10
                                ; LOE eax ecx ebx esi
.B2.54:                         ; Preds .B2.52 .B2.53
        test      esi, esi                                      ;134.10
        jbe       .B2.281       ; Prob 10%                      ;134.10
                                ; LOE eax ecx ebx esi
.B2.55:                         ; Preds .B2.54
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.56:                         ; Preds .B2.56 .B2.55
        mov       edi, edx                                      ;134.10
        inc       edx                                           ;134.10
        shl       edi, 9                                        ;134.10
        cmp       edx, esi                                      ;134.10
        mov       DWORD PTR [36+ecx+edi], ebx                   ;134.10
        mov       DWORD PTR [292+ecx+edi], ebx                  ;134.10
        jb        .B2.56        ; Prob 63%                      ;134.10
                                ; LOE eax edx ecx ebx esi
.B2.57:                         ; Preds .B2.56
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;134.10
                                ; LOE eax edx ecx ebx esi
.B2.58:                         ; Preds .B2.57 .B2.281
        lea       edi, DWORD PTR [-1+edx]                       ;134.10
        cmp       eax, edi                                      ;134.10
        jbe       .B2.60        ; Prob 10%                      ;134.10
                                ; LOE eax edx ecx ebx esi
.B2.59:                         ; Preds .B2.58
        mov       edi, ebx                                      ;134.10
        add       edx, ebx                                      ;134.10
        neg       edi                                           ;134.10
        add       edi, edx                                      ;134.10
        shl       edi, 8                                        ;134.10
        mov       DWORD PTR [-220+ecx+edi], 0                   ;134.10
                                ; LOE eax ecx ebx esi
.B2.60:                         ; Preds .B2.58 .B2.59
        test      esi, esi                                      ;135.10
        jbe       .B2.280       ; Prob 10%                      ;135.10
                                ; LOE eax ecx ebx esi
.B2.61:                         ; Preds .B2.60
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edx, edx                                      ;
        mov       ebx, 1                                        ;
                                ; LOE eax edx ecx ebx esi
.B2.62:                         ; Preds .B2.62 .B2.61
        mov       edi, edx                                      ;135.10
        inc       edx                                           ;135.10
        shl       edi, 9                                        ;135.10
        cmp       edx, esi                                      ;135.10
        mov       BYTE PTR [40+ecx+edi], bl                     ;135.10
        mov       BYTE PTR [296+ecx+edi], bl                    ;135.10
        jb        .B2.62        ; Prob 63%                      ;135.10
                                ; LOE eax edx ecx ebx esi
.B2.63:                         ; Preds .B2.62
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       esi, DWORD PTR [1+edx+edx]                    ;135.10
                                ; LOE eax ecx ebx esi
.B2.64:                         ; Preds .B2.63 .B2.280
        lea       edx, DWORD PTR [-1+esi]                       ;135.10
        cmp       eax, edx                                      ;135.10
        jbe       .B2.66        ; Prob 10%                      ;135.10
                                ; LOE eax ecx ebx esi
.B2.65:                         ; Preds .B2.64
        mov       edx, ebx                                      ;135.10
        add       esi, ebx                                      ;135.10
        neg       edx                                           ;135.10
        add       edx, esi                                      ;135.10
        shl       edx, 8                                        ;135.10
        mov       BYTE PTR [-216+ecx+edx], 1                    ;135.10
                                ; LOE eax ebx
.B2.66:                         ; Preds .B2.17 .B2.64 .B2.65
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;136.10
        test      edx, edx                                      ;136.10
        mov       DWORD PTR [4+esp], edx                        ;136.10
        jle       .B2.78        ; Prob 29%                      ;136.10
                                ; LOE eax ebx
.B2.67:                         ; Preds .B2.66
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;137.14
        xor       edx, edx                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;142.14
        mov       eax, ecx                                      ;137.14
        xor       esi, esi                                      ;
        pxor      xmm0, xmm0                                    ;142.14
        mov       edx, DWORD PTR [esp]                          ;142.14
                                ; LOE eax edx ebx esi
.B2.68:                         ; Preds .B2.475 .B2.67
        shl       edx, 8                                        ;137.14
        mov       edi, esi                                      ;137.14
        shl       edi, 8                                        ;137.14
        sub       eax, edx                                      ;137.14
        shl       ebx, 8                                        ;137.14
        neg       ebx                                           ;137.14
        lea       edx, DWORD PTR [300+eax+edi]                  ;137.14
        mov       eax, DWORD PTR [8+esp]                        ;137.14
        add       eax, ebx                                      ;137.14
        mov       ebx, DWORD PTR [312+edi+eax]                  ;137.14
        and       ebx, 1                                        ;137.14
        add       ebx, ebx                                      ;137.14
        or        ebx, 262145                                   ;137.14
        push      ebx                                           ;137.14
        push      edx                                           ;137.14
        push      2                                             ;137.14
        call      _for_alloc_allocatable                        ;137.14
                                ; LOE eax esi edi
.B2.444:                        ; Preds .B2.68
        add       esp, 12                                       ;137.14
                                ; LOE eax esi edi
.B2.69:                         ; Preds .B2.444
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;137.14
        test      eax, eax                                      ;137.14
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;137.14
        mov       DWORD PTR [8+esp], edx                        ;137.14
        jne       .B2.71        ; Prob 50%                      ;137.14
                                ; LOE eax ebx esi edi
.B2.70:                         ; Preds .B2.69
        mov       ecx, ebx                                      ;137.14
        mov       edx, 1                                        ;137.14
        shl       ecx, 8                                        ;137.14
        sub       edi, ecx                                      ;137.14
        mov       ecx, 2                                        ;137.14
        add       edi, DWORD PTR [8+esp]                        ;137.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;137.14
        mov       DWORD PTR [312+edi], 133                      ;137.14
        mov       DWORD PTR [304+edi], edx                      ;137.14
        mov       DWORD PTR [316+edi], edx                      ;137.14
        mov       DWORD PTR [308+edi], 0                        ;137.14
        mov       DWORD PTR [332+edi], edx                      ;137.14
        mov       DWORD PTR [324+edi], 2                        ;137.14
        mov       DWORD PTR [328+edi], edx                      ;137.14
        jmp       .B2.74        ; Prob 100%                     ;137.14
                                ; LOE ecx ebx esi edi
.B2.71:                         ; Preds .B2.69
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;137.14
        lea       edx, DWORD PTR [64+esp]                       ;139.18
        mov       DWORD PTR [64+esp], 0                         ;139.18
        lea       eax, DWORD PTR [104+esp]                      ;139.18
        mov       DWORD PTR [104+esp], 58                       ;139.18
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_12 ;139.18
        push      32                                            ;139.18
        push      eax                                           ;139.18
        push      OFFSET FLAT: __STRLITPACK_18.0.2              ;139.18
        push      -2088435968                                   ;139.18
        push      911                                           ;139.18
        push      edx                                           ;139.18
        call      _for_write_seq_lis                            ;139.18
                                ; LOE ebx esi edi
.B2.72:                         ; Preds .B2.71
        push      32                                            ;140.18
        xor       eax, eax                                      ;140.18
        push      eax                                           ;140.18
        push      eax                                           ;140.18
        push      -2088435968                                   ;140.18
        push      eax                                           ;140.18
        push      OFFSET FLAT: __STRLITPACK_19                  ;140.18
        call      _for_stop_core                                ;140.18
                                ; LOE ebx esi edi
.B2.445:                        ; Preds .B2.72
        add       esp, 48                                       ;140.18
                                ; LOE ebx esi edi
.B2.73:                         ; Preds .B2.445
        mov       eax, ebx                                      ;
        shl       eax, 8                                        ;
        sub       edi, eax                                      ;
        add       edi, DWORD PTR [8+esp]                        ;
        mov       ecx, DWORD PTR [324+edi]                      ;142.14
        test      ecx, ecx                                      ;142.14
        jle       .B2.76        ; Prob 50%                      ;142.14
                                ; LOE ecx ebx esi edi
.B2.74:                         ; Preds .B2.70 .B2.73
        cmp       ecx, 96                                       ;142.14
        jle       .B2.288       ; Prob 0%                       ;142.14
                                ; LOE ecx ebx esi edi
.B2.75:                         ; Preds .B2.74
        push      ecx                                           ;142.14
        push      0                                             ;142.14
        push      DWORD PTR [300+edi]                           ;142.14
        call      __intel_fast_memset                           ;142.14
                                ; LOE ebx esi
.B2.446:                        ; Preds .B2.75
        add       esp, 12                                       ;142.14
                                ; LOE ebx esi
.B2.76:                         ; Preds .B2.294 .B2.73 .B2.292 .B2.446
        inc       esi                                           ;136.10
        cmp       esi, DWORD PTR [4+esp]                        ;136.10
        jae       .B2.77        ; Prob 18%                      ;136.10
                                ; LOE ebx esi
.B2.475:                        ; Preds .B2.76
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;122.13
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;122.13
        jmp       .B2.68        ; Prob 100%                     ;122.13
                                ; LOE eax edx ebx esi
.B2.77:                         ; Preds .B2.76
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;154.10
        mov       DWORD PTR [4+esp], eax                        ;154.10
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+24] ;144.10
                                ; LOE eax ebx
.B2.78:                         ; Preds .B2.77 .B2.66
        test      eax, eax                                      ;144.10
        jle       .B2.133       ; Prob 50%                      ;144.10
                                ; LOE eax ebx
.B2.79:                         ; Preds .B2.78
        mov       esi, eax                                      ;144.10
        shr       esi, 31                                       ;144.10
        add       esi, eax                                      ;144.10
        sar       esi, 1                                        ;144.10
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;144.10
        test      esi, esi                                      ;144.10
        jbe       .B2.305       ; Prob 10%                      ;144.10
                                ; LOE eax ecx ebx esi
.B2.80:                         ; Preds .B2.79
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edx, edx                                      ;
        mov       ebx, 1                                        ;
                                ; LOE eax edx ecx ebx esi
.B2.81:                         ; Preds .B2.81 .B2.80
        mov       edi, edx                                      ;144.10
        inc       edx                                           ;144.10
        shl       edi, 9                                        ;144.10
        cmp       edx, esi                                      ;144.10
        mov       BYTE PTR [80+ecx+edi], bl                     ;144.10
        mov       BYTE PTR [336+ecx+edi], bl                    ;144.10
        jb        .B2.81        ; Prob 63%                      ;144.10
                                ; LOE eax edx ecx ebx esi
.B2.82:                         ; Preds .B2.81
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;144.10
                                ; LOE eax edx ecx ebx esi
.B2.83:                         ; Preds .B2.82 .B2.305
        lea       edi, DWORD PTR [-1+edx]                       ;144.10
        cmp       eax, edi                                      ;144.10
        jbe       .B2.85        ; Prob 10%                      ;144.10
                                ; LOE eax edx ecx ebx esi
.B2.84:                         ; Preds .B2.83
        mov       edi, ebx                                      ;144.10
        add       edx, ebx                                      ;144.10
        neg       edi                                           ;144.10
        add       edi, edx                                      ;144.10
        shl       edi, 8                                        ;144.10
        mov       BYTE PTR [-176+ecx+edi], 1                    ;144.10
                                ; LOE eax ecx ebx esi
.B2.85:                         ; Preds .B2.83 .B2.84
        test      esi, esi                                      ;145.10
        jbe       .B2.304       ; Prob 10%                      ;145.10
                                ; LOE eax ecx ebx esi
.B2.86:                         ; Preds .B2.85
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.87:                         ; Preds .B2.87 .B2.86
        inc       edx                                           ;145.10
        mov       DWORD PTR [84+edi+ecx], ebx                   ;145.10
        mov       DWORD PTR [340+edi+ecx], ebx                  ;145.10
        add       edi, 512                                      ;145.10
        cmp       edx, esi                                      ;145.10
        jb        .B2.87        ; Prob 63%                      ;145.10
                                ; LOE eax edx ecx ebx esi edi
.B2.88:                         ; Preds .B2.87
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;145.10
                                ; LOE eax edx ecx ebx esi
.B2.89:                         ; Preds .B2.88 .B2.304
        lea       edi, DWORD PTR [-1+edx]                       ;145.10
        cmp       eax, edi                                      ;145.10
        jbe       .B2.91        ; Prob 10%                      ;145.10
                                ; LOE eax edx ecx ebx esi
.B2.90:                         ; Preds .B2.89
        mov       edi, ebx                                      ;145.10
        add       edx, ebx                                      ;145.10
        neg       edi                                           ;145.10
        add       edi, edx                                      ;145.10
        shl       edi, 8                                        ;145.10
        mov       DWORD PTR [-172+ecx+edi], 0                   ;145.10
                                ; LOE eax ecx ebx esi
.B2.91:                         ; Preds .B2.89 .B2.90
        test      esi, esi                                      ;146.10
        jbe       .B2.303       ; Prob 10%                      ;146.10
                                ; LOE eax ecx ebx esi
.B2.92:                         ; Preds .B2.91
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.93:                         ; Preds .B2.93 .B2.92
        inc       edx                                           ;146.10
        mov       DWORD PTR [88+edi+ecx], ebx                   ;146.10
        mov       DWORD PTR [344+edi+ecx], ebx                  ;146.10
        add       edi, 512                                      ;146.10
        cmp       edx, esi                                      ;146.10
        jb        .B2.93        ; Prob 63%                      ;146.10
                                ; LOE eax edx ecx ebx esi edi
.B2.94:                         ; Preds .B2.93
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;146.10
                                ; LOE eax edx ecx ebx esi
.B2.95:                         ; Preds .B2.94 .B2.303
        lea       edi, DWORD PTR [-1+edx]                       ;146.10
        cmp       eax, edi                                      ;146.10
        jbe       .B2.97        ; Prob 10%                      ;146.10
                                ; LOE eax edx ecx ebx esi
.B2.96:                         ; Preds .B2.95
        mov       edi, ebx                                      ;146.10
        add       edx, ebx                                      ;146.10
        neg       edi                                           ;146.10
        add       edi, edx                                      ;146.10
        shl       edi, 8                                        ;146.10
        mov       DWORD PTR [-168+ecx+edi], 0                   ;146.10
                                ; LOE eax ecx ebx esi
.B2.97:                         ; Preds .B2.95 .B2.96
        test      esi, esi                                      ;147.10
        jbe       .B2.302       ; Prob 10%                      ;147.10
                                ; LOE eax ecx ebx esi
.B2.98:                         ; Preds .B2.97
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.99:                         ; Preds .B2.99 .B2.98
        inc       edx                                           ;147.10
        mov       DWORD PTR [92+edi+ecx], ebx                   ;147.10
        mov       DWORD PTR [348+edi+ecx], ebx                  ;147.10
        add       edi, 512                                      ;147.10
        cmp       edx, esi                                      ;147.10
        jb        .B2.99        ; Prob 63%                      ;147.10
                                ; LOE eax edx ecx ebx esi edi
.B2.100:                        ; Preds .B2.99
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;147.10
                                ; LOE eax edx ecx ebx esi
.B2.101:                        ; Preds .B2.100 .B2.302
        lea       edi, DWORD PTR [-1+edx]                       ;147.10
        cmp       eax, edi                                      ;147.10
        jbe       .B2.103       ; Prob 10%                      ;147.10
                                ; LOE eax edx ecx ebx esi
.B2.102:                        ; Preds .B2.101
        mov       edi, ebx                                      ;147.10
        add       edx, ebx                                      ;147.10
        neg       edi                                           ;147.10
        add       edi, edx                                      ;147.10
        shl       edi, 8                                        ;147.10
        mov       DWORD PTR [-164+ecx+edi], 0                   ;147.10
                                ; LOE eax ecx ebx esi
.B2.103:                        ; Preds .B2.101 .B2.102
        test      esi, esi                                      ;148.10
        jbe       .B2.301       ; Prob 10%                      ;148.10
                                ; LOE eax ecx ebx esi
.B2.104:                        ; Preds .B2.103
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.105:                        ; Preds .B2.105 .B2.104
        mov       edi, edx                                      ;148.10
        inc       edx                                           ;148.10
        shl       edi, 9                                        ;148.10
        cmp       edx, esi                                      ;148.10
        mov       BYTE PTR [96+ecx+edi], bl                     ;148.10
        mov       BYTE PTR [352+ecx+edi], bl                    ;148.10
        jb        .B2.105       ; Prob 63%                      ;148.10
                                ; LOE eax edx ecx ebx esi
.B2.106:                        ; Preds .B2.105
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;148.10
                                ; LOE eax edx ecx ebx esi
.B2.107:                        ; Preds .B2.106 .B2.301
        lea       edi, DWORD PTR [-1+edx]                       ;148.10
        cmp       eax, edi                                      ;148.10
        jbe       .B2.109       ; Prob 10%                      ;148.10
                                ; LOE eax edx ecx ebx esi
.B2.108:                        ; Preds .B2.107
        mov       edi, ebx                                      ;148.10
        add       edx, ebx                                      ;148.10
        neg       edi                                           ;148.10
        add       edi, edx                                      ;148.10
        shl       edi, 8                                        ;148.10
        mov       BYTE PTR [-160+ecx+edi], 0                    ;148.10
                                ; LOE eax ecx ebx esi
.B2.109:                        ; Preds .B2.107 .B2.108
        test      esi, esi                                      ;149.10
        jbe       .B2.300       ; Prob 10%                      ;149.10
                                ; LOE eax ecx ebx esi
.B2.110:                        ; Preds .B2.109
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.111:                        ; Preds .B2.111 .B2.110
        inc       edx                                           ;149.10
        mov       DWORD PTR [100+edi+ecx], ebx                  ;149.10
        mov       DWORD PTR [356+edi+ecx], ebx                  ;149.10
        add       edi, 512                                      ;149.10
        cmp       edx, esi                                      ;149.10
        jb        .B2.111       ; Prob 63%                      ;149.10
                                ; LOE eax edx ecx ebx esi edi
.B2.112:                        ; Preds .B2.111
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;149.10
                                ; LOE eax edx ecx ebx esi
.B2.113:                        ; Preds .B2.112 .B2.300
        lea       edi, DWORD PTR [-1+edx]                       ;149.10
        cmp       eax, edi                                      ;149.10
        jbe       .B2.115       ; Prob 10%                      ;149.10
                                ; LOE eax edx ecx ebx esi
.B2.114:                        ; Preds .B2.113
        mov       edi, ebx                                      ;149.10
        add       edx, ebx                                      ;149.10
        neg       edi                                           ;149.10
        add       edi, edx                                      ;149.10
        shl       edi, 8                                        ;149.10
        mov       DWORD PTR [-156+ecx+edi], 0                   ;149.10
                                ; LOE eax ecx ebx esi
.B2.115:                        ; Preds .B2.113 .B2.114
        test      esi, esi                                      ;150.10
        jbe       .B2.299       ; Prob 10%                      ;150.10
                                ; LOE eax ecx ebx esi
.B2.116:                        ; Preds .B2.115
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.117:                        ; Preds .B2.117 .B2.116
        inc       edx                                           ;150.10
        mov       DWORD PTR [104+edi+ecx], ebx                  ;150.10
        mov       DWORD PTR [360+edi+ecx], ebx                  ;150.10
        add       edi, 512                                      ;150.10
        cmp       edx, esi                                      ;150.10
        jb        .B2.117       ; Prob 63%                      ;150.10
                                ; LOE eax edx ecx ebx esi edi
.B2.118:                        ; Preds .B2.117
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;150.10
                                ; LOE eax edx ecx ebx esi
.B2.119:                        ; Preds .B2.118 .B2.299
        lea       edi, DWORD PTR [-1+edx]                       ;150.10
        cmp       eax, edi                                      ;150.10
        jbe       .B2.121       ; Prob 10%                      ;150.10
                                ; LOE eax edx ecx ebx esi
.B2.120:                        ; Preds .B2.119
        mov       edi, ebx                                      ;150.10
        add       edx, ebx                                      ;150.10
        neg       edi                                           ;150.10
        add       edi, edx                                      ;150.10
        shl       edi, 8                                        ;150.10
        mov       DWORD PTR [-152+ecx+edi], 0                   ;150.10
                                ; LOE eax ecx ebx esi
.B2.121:                        ; Preds .B2.119 .B2.120
        test      esi, esi                                      ;151.10
        jbe       .B2.298       ; Prob 10%                      ;151.10
                                ; LOE eax ecx ebx esi
.B2.122:                        ; Preds .B2.121
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.123:                        ; Preds .B2.123 .B2.122
        inc       edx                                           ;151.10
        mov       DWORD PTR [108+edi+ecx], ebx                  ;151.10
        mov       DWORD PTR [364+edi+ecx], ebx                  ;151.10
        add       edi, 512                                      ;151.10
        cmp       edx, esi                                      ;151.10
        jb        .B2.123       ; Prob 63%                      ;151.10
                                ; LOE eax edx ecx ebx esi edi
.B2.124:                        ; Preds .B2.123
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;151.10
                                ; LOE eax edx ecx ebx esi
.B2.125:                        ; Preds .B2.124 .B2.298
        lea       edi, DWORD PTR [-1+edx]                       ;151.10
        cmp       eax, edi                                      ;151.10
        jbe       .B2.127       ; Prob 10%                      ;151.10
                                ; LOE eax edx ecx ebx esi
.B2.126:                        ; Preds .B2.125
        mov       edi, ebx                                      ;151.10
        add       edx, ebx                                      ;151.10
        neg       edi                                           ;151.10
        add       edi, edx                                      ;151.10
        shl       edi, 8                                        ;151.10
        mov       DWORD PTR [-148+ecx+edi], 0                   ;151.10
                                ; LOE eax ecx ebx esi
.B2.127:                        ; Preds .B2.125 .B2.126
        test      esi, esi                                      ;152.10
        jbe       .B2.297       ; Prob 10%                      ;152.10
                                ; LOE eax ecx ebx esi
.B2.128:                        ; Preds .B2.127
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.129:                        ; Preds .B2.129 .B2.128
        mov       edi, edx                                      ;152.10
        inc       edx                                           ;152.10
        shl       edi, 9                                        ;152.10
        cmp       edx, esi                                      ;152.10
        mov       DWORD PTR [112+ecx+edi], ebx                  ;152.10
        mov       DWORD PTR [368+ecx+edi], ebx                  ;152.10
        jb        .B2.129       ; Prob 63%                      ;152.10
                                ; LOE eax edx ecx ebx esi
.B2.130:                        ; Preds .B2.129
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       esi, DWORD PTR [1+edx+edx]                    ;152.10
                                ; LOE eax ecx ebx esi
.B2.131:                        ; Preds .B2.130 .B2.297
        lea       edx, DWORD PTR [-1+esi]                       ;152.10
        cmp       eax, edx                                      ;152.10
        jbe       .B2.133       ; Prob 10%                      ;152.10
                                ; LOE eax ecx ebx esi
.B2.132:                        ; Preds .B2.131
        mov       edx, ebx                                      ;152.10
        add       esi, ebx                                      ;152.10
        neg       edx                                           ;152.10
        add       edx, esi                                      ;152.10
        shl       edx, 8                                        ;152.10
        mov       DWORD PTR [-144+ecx+edx], 0                   ;152.10
                                ; LOE eax ebx
.B2.133:                        ; Preds .B2.78 .B2.131 .B2.132
        cmp       DWORD PTR [4+esp], 0                          ;154.10
        jle       .B2.145       ; Prob 29%                      ;154.10
                                ; LOE eax ebx
.B2.134:                        ; Preds .B2.133
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;155.14
        xor       edx, edx                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;160.14
        mov       eax, ecx                                      ;155.14
        xor       esi, esi                                      ;
        pxor      xmm0, xmm0                                    ;160.14
                                ; LOE eax ebx esi
.B2.135:                        ; Preds .B2.474 .B2.134
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;155.14
        mov       edi, esi                                      ;155.14
        shl       edx, 8                                        ;155.14
        shl       edi, 8                                        ;155.14
        sub       eax, edx                                      ;155.14
        shl       ebx, 8                                        ;155.14
        neg       ebx                                           ;155.14
        lea       ecx, DWORD PTR [372+eax+edi]                  ;155.14
        mov       eax, DWORD PTR [8+esp]                        ;155.14
        add       eax, ebx                                      ;155.14
        mov       ebx, DWORD PTR [384+edi+eax]                  ;155.14
        and       ebx, 1                                        ;155.14
        add       ebx, ebx                                      ;155.14
        or        ebx, 262145                                   ;155.14
        push      ebx                                           ;155.14
        push      ecx                                           ;155.14
        push      8                                             ;155.14
        call      _for_alloc_allocatable                        ;155.14
                                ; LOE eax esi edi
.B2.447:                        ; Preds .B2.135
        add       esp, 12                                       ;155.14
                                ; LOE eax esi edi
.B2.136:                        ; Preds .B2.447
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;155.14
        test      eax, eax                                      ;155.14
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;155.14
        mov       DWORD PTR [8+esp], edx                        ;155.14
        jne       .B2.138       ; Prob 50%                      ;155.14
                                ; LOE eax ebx esi edi
.B2.137:                        ; Preds .B2.136
        mov       ecx, ebx                                      ;155.14
        mov       edx, 4                                        ;155.14
        shl       ecx, 8                                        ;155.14
        sub       edi, ecx                                      ;155.14
        mov       ecx, 1                                        ;155.14
        add       edi, DWORD PTR [8+esp]                        ;155.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;155.14
        mov       DWORD PTR [384+edi], 133                      ;155.14
        mov       DWORD PTR [376+edi], edx                      ;155.14
        mov       DWORD PTR [388+edi], ecx                      ;155.14
        mov       DWORD PTR [380+edi], 0                        ;155.14
        mov       DWORD PTR [404+edi], ecx                      ;155.14
        mov       ecx, 2                                        ;155.14
        mov       DWORD PTR [396+edi], 2                        ;155.14
        mov       DWORD PTR [400+edi], edx                      ;155.14
        jmp       .B2.141       ; Prob 100%                     ;155.14
                                ; LOE ecx ebx esi edi
.B2.138:                        ; Preds .B2.136
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;155.14
        lea       edx, DWORD PTR [64+esp]                       ;157.18
        mov       DWORD PTR [64+esp], 0                         ;157.18
        lea       eax, DWORD PTR [112+esp]                      ;157.18
        mov       DWORD PTR [112+esp], 57                       ;157.18
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_10 ;157.18
        push      32                                            ;157.18
        push      eax                                           ;157.18
        push      OFFSET FLAT: __STRLITPACK_20.0.2              ;157.18
        push      -2088435968                                   ;157.18
        push      911                                           ;157.18
        push      edx                                           ;157.18
        call      _for_write_seq_lis                            ;157.18
                                ; LOE ebx esi edi
.B2.139:                        ; Preds .B2.138
        push      32                                            ;158.18
        xor       eax, eax                                      ;158.18
        push      eax                                           ;158.18
        push      eax                                           ;158.18
        push      -2088435968                                   ;158.18
        push      eax                                           ;158.18
        push      OFFSET FLAT: __STRLITPACK_21                  ;158.18
        call      _for_stop_core                                ;158.18
                                ; LOE ebx esi edi
.B2.448:                        ; Preds .B2.139
        add       esp, 48                                       ;158.18
                                ; LOE ebx esi edi
.B2.140:                        ; Preds .B2.448
        mov       eax, ebx                                      ;
        shl       eax, 8                                        ;
        sub       edi, eax                                      ;
        add       edi, DWORD PTR [8+esp]                        ;
        mov       ecx, DWORD PTR [396+edi]                      ;160.14
        test      ecx, ecx                                      ;160.14
        jle       .B2.143       ; Prob 50%                      ;160.14
                                ; LOE ecx ebx esi edi
.B2.141:                        ; Preds .B2.137 .B2.140
        cmp       ecx, 24                                       ;160.14
        jle       .B2.306       ; Prob 0%                       ;160.14
                                ; LOE ecx ebx esi edi
.B2.142:                        ; Preds .B2.141
        shl       ecx, 2                                        ;160.14
        push      ecx                                           ;160.14
        push      0                                             ;160.14
        push      DWORD PTR [372+edi]                           ;160.14
        call      __intel_fast_memset                           ;160.14
                                ; LOE ebx esi
.B2.449:                        ; Preds .B2.142
        add       esp, 12                                       ;160.14
                                ; LOE ebx esi
.B2.143:                        ; Preds .B2.312 .B2.449 .B2.140 .B2.310
        inc       esi                                           ;154.10
        cmp       esi, DWORD PTR [4+esp]                        ;154.10
        jae       .B2.144       ; Prob 18%                      ;154.10
                                ; LOE ebx esi
.B2.474:                        ; Preds .B2.143
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;122.13
        jmp       .B2.135       ; Prob 100%                     ;122.13
                                ; LOE eax ebx esi
.B2.144:                        ; Preds .B2.143
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;165.13
        mov       DWORD PTR [4+esp], eax                        ;165.13
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+24] ;162.10
                                ; LOE eax ebx
.B2.145:                        ; Preds .B2.144 .B2.133
        test      eax, eax                                      ;162.10
        jle       .B2.164       ; Prob 50%                      ;162.10
                                ; LOE eax ebx
.B2.146:                        ; Preds .B2.145
        mov       esi, eax                                      ;162.10
        shr       esi, 31                                       ;162.10
        add       esi, eax                                      ;162.10
        sar       esi, 1                                        ;162.10
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;162.10
        test      esi, esi                                      ;162.10
        jbe       .B2.318       ; Prob 10%                      ;162.10
                                ; LOE eax ecx ebx esi
.B2.147:                        ; Preds .B2.146
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.148:                        ; Preds .B2.148 .B2.147
        inc       edx                                           ;162.10
        mov       DWORD PTR [152+edi+ecx], ebx                  ;162.10
        mov       DWORD PTR [408+edi+ecx], ebx                  ;162.10
        add       edi, 512                                      ;162.10
        cmp       edx, esi                                      ;162.10
        jb        .B2.148       ; Prob 63%                      ;162.10
                                ; LOE eax edx ecx ebx esi edi
.B2.149:                        ; Preds .B2.148
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;162.10
                                ; LOE eax edx ecx ebx esi
.B2.150:                        ; Preds .B2.149 .B2.318
        lea       edi, DWORD PTR [-1+edx]                       ;162.10
        cmp       eax, edi                                      ;162.10
        jbe       .B2.317       ; Prob 10%                      ;162.10
                                ; LOE eax edx ecx ebx esi
.B2.151:                        ; Preds .B2.150
        movss     xmm3, DWORD PTR [_2il0floatpacket.0]          ;163.35
        mov       edi, ebx                                      ;162.10
        mulss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VHPCEFACTOR] ;163.35
        neg       edi                                           ;162.10
        movss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;163.35
        add       edx, ebx                                      ;162.10
        andps     xmm0, xmm3                                    ;163.35
        add       edi, edx                                      ;162.10
        pxor      xmm3, xmm0                                    ;163.35
        movss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;163.35
        movaps    xmm2, xmm3                                    ;163.35
        movss     xmm4, DWORD PTR [_2il0floatpacket.4]          ;163.35
        cmpltss   xmm2, xmm1                                    ;163.35
        andps     xmm1, xmm2                                    ;163.35
        movaps    xmm2, xmm3                                    ;163.35
        movaps    xmm6, xmm4                                    ;163.35
        addss     xmm2, xmm1                                    ;163.35
        addss     xmm6, xmm4                                    ;163.35
        subss     xmm2, xmm1                                    ;163.35
        movaps    xmm7, xmm2                                    ;163.35
        shl       edi, 8                                        ;162.10
        subss     xmm7, xmm3                                    ;163.35
        movaps    xmm5, xmm7                                    ;163.35
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.5]          ;163.35
        cmpnless  xmm5, xmm4                                    ;163.35
        andps     xmm5, xmm6                                    ;163.35
        andps     xmm7, xmm6                                    ;163.35
        mov       DWORD PTR [-104+ecx+edi], 0                   ;162.10
        subss     xmm2, xmm5                                    ;163.35
        addss     xmm2, xmm7                                    ;163.35
        orps      xmm2, xmm0                                    ;163.35
        cvtss2si  edx, xmm2                                     ;163.35
        cvtsi2ss  xmm0, edx                                     ;163.13
                                ; LOE eax ecx ebx esi xmm0
.B2.152:                        ; Preds .B2.151 .B2.317
        test      esi, esi                                      ;163.13
        jbe       .B2.316       ; Prob 10%                      ;163.13
                                ; LOE eax ecx ebx esi xmm0
.B2.153:                        ; Preds .B2.152
        xor       edx, edx                                      ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.154:                        ; Preds .B2.154 .B2.153
        inc       edx                                           ;163.13
        movss     DWORD PTR [156+edi+ecx], xmm0                 ;163.13
        movss     DWORD PTR [412+edi+ecx], xmm0                 ;163.13
        add       edi, 512                                      ;163.13
        cmp       edx, esi                                      ;163.13
        jb        .B2.154       ; Prob 63%                      ;163.13
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.155:                        ; Preds .B2.154
        lea       edx, DWORD PTR [1+edx+edx]                    ;163.13
                                ; LOE eax edx ecx ebx esi xmm0
.B2.156:                        ; Preds .B2.155 .B2.316
        lea       edi, DWORD PTR [-1+edx]                       ;163.13
        cmp       eax, edi                                      ;163.13
        jbe       .B2.158       ; Prob 10%                      ;163.13
                                ; LOE eax edx ecx ebx esi xmm0
.B2.157:                        ; Preds .B2.156
        mov       edi, ebx                                      ;163.13
        add       edx, ebx                                      ;163.13
        neg       edi                                           ;163.13
        add       edi, edx                                      ;163.13
        shl       edi, 8                                        ;163.13
        movss     DWORD PTR [-100+ecx+edi], xmm0                ;163.13
                                ; LOE eax ecx ebx esi
.B2.158:                        ; Preds .B2.156 .B2.157
        test      esi, esi                                      ;164.13
        jbe       .B2.315       ; Prob 10%                      ;164.13
                                ; LOE eax ecx ebx esi
.B2.159:                        ; Preds .B2.158
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edx, edx                                      ;
        mov       ebx, 1                                        ;
                                ; LOE eax edx ecx ebx esi
.B2.160:                        ; Preds .B2.160 .B2.159
        mov       edi, edx                                      ;164.13
        inc       edx                                           ;164.13
        shl       edi, 9                                        ;164.13
        cmp       edx, esi                                      ;164.13
        mov       BYTE PTR [160+ecx+edi], bl                    ;164.13
        mov       BYTE PTR [416+ecx+edi], bl                    ;164.13
        jb        .B2.160       ; Prob 63%                      ;164.13
                                ; LOE eax edx ecx ebx esi
.B2.161:                        ; Preds .B2.160
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       esi, DWORD PTR [1+edx+edx]                    ;164.13
                                ; LOE eax ecx ebx esi
.B2.162:                        ; Preds .B2.161 .B2.315
        lea       edx, DWORD PTR [-1+esi]                       ;164.13
        cmp       eax, edx                                      ;164.13
        jbe       .B2.164       ; Prob 10%                      ;164.13
                                ; LOE eax ecx ebx esi
.B2.163:                        ; Preds .B2.162
        mov       edx, ebx                                      ;164.13
        add       esi, ebx                                      ;164.13
        neg       edx                                           ;164.13
        add       edx, esi                                      ;164.13
        shl       edx, 8                                        ;164.13
        mov       BYTE PTR [-96+ecx+edx], 1                     ;164.13
                                ; LOE eax ebx
.B2.164:                        ; Preds .B2.145 .B2.162 .B2.163
        cmp       DWORD PTR [4+esp], 0                          ;165.13
        jle       .B2.188       ; Prob 28%                      ;165.13
                                ; LOE eax ebx
.B2.165:                        ; Preds .B2.164
        mov       esi, 1                                        ;
        mov       DWORD PTR [12+esp], esi                       ;205.14
        pxor      xmm0, xmm0                                    ;205.14
                                ; LOE
.B2.166:                        ; Preds .B2.186 .B2.165
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFSTOPS] ;166.13
        xor       eax, eax                                      ;166.13
        mov       ecx, 2                                        ;166.13
        test      edx, edx                                      ;166.13
        push      ecx                                           ;166.13
        cmovle    edx, eax                                      ;166.13
        push      edx                                           ;166.13
        push      ecx                                           ;166.13
        lea       ebx, DWORD PTR [136+esp]                      ;166.13
        push      ebx                                           ;166.13
        call      _for_check_mult_overflow                      ;166.13
                                ; LOE eax
.B2.167:                        ; Preds .B2.166
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;166.13
        and       eax, 1                                        ;166.13
        shl       edx, 8                                        ;166.13
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;166.13
        mov       ebx, DWORD PTR [28+esp]                       ;166.13
        sub       ecx, edx                                      ;166.13
        shl       ebx, 8                                        ;166.13
        shl       eax, 4                                        ;166.13
        mov       esi, DWORD PTR [176+ebx+ecx]                  ;166.13
        lea       edi, DWORD PTR [164+ecx+ebx]                  ;166.13
        and       esi, 1                                        ;166.13
        add       esi, esi                                      ;166.13
        or        esi, 1                                        ;166.13
        or        esi, eax                                      ;166.13
        or        esi, 262144                                   ;166.13
        push      esi                                           ;166.13
        push      edi                                           ;166.13
        push      DWORD PTR [148+esp]                           ;166.13
        call      _for_alloc_allocatable                        ;166.13
                                ; LOE eax ebx
.B2.451:                        ; Preds .B2.167
        add       esp, 28                                       ;166.13
        mov       esi, eax                                      ;166.13
                                ; LOE ebx esi
.B2.168:                        ; Preds .B2.451
        test      esi, esi                                      ;166.13
        jne       .B2.171       ; Prob 50%                      ;166.13
                                ; LOE ebx esi
.B2.169:                        ; Preds .B2.168
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;166.13
        mov       edx, 1                                        ;166.13
        shl       edi, 8                                        ;166.13
        xor       eax, eax                                      ;166.13
        neg       edi                                           ;166.13
        add       edi, ebx                                      ;166.13
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;166.13
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, 2                                        ;166.13
        mov       DWORD PTR [180+ecx+edi], edx                  ;166.13
        mov       DWORD PTR [196+ecx+edi], edx                  ;166.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFSTOPS] ;166.13
        test      edx, edx                                      ;166.13
        mov       DWORD PTR [172+ecx+edi], eax                  ;166.13
        cmovle    edx, eax                                      ;166.13
        lea       eax, DWORD PTR [100+esp]                      ;166.13
        push      ebx                                           ;166.13
        push      edx                                           ;166.13
        push      ebx                                           ;166.13
        push      eax                                           ;166.13
        mov       DWORD PTR [168+ecx+edi], ebx                  ;166.13
        mov       DWORD PTR [192+ecx+edi], ebx                  ;166.13
        mov       DWORD PTR [176+ecx+edi], 133                  ;166.13
        mov       DWORD PTR [188+ecx+edi], edx                  ;166.13
        mov       ebx, DWORD PTR [16+esp]                       ;166.13
        call      _for_check_mult_overflow                      ;166.13
                                ; LOE ebx esi bl bh
.B2.452:                        ; Preds .B2.169
        add       esp, 16                                       ;166.13
                                ; LOE ebx esi bl bh
.B2.170:                        ; Preds .B2.452
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], esi ;166.13
        jmp       .B2.173       ; Prob 100%                     ;166.13
                                ; LOE ebx
.B2.171:                        ; Preds .B2.168
        mov       DWORD PTR [64+esp], 0                         ;168.16
        lea       edx, DWORD PTR [64+esp]                       ;168.16
        mov       DWORD PTR [128+esp], 61                       ;168.16
        lea       eax, DWORD PTR [128+esp]                      ;168.16
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_8 ;168.16
        push      32                                            ;168.16
        push      eax                                           ;168.16
        push      OFFSET FLAT: __STRLITPACK_22.0.2              ;168.16
        push      -2088435968                                   ;168.16
        push      911                                           ;168.16
        push      edx                                           ;168.16
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], esi ;166.13
        call      _for_write_seq_lis                            ;168.16
                                ; LOE ebx
.B2.172:                        ; Preds .B2.171
        push      32                                            ;169.16
        xor       eax, eax                                      ;169.16
        push      eax                                           ;169.16
        push      eax                                           ;169.16
        push      -2088435968                                   ;169.16
        push      eax                                           ;169.16
        push      OFFSET FLAT: __STRLITPACK_23                  ;169.16
        call      _for_stop_core                                ;169.16
                                ; LOE ebx
.B2.453:                        ; Preds .B2.172
        add       esp, 48                                       ;169.16
                                ; LOE ebx
.B2.173:                        ; Preds .B2.453 .B2.170
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;171.13
        shl       esi, 8                                        ;171.13
        neg       esi                                           ;171.13
        add       esi, ebx                                      ;171.13
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;171.13
        mov       edi, DWORD PTR [188+esi+ecx]                  ;171.13
        test      edi, edi                                      ;171.13
        jle       .B2.176       ; Prob 50%                      ;171.13
                                ; LOE ecx ebx esi edi
.B2.174:                        ; Preds .B2.173
        cmp       edi, 48                                       ;171.13
        jle       .B2.319       ; Prob 0%                       ;171.13
                                ; LOE ecx ebx esi edi
.B2.175:                        ; Preds .B2.174
        add       edi, edi                                      ;171.13
        push      edi                                           ;171.13
        push      0                                             ;171.13
        push      DWORD PTR [164+esi+ecx]                       ;171.13
        call      __intel_fast_memset                           ;171.13
                                ; LOE ebx
.B2.454:                        ; Preds .B2.175
        add       esp, 12                                       ;171.13
                                ; LOE ebx
.B2.176:                        ; Preds .B2.325 .B2.454 .B2.173 .B2.323
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFSTOPS] ;172.13
        xor       eax, eax                                      ;172.13
        mov       ecx, 2                                        ;172.13
        test      edx, edx                                      ;172.13
        push      ecx                                           ;172.13
        cmovle    edx, eax                                      ;172.13
        push      edx                                           ;172.13
        push      ecx                                           ;172.13
        lea       esi, DWORD PTR [156+esp]                      ;172.13
        push      esi                                           ;172.13
        call      _for_check_mult_overflow                      ;172.13
                                ; LOE eax ebx
.B2.177:                        ; Preds .B2.176
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;172.13
        and       eax, 1                                        ;172.13
        shl       edx, 8                                        ;172.13
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;172.13
        sub       ecx, edx                                      ;172.13
        shl       eax, 4                                        ;172.13
        mov       esi, DWORD PTR [212+ebx+ecx]                  ;172.13
        lea       edi, DWORD PTR [200+ecx+ebx]                  ;172.13
        and       esi, 1                                        ;172.13
        add       esi, esi                                      ;172.13
        or        esi, 1                                        ;172.13
        or        esi, eax                                      ;172.13
        or        esi, 262144                                   ;172.13
        push      esi                                           ;172.13
        push      edi                                           ;172.13
        push      DWORD PTR [168+esp]                           ;172.13
        call      _for_alloc_allocatable                        ;172.13
                                ; LOE eax ebx
.B2.456:                        ; Preds .B2.177
        add       esp, 28                                       ;172.13
        mov       esi, eax                                      ;172.13
                                ; LOE ebx esi
.B2.178:                        ; Preds .B2.456
        test      esi, esi                                      ;172.13
        jne       .B2.181       ; Prob 50%                      ;172.13
                                ; LOE ebx esi
.B2.179:                        ; Preds .B2.178
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;172.13
        mov       edx, 1                                        ;172.13
        shl       edi, 8                                        ;172.13
        xor       eax, eax                                      ;172.13
        neg       edi                                           ;172.13
        add       edi, ebx                                      ;172.13
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;172.13
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, 2                                        ;172.13
        mov       DWORD PTR [216+ecx+edi], edx                  ;172.13
        mov       DWORD PTR [232+ecx+edi], edx                  ;172.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFSTOPS] ;172.13
        test      edx, edx                                      ;172.13
        mov       DWORD PTR [208+ecx+edi], eax                  ;172.13
        cmovle    edx, eax                                      ;172.13
        lea       eax, DWORD PTR [120+esp]                      ;172.13
        push      ebx                                           ;172.13
        push      edx                                           ;172.13
        push      ebx                                           ;172.13
        push      eax                                           ;172.13
        mov       DWORD PTR [204+ecx+edi], ebx                  ;172.13
        mov       DWORD PTR [228+ecx+edi], ebx                  ;172.13
        mov       DWORD PTR [212+ecx+edi], 133                  ;172.13
        mov       DWORD PTR [224+ecx+edi], edx                  ;172.13
        mov       ebx, DWORD PTR [16+esp]                       ;172.13
        call      _for_check_mult_overflow                      ;172.13
                                ; LOE ebx esi bl bh
.B2.457:                        ; Preds .B2.179
        add       esp, 16                                       ;172.13
                                ; LOE ebx esi bl bh
.B2.180:                        ; Preds .B2.457
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], esi ;172.13
        jmp       .B2.183       ; Prob 100%                     ;172.13
                                ; LOE ebx
.B2.181:                        ; Preds .B2.178
        mov       DWORD PTR [64+esp], 0                         ;174.16
        lea       edx, DWORD PTR [64+esp]                       ;174.16
        mov       DWORD PTR [136+esp], 62                       ;174.16
        lea       eax, DWORD PTR [136+esp]                      ;174.16
        mov       DWORD PTR [140+esp], OFFSET FLAT: __STRLITPACK_6 ;174.16
        push      32                                            ;174.16
        push      eax                                           ;174.16
        push      OFFSET FLAT: __STRLITPACK_24.0.2              ;174.16
        push      -2088435968                                   ;174.16
        push      911                                           ;174.16
        push      edx                                           ;174.16
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], esi ;172.13
        call      _for_write_seq_lis                            ;174.16
                                ; LOE ebx
.B2.182:                        ; Preds .B2.181
        push      32                                            ;175.16
        xor       eax, eax                                      ;175.16
        push      eax                                           ;175.16
        push      eax                                           ;175.16
        push      -2088435968                                   ;175.16
        push      eax                                           ;175.16
        push      OFFSET FLAT: __STRLITPACK_25                  ;175.16
        call      _for_stop_core                                ;175.16
                                ; LOE ebx
.B2.458:                        ; Preds .B2.182
        add       esp, 48                                       ;175.16
                                ; LOE ebx
.B2.183:                        ; Preds .B2.458 .B2.180
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;177.13
        mov       DWORD PTR [20+esp], eax                       ;177.13
        shl       eax, 8                                        ;177.13
        sub       ebx, eax                                      ;177.13
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;177.13
        mov       esi, DWORD PTR [224+ebx+edx]                  ;177.13
        test      esi, esi                                      ;177.13
        jle       .B2.186       ; Prob 50%                      ;177.13
                                ; LOE edx ebx esi
.B2.184:                        ; Preds .B2.183
        cmp       esi, 48                                       ;177.13
        jle       .B2.328       ; Prob 0%                       ;177.13
                                ; LOE edx ebx esi
.B2.185:                        ; Preds .B2.184
        add       esi, esi                                      ;177.13
        push      esi                                           ;177.13
        push      0                                             ;177.13
        push      DWORD PTR [200+ebx+edx]                       ;177.13
        call      __intel_fast_memset                           ;177.13
                                ; LOE
.B2.459:                        ; Preds .B2.185
        add       esp, 12                                       ;177.13
                                ; LOE
.B2.186:                        ; Preds .B2.334 .B2.459 .B2.183 .B2.332
        mov       eax, DWORD PTR [12+esp]                       ;178.13
        inc       eax                                           ;178.13
        mov       DWORD PTR [12+esp], eax                       ;178.13
        cmp       eax, DWORD PTR [4+esp]                        ;178.13
        jle       .B2.166       ; Prob 82%                      ;178.13
                                ; LOE
.B2.187:                        ; Preds .B2.186
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+24] ;179.10
                                ; LOE eax ebx
.B2.188:                        ; Preds .B2.187 .B2.164
        test      eax, eax                                      ;179.10
        jle       .B2.201       ; Prob 50%                      ;179.10
                                ; LOE eax ebx
.B2.189:                        ; Preds .B2.188
        mov       esi, eax                                      ;179.10
        shr       esi, 31                                       ;179.10
        add       esi, eax                                      ;179.10
        sar       esi, 1                                        ;179.10
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;179.10
        test      esi, esi                                      ;179.10
        jbe       .B2.338       ; Prob 10%                      ;179.10
                                ; LOE eax ecx ebx esi
.B2.190:                        ; Preds .B2.189
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edx, edx                                      ;
        mov       ebx, 1                                        ;
                                ; LOE eax edx ecx ebx esi
.B2.191:                        ; Preds .B2.191 .B2.190
        mov       edi, edx                                      ;179.10
        inc       edx                                           ;179.10
        shl       edi, 9                                        ;179.10
        cmp       edx, esi                                      ;179.10
        mov       BYTE PTR [236+ecx+edi], bl                    ;179.10
        mov       BYTE PTR [492+ecx+edi], bl                    ;179.10
        jb        .B2.191       ; Prob 63%                      ;179.10
                                ; LOE eax edx ecx ebx esi
.B2.192:                        ; Preds .B2.191
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;179.10
                                ; LOE eax edx ecx ebx esi
.B2.193:                        ; Preds .B2.192 .B2.338
        lea       edi, DWORD PTR [-1+edx]                       ;179.10
        cmp       eax, edi                                      ;179.10
        jbe       .B2.195       ; Prob 10%                      ;179.10
                                ; LOE eax edx ecx ebx esi
.B2.194:                        ; Preds .B2.193
        mov       edi, ebx                                      ;179.10
        add       edx, ebx                                      ;179.10
        neg       edi                                           ;179.10
        add       edi, edx                                      ;179.10
        shl       edi, 8                                        ;179.10
        mov       BYTE PTR [-20+ecx+edi], 1                     ;179.10
                                ; LOE eax ecx ebx esi
.B2.195:                        ; Preds .B2.193 .B2.194
        test      esi, esi                                      ;180.10
        jbe       .B2.337       ; Prob 10%                      ;180.10
                                ; LOE eax ecx ebx esi
.B2.196:                        ; Preds .B2.195
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.197:                        ; Preds .B2.197 .B2.196
        mov       edi, edx                                      ;180.10
        inc       edx                                           ;180.10
        shl       edi, 9                                        ;180.10
        cmp       edx, esi                                      ;180.10
        mov       BYTE PTR [237+ecx+edi], bl                    ;180.10
        mov       BYTE PTR [493+ecx+edi], bl                    ;180.10
        jb        .B2.197       ; Prob 63%                      ;180.10
                                ; LOE eax edx ecx ebx esi
.B2.198:                        ; Preds .B2.197
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       esi, DWORD PTR [1+edx+edx]                    ;180.10
                                ; LOE eax ecx ebx esi
.B2.199:                        ; Preds .B2.198 .B2.337
        lea       edx, DWORD PTR [-1+esi]                       ;180.10
        cmp       eax, edx                                      ;180.10
        jbe       .B2.201       ; Prob 10%                      ;180.10
                                ; LOE eax ecx ebx esi
.B2.200:                        ; Preds .B2.199
        mov       edx, ebx                                      ;180.10
        add       esi, ebx                                      ;180.10
        neg       edx                                           ;180.10
        add       edx, esi                                      ;180.10
        shl       edx, 8                                        ;180.10
        mov       BYTE PTR [-19+ecx+edx], 0                     ;180.10
                                ; LOE eax ebx
.B2.201:                        ; Preds .B2.199 .B2.188 .B2.200
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;181.10
        test      edx, edx                                      ;181.23
        mov       DWORD PTR [12+esp], edx                       ;181.10
        jne       .B2.252       ; Prob 50%                      ;181.23
                                ; LOE eax ebx
.B2.202:                        ; Preds .B2.201
        test      eax, eax                                      ;182.12
        jle       .B2.209       ; Prob 50%                      ;182.12
                                ; LOE eax ebx
.B2.203:                        ; Preds .B2.202
        mov       esi, eax                                      ;182.12
        shr       esi, 31                                       ;182.12
        add       esi, eax                                      ;182.12
        sar       esi, 1                                        ;182.12
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;182.12
        test      esi, esi                                      ;182.12
        jbe       .B2.339       ; Prob 10%                      ;182.12
                                ; LOE eax ecx ebx esi
.B2.204:                        ; Preds .B2.203
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       edi, edi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.205:                        ; Preds .B2.205 .B2.204
        inc       edx                                           ;182.12
        mov       DWORD PTR [240+edi+ecx], ebx                  ;182.12
        mov       DWORD PTR [496+edi+ecx], ebx                  ;182.12
        add       edi, 512                                      ;182.12
        cmp       edx, esi                                      ;182.12
        jb        .B2.205       ; Prob 63%                      ;182.12
                                ; LOE eax edx ecx ebx esi edi
.B2.206:                        ; Preds .B2.205
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;182.12
                                ; LOE eax edx ecx ebx esi
.B2.207:                        ; Preds .B2.206 .B2.339
        lea       edi, DWORD PTR [-1+edx]                       ;182.12
        cmp       eax, edi                                      ;182.12
        jbe       .B2.259       ; Prob 10%                      ;182.12
                                ; LOE eax edx ecx ebx esi
.B2.208:                        ; Preds .B2.207
        mov       edi, ebx                                      ;182.12
        add       edx, ebx                                      ;182.12
        neg       edi                                           ;182.12
        add       edi, edx                                      ;182.12
        shl       edi, 8                                        ;182.12
        mov       DWORD PTR [-16+ecx+edi], 0                    ;182.12
        jmp       .B2.259       ; Prob 100%                     ;182.12
                                ; LOE eax ecx ebx esi
.B2.209:                        ; Preds .B2.202 .B2.277
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;192.13
        xor       eax, eax                                      ;192.13
        test      edx, edx                                      ;192.13
        lea       ecx, DWORD PTR [28+esp]                       ;192.13
        push      64                                            ;192.13
        cmovle    edx, eax                                      ;192.13
        push      edx                                           ;192.13
        push      2                                             ;192.13
        push      ecx                                           ;192.13
        call      _for_check_mult_overflow                      ;192.13
                                ; LOE eax
.B2.210:                        ; Preds .B2.209
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+12] ;192.13
        and       eax, 1                                        ;192.13
        and       edx, 1                                        ;192.13
        add       edx, edx                                      ;192.13
        shl       eax, 4                                        ;192.13
        or        edx, 1                                        ;192.13
        or        edx, eax                                      ;192.13
        or        edx, 262144                                   ;192.13
        push      edx                                           ;192.13
        push      OFFSET FLAT: _DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE ;192.13
        push      DWORD PTR [52+esp]                            ;192.13
        call      _for_alloc_allocatable                        ;192.13
                                ; LOE eax
.B2.461:                        ; Preds .B2.210
        add       esp, 28                                       ;192.13
        mov       ebx, eax                                      ;192.13
                                ; LOE ebx
.B2.211:                        ; Preds .B2.461
        test      ebx, ebx                                      ;192.13
        jne       .B2.221       ; Prob 50%                      ;192.13
                                ; LOE ebx
.B2.212:                        ; Preds .B2.211
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;192.13
        xor       eax, eax                                      ;192.13
        test      esi, esi                                      ;192.13
        cmovle    esi, eax                                      ;192.13
        mov       ecx, 64                                       ;192.13
        lea       edi, DWORD PTR [8+esp]                        ;192.13
        push      ecx                                           ;192.13
        push      esi                                           ;192.13
        push      2                                             ;192.13
        push      edi                                           ;192.13
        mov       edx, 1                                        ;192.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+12], 133 ;192.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+4], ecx ;192.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+16], edx ;192.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+8], eax ;192.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32], edx ;192.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+24], esi ;192.13
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+28], ecx ;192.13
        call      _for_check_mult_overflow                      ;192.13
                                ; LOE ebx
.B2.462:                        ; Preds .B2.212
        add       esp, 16                                       ;192.13
                                ; LOE ebx
.B2.213:                        ; Preds .B2.462
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;192.13
        mov       esi, DWORD PTR [8+esp]                        ;192.13
        add       esi, ecx                                      ;192.13
        cmp       ecx, esi                                      ;192.13
        jae       .B2.222       ; Prob 50%                      ;192.13
                                ; LOE ecx ebx esi
.B2.214:                        ; Preds .B2.213
        xor       eax, eax                                      ;192.13
        sub       esi, ecx                                      ;192.13
        sbb       eax, 0                                        ;192.13
        add       esi, 63                                       ;192.13
        adc       eax, 0                                        ;192.13
        jns       .B2.431       ; Prob 50%                      ;192.13
                                ; LOE eax ecx ebx esi
.B2.432:                        ; Preds .B2.214
        add       esi, 63                                       ;192.13
        adc       eax, 0                                        ;192.13
                                ; LOE eax ecx ebx esi
.B2.431:                        ; Preds .B2.214 .B2.432
        shrd      esi, eax, 6                                   ;192.13
        mov       edx, esi                                      ;192.13
        shr       edx, 1                                        ;192.13
        test      edx, edx                                      ;192.13
        ja        .B2.216       ; Prob 67%                      ;192.13
                                ; LOE edx ecx ebx esi
.B2.215:                        ; Preds .B2.431
        mov       eax, 1                                        ;
        jmp       .B2.219       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B2.216:                        ; Preds .B2.431
        mov       DWORD PTR [esp], esi                          ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [4+esp], ebx                        ;
                                ; LOE eax edx ecx
.B2.217:                        ; Preds .B2.436 .B2.216
        mov       ebx, eax                                      ;192.13
        mov       edi, 64                                       ;192.13
        shl       ebx, 7                                        ;192.13
        lea       esi, DWORD PTR [ecx+ebx]                      ;192.13
                                ; LOE eax edx ecx ebx esi edi
.B2.435:                        ; Preds .B2.435 .B2.217
        movsd     xmm0, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-8+edi] ;192.13
        movsd     xmm1, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-16+edi] ;192.13
        movsd     xmm2, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-24+edi] ;192.13
        movsd     xmm3, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-32+edi] ;192.13
        movsd     QWORD PTR [-8+esi+edi], xmm0                  ;192.13
        movsd     QWORD PTR [-16+esi+edi], xmm1                 ;192.13
        movsd     QWORD PTR [-24+esi+edi], xmm2                 ;192.13
        movsd     QWORD PTR [-32+esi+edi], xmm3                 ;192.13
        sub       edi, 32                                       ;192.13
        jne       .B2.435       ; Prob 50%                      ;192.13
                                ; LOE eax edx ecx ebx esi edi
.B2.434:                        ; Preds .B2.435
        lea       esi, DWORD PTR [64+ebx+ecx]                   ;192.13
        mov       ebx, 64                                       ;192.13
                                ; LOE eax edx ecx ebx esi
.B2.437:                        ; Preds .B2.437 .B2.434
        movsd     xmm0, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-8+ebx] ;192.13
        movsd     xmm1, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-16+ebx] ;192.13
        movsd     xmm2, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-24+ebx] ;192.13
        movsd     xmm3, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-32+ebx] ;192.13
        movsd     QWORD PTR [-8+esi+ebx], xmm0                  ;192.13
        movsd     QWORD PTR [-16+esi+ebx], xmm1                 ;192.13
        movsd     QWORD PTR [-24+esi+ebx], xmm2                 ;192.13
        movsd     QWORD PTR [-32+esi+ebx], xmm3                 ;192.13
        sub       ebx, 32                                       ;192.13
        jne       .B2.437       ; Prob 50%                      ;192.13
                                ; LOE eax edx ecx ebx esi
.B2.436:                        ; Preds .B2.437
        inc       eax                                           ;192.13
        cmp       eax, edx                                      ;192.13
        jb        .B2.217       ; Prob 24%                      ;192.13
                                ; LOE eax edx ecx
.B2.218:                        ; Preds .B2.436
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;192.13
        mov       ebx, DWORD PTR [4+esp]                        ;
                                ; LOE eax ecx ebx esi
.B2.219:                        ; Preds .B2.218 .B2.215
        dec       eax                                           ;192.13
        cmp       eax, esi                                      ;192.13
        jae       .B2.476       ; Prob 33%                      ;192.13
                                ; LOE eax ecx ebx
.B2.220:                        ; Preds .B2.219
        shl       eax, 6                                        ;192.13
        add       ecx, eax                                      ;192.13
        mov       eax, 64                                       ;192.13
                                ; LOE eax ecx ebx
.B2.439:                        ; Preds .B2.439 .B2.220
        movsd     xmm0, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-8+eax] ;192.13
        movsd     xmm1, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-16+eax] ;192.13
        movsd     xmm2, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-24+eax] ;192.13
        movsd     xmm3, QWORD PTR [_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_-32+eax] ;192.13
        movsd     QWORD PTR [-8+ecx+eax], xmm0                  ;192.13
        movsd     QWORD PTR [-16+ecx+eax], xmm1                 ;192.13
        movsd     QWORD PTR [-24+ecx+eax], xmm2                 ;192.13
        movsd     QWORD PTR [-32+ecx+eax], xmm3                 ;192.13
        sub       eax, 32                                       ;192.13
        jne       .B2.439       ; Prob 50%                      ;192.13
        jmp       .B2.222       ; Prob 100%                     ;192.13
                                ; LOE eax ecx ebx
.B2.221:                        ; Preds .B2.211 .B2.476
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;192.13
        jne       .B2.340       ; Prob 5%                       ;193.22
        jmp       .B2.223       ; Prob 100%                     ;193.22
                                ; LOE
.B2.222:                        ; Preds .B2.439 .B2.213
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;192.13
                                ; LOE
.B2.223:                        ; Preds .B2.221 .B2.469 .B2.222
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+24] ;197.10
        test      ebx, ebx                                      ;197.10
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;197.10
        jle       .B2.236       ; Prob 50%                      ;197.10
                                ; LOE ebx esi
.B2.224:                        ; Preds .B2.223
        mov       eax, ebx                                      ;197.10
        shr       eax, 31                                       ;197.10
        add       eax, ebx                                      ;197.10
        sar       eax, 1                                        ;197.10
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;197.10
        test      eax, eax                                      ;197.10
        jbe       .B2.343       ; Prob 10%                      ;197.10
                                ; LOE eax edx ebx esi
.B2.225:                        ; Preds .B2.224
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [12+esp], esi                       ;
        xor       edi, edi                                      ;
        mov       esi, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.226:                        ; Preds .B2.226 .B2.225
        inc       edi                                           ;197.10
        mov       DWORD PTR [8+esi+edx], ecx                    ;197.10
        mov       DWORD PTR [72+esi+edx], ecx                   ;197.10
        add       esi, 128                                      ;197.10
        cmp       edi, eax                                      ;197.10
        jb        .B2.226       ; Prob 64%                      ;197.10
                                ; LOE eax edx ecx ebx esi edi
.B2.227:                        ; Preds .B2.226
        mov       esi, DWORD PTR [12+esp]                       ;
        lea       ecx, DWORD PTR [1+edi+edi]                    ;197.10
                                ; LOE eax edx ecx ebx esi
.B2.228:                        ; Preds .B2.227 .B2.343
        lea       edi, DWORD PTR [-1+ecx]                       ;197.10
        cmp       ebx, edi                                      ;197.10
        jbe       .B2.230       ; Prob 10%                      ;197.10
                                ; LOE eax edx ecx ebx esi
.B2.229:                        ; Preds .B2.228
        mov       edi, esi                                      ;197.10
        add       ecx, esi                                      ;197.10
        neg       edi                                           ;197.10
        add       edi, ecx                                      ;197.10
        shl       edi, 6                                        ;197.10
        mov       DWORD PTR [-56+edx+edi], 0                    ;197.10
                                ; LOE eax edx ebx esi
.B2.230:                        ; Preds .B2.228 .B2.229
        test      eax, eax                                      ;198.10
        jbe       .B2.342       ; Prob 10%                      ;198.10
                                ; LOE eax edx ebx esi
.B2.231:                        ; Preds .B2.230
        mov       DWORD PTR [12+esp], esi                       ;
        xor       ecx, ecx                                      ;
        mov       edi, 1                                        ;
                                ; LOE eax edx ecx ebx edi
.B2.232:                        ; Preds .B2.232 .B2.231
        mov       esi, ecx                                      ;198.10
        inc       ecx                                           ;198.10
        shl       esi, 7                                        ;198.10
        cmp       ecx, eax                                      ;198.10
        mov       WORD PTR [12+edx+esi], di                     ;198.10
        mov       WORD PTR [76+edx+esi], di                     ;198.10
        jb        .B2.232       ; Prob 64%                      ;198.10
                                ; LOE eax edx ecx ebx edi
.B2.233:                        ; Preds .B2.232
        mov       esi, DWORD PTR [12+esp]                       ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;198.10
                                ; LOE edx ecx ebx esi
.B2.234:                        ; Preds .B2.233 .B2.342
        lea       eax, DWORD PTR [-1+ecx]                       ;198.10
        cmp       ebx, eax                                      ;198.10
        jbe       .B2.236       ; Prob 10%                      ;198.10
                                ; LOE edx ecx esi
.B2.235:                        ; Preds .B2.234
        mov       eax, esi                                      ;198.10
        add       ecx, esi                                      ;198.10
        neg       eax                                           ;198.10
        mov       esi, 1                                        ;198.10
        add       eax, ecx                                      ;198.10
        shl       eax, 6                                        ;198.10
        mov       WORD PTR [-52+edx+eax], si                    ;198.10
                                ; LOE
.B2.236:                        ; Preds .B2.223 .B2.234 .B2.235
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;199.10
        test      ecx, ecx                                      ;199.10
        jle       .B2.250       ; Prob 28%                      ;199.10
                                ; LOE ecx
.B2.238:                        ; Preds .B2.236
        mov       DWORD PTR [24+esp], ecx                       ;205.14
        xor       ebx, ebx                                      ;199.10
        pxor      xmm0, xmm0                                    ;205.14
                                ; LOE ebx
.B2.239:                        ; Preds .B2.249 .B2.238
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFSTOPS] ;200.17
        xor       eax, eax                                      ;200.17
        mov       ecx, 2                                        ;200.17
        test      edx, edx                                      ;200.17
        push      ecx                                           ;200.17
        cmovle    edx, eax                                      ;200.17
        push      edx                                           ;200.17
        push      ecx                                           ;200.17
        lea       esi, DWORD PTR [60+esp]                       ;200.17
        push      esi                                           ;200.17
        call      _for_check_mult_overflow                      ;200.17
                                ; LOE eax ebx
.B2.240:                        ; Preds .B2.239
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;200.17
        mov       esi, ebx                                      ;200.17
        shl       edx, 6                                        ;200.17
        and       eax, 1                                        ;200.17
        neg       edx                                           ;200.17
        shl       esi, 6                                        ;200.17
        add       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;200.17
        shl       eax, 4                                        ;200.17
        mov       ecx, DWORD PTR [92+esi+edx]                   ;200.17
        lea       edi, DWORD PTR [80+edx+esi]                   ;200.17
        and       ecx, 1                                        ;200.17
        add       ecx, ecx                                      ;200.17
        or        ecx, 1                                        ;200.17
        or        ecx, eax                                      ;200.17
        or        ecx, 262144                                   ;200.17
        push      ecx                                           ;200.17
        push      edi                                           ;200.17
        push      DWORD PTR [72+esp]                            ;200.17
        call      _for_alloc_allocatable                        ;200.17
                                ; LOE eax ebx esi
.B2.464:                        ; Preds .B2.240
        add       esp, 28                                       ;200.17
        mov       edi, eax                                      ;200.17
                                ; LOE ebx esi edi
.B2.241:                        ; Preds .B2.464
        test      edi, edi                                      ;200.17
        je        .B2.244       ; Prob 50%                      ;200.17
                                ; LOE ebx esi edi
.B2.242:                        ; Preds .B2.241
        mov       DWORD PTR [64+esp], 0                         ;202.18
        lea       edx, DWORD PTR [64+esp]                       ;202.18
        mov       DWORD PTR [40+esp], 65                        ;202.18
        lea       eax, DWORD PTR [40+esp]                       ;202.18
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_2 ;202.18
        push      32                                            ;202.18
        push      eax                                           ;202.18
        push      OFFSET FLAT: __STRLITPACK_28.0.2              ;202.18
        push      -2088435968                                   ;202.18
        push      911                                           ;202.18
        push      edx                                           ;202.18
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], edi ;200.17
        call      _for_write_seq_lis                            ;202.18
                                ; LOE ebx esi
.B2.243:                        ; Preds .B2.242
        push      32                                            ;203.18
        xor       eax, eax                                      ;203.18
        push      eax                                           ;203.18
        push      eax                                           ;203.18
        push      -2088435968                                   ;203.18
        push      eax                                           ;203.18
        push      OFFSET FLAT: __STRLITPACK_29                  ;203.18
        call      _for_stop_core                                ;203.18
                                ; LOE ebx esi
.B2.465:                        ; Preds .B2.243
        add       esp, 48                                       ;203.18
        jmp       .B2.246       ; Prob 100%                     ;203.18
                                ; LOE ebx esi
.B2.244:                        ; Preds .B2.241
        mov       DWORD PTR [12+esp], edi                       ;
        mov       edx, 1                                        ;200.17
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;200.17
        xor       eax, eax                                      ;200.17
        shl       edi, 6                                        ;200.17
        neg       edi                                           ;200.17
        add       edi, esi                                      ;200.17
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;200.17
        mov       DWORD PTR [16+esp], esi                       ;
        mov       esi, 2                                        ;200.17
        mov       DWORD PTR [96+ecx+edi], edx                   ;200.17
        mov       DWORD PTR [112+ecx+edi], edx                  ;200.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFSTOPS] ;200.17
        test      edx, edx                                      ;200.17
        mov       DWORD PTR [88+ecx+edi], eax                   ;200.17
        cmovle    edx, eax                                      ;200.17
        lea       eax, DWORD PTR [36+esp]                       ;200.17
        push      esi                                           ;200.17
        push      edx                                           ;200.17
        push      esi                                           ;200.17
        push      eax                                           ;200.17
        mov       DWORD PTR [92+ecx+edi], 133                   ;200.17
        mov       DWORD PTR [84+ecx+edi], esi                   ;200.17
        mov       DWORD PTR [104+ecx+edi], edx                  ;200.17
        mov       DWORD PTR [108+ecx+edi], esi                  ;200.17
        mov       esi, DWORD PTR [32+esp]                       ;200.17
        mov       edi, DWORD PTR [28+esp]                       ;200.17
        call      _for_check_mult_overflow                      ;200.17
                                ; LOE ebx esi edi
.B2.466:                        ; Preds .B2.244
        add       esp, 16                                       ;200.17
                                ; LOE ebx esi edi
.B2.245:                        ; Preds .B2.466
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], edi ;200.17
                                ; LOE ebx esi
.B2.246:                        ; Preds .B2.465 .B2.245
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;205.14
        shl       eax, 6                                        ;205.14
        neg       eax                                           ;205.14
        add       esi, eax                                      ;205.14
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;205.14
        mov       edi, DWORD PTR [104+ecx+esi]                  ;205.14
        test      edi, edi                                      ;205.14
        jle       .B2.249       ; Prob 50%                      ;205.14
                                ; LOE ecx ebx esi edi
.B2.247:                        ; Preds .B2.246
        cmp       edi, 48                                       ;205.14
        jle       .B2.344       ; Prob 0%                       ;205.14
                                ; LOE ecx ebx esi edi
.B2.248:                        ; Preds .B2.247
        add       edi, edi                                      ;205.14
        push      edi                                           ;205.14
        push      0                                             ;205.14
        push      DWORD PTR [80+ecx+esi]                        ;205.14
        call      __intel_fast_memset                           ;205.14
                                ; LOE ebx
.B2.467:                        ; Preds .B2.248
        add       esp, 12                                       ;205.14
                                ; LOE ebx
.B2.249:                        ; Preds .B2.350 .B2.246 .B2.348 .B2.467
        inc       ebx                                           ;199.10
        cmp       ebx, DWORD PTR [24+esp]                       ;199.10
        jb        .B2.239       ; Prob 82%                      ;199.10
                                ; LOE ebx
.B2.250:                        ; Preds .B2.236 .B2.249
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;209.8
                                ; LOE eax
.B2.251:                        ; Preds .B2.250
        test      eax, eax                                      ;209.21
        je        .B2.353       ; Prob 5%                       ;209.21
        jmp       .B2.278       ; Prob 100%                     ;209.21
                                ; LOE
.B2.252:                        ; Preds .B2.201
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;184.12
        test      eax, eax                                      ;184.12
        mov       DWORD PTR [4+esp], edx                        ;184.12
        jle       .B2.278       ; Prob 50%                      ;184.12
                                ; LOE eax ebx
.B2.253:                        ; Preds .B2.252
        mov       esi, eax                                      ;184.12
        shr       esi, 31                                       ;184.12
        add       esi, eax                                      ;184.12
        sar       esi, 1                                        ;184.12
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;184.12
        test      esi, esi                                      ;184.12
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;184.12
        mov       DWORD PTR [8+esp], edx                        ;184.12
        jbe       .B2.427       ; Prob 10%                      ;184.12
                                ; LOE eax ecx ebx esi
.B2.254:                        ; Preds .B2.253
        xor       edx, edx                                      ;
        mov       DWORD PTR [esp], eax                          ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [16+esp], esi                       ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       eax, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx edi
.B2.255:                        ; Preds .B2.255 .B2.254
        mov       esi, edx                                      ;184.12
        inc       edx                                           ;184.12
        shl       esi, 7                                        ;184.12
        mov       ebx, DWORD PTR [8+eax+esi]                    ;184.12
        mov       DWORD PTR [240+edi+ecx], ebx                  ;184.12
        mov       ebx, DWORD PTR [72+eax+esi]                   ;184.12
        mov       DWORD PTR [496+edi+ecx], ebx                  ;184.12
        add       edi, 512                                      ;184.12
        cmp       edx, DWORD PTR [16+esp]                       ;184.12
        jb        .B2.255       ; Prob 63%                      ;184.12
                                ; LOE eax edx ecx edi
.B2.256:                        ; Preds .B2.255
        mov       esi, DWORD PTR [16+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;184.12
        mov       eax, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B2.257:                        ; Preds .B2.256 .B2.427
        lea       edi, DWORD PTR [-1+edx]                       ;184.12
        cmp       eax, edi                                      ;184.12
        jbe       .B2.259       ; Prob 10%                      ;184.12
                                ; LOE eax edx ecx ebx esi
.B2.258:                        ; Preds .B2.257
        mov       DWORD PTR [16+esp], esi                       ;
        mov       esi, DWORD PTR [4+esp]                        ;184.12
        mov       edi, esi                                      ;184.12
        neg       edi                                           ;184.12
        add       esi, edx                                      ;184.12
        add       edi, esi                                      ;184.12
        mov       esi, ebx                                      ;184.12
        neg       esi                                           ;184.12
        add       edx, ebx                                      ;184.12
        shl       edi, 6                                        ;184.12
        add       esi, edx                                      ;184.12
        mov       edx, DWORD PTR [8+esp]                        ;184.12
        shl       esi, 8                                        ;184.12
        mov       edx, DWORD PTR [-56+edx+edi]                  ;184.12
        mov       DWORD PTR [-16+ecx+esi], edx                  ;184.12
        mov       esi, DWORD PTR [16+esp]                       ;184.12
                                ; LOE eax ecx ebx esi
.B2.259:                        ; Preds .B2.208 .B2.207 .B2.258 .B2.257
        test      esi, esi                                      ;186.13
        jbe       .B2.426       ; Prob 10%                      ;186.13
                                ; LOE eax ecx ebx esi
.B2.260:                        ; Preds .B2.259
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.261:                        ; Preds .B2.261 .B2.260
        mov       edi, edx                                      ;186.13
        inc       edx                                           ;186.13
        shl       edi, 9                                        ;186.13
        cmp       edx, esi                                      ;186.13
        mov       DWORD PTR [244+ecx+edi], ebx                  ;186.13
        mov       DWORD PTR [500+ecx+edi], ebx                  ;186.13
        jb        .B2.261       ; Prob 63%                      ;186.13
                                ; LOE eax edx ecx ebx esi
.B2.262:                        ; Preds .B2.261
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;186.13
                                ; LOE eax edx ecx ebx esi
.B2.263:                        ; Preds .B2.262 .B2.426
        lea       edi, DWORD PTR [-1+edx]                       ;186.13
        cmp       eax, edi                                      ;186.13
        jbe       .B2.265       ; Prob 10%                      ;186.13
                                ; LOE eax edx ecx ebx esi
.B2.264:                        ; Preds .B2.263
        mov       edi, ebx                                      ;186.13
        add       edx, ebx                                      ;186.13
        neg       edi                                           ;186.13
        add       edi, edx                                      ;186.13
        shl       edi, 8                                        ;186.13
        mov       DWORD PTR [-12+ecx+edi], 0                    ;186.13
                                ; LOE eax ecx ebx esi
.B2.265:                        ; Preds .B2.263 .B2.264
        test      esi, esi                                      ;187.13
        jbe       .B2.425       ; Prob 10%                      ;187.13
                                ; LOE eax ecx ebx esi
.B2.266:                        ; Preds .B2.265
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.267:                        ; Preds .B2.267 .B2.266
        mov       edi, edx                                      ;187.13
        inc       edx                                           ;187.13
        shl       edi, 9                                        ;187.13
        cmp       edx, esi                                      ;187.13
        mov       DWORD PTR [248+ecx+edi], ebx                  ;187.13
        mov       DWORD PTR [504+ecx+edi], ebx                  ;187.13
        jb        .B2.267       ; Prob 63%                      ;187.13
                                ; LOE eax edx ecx ebx esi
.B2.268:                        ; Preds .B2.267
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;187.13
                                ; LOE eax edx ecx ebx esi
.B2.269:                        ; Preds .B2.268 .B2.425
        lea       edi, DWORD PTR [-1+edx]                       ;187.13
        cmp       eax, edi                                      ;187.13
        jbe       .B2.271       ; Prob 10%                      ;187.13
                                ; LOE eax edx ecx ebx esi
.B2.270:                        ; Preds .B2.269
        mov       edi, ebx                                      ;187.13
        add       edx, ebx                                      ;187.13
        neg       edi                                           ;187.13
        add       edi, edx                                      ;187.13
        shl       edi, 8                                        ;187.13
        mov       DWORD PTR [-8+ecx+edi], 0                     ;187.13
                                ; LOE eax ecx ebx esi
.B2.271:                        ; Preds .B2.269 .B2.270
        test      esi, esi                                      ;188.13
        jbe       .B2.424       ; Prob 10%                      ;188.13
                                ; LOE eax ecx ebx esi
.B2.272:                        ; Preds .B2.271
        xor       edx, edx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.273:                        ; Preds .B2.273 .B2.272
        mov       edi, edx                                      ;188.13
        inc       edx                                           ;188.13
        shl       edi, 9                                        ;188.13
        cmp       edx, esi                                      ;188.13
        mov       BYTE PTR [252+ecx+edi], bl                    ;188.13
        mov       BYTE PTR [508+ecx+edi], bl                    ;188.13
        jb        .B2.273       ; Prob 63%                      ;188.13
                                ; LOE eax edx ecx ebx esi
.B2.274:                        ; Preds .B2.273
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       esi, DWORD PTR [1+edx+edx]                    ;188.13
                                ; LOE eax ecx ebx esi
.B2.275:                        ; Preds .B2.274 .B2.424
        lea       edx, DWORD PTR [-1+esi]                       ;188.13
        cmp       eax, edx                                      ;188.13
        jbe       .B2.277       ; Prob 10%                      ;188.13
                                ; LOE ecx ebx esi
.B2.276:                        ; Preds .B2.275
        mov       eax, ebx                                      ;188.13
        add       ebx, esi                                      ;188.13
        neg       eax                                           ;188.13
        add       eax, ebx                                      ;188.13
        shl       eax, 8                                        ;188.13
        mov       BYTE PTR [-4+ecx+eax], 0                      ;188.13
                                ; LOE
.B2.277:                        ; Preds .B2.275 .B2.276
        cmp       DWORD PTR [12+esp], 0                         ;191.21
        je        .B2.209       ; Prob 5%                       ;191.21
                                ; LOE
.B2.278:                        ; Preds .B2.252 .B2.277 .B2.251 .B2.360 .B2.413
                                ;       .B2.414
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFVEH   ;227.18
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DSETUP ;227.18
                                ; LOE
.B2.279:                        ; Preds .B2.278
        add       esp, 152                                      ;228.1
        pop       ebx                                           ;228.1
        pop       edi                                           ;228.1
        pop       esi                                           ;228.1
        mov       esp, ebp                                      ;228.1
        pop       ebp                                           ;228.1
        ret                                                     ;228.1
                                ; LOE
.B2.280:                        ; Preds .B2.60                  ; Infreq
        mov       edx, 1                                        ;
        mov       esi, edx                                      ;
        jmp       .B2.64        ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B2.281:                        ; Preds .B2.54                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.58        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.282:                        ; Preds .B2.48                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.52        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.283:                        ; Preds .B2.42                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.46        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.284:                        ; Preds .B2.36                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.40        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.285:                        ; Preds .B2.30                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.34        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.286:                        ; Preds .B2.24                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.28        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.287:                        ; Preds .B2.18                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.22        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.288:                        ; Preds .B2.74                  ; Infreq
        cmp       ecx, 16                                       ;142.14
        jl        .B2.296       ; Prob 10%                      ;142.14
                                ; LOE ecx ebx esi edi
.B2.289:                        ; Preds .B2.288                 ; Infreq
        xor       eax, eax                                      ;142.14
        mov       edx, ecx                                      ;142.14
        mov       DWORD PTR [esp], eax                          ;142.14
        and       edx, -16                                      ;142.14
        mov       DWORD PTR [20+esp], ebx                       ;142.14
        pxor      xmm0, xmm0                                    ;142.14
        mov       eax, DWORD PTR [300+edi]                      ;142.14
        mov       ebx, DWORD PTR [esp]                          ;142.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.290:                        ; Preds .B2.290 .B2.289         ; Infreq
        movdqu    XMMWORD PTR [ebx+eax], xmm0                   ;142.14
        add       ebx, 16                                       ;142.14
        cmp       ebx, edx                                      ;142.14
        jb        .B2.290       ; Prob 82%                      ;142.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.291:                        ; Preds .B2.290                 ; Infreq
        mov       ebx, DWORD PTR [20+esp]                       ;
                                ; LOE edx ecx ebx esi edi
.B2.292:                        ; Preds .B2.291 .B2.296         ; Infreq
        cmp       edx, ecx                                      ;142.14
        jae       .B2.76        ; Prob 10%                      ;142.14
                                ; LOE edx ecx ebx esi edi
.B2.293:                        ; Preds .B2.292                 ; Infreq
        mov       eax, DWORD PTR [300+edi]                      ;142.14
                                ; LOE eax edx ecx ebx esi
.B2.294:                        ; Preds .B2.294 .B2.293         ; Infreq
        mov       BYTE PTR [edx+eax], 0                         ;142.14
        inc       edx                                           ;142.14
        cmp       edx, ecx                                      ;142.14
        jb        .B2.294       ; Prob 82%                      ;142.14
        jmp       .B2.76        ; Prob 100%                     ;142.14
                                ; LOE eax edx ecx ebx esi
.B2.296:                        ; Preds .B2.288                 ; Infreq
        xor       edx, edx                                      ;142.14
        jmp       .B2.292       ; Prob 100%                     ;142.14
                                ; LOE edx ecx ebx esi edi
.B2.297:                        ; Preds .B2.127                 ; Infreq
        mov       edx, 1                                        ;
        mov       esi, edx                                      ;
        jmp       .B2.131       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B2.298:                        ; Preds .B2.121                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.125       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.299:                        ; Preds .B2.115                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.119       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.300:                        ; Preds .B2.109                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.113       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.301:                        ; Preds .B2.103                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.107       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.302:                        ; Preds .B2.97                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.101       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.303:                        ; Preds .B2.91                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.95        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.304:                        ; Preds .B2.85                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.89        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.305:                        ; Preds .B2.79                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.83        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.306:                        ; Preds .B2.141                 ; Infreq
        cmp       ecx, 4                                        ;160.14
        jl        .B2.314       ; Prob 10%                      ;160.14
                                ; LOE ecx ebx esi edi
.B2.307:                        ; Preds .B2.306                 ; Infreq
        xor       edx, edx                                      ;160.14
        mov       eax, ecx                                      ;160.14
        mov       DWORD PTR [esp], edx                          ;160.14
        and       eax, -4                                       ;160.14
        mov       DWORD PTR [20+esp], ebx                       ;160.14
        pxor      xmm0, xmm0                                    ;160.14
        mov       edx, DWORD PTR [372+edi]                      ;160.14
        mov       ebx, DWORD PTR [esp]                          ;160.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.308:                        ; Preds .B2.308 .B2.307         ; Infreq
        movdqu    XMMWORD PTR [edx+ebx*4], xmm0                 ;160.14
        add       ebx, 4                                        ;160.14
        cmp       ebx, eax                                      ;160.14
        jb        .B2.308       ; Prob 82%                      ;160.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.309:                        ; Preds .B2.308                 ; Infreq
        mov       ebx, DWORD PTR [20+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B2.310:                        ; Preds .B2.309 .B2.314         ; Infreq
        cmp       eax, ecx                                      ;160.14
        jae       .B2.143       ; Prob 10%                      ;160.14
                                ; LOE eax ecx ebx esi edi
.B2.311:                        ; Preds .B2.310                 ; Infreq
        mov       edx, DWORD PTR [372+edi]                      ;160.14
                                ; LOE eax edx ecx ebx esi
.B2.312:                        ; Preds .B2.312 .B2.311         ; Infreq
        mov       DWORD PTR [edx+eax*4], 0                      ;160.14
        inc       eax                                           ;160.14
        cmp       eax, ecx                                      ;160.14
        jb        .B2.312       ; Prob 82%                      ;160.14
        jmp       .B2.143       ; Prob 100%                     ;160.14
                                ; LOE eax edx ecx ebx esi
.B2.314:                        ; Preds .B2.306                 ; Infreq
        xor       eax, eax                                      ;160.14
        jmp       .B2.310       ; Prob 100%                     ;160.14
                                ; LOE eax ecx ebx esi edi
.B2.315:                        ; Preds .B2.158                 ; Infreq
        mov       edx, 1                                        ;
        mov       esi, edx                                      ;
        jmp       .B2.162       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B2.316:                        ; Preds .B2.152                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.156       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi xmm0
.B2.317:                        ; Preds .B2.150                 ; Infreq
        movss     xmm3, DWORD PTR [_2il0floatpacket.0]          ;163.35
        mulss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VHPCEFACTOR] ;163.35
        movss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;163.35
        andps     xmm0, xmm3                                    ;163.35
        pxor      xmm3, xmm0                                    ;163.35
        movss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;163.35
        movaps    xmm2, xmm3                                    ;163.35
        movss     xmm4, DWORD PTR [_2il0floatpacket.4]          ;163.35
        cmpltss   xmm2, xmm1                                    ;163.35
        andps     xmm1, xmm2                                    ;163.35
        movaps    xmm2, xmm3                                    ;163.35
        movaps    xmm6, xmm4                                    ;163.35
        addss     xmm2, xmm1                                    ;163.35
        addss     xmm6, xmm4                                    ;163.35
        subss     xmm2, xmm1                                    ;163.35
        movaps    xmm7, xmm2                                    ;163.35
        subss     xmm7, xmm3                                    ;163.35
        movaps    xmm5, xmm7                                    ;163.35
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.5]          ;163.35
        cmpnless  xmm5, xmm4                                    ;163.35
        andps     xmm5, xmm6                                    ;163.35
        andps     xmm7, xmm6                                    ;163.35
        subss     xmm2, xmm5                                    ;163.35
        addss     xmm2, xmm7                                    ;163.35
        orps      xmm2, xmm0                                    ;163.35
        cvtss2si  edx, xmm2                                     ;163.35
        cvtsi2ss  xmm0, edx                                     ;163.13
        jmp       .B2.152       ; Prob 100%                     ;163.13
                                ; LOE eax ecx ebx esi xmm0
.B2.318:                        ; Preds .B2.146                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.150       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.319:                        ; Preds .B2.174                 ; Infreq
        cmp       edi, 8                                        ;171.13
        jl        .B2.327       ; Prob 10%                      ;171.13
                                ; LOE ecx ebx esi edi
.B2.320:                        ; Preds .B2.319                 ; Infreq
        xor       edx, edx                                      ;171.13
        mov       eax, edi                                      ;171.13
        mov       DWORD PTR [16+esp], edx                       ;171.13
        and       eax, -8                                       ;171.13
        mov       edx, DWORD PTR [164+esi+ecx]                  ;171.13
        pxor      xmm0, xmm0                                    ;171.13
        mov       DWORD PTR [8+esp], esi                        ;171.13
        mov       esi, DWORD PTR [16+esp]                       ;171.13
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.321:                        ; Preds .B2.321 .B2.320         ; Infreq
        movdqu    XMMWORD PTR [edx+esi*2], xmm0                 ;171.13
        add       esi, 8                                        ;171.13
        cmp       esi, eax                                      ;171.13
        jb        .B2.321       ; Prob 82%                      ;171.13
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.322:                        ; Preds .B2.321                 ; Infreq
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax ecx ebx esi edi
.B2.323:                        ; Preds .B2.322 .B2.327         ; Infreq
        cmp       eax, edi                                      ;171.13
        jae       .B2.176       ; Prob 10%                      ;171.13
                                ; LOE eax ecx ebx esi edi
.B2.324:                        ; Preds .B2.323                 ; Infreq
        mov       ecx, DWORD PTR [164+esi+ecx]                  ;171.13
        xor       edx, edx                                      ;171.13
                                ; LOE eax edx ecx ebx edi
.B2.325:                        ; Preds .B2.325 .B2.324         ; Infreq
        mov       WORD PTR [ecx+eax*2], dx                      ;171.13
        inc       eax                                           ;171.13
        cmp       eax, edi                                      ;171.13
        jb        .B2.325       ; Prob 82%                      ;171.13
        jmp       .B2.176       ; Prob 100%                     ;171.13
                                ; LOE eax edx ecx ebx edi
.B2.327:                        ; Preds .B2.319                 ; Infreq
        xor       eax, eax                                      ;171.13
        jmp       .B2.323       ; Prob 100%                     ;171.13
                                ; LOE eax ecx ebx esi edi
.B2.328:                        ; Preds .B2.184                 ; Infreq
        cmp       esi, 8                                        ;177.13
        jl        .B2.336       ; Prob 10%                      ;177.13
                                ; LOE edx ebx esi
.B2.329:                        ; Preds .B2.328                 ; Infreq
        mov       ecx, esi                                      ;177.13
        xor       eax, eax                                      ;177.13
        mov       edi, DWORD PTR [200+ebx+edx]                  ;177.13
        and       ecx, -8                                       ;177.13
        pxor      xmm0, xmm0                                    ;177.13
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.330:                        ; Preds .B2.330 .B2.329         ; Infreq
        movdqu    XMMWORD PTR [edi+eax*2], xmm0                 ;177.13
        add       eax, 8                                        ;177.13
        cmp       eax, ecx                                      ;177.13
        jb        .B2.330       ; Prob 82%                      ;177.13
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.332:                        ; Preds .B2.330 .B2.336         ; Infreq
        cmp       ecx, esi                                      ;177.13
        jae       .B2.186       ; Prob 10%                      ;177.13
                                ; LOE edx ecx ebx esi
.B2.333:                        ; Preds .B2.332                 ; Infreq
        mov       eax, DWORD PTR [200+ebx+edx]                  ;177.13
        xor       edx, edx                                      ;177.13
                                ; LOE eax edx ecx esi
.B2.334:                        ; Preds .B2.334 .B2.333         ; Infreq
        mov       WORD PTR [eax+ecx*2], dx                      ;177.13
        inc       ecx                                           ;177.13
        cmp       ecx, esi                                      ;177.13
        jb        .B2.334       ; Prob 82%                      ;177.13
        jmp       .B2.186       ; Prob 100%                     ;177.13
                                ; LOE eax edx ecx esi
.B2.336:                        ; Preds .B2.328                 ; Infreq
        xor       ecx, ecx                                      ;177.13
        jmp       .B2.332       ; Prob 100%                     ;177.13
                                ; LOE edx ecx ebx esi
.B2.337:                        ; Preds .B2.195                 ; Infreq
        mov       edx, 1                                        ;
        mov       esi, edx                                      ;
        jmp       .B2.199       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B2.338:                        ; Preds .B2.189                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.193       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.339:                        ; Preds .B2.203                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.207       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.340:                        ; Preds .B2.221                 ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;194.17
        lea       edx, DWORD PTR [64+esp]                       ;194.17
        mov       DWORD PTR [esp], 53                           ;194.17
        lea       eax, DWORD PTR [esp]                          ;194.17
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_4 ;194.17
        push      32                                            ;194.17
        push      eax                                           ;194.17
        push      OFFSET FLAT: __STRLITPACK_26.0.2              ;194.17
        push      -2088435968                                   ;194.17
        push      911                                           ;194.17
        push      edx                                           ;194.17
        call      _for_write_seq_lis                            ;194.17
                                ; LOE
.B2.341:                        ; Preds .B2.340                 ; Infreq
        push      32                                            ;195.14
        xor       eax, eax                                      ;195.14
        push      eax                                           ;195.14
        push      eax                                           ;195.14
        push      -2088435968                                   ;195.14
        push      eax                                           ;195.14
        push      OFFSET FLAT: __STRLITPACK_27                  ;195.14
        call      _for_stop_core                                ;195.14
                                ; LOE
.B2.469:                        ; Preds .B2.341                 ; Infreq
        add       esp, 48                                       ;195.14
        jmp       .B2.223       ; Prob 100%                     ;195.14
                                ; LOE
.B2.342:                        ; Preds .B2.230                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B2.234       ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B2.343:                        ; Preds .B2.224                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B2.228       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.344:                        ; Preds .B2.247                 ; Infreq
        cmp       edi, 8                                        ;205.14
        jl        .B2.352       ; Prob 10%                      ;205.14
                                ; LOE ecx ebx esi edi
.B2.345:                        ; Preds .B2.344                 ; Infreq
        xor       edx, edx                                      ;205.14
        mov       eax, edi                                      ;205.14
        mov       DWORD PTR [32+esp], edx                       ;205.14
        and       eax, -8                                       ;205.14
        mov       DWORD PTR [20+esp], ebx                       ;205.14
        pxor      xmm0, xmm0                                    ;205.14
        mov       edx, DWORD PTR [80+ecx+esi]                   ;205.14
        mov       ebx, DWORD PTR [32+esp]                       ;205.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.346:                        ; Preds .B2.346 .B2.345         ; Infreq
        movdqu    XMMWORD PTR [edx+ebx*2], xmm0                 ;205.14
        add       ebx, 8                                        ;205.14
        cmp       ebx, eax                                      ;205.14
        jb        .B2.346       ; Prob 82%                      ;205.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.347:                        ; Preds .B2.346                 ; Infreq
        mov       ebx, DWORD PTR [20+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B2.348:                        ; Preds .B2.347 .B2.352         ; Infreq
        cmp       eax, edi                                      ;205.14
        jae       .B2.249       ; Prob 10%                      ;205.14
                                ; LOE eax ecx ebx esi edi
.B2.349:                        ; Preds .B2.348                 ; Infreq
        mov       ecx, DWORD PTR [80+ecx+esi]                   ;205.14
        xor       edx, edx                                      ;205.14
                                ; LOE eax edx ecx ebx edi
.B2.350:                        ; Preds .B2.350 .B2.349         ; Infreq
        mov       WORD PTR [ecx+eax*2], dx                      ;205.14
        inc       eax                                           ;205.14
        cmp       eax, edi                                      ;205.14
        jb        .B2.350       ; Prob 82%                      ;205.14
        jmp       .B2.249       ; Prob 100%                     ;205.14
                                ; LOE eax edx ecx ebx edi
.B2.352:                        ; Preds .B2.344                 ; Infreq
        xor       eax, eax                                      ;205.14
        jmp       .B2.348       ; Prob 100%                     ;205.14
                                ; LOE eax ecx ebx esi edi
.B2.353:                        ; Preds .B2.251                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;210.10
        xor       eax, eax                                      ;210.10
        test      edx, edx                                      ;210.10
        lea       ecx, DWORD PTR [32+esp]                       ;210.10
        push      32                                            ;210.10
        cmovle    edx, eax                                      ;210.10
        push      edx                                           ;210.10
        push      2                                             ;210.10
        push      ecx                                           ;210.10
        call      _for_check_mult_overflow                      ;210.10
                                ; LOE eax
.B2.354:                        ; Preds .B2.353                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+12] ;210.10
        and       eax, 1                                        ;210.10
        and       edx, 1                                        ;210.10
        add       edx, edx                                      ;210.10
        shl       eax, 4                                        ;210.10
        or        edx, 1                                        ;210.10
        or        edx, eax                                      ;210.10
        or        edx, 262144                                   ;210.10
        push      edx                                           ;210.10
        push      OFFSET FLAT: _DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND ;210.10
        push      DWORD PTR [56+esp]                            ;210.10
        call      _for_alloc_allocatable                        ;210.10
                                ; LOE eax
.B2.471:                        ; Preds .B2.354                 ; Infreq
        add       esp, 28                                       ;210.10
        mov       ebx, eax                                      ;210.10
                                ; LOE ebx
.B2.355:                        ; Preds .B2.471                 ; Infreq
        test      ebx, ebx                                      ;210.10
        je        .B2.358       ; Prob 50%                      ;210.10
                                ; LOE ebx
.B2.356:                        ; Preds .B2.355                 ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;212.17
        lea       edx, DWORD PTR [64+esp]                       ;212.17
        mov       DWORD PTR [16+esp], 56                        ;212.17
        lea       eax, DWORD PTR [16+esp]                       ;212.17
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_0 ;212.17
        push      32                                            ;212.17
        push      eax                                           ;212.17
        push      OFFSET FLAT: __STRLITPACK_30.0.2              ;212.17
        push      -2088435968                                   ;212.17
        push      911                                           ;212.17
        push      edx                                           ;212.17
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;210.10
        call      _for_write_seq_lis                            ;212.17
                                ; LOE
.B2.357:                        ; Preds .B2.356                 ; Infreq
        push      32                                            ;213.14
        xor       eax, eax                                      ;213.14
        push      eax                                           ;213.14
        push      eax                                           ;213.14
        push      -2088435968                                   ;213.14
        push      eax                                           ;213.14
        push      OFFSET FLAT: __STRLITPACK_31                  ;213.14
        call      _for_stop_core                                ;213.14
                                ; LOE
.B2.472:                        ; Preds .B2.357                 ; Infreq
        add       esp, 48                                       ;213.14
        jmp       .B2.360       ; Prob 100%                     ;213.14
                                ; LOE
.B2.358:                        ; Preds .B2.355                 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;210.10
        xor       eax, eax                                      ;210.10
        test      esi, esi                                      ;210.10
        cmovle    esi, eax                                      ;210.10
        mov       ecx, 32                                       ;210.10
        lea       edi, DWORD PTR [12+esp]                       ;210.10
        push      ecx                                           ;210.10
        push      esi                                           ;210.10
        push      2                                             ;210.10
        push      edi                                           ;210.10
        mov       edx, 1                                        ;210.10
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+12], 133 ;210.10
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+4], ecx ;210.10
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+16], edx ;210.10
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+8], eax ;210.10
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32], edx ;210.10
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+24], esi ;210.10
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+28], ecx ;210.10
        call      _for_check_mult_overflow                      ;210.10
                                ; LOE ebx
.B2.473:                        ; Preds .B2.358                 ; Infreq
        add       esp, 16                                       ;210.10
                                ; LOE ebx
.B2.359:                        ; Preds .B2.473                 ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;210.10
                                ; LOE
.B2.360:                        ; Preds .B2.472 .B2.359         ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+24] ;216.10
        test      ecx, ecx                                      ;216.10
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;216.10
        jle       .B2.278       ; Prob 50%                      ;216.10
                                ; LOE edx ecx
.B2.361:                        ; Preds .B2.360                 ; Infreq
        mov       esi, ecx                                      ;216.10
        shr       esi, 31                                       ;216.10
        add       esi, ecx                                      ;216.10
        sar       esi, 1                                        ;216.10
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;216.10
        test      esi, esi                                      ;216.10
        jbe       .B2.423       ; Prob 10%                      ;216.10
                                ; LOE edx ecx ebx esi
.B2.362:                        ; Preds .B2.361                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.363:                        ; Preds .B2.363 .B2.362         ; Infreq
        inc       eax                                           ;216.10
        mov       DWORD PTR [edi+ebx], edx                      ;216.10
        mov       DWORD PTR [32+edi+ebx], edx                   ;216.10
        add       edi, 64                                       ;216.10
        cmp       eax, esi                                      ;216.10
        jb        .B2.363       ; Prob 64%                      ;216.10
                                ; LOE eax edx ecx ebx esi edi
.B2.364:                        ; Preds .B2.363                 ; Infreq
        mov       edx, DWORD PTR [24+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;216.10
                                ; LOE eax edx ecx ebx esi
.B2.365:                        ; Preds .B2.364 .B2.423         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;216.10
        cmp       ecx, edi                                      ;216.10
        jbe       .B2.367       ; Prob 10%                      ;216.10
                                ; LOE eax edx ecx ebx esi
.B2.366:                        ; Preds .B2.365                 ; Infreq
        mov       edi, edx                                      ;216.10
        add       eax, edx                                      ;216.10
        neg       edi                                           ;216.10
        add       edi, eax                                      ;216.10
        shl       edi, 5                                        ;216.10
        mov       DWORD PTR [-32+ebx+edi], 0                    ;216.10
                                ; LOE edx ecx ebx esi
.B2.367:                        ; Preds .B2.365 .B2.366         ; Infreq
        test      esi, esi                                      ;217.10
        jbe       .B2.422       ; Prob 10%                      ;217.10
                                ; LOE edx ecx ebx esi
.B2.368:                        ; Preds .B2.367                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.369:                        ; Preds .B2.369 .B2.368         ; Infreq
        mov       edi, eax                                      ;217.10
        inc       eax                                           ;217.10
        shl       edi, 6                                        ;217.10
        cmp       eax, esi                                      ;217.10
        mov       BYTE PTR [4+ebx+edi], dl                      ;217.10
        mov       BYTE PTR [36+ebx+edi], dl                     ;217.10
        jb        .B2.369       ; Prob 64%                      ;217.10
                                ; LOE eax edx ecx ebx esi
.B2.370:                        ; Preds .B2.369                 ; Infreq
        mov       edx, DWORD PTR [24+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;217.10
                                ; LOE eax edx ecx ebx esi
.B2.371:                        ; Preds .B2.370 .B2.422         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;217.10
        cmp       ecx, edi                                      ;217.10
        jbe       .B2.373       ; Prob 10%                      ;217.10
                                ; LOE eax edx ecx ebx esi
.B2.372:                        ; Preds .B2.371                 ; Infreq
        mov       edi, edx                                      ;217.10
        add       eax, edx                                      ;217.10
        neg       edi                                           ;217.10
        add       edi, eax                                      ;217.10
        shl       edi, 5                                        ;217.10
        mov       BYTE PTR [-28+ebx+edi], 0                     ;217.10
                                ; LOE edx ecx ebx esi
.B2.373:                        ; Preds .B2.371 .B2.372         ; Infreq
        test      esi, esi                                      ;218.10
        jbe       .B2.421       ; Prob 10%                      ;218.10
                                ; LOE edx ecx ebx esi
.B2.374:                        ; Preds .B2.373                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.375:                        ; Preds .B2.375 .B2.374         ; Infreq
        mov       edi, eax                                      ;218.10
        inc       eax                                           ;218.10
        shl       edi, 6                                        ;218.10
        cmp       eax, esi                                      ;218.10
        mov       BYTE PTR [5+ebx+edi], dl                      ;218.10
        mov       BYTE PTR [37+ebx+edi], dl                     ;218.10
        jb        .B2.375       ; Prob 64%                      ;218.10
                                ; LOE eax edx ecx ebx esi
.B2.376:                        ; Preds .B2.375                 ; Infreq
        mov       edx, DWORD PTR [24+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;218.10
                                ; LOE eax edx ecx ebx esi
.B2.377:                        ; Preds .B2.376 .B2.421         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;218.10
        cmp       ecx, edi                                      ;218.10
        jbe       .B2.379       ; Prob 10%                      ;218.10
                                ; LOE eax edx ecx ebx esi
.B2.378:                        ; Preds .B2.377                 ; Infreq
        mov       edi, edx                                      ;218.10
        add       eax, edx                                      ;218.10
        neg       edi                                           ;218.10
        add       edi, eax                                      ;218.10
        shl       edi, 5                                        ;218.10
        mov       BYTE PTR [-27+ebx+edi], 0                     ;218.10
                                ; LOE edx ecx ebx esi
.B2.379:                        ; Preds .B2.377 .B2.378         ; Infreq
        test      esi, esi                                      ;219.10
        jbe       .B2.420       ; Prob 10%                      ;219.10
                                ; LOE edx ecx ebx esi
.B2.380:                        ; Preds .B2.379                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.381:                        ; Preds .B2.381 .B2.380         ; Infreq
        inc       eax                                           ;219.10
        mov       DWORD PTR [8+edi+ebx], edx                    ;219.10
        mov       DWORD PTR [40+edi+ebx], edx                   ;219.10
        add       edi, 64                                       ;219.10
        cmp       eax, esi                                      ;219.10
        jb        .B2.381       ; Prob 64%                      ;219.10
                                ; LOE eax edx ecx ebx esi edi
.B2.382:                        ; Preds .B2.381                 ; Infreq
        mov       edx, DWORD PTR [24+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;219.10
                                ; LOE eax edx ecx ebx esi
.B2.383:                        ; Preds .B2.382 .B2.420         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;219.10
        cmp       ecx, edi                                      ;219.10
        jbe       .B2.385       ; Prob 10%                      ;219.10
                                ; LOE eax edx ecx ebx esi
.B2.384:                        ; Preds .B2.383                 ; Infreq
        mov       edi, edx                                      ;219.10
        add       eax, edx                                      ;219.10
        neg       edi                                           ;219.10
        add       edi, eax                                      ;219.10
        shl       edi, 5                                        ;219.10
        mov       DWORD PTR [-24+ebx+edi], 0                    ;219.10
                                ; LOE edx ecx ebx esi
.B2.385:                        ; Preds .B2.383 .B2.384         ; Infreq
        test      esi, esi                                      ;220.10
        jbe       .B2.419       ; Prob 10%                      ;220.10
                                ; LOE edx ecx ebx esi
.B2.386:                        ; Preds .B2.385                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        xor       edi, edi                                      ;
        mov       edx, -943501440                               ;
                                ; LOE eax edx ecx ebx esi edi
.B2.387:                        ; Preds .B2.387 .B2.386         ; Infreq
        inc       eax                                           ;220.10
        mov       DWORD PTR [12+edi+ebx], edx                   ;220.10
        mov       DWORD PTR [44+edi+ebx], edx                   ;220.10
        add       edi, 64                                       ;220.10
        cmp       eax, esi                                      ;220.10
        jb        .B2.387       ; Prob 64%                      ;220.10
                                ; LOE eax edx ecx ebx esi edi
.B2.388:                        ; Preds .B2.387                 ; Infreq
        mov       edx, DWORD PTR [24+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;220.10
                                ; LOE eax edx ecx ebx esi
.B2.389:                        ; Preds .B2.388 .B2.419         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;220.10
        cmp       ecx, edi                                      ;220.10
        jbe       .B2.391       ; Prob 10%                      ;220.10
                                ; LOE eax edx ecx ebx esi
.B2.390:                        ; Preds .B2.389                 ; Infreq
        mov       edi, edx                                      ;220.10
        add       eax, edx                                      ;220.10
        neg       edi                                           ;220.10
        add       edi, eax                                      ;220.10
        shl       edi, 5                                        ;220.10
        mov       DWORD PTR [-20+ebx+edi], -943501440           ;220.10
                                ; LOE edx ecx ebx esi
.B2.391:                        ; Preds .B2.389 .B2.390         ; Infreq
        test      esi, esi                                      ;221.13
        jbe       .B2.418       ; Prob 10%                      ;221.13
                                ; LOE edx ecx ebx esi
.B2.392:                        ; Preds .B2.391                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.393:                        ; Preds .B2.393 .B2.392         ; Infreq
        inc       eax                                           ;221.13
        mov       DWORD PTR [16+edi+ebx], edx                   ;221.13
        mov       DWORD PTR [48+edi+ebx], edx                   ;221.13
        add       edi, 64                                       ;221.13
        cmp       eax, esi                                      ;221.13
        jb        .B2.393       ; Prob 64%                      ;221.13
                                ; LOE eax edx ecx ebx esi edi
.B2.394:                        ; Preds .B2.393                 ; Infreq
        mov       edx, DWORD PTR [24+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;221.13
                                ; LOE eax edx ecx ebx esi
.B2.395:                        ; Preds .B2.394 .B2.418         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;221.13
        cmp       ecx, edi                                      ;221.13
        jbe       .B2.397       ; Prob 10%                      ;221.13
                                ; LOE eax edx ecx ebx esi
.B2.396:                        ; Preds .B2.395                 ; Infreq
        mov       edi, edx                                      ;221.13
        add       eax, edx                                      ;221.13
        neg       edi                                           ;221.13
        add       edi, eax                                      ;221.13
        shl       edi, 5                                        ;221.13
        mov       DWORD PTR [-16+ebx+edi], 0                    ;221.13
                                ; LOE edx ecx ebx esi
.B2.397:                        ; Preds .B2.395 .B2.396         ; Infreq
        test      esi, esi                                      ;222.10
        jbe       .B2.417       ; Prob 10%                      ;222.10
                                ; LOE edx ecx ebx esi
.B2.398:                        ; Preds .B2.397                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.399:                        ; Preds .B2.399 .B2.398         ; Infreq
        mov       edi, eax                                      ;222.10
        inc       eax                                           ;222.10
        shl       edi, 6                                        ;222.10
        cmp       eax, esi                                      ;222.10
        mov       WORD PTR [20+ebx+edi], dx                     ;222.10
        mov       WORD PTR [52+ebx+edi], dx                     ;222.10
        jb        .B2.399       ; Prob 64%                      ;222.10
                                ; LOE eax edx ecx ebx esi
.B2.400:                        ; Preds .B2.399                 ; Infreq
        mov       edx, DWORD PTR [24+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;222.10
                                ; LOE eax edx ecx ebx esi
.B2.401:                        ; Preds .B2.400 .B2.417         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;222.10
        cmp       ecx, edi                                      ;222.10
        jbe       .B2.403       ; Prob 10%                      ;222.10
                                ; LOE eax edx ecx ebx esi
.B2.402:                        ; Preds .B2.401                 ; Infreq
        mov       edi, edx                                      ;222.10
        add       eax, edx                                      ;222.10
        neg       edi                                           ;222.10
        add       edi, eax                                      ;222.10
        xor       eax, eax                                      ;222.10
        shl       edi, 5                                        ;222.10
        mov       WORD PTR [-12+ebx+edi], ax                    ;222.10
                                ; LOE edx ecx ebx esi
.B2.403:                        ; Preds .B2.401 .B2.402         ; Infreq
        test      esi, esi                                      ;223.10
        jbe       .B2.416       ; Prob 10%                      ;223.10
                                ; LOE edx ecx ebx esi
.B2.404:                        ; Preds .B2.403                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.405:                        ; Preds .B2.405 .B2.404         ; Infreq
        mov       edi, eax                                      ;223.10
        inc       eax                                           ;223.10
        shl       edi, 6                                        ;223.10
        cmp       eax, esi                                      ;223.10
        mov       WORD PTR [22+ebx+edi], dx                     ;223.10
        mov       WORD PTR [54+ebx+edi], dx                     ;223.10
        jb        .B2.405       ; Prob 64%                      ;223.10
                                ; LOE eax edx ecx ebx esi
.B2.406:                        ; Preds .B2.405                 ; Infreq
        mov       edx, DWORD PTR [24+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;223.10
                                ; LOE eax edx ecx ebx esi
.B2.407:                        ; Preds .B2.406 .B2.416         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;223.10
        cmp       ecx, edi                                      ;223.10
        jbe       .B2.409       ; Prob 10%                      ;223.10
                                ; LOE eax edx ecx ebx esi
.B2.408:                        ; Preds .B2.407                 ; Infreq
        mov       edi, edx                                      ;223.10
        add       eax, edx                                      ;223.10
        neg       edi                                           ;223.10
        add       edi, eax                                      ;223.10
        xor       eax, eax                                      ;223.10
        shl       edi, 5                                        ;223.10
        mov       WORD PTR [-10+ebx+edi], ax                    ;223.10
                                ; LOE edx ecx ebx esi
.B2.409:                        ; Preds .B2.407 .B2.408         ; Infreq
        test      esi, esi                                      ;224.10
        jbe       .B2.415       ; Prob 10%                      ;224.10
                                ; LOE edx ecx ebx esi
.B2.410:                        ; Preds .B2.409                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.411:                        ; Preds .B2.411 .B2.410         ; Infreq
        mov       edi, eax                                      ;224.10
        inc       eax                                           ;224.10
        shl       edi, 6                                        ;224.10
        cmp       eax, esi                                      ;224.10
        mov       DWORD PTR [28+ebx+edi], edx                   ;224.10
        mov       DWORD PTR [60+ebx+edi], edx                   ;224.10
        jb        .B2.411       ; Prob 64%                      ;224.10
                                ; LOE eax edx ecx ebx esi
.B2.412:                        ; Preds .B2.411                 ; Infreq
        mov       edx, DWORD PTR [24+esp]                       ;
        lea       esi, DWORD PTR [1+eax+eax]                    ;224.10
                                ; LOE edx ecx ebx esi
.B2.413:                        ; Preds .B2.412 .B2.415         ; Infreq
        lea       eax, DWORD PTR [-1+esi]                       ;224.10
        cmp       ecx, eax                                      ;224.10
        jbe       .B2.278       ; Prob 10%                      ;224.10
                                ; LOE edx ebx esi
.B2.414:                        ; Preds .B2.413                 ; Infreq
        mov       eax, edx                                      ;224.10
        add       esi, edx                                      ;224.10
        neg       eax                                           ;224.10
        add       eax, esi                                      ;224.10
        shl       eax, 5                                        ;224.10
        mov       DWORD PTR [-4+ebx+eax], 0                     ;224.10
        jmp       .B2.278       ; Prob 100%                     ;224.10
                                ; LOE
.B2.415:                        ; Preds .B2.409                 ; Infreq
        mov       esi, 1                                        ;
        jmp       .B2.413       ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B2.416:                        ; Preds .B2.403                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.407       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.417:                        ; Preds .B2.397                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.401       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.418:                        ; Preds .B2.391                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.395       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.419:                        ; Preds .B2.385                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.389       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.420:                        ; Preds .B2.379                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.383       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.421:                        ; Preds .B2.373                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.377       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.422:                        ; Preds .B2.367                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.371       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.423:                        ; Preds .B2.361                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.365       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.424:                        ; Preds .B2.271                 ; Infreq
        mov       esi, 1                                        ;
        jmp       .B2.275       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B2.425:                        ; Preds .B2.265                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.269       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.426:                        ; Preds .B2.259                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.263       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.427:                        ; Preds .B2.253                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.257       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.428:                        ; Preds .B2.1                   ; Infreq
        cvtsi2ss  xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESTABLE] ;114.29
        mulss     xmm3, xmm0                                    ;114.29
        mov       ecx, 10                                       ;114.14
        movss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;114.29
        andps     xmm0, xmm3                                    ;114.29
        pxor      xmm3, xmm0                                    ;114.29
        movss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;114.29
        movaps    xmm2, xmm3                                    ;114.29
        movss     xmm4, DWORD PTR [_2il0floatpacket.4]          ;114.29
        cmpltss   xmm2, xmm1                                    ;114.29
        andps     xmm1, xmm2                                    ;114.29
        movaps    xmm2, xmm3                                    ;114.29
        movaps    xmm6, xmm4                                    ;114.29
        addss     xmm2, xmm1                                    ;114.29
        addss     xmm6, xmm4                                    ;114.29
        subss     xmm2, xmm1                                    ;114.29
        movaps    xmm7, xmm2                                    ;114.29
        subss     xmm7, xmm3                                    ;114.29
        movaps    xmm5, xmm7                                    ;114.29
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.5]          ;114.29
        cmpnless  xmm5, xmm4                                    ;114.29
        andps     xmm5, xmm6                                    ;114.29
        andps     xmm7, xmm6                                    ;114.29
        subss     xmm2, xmm5                                    ;114.29
        addss     xmm2, xmm7                                    ;114.29
        orps      xmm2, xmm0                                    ;114.29
        cvtss2si  edx, xmm2                                     ;114.29
        add       eax, edx                                      ;114.65
        cmp       eax, 10                                       ;114.14
        cmovge    ecx, eax                                      ;114.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH], ecx ;114.14
        jmp       .B2.5         ; Prob 100%                     ;114.14
                                ; LOE ecx
.B2.476:                        ; Preds .B2.219                 ; Infreq
        test      ebx, ebx                                      ;192.13
        jmp       .B2.221       ; Prob 100%                     ;192.13
        ALIGN     16
                                ; LOE ebx
; mark_end;
_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_16.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_28.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_MODULE_mp_DEALLOCATE_DYNUST_VEH
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_MODULE_mp_DEALLOCATE_DYNUST_VEH
_DYNUST_VEH_MODULE_mp_DEALLOCATE_DYNUST_VEH	PROC NEAR 
.B3.1:                          ; Preds .B3.0
        push      esi                                           ;231.12
        push      edi                                           ;231.12
        push      ebx                                           ;231.12
        push      ebp                                           ;231.12
        push      esi                                           ;231.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;235.4
        test      ecx, ecx                                      ;235.4
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;241.4
        jle       .B3.8         ; Prob 28%                      ;235.4
                                ; LOE eax ecx ebx esi edi
.B3.2:                          ; Preds .B3.1
        mov       ebp, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;236.7
        mov       edx, 1                                        ;
        shl       ebp, 8                                        ;
        mov       ebx, edx                                      ;
        neg       ebp                                           ;
        mov       esi, ecx                                      ;
        mov       DWORD PTR [esp], eax                          ;
        add       ebp, eax                                      ;
                                ; LOE ebx ebp esi
.B3.3:                          ; Preds .B3.6 .B3.2
        mov       edi, ebx                                      ;236.7
        shl       edi, 8                                        ;236.7
        mov       eax, DWORD PTR [56+edi+ebp]                   ;236.18
        mov       edx, eax                                      ;236.7
        shr       edx, 1                                        ;236.7
        and       eax, 1                                        ;236.7
        and       edx, 1                                        ;236.7
        add       eax, eax                                      ;236.7
        shl       edx, 2                                        ;236.7
        or        edx, eax                                      ;236.7
        or        edx, 262144                                   ;236.7
        push      edx                                           ;236.7
        push      DWORD PTR [44+edi+ebp]                        ;236.7
        call      _for_dealloc_allocatable                      ;236.7
                                ; LOE ebx ebp esi edi
.B3.4:                          ; Preds .B3.3
        mov       eax, DWORD PTR [176+edi+ebp]                  ;237.18
        mov       edx, eax                                      ;237.7
        shr       edx, 1                                        ;237.7
        and       eax, 1                                        ;237.7
        and       edx, 1                                        ;237.7
        add       eax, eax                                      ;237.7
        shl       edx, 2                                        ;237.7
        or        edx, eax                                      ;237.7
        or        edx, 262144                                   ;237.7
        push      edx                                           ;237.7
        push      DWORD PTR [164+edi+ebp]                       ;237.7
        mov       DWORD PTR [44+ebp+edi], 0                     ;236.7
        and       DWORD PTR [56+ebp+edi], -2                    ;236.7
        call      _for_dealloc_allocatable                      ;237.7
                                ; LOE ebx ebp esi edi
.B3.5:                          ; Preds .B3.4
        mov       eax, DWORD PTR [212+edi+ebp]                  ;238.18
        mov       edx, eax                                      ;238.7
        shr       edx, 1                                        ;238.7
        and       eax, 1                                        ;238.7
        and       edx, 1                                        ;238.7
        add       eax, eax                                      ;238.7
        shl       edx, 2                                        ;238.7
        or        edx, eax                                      ;238.7
        or        edx, 262144                                   ;238.7
        push      edx                                           ;238.7
        push      DWORD PTR [200+edi+ebp]                       ;238.7
        mov       DWORD PTR [164+ebp+edi], 0                    ;237.7
        and       DWORD PTR [176+ebp+edi], -2                   ;237.7
        call      _for_dealloc_allocatable                      ;238.7
                                ; LOE ebx ebp esi edi
.B3.29:                         ; Preds .B3.5
        add       esp, 24                                       ;238.7
                                ; LOE ebx ebp esi edi
.B3.6:                          ; Preds .B3.29
        inc       ebx                                           ;239.4
        and       DWORD PTR [212+ebp+edi], -2                   ;238.7
        mov       DWORD PTR [200+ebp+edi], 0                    ;238.7
        cmp       ebx, esi                                      ;239.4
        jle       .B3.3         ; Prob 82%                      ;239.4
                                ; LOE ebx ebp esi
.B3.7:                          ; Preds .B3.6
        mov       eax, DWORD PTR [esp]                          ;
                                ; LOE eax ebx esi edi
.B3.8:                          ; Preds .B3.1 .B3.7
        mov       ebp, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+12] ;241.4
        test      ebp, 1                                        ;241.4
        je        .B3.17        ; Prob 60%                      ;241.4
                                ; LOE eax ebx ebp esi edi
.B3.9:                          ; Preds .B3.8
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+24] ;241.4
        mov       ecx, eax                                      ;241.4
        shl       edx, 8                                        ;241.4
        add       edx, eax                                      ;241.4
        cmp       eax, edx                                      ;241.4
        jae       .B3.17        ; Prob 10%                      ;241.4
                                ; LOE eax edx ecx ebx ebp esi edi
.B3.10:                         ; Preds .B3.9
        mov       DWORD PTR [esp], eax                          ;
        mov       ebx, edx                                      ;
        mov       esi, ecx                                      ;
                                ; LOE ebx ebp esi
.B3.11:                         ; Preds .B3.15 .B3.10
        mov       edi, DWORD PTR [56+esi]                       ;241.4
        test      edi, 1                                        ;241.4
        jne       .B3.25        ; Prob 3%                       ;241.4
                                ; LOE ebx ebp esi edi
.B3.12:                         ; Preds .B3.11 .B3.26
        mov       edi, DWORD PTR [128+esi]                      ;241.4
        test      edi, 1                                        ;241.4
        jne       .B3.23        ; Prob 3%                       ;241.4
                                ; LOE ebx ebp esi edi
.B3.13:                         ; Preds .B3.12 .B3.24
        mov       edi, DWORD PTR [176+esi]                      ;241.4
        test      edi, 1                                        ;241.4
        jne       .B3.21        ; Prob 3%                       ;241.4
                                ; LOE ebx ebp esi edi
.B3.14:                         ; Preds .B3.13 .B3.22
        mov       edi, DWORD PTR [212+esi]                      ;241.4
        test      edi, 1                                        ;241.4
        jne       .B3.19        ; Prob 3%                       ;241.4
                                ; LOE ebx ebp esi edi
.B3.15:                         ; Preds .B3.14 .B3.20
        add       esi, 256                                      ;241.4
        cmp       esi, ebx                                      ;241.4
        jb        .B3.11        ; Prob 82%                      ;241.4
                                ; LOE ebx ebp esi
.B3.16:                         ; Preds .B3.15
        mov       eax, DWORD PTR [esp]                          ;
                                ; LOE eax ebx ebp esi edi
.B3.17:                         ; Preds .B3.16 .B3.9 .B3.8
        mov       ecx, ebp                                      ;241.4
        mov       edx, ebp                                      ;241.4
        shr       ecx, 1                                        ;241.4
        and       edx, 1                                        ;241.4
        and       ecx, 1                                        ;241.4
        add       edx, edx                                      ;241.4
        shl       ecx, 2                                        ;241.4
        or        ecx, edx                                      ;241.4
        or        ecx, 262144                                   ;241.4
        push      ecx                                           ;241.4
        push      eax                                           ;241.4
        call      _for_dealloc_allocatable                      ;241.4
                                ; LOE ebx ebp esi edi
.B3.18:                         ; Preds .B3.17
        and       ebp, -2                                       ;241.4
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH], 0 ;241.4
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+12], ebp ;241.4
        add       esp, 12                                       ;242.1
        pop       ebp                                           ;242.1
        pop       ebx                                           ;242.1
        pop       edi                                           ;242.1
        pop       esi                                           ;242.1
        ret                                                     ;242.1
                                ; LOE
.B3.19:                         ; Preds .B3.14                  ; Infreq
        push      262145                                        ;241.4
        push      DWORD PTR [200+esi]                           ;241.4
        call      _for_deallocate                               ;241.4
                                ; LOE ebx ebp esi edi
.B3.31:                         ; Preds .B3.19                  ; Infreq
        add       esp, 8                                        ;241.4
                                ; LOE ebx ebp esi edi
.B3.20:                         ; Preds .B3.31                  ; Infreq
        and       edi, -2                                       ;241.4
        mov       DWORD PTR [200+esi], 0                        ;241.4
        mov       DWORD PTR [212+esi], edi                      ;241.4
        jmp       .B3.15        ; Prob 100%                     ;241.4
                                ; LOE ebx ebp esi
.B3.21:                         ; Preds .B3.13                  ; Infreq
        push      262145                                        ;241.4
        push      DWORD PTR [164+esi]                           ;241.4
        call      _for_deallocate                               ;241.4
                                ; LOE ebx ebp esi edi
.B3.32:                         ; Preds .B3.21                  ; Infreq
        add       esp, 8                                        ;241.4
                                ; LOE ebx ebp esi edi
.B3.22:                         ; Preds .B3.32                  ; Infreq
        and       edi, -2                                       ;241.4
        mov       DWORD PTR [164+esi], 0                        ;241.4
        mov       DWORD PTR [176+esi], edi                      ;241.4
        jmp       .B3.14        ; Prob 100%                     ;241.4
                                ; LOE ebx ebp esi
.B3.23:                         ; Preds .B3.12                  ; Infreq
        push      262145                                        ;241.4
        push      DWORD PTR [116+esi]                           ;241.4
        call      _for_deallocate                               ;241.4
                                ; LOE ebx ebp esi edi
.B3.33:                         ; Preds .B3.23                  ; Infreq
        add       esp, 8                                        ;241.4
                                ; LOE ebx ebp esi edi
.B3.24:                         ; Preds .B3.33                  ; Infreq
        and       edi, -2                                       ;241.4
        mov       DWORD PTR [116+esi], 0                        ;241.4
        mov       DWORD PTR [128+esi], edi                      ;241.4
        jmp       .B3.13        ; Prob 100%                     ;241.4
                                ; LOE ebx ebp esi
.B3.25:                         ; Preds .B3.11                  ; Infreq
        push      262145                                        ;241.4
        push      DWORD PTR [44+esi]                            ;241.4
        call      _for_deallocate                               ;241.4
                                ; LOE ebx ebp esi edi
.B3.34:                         ; Preds .B3.25                  ; Infreq
        add       esp, 8                                        ;241.4
                                ; LOE ebx ebp esi edi
.B3.26:                         ; Preds .B3.34                  ; Infreq
        and       edi, -2                                       ;241.4
        mov       DWORD PTR [44+esi], 0                         ;241.4
        mov       DWORD PTR [56+esi], edi                       ;241.4
        jmp       .B3.12        ; Prob 100%                     ;241.4
        ALIGN     16
                                ; LOE ebx ebp esi
; mark_end;
_DYNUST_VEH_MODULE_mp_DEALLOCATE_DYNUST_VEH ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_MODULE_mp_DEALLOCATE_DYNUST_VEH
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_MODULE_mp_SETDELAYTOLE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_MODULE_mp_SETDELAYTOLE
_DYNUST_VEH_MODULE_mp_SETDELAYTOLE	PROC NEAR 
; parameter 1: 8 + esp
.B4.1:                          ; Preds .B4.0
        push      ebx                                           ;245.12
        mov       ebx, DWORD PTR [8+esp]                        ;245.12
        mov       eax, DWORD PTR [ebx]                          ;251.4
        cmp       eax, 1                                        ;251.9
        je        .B4.6         ; Prob 16%                      ;251.9
                                ; LOE eax ebx ebp esi edi
.B4.2:                          ; Preds .B4.10 .B4.1
        cmp       eax, 1000000                                  ;258.9
        jge       .B4.4         ; Prob 50%                      ;258.9
                                ; LOE eax ebp esi edi
.B4.3:                          ; Preds .B4.2
        mov       edx, eax                                      ;259.6
        jmp       .B4.5         ; Prob 100%                     ;259.6
                                ; LOE eax edx ebp esi edi
.B4.4:                          ; Preds .B4.2
        cvtsi2ss  xmm0, eax                                     ;261.27
        divss     xmm0, DWORD PTR [_2il0floatpacket.8]          ;261.22
        cvttss2si edx, xmm0                                     ;261.22
        imul      edx, edx, -1000000                            ;261.44
        add       edx, eax                                      ;261.6
        test      edx, edx                                      ;261.6
        jg        L5            ; Prob 50%                      ;261.6
        mov       edx, 1                                        ;261.6
L5:                                                             ;
                                ; LOE eax edx ebp esi edi
.B4.5:                          ; Preds .B4.3 .B4.4
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;263.32
        pxor      xmm1, xmm1                                    ;263.4
        neg       ecx                                           ;263.4
        add       ecx, eax                                      ;263.4
        movss     xmm0, DWORD PTR [_DYNUST_VEH_MODULE_mp_SETDELAYTOLE$RANTMP.0.4-4+edx*4] ;263.40
        mulss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;263.53
        shl       ecx, 8                                        ;263.4
        maxss     xmm1, xmm0                                    ;263.4
        cvttss2si edx, xmm1                                     ;263.4
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;263.32
        mov       DWORD PTR [244+eax+ecx], edx                  ;263.4
        pop       ebx                                           ;264.1
        ret                                                     ;264.1
                                ; LOE
.B4.6:                          ; Preds .B4.1                   ; Infreq
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_ISTRM     ;253.11
        call      _RNSET                                        ;253.11
                                ; LOE ebx ebp esi edi
.B4.7:                          ; Preds .B4.6                   ; Infreq
        push      OFFSET FLAT: _DYNUST_VEH_MODULE_mp_SETDELAYTOLE$RANTMP.0.4 ;254.11
        push      OFFSET FLAT: __NLITPACK_0.0.4                 ;254.11
        call      _RNNOA                                        ;254.11
                                ; LOE ebx ebp esi edi
.B4.8:                          ; Preds .B4.7                   ; Infreq
        push      OFFSET FLAT: __NLITPACK_1.0.4                 ;255.11
        push      OFFSET FLAT: _DYNUST_VEH_MODULE_mp_SETDELAYTOLE$RANTMP.0.4 ;255.11
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_DELTOLSTD ;255.11
        push      OFFSET FLAT: __NLITPACK_0.0.4                 ;255.11
        call      _SSCAL                                        ;255.11
                                ; LOE ebx ebp esi edi
.B4.9:                          ; Preds .B4.8                   ; Infreq
        push      OFFSET FLAT: __NLITPACK_1.0.4                 ;256.11
        push      OFFSET FLAT: _DYNUST_VEH_MODULE_mp_SETDELAYTOLE$RANTMP.0.4 ;256.11
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_DELTOLMEAN ;256.11
        push      OFFSET FLAT: __NLITPACK_0.0.4                 ;256.11
        call      _SADD                                         ;256.11
                                ; LOE ebx ebp esi edi
.B4.13:                         ; Preds .B4.9                   ; Infreq
        add       esp, 44                                       ;256.11
                                ; LOE ebx ebp esi edi
.B4.10:                         ; Preds .B4.13                  ; Infreq
        mov       eax, DWORD PTR [ebx]                          ;258.4
        jmp       .B4.2         ; Prob 100%                     ;258.4
        ALIGN     16
                                ; LOE eax ebp esi edi
; mark_end;
_DYNUST_VEH_MODULE_mp_SETDELAYTOLE ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_DYNUST_VEH_MODULE_mp_SETDELAYTOLE$RANTMP.0.4	DB ?	; pad
	ORG $+3999998	; pad
	DB ?	; pad
_BSS	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.4	DD	1000000
__NLITPACK_1.0.4	DD	1
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_MODULE_mp_SETDELAYTOLE
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
	PUBLIC _DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND
_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE
_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_VEH_MODULE_mp_M_DYNUST_VEH
_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 3 DUP (0H)	; pad
_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T205_	DD 11 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 9 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 3 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 5 DUP (0H)	; pad
_DYNUST_VEH_MODULE_mp_ALLOCATE_DYNUST_VEH$BLK..T394_	DD 4 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 3 DUP (0H)	; pad
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DD 6 DUP (0H)	; pad
__STRLITPACK_2	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	95
	DB	110
	DB	100
	DB	101
	DB	37
	DB	73
	DB	110
	DB	116
	DB	68
	DB	101
	DB	115
	DB	116
	DB	90
	DB	111
	DB	110
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_14	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_17	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	37
	DB	100
	DB	101
	DB	99
	DB	105
	DB	115
	DB	105
	DB	111
	DB	110
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_19	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	37
	DB	110
	DB	101
	DB	120
	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_21	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	37
	DB	73
	DB	110
	DB	116
	DB	68
	DB	101
	DB	115
	DB	116
	DB	80
	DB	97
	DB	116
	DB	104
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_23	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_6	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	37
	DB	73
	DB	110
	DB	116
	DB	68
	DB	101
	DB	115
	DB	116
	DB	68
	DB	119
	DB	101
	DB	108
	DB	108
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_25	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_4	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	95
	DB	110
	DB	100
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_27	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_0	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	108
	DB	97
	DB	115
	DB	116
	DB	95
	DB	115
	DB	116
	DB	97
	DB	110
	DB	100
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.0	DD	042c80000H
_2il0floatpacket.1	DD	0c7c34f80H
_2il0floatpacket.2	DD	080000000H
_2il0floatpacket.3	DD	04b000000H
_2il0floatpacket.4	DD	03f000000H
_2il0floatpacket.5	DD	0bf000000H
_2il0floatpacket.6	DD	03f800000H
_2il0floatpacket.8	DD	049742400H
_2il0floatpacket.9	DD	042c80000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_DELTOLMEAN:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DELTOLSTD:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ISTRM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFSTOPS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VHPCEFACTOR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NV_VEBUFFER:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXVEHICLESTABLE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RUNMODE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ERROR:BYTE
	COMM _DYNUST_VEH_MODULE_mp_CLASSPRO:BYTE:20
	COMM _DYNUST_VEH_MODULE_mp_CLASSPRO2:BYTE:28
	COMM _DYNUST_VEH_MODULE_mp_NOOFBUSES:BYTE:4
_DATA	ENDS
EXTRN	_RNSET:PROC
EXTRN	_RNNOA:PROC
EXTRN	_SSCAL:PROC
EXTRN	_SADD:PROC
EXTRN	_for_deallocate:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DSETUP:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
