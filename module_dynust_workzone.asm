; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _WORKZONE_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _WORKZONE_MODULE$
_WORKZONE_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_WORKZONE_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WORKZONE_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _WORKZONE_MODULE_mp_WZ_SCAN
; mark_begin;
       ALIGN     16
	PUBLIC _WORKZONE_MODULE_mp_WZ_SCAN
_WORKZONE_MODULE_mp_WZ_SCAN	PROC NEAR 
; parameter 1: 32 + esp
.B2.1:                          ; Preds .B2.0
        push      esi                                           ;26.12
        push      edi                                           ;26.12
        push      ebx                                           ;26.12
        push      ebp                                           ;26.12
        sub       esp, 12                                       ;26.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONENUM] ;27.7
        test      eax, eax                                      ;27.7
        jle       .B2.10        ; Prob 2%                       ;27.7
                                ; LOE eax ebx ebp edi
.B2.2:                          ; Preds .B2.1
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE+32] ;28.10
        mov       esi, 1                                        ;
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE] ;28.10
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [4+esp], eax                        ;
                                ; LOE edx esi
.B2.3:                          ; Preds .B2.8 .B2.2
        mov       eax, DWORD PTR [32+esp]                       ;28.10
        lea       ecx, DWORD PTR [esi*4]                        ;28.33
        lea       ebx, DWORD PTR [ecx+ecx*8]                    ;28.33
        lea       ecx, DWORD PTR [edx+ebx]                      ;28.33
        movss     xmm1, DWORD PTR [eax]                         ;28.10
        imul      eax, DWORD PTR [8+esp], -36                   ;28.33
        comiss    xmm1, DWORD PTR [8+eax+ecx]                   ;28.16
        jb        .B2.8         ; Prob 50%                      ;28.16
                                ; LOE eax edx ecx ebx esi xmm1
.B2.4:                          ; Preds .B2.3
        movss     xmm0, DWORD PTR [12+eax+ecx]                  ;28.43
        comiss    xmm0, xmm1                                    ;28.41
        jbe       .B2.8         ; Prob 50%                      ;28.41
                                ; LOE eax edx ecx ebx esi
.B2.5:                          ; Preds .B2.4
        mov       edi, esi                                      ;29.10
        sub       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WZSTARTFLAG+32] ;29.10
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WZSTARTFLAG] ;29.7
        test      BYTE PTR [ebp+edi*4], 1                       ;29.15
        jne       .B2.8         ; Prob 40%                      ;29.15
                                ; LOE eax edx ecx ebx ebp esi edi
.B2.6:                          ; Preds .B2.5
        movss     xmm0, DWORD PTR [16+eax+ecx]                  ;31.19
        imul      eax, DWORD PTR [8+esp], -36                   ;31.19
        movss     DWORD PTR [esp], xmm0                         ;31.19
        add       edx, eax                                      ;31.19
        push      OFFSET FLAT: __NLITPACK_0.0.3                 ;31.19
        lea       ecx, DWORD PTR [ebx+edx]                      ;31.19
        mov       DWORD PTR [ebp+edi*4], -1                     ;30.14
        lea       edx, DWORD PTR [4+ebx+edx]                    ;31.19
        push      edx                                           ;31.19
        push      ecx                                           ;31.19
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;31.19
                                ; LOE eax ebx esi
.B2.21:                         ; Preds .B2.6
        add       esp, 12                                       ;31.19
                                ; LOE eax ebx esi
.B2.7:                          ; Preds .B2.21
        imul      ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE+32], -36 ;31.19
        movss     xmm3, DWORD PTR [_2il0floatpacket.6]          ;31.19
        subss     xmm3, DWORD PTR [esp]                         ;31.19
        add       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE] ;31.19
        movaps    xmm0, xmm3                                    ;31.19
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;31.19
        neg       edi                                           ;31.19
        movss     xmm1, DWORD PTR [24+ebp+ebx]                  ;31.19
        add       edi, eax                                      ;31.19
        divss     xmm1, DWORD PTR [_2il0floatpacket.5]          ;31.19
        imul      edx, edi, 900                                 ;31.19
        mulss     xmm3, xmm1                                    ;31.19
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;31.19
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;31.19
        mov       edi, DWORD PTR [432+ecx+edx]                  ;31.19
        mov       DWORD PTR [28+ebp+ebx], edi                   ;31.19
        movsx     edi, WORD PTR [682+ecx+edx]                   ;31.19
        mov       DWORD PTR [32+ebp+ebx], edi                   ;31.19
        movsx     edi, BYTE PTR [668+ecx+edx]                   ;31.19
        cvtsi2ss  xmm2, edi                                     ;31.19
        imul      edi, eax, 152                                 ;31.19
        mulss     xmm0, xmm2                                    ;31.19
        mulss     xmm3, xmm2                                    ;31.19
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;31.19
        mov       ebx, DWORD PTR [20+ebp+ebx]                   ;31.19
        movss     DWORD PTR [432+ecx+edx], xmm3                 ;31.19
        mulss     xmm0, DWORD PTR [20+eax+edi]                  ;31.19
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE+32] ;31.19
        movss     DWORD PTR [672+ecx+edx], xmm0                 ;31.19
        mov       WORD PTR [682+ecx+edx], bx                    ;31.19
        mov       DWORD PTR [8+esp], eax                        ;31.19
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE] ;31.19
                                ; LOE edx esi
.B2.8:                          ; Preds .B2.7 .B2.5 .B2.4 .B2.3
        inc       esi                                           ;34.7
        cmp       esi, DWORD PTR [4+esp]                        ;34.7
        jle       .B2.3         ; Prob 82%                      ;34.7
                                ; LOE edx esi
.B2.9:                          ; Preds .B2.8
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONENUM] ;36.7
                                ; LOE eax ebx ebp edi
.B2.10:                         ; Preds .B2.9 .B2.1
        test      eax, eax                                      ;36.7
        jle       .B2.18        ; Prob 2%                       ;36.7
                                ; LOE eax ebx ebp edi
.B2.11:                         ; Preds .B2.10
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE] ;37.9
        mov       esi, 1                                        ;
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE+32] ;37.9
        mov       DWORD PTR [4+esp], eax                        ;
                                ; LOE edx ecx esi
.B2.12:                         ; Preds .B2.16 .B2.11
        imul      ebp, edx, -36                                 ;37.15
        lea       ebx, DWORD PTR [esi*4]                        ;37.15
        mov       eax, DWORD PTR [32+esp]                       ;37.9
        lea       ebx, DWORD PTR [ebx+ebx*8]                    ;37.15
        lea       edi, DWORD PTR [ecx+ebx]                      ;37.15
        movss     xmm0, DWORD PTR [eax]                         ;37.9
        comiss    xmm0, DWORD PTR [12+ebp+edi]                  ;37.15
        jbe       .B2.16        ; Prob 50%                      ;37.15
                                ; LOE edx ecx ebx esi
.B2.13:                         ; Preds .B2.12
        mov       ebp, esi                                      ;38.9
        sub       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WZSTARTFLAG+32] ;38.9
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WZSTARTFLAG] ;38.6
        test      BYTE PTR [eax+ebp*4], 1                       ;38.9
        je        .B2.16        ; Prob 60%                      ;38.9
                                ; LOE eax edx ecx ebx ebp esi
.B2.14:                         ; Preds .B2.13
        mov       DWORD PTR [eax+ebp*4], 0                      ;39.10
        imul      eax, edx, -36                                 ;40.18
        add       ecx, eax                                      ;40.18
        push      OFFSET FLAT: __NLITPACK_1.0.4                 ;40.18
        lea       edx, DWORD PTR [ebx+ecx]                      ;40.18
        lea       ecx, DWORD PTR [4+ecx+ebx]                    ;40.18
        push      ecx                                           ;40.18
        push      edx                                           ;40.18
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;40.18
                                ; LOE eax ebx esi
.B2.22:                         ; Preds .B2.14
        add       esp, 12                                       ;40.18
                                ; LOE eax ebx esi
.B2.15:                         ; Preds .B2.22
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;40.18
        neg       ecx                                           ;40.18
        add       ecx, eax                                      ;40.18
        imul      edx, ecx, 900                                 ;40.18
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;40.18
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;40.18
        imul      ecx, eax, 152                                 ;40.18
        movsx     edi, BYTE PTR [668+ebp+edx]                   ;40.18
        cvtsi2ss  xmm0, edi                                     ;40.18
        imul      edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE+32], -36 ;40.18
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;40.18
        mulss     xmm0, DWORD PTR [20+eax+ecx]                  ;40.18
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE] ;40.18
        add       ebx, ecx                                      ;40.18
        movss     DWORD PTR [672+ebp+edx], xmm0                 ;40.18
        mov       eax, DWORD PTR [28+edi+ebx]                   ;40.18
        mov       ebx, DWORD PTR [32+edi+ebx]                   ;40.18
        mov       DWORD PTR [432+ebp+edx], eax                  ;40.18
        mov       WORD PTR [682+ebp+edx], bx                    ;40.18
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE+32] ;40.18
                                ; LOE edx ecx esi
.B2.16:                         ; Preds .B2.15 .B2.13 .B2.12
        inc       esi                                           ;43.7
        cmp       esi, DWORD PTR [4+esp]                        ;43.7
        jle       .B2.12        ; Prob 82%                      ;43.7
                                ; LOE edx ecx esi
.B2.18:                         ; Preds .B2.16 .B2.10
        add       esp, 12                                       ;45.1
        pop       ebp                                           ;45.1
        pop       ebx                                           ;45.1
        pop       edi                                           ;45.1
        pop       esi                                           ;45.1
        ret                                                     ;45.1
        ALIGN     16
                                ; LOE
; mark_end;
_WORKZONE_MODULE_mp_WZ_SCAN ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WORKZONE_MODULE_mp_WZ_SCAN
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _WORKZONE_MODULE_mp_WZ_ACTIVATE
; mark_begin;
       ALIGN     16
	PUBLIC _WORKZONE_MODULE_mp_WZ_ACTIVATE
_WORKZONE_MODULE_mp_WZ_ACTIVATE	PROC NEAR 
; parameter 1: 20 + esp
.B3.1:                          ; Preds .B3.0
        push      esi                                           ;48.12
        push      ebx                                           ;48.12
        push      ebp                                           ;48.12
        push      esi                                           ;48.12
        mov       ebx, DWORD PTR [20+esp]                       ;48.12
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE+32], -36 ;49.7
        mov       eax, DWORD PTR [ebx]                          ;49.12
        shl       eax, 2                                        ;49.12
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE] ;49.7
        lea       ecx, DWORD PTR [eax+eax*8]                    ;49.12
        movss     xmm0, DWORD PTR [16+ecx+edx]                  ;49.7
        lea       eax, DWORD PTR [ecx+edx]                      ;50.13
        movss     DWORD PTR [esp], xmm0                         ;49.7
        lea       edx, DWORD PTR [4+edx+ecx]                    ;50.13
        push      OFFSET FLAT: __NLITPACK_0.0.3                 ;50.13
        push      edx                                           ;50.13
        push      eax                                           ;50.13
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;50.13
                                ; LOE eax ebx edi
.B3.5:                          ; Preds .B3.1
        add       esp, 12                                       ;50.13
                                ; LOE eax ebx edi
.B3.2:                          ; Preds .B3.5
        imul      ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE+32], -36 ;51.4
        movss     xmm2, DWORD PTR [_2il0floatpacket.9]          ;53.112
        subss     xmm2, DWORD PTR [esp]                         ;53.112
        mov       esi, DWORD PTR [ebx]                          ;51.4
        shl       esi, 2                                        ;51.4
        add       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE] ;51.4
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;51.4
        neg       edx                                           ;51.4
        add       edx, eax                                      ;51.4
        lea       ebx, DWORD PTR [esi+esi*8]                    ;51.4
        movss     xmm3, DWORD PTR [24+ebx+ebp]                  ;54.50
        divss     xmm3, DWORD PTR [_2il0floatpacket.8]          ;54.71
        imul      ecx, edx, 900                                 ;51.4
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;51.4
        mov       esi, DWORD PTR [432+edx+ecx]                  ;51.4
        mov       DWORD PTR [28+ebx+ebp], esi                   ;51.4
        movsx     esi, WORD PTR [682+edx+ecx]                   ;52.27
        mov       DWORD PTR [32+ebx+ebp], esi                   ;52.4
        movsx     esi, BYTE PTR [668+edx+ecx]                   ;53.38
        cvtsi2ss  xmm1, esi                                     ;53.38
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;53.38
        neg       esi                                           ;53.4
        add       esi, eax                                      ;53.4
        imul      esi, esi, 152                                 ;53.4
        mulss     xmm3, xmm1                                    ;54.78
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;53.38
        mulss     xmm3, xmm2                                    ;54.7
        movss     xmm0, DWORD PTR [20+eax+esi]                  ;53.76
        mulss     xmm0, xmm1                                    ;53.75
        mulss     xmm0, xmm2                                    ;53.4
        mov       eax, DWORD PTR [20+ebx+ebp]                   ;55.46
        movss     DWORD PTR [672+edx+ecx], xmm0                 ;53.4
        movss     DWORD PTR [432+edx+ecx], xmm3                 ;54.7
        mov       WORD PTR [682+edx+ecx], ax                    ;55.4
        pop       ecx                                           ;56.1
        pop       ebp                                           ;56.1
        pop       ebx                                           ;56.1
        pop       esi                                           ;56.1
        ret                                                     ;56.1
        ALIGN     16
                                ; LOE
; mark_end;
_WORKZONE_MODULE_mp_WZ_ACTIVATE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WORKZONE_MODULE_mp_WZ_ACTIVATE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _WORKZONE_MODULE_mp_WZ_CAPACITY_RESTORE
; mark_begin;
       ALIGN     16
	PUBLIC _WORKZONE_MODULE_mp_WZ_CAPACITY_RESTORE
_WORKZONE_MODULE_mp_WZ_CAPACITY_RESTORE	PROC NEAR 
; parameter 1: 12 + esp
.B4.1:                          ; Preds .B4.0
        push      esi                                           ;60.12
        push      edi                                           ;60.12
        mov       esi, DWORD PTR [12+esp]                       ;60.12
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE+32], -36 ;61.13
        mov       eax, DWORD PTR [esi]                          ;61.30
        shl       eax, 2                                        ;61.48
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE] ;61.13
        push      OFFSET FLAT: __NLITPACK_1.0.4                 ;61.13
        lea       ecx, DWORD PTR [eax+eax*8]                    ;61.48
        lea       eax, DWORD PTR [ecx+edx]                      ;61.13
        lea       edx, DWORD PTR [4+edx+ecx]                    ;61.13
        push      edx                                           ;61.13
        push      eax                                           ;61.13
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;61.13
                                ; LOE eax ebx ebp esi
.B4.2:                          ; Preds .B4.1
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;62.7
        neg       edi                                           ;62.7
        add       edi, eax                                      ;62.7
        imul      ecx, edi, 900                                 ;62.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;62.7
        mov       esi, DWORD PTR [esi]                          ;63.50
        shl       esi, 2                                        ;63.50
        movsx     edi, BYTE PTR [668+edx+ecx]                   ;62.41
        cvtsi2ss  xmm0, edi                                     ;62.41
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;62.41
        neg       edi                                           ;62.7
        add       edi, eax                                      ;62.7
        imul      edi, edi, 152                                 ;62.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;62.41
        mulss     xmm0, DWORD PTR [20+eax+edi]                  ;62.7
        lea       edi, DWORD PTR [esi+esi*8]                    ;63.50
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE+32], -36 ;63.7
        movss     DWORD PTR [672+edx+ecx], xmm0                 ;62.7
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE] ;63.7
        mov       esi, DWORD PTR [28+edi+eax]                   ;63.7
        mov       eax, DWORD PTR [32+edi+eax]                   ;64.46
        mov       DWORD PTR [432+edx+ecx], esi                  ;63.7
        mov       WORD PTR [682+edx+ecx], ax                    ;64.4
        add       esp, 12                                       ;66.1
        pop       edi                                           ;66.1
        pop       esi                                           ;66.1
        ret                                                     ;66.1
        ALIGN     16
                                ; LOE
; mark_end;
_WORKZONE_MODULE_mp_WZ_CAPACITY_RESTORE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WORKZONE_MODULE_mp_WZ_CAPACITY_RESTORE
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.3	DD	5
__NLITPACK_1.0.4	DD	5
_2il0floatpacket.5	DD	045610000H
_2il0floatpacket.6	DD	03f800000H
_2il0floatpacket.8	DD	045610000H
_2il0floatpacket.9	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_WZSTARTFLAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_WORKZONE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_WORKZONENUM:BYTE
_DATA	ENDS
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
