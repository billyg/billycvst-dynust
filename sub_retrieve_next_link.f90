SUBROUTINE RETRIEVE_NEXT_LINK(t,j,i,Culnk,Nlnk,Nlnkmv)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_NETWORK_MODULE
	USE DYNUST_VEH_PATH_ATT_MODULE
	INTEGER Itp1, ih
	INTEGER,INTENT(OUT):: NlnkMv,Nlnk

	INTEGER Culnk,jd4,NodeSum
    LOGICAL Itp2
    REAL r

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
!   NOTE THAT NLNK AND NLNKMV ARE NOT DUMMIES THAT HAVE NO USE    
	NlnkMv = 0
	Nlnk = 0
    icu=i
	Itp1= Culnk+1
	Itp2 = .False.
	inode = nint(vehatt_Value(j,Itp1,1))

     DO ih = 1, m_dynust_network_arc_nde(i)%llink%no  
       IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(ih))%idnod == inode) THEN
         m_dynust_veh(j)%nexlink(1) = m_dynust_network_arc_nde(i)%llink%p(ih)
         m_dynust_veh(j)%nexlink(2) = ih
         Itp2 = .True.
         exit
       ENDIF
     ENDDO
    
    iback=m_dynust_network_arc_nde(m_dynust_veh(j)%nexlink(1))%ForToBackLink
    ipen=m_dynust_network_arc_nde(i)%ForToBackLink-backpointr(m_dynust_network_arc_nde(i)%idnod)+1         
    IF(m_dynust_network_arcmove_nde(iback,ipen)%penalty > 9990.and.isigcount > 1) THEN
       jd4 = m_dynust_last_stand(j)%jdest
       !CALL RETRIEVE_VEH_PATH(j,i,1,Culnk,jd4,0,NodeSum)
       CALL RETRIEVE_VEH_PATH_AStar(j,i,1,Culnk,jd4,0,NodeSum)
        DO k=backpointr(nint(vehatt_Value(j,Culnk+1,1))),backpointr(nint(vehatt_Value(j,Culnk+1,1))+1)-1
          IF(m_dynust_network_arc_nde(i)%idnod == m_dynust_network_arc_nde(k)%UNodeOfBackLink) THEN
            m_dynust_veh(j)%nexlink(1) = m_dynust_network_arc_nde(k)%BackToForLink
            EXIT
          ENDIF
        ENDDO   
    ENDIF

    IF(.not.Itp2.or.i == m_dynust_veh(j)%nexlink(1).OR.m_dynust_veh(j)%nexlink(2) < 1.OR.m_dynust_veh(j)%nexlink(1) < 1) THEN
       WRITE(911,*) 'Error in RETRIEVE_NEXT_LINK'
	   WRITE(911,*) 'Vehicle Type', m_dynust_last_stand(j)%vehtype
	   WRITE(911,*) 'Vehicle class', m_dynust_last_stand(j)%vehclass
       WRITE(911,*) 'Vehicle destination', m_dynust_last_stand(j)%jdest
	   WRITE(911,*) 'Vehicle number', j
	   WRITE(911,*) 'Current node #', m_dynust_veh_nde(j)%icurrnt
	   WRITE(911,*) 'current upcoming node', m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%idnod)%IntoOutNodeNum
	   WRITE(911,*) 'next node',      m_dynust_network_node_nde(inode)%IntoOutNodeNum
       WRITE(911,'(20i7)') (m_dynust_network_node_nde(nint(vehatt_Value(j,js,1)))%IntoOutNodeNum,js=1,vehatt_P_Size(j))
	   STOP
	ENDIF   

END SUBROUTINE