; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _TDSP_MAIN_MIVA
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _TDSP_MAIN_MIVA
_TDSP_MAIN_MIVA	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.18
        mov       ebp, esp                                      ;1.18
        and       esp, -16                                      ;1.18
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        sub       esp, 100                                      ;1.18
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;29.10
        push      DWORD PTR [12+ebp]                            ;29.10
        push      DWORD PTR [8+ebp]                             ;29.10
        push      DWORD PTR [16+ebp]                            ;29.10
        call      _TOLL_LINK_PRICING                            ;29.10
                                ; LOE
.B1.100:                        ; Preds .B1.1
        add       esp, 16                                       ;29.10
                                ; LOE
.B1.2:                          ; Preds .B1.100
        mov       ecx, DWORD PTR [8+ebp]                        ;31.2
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_KAY]   ;32.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;33.4
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITI_NU] ;57.10
        mov       ecx, DWORD PTR [ecx]                          ;31.2
        test      ecx, ecx                                      ;31.22
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_KPATHS], eax ;32.7
        mov       DWORD PTR [92+esp], edx                       ;33.4
        mov       DWORD PTR [88+esp], ebx                       ;57.10
        jne       .B1.24        ; Prob 50%                      ;31.22
                                ; LOE ecx
.B1.3:                          ; Preds .B1.2
        cmp       DWORD PTR [92+esp], 0                         ;33.4
        jle       .B1.57        ; Prob 2%                       ;33.4
                                ; LOE ecx
.B1.4:                          ; Preds .B1.3
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME] ;35.30
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+40] ;35.13
        mov       DWORD PTR [64+esp], ebx                       ;35.30
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;35.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;35.13
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+44] ;35.13
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+32] ;35.30
        mov       DWORD PTR [56+esp], edx                       ;35.13
        cmp       DWORD PTR [88+esp], 0                         ;34.10
        jle       .B1.38        ; Prob 0%                       ;34.10
                                ; LOE eax ecx ebx esi edi
.B1.5:                          ; Preds .B1.4
        imul      esi, esi, -152                                ;
        xor       edx, edx                                      ;
        add       eax, esi                                      ;
        mov       DWORD PTR [esp], eax                          ;
        mov       eax, DWORD PTR [56+esp]                       ;
        imul      ebx, eax                                      ;
        mov       esi, DWORD PTR [64+esp]                       ;
        sub       esi, ebx                                      ;
        shl       edi, 2                                        ;
        add       esi, eax                                      ;
        sub       esi, edi                                      ;
        mov       edi, DWORD PTR [92+esp]                       ;
        mov       ebx, edi                                      ;
        shr       ebx, 31                                       ;
        add       ebx, edi                                      ;
        mov       eax, DWORD PTR [esp]                          ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       DWORD PTR [64+esp], esi                       ;
        sar       ebx, 1                                        ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       DWORD PTR [60+esp], ecx                       ;
                                ; LOE eax ebx esi
.B1.6:                          ; Preds .B1.12 .B1.5
        test      ebx, ebx                                      ;35.13
        jbe       .B1.56        ; Prob 0%                       ;35.13
                                ; LOE eax ebx esi
.B1.7:                          ; Preds .B1.6
        mov       DWORD PTR [12+esp], esi                       ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       ecx, ecx                                      ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx ebx edi
.B1.8:                          ; Preds .B1.8 .B1.7
        mov       esi, DWORD PTR [160+ecx+eax]                  ;35.13
        inc       edi                                           ;35.13
        mov       DWORD PTR [4+edx], esi                        ;35.13
        mov       esi, DWORD PTR [312+ecx+eax]                  ;35.13
        add       ecx, 304                                      ;35.13
        mov       DWORD PTR [8+edx], esi                        ;35.13
        add       edx, 8                                        ;35.13
        cmp       edi, ebx                                      ;35.13
        jb        .B1.8         ; Prob 63%                      ;35.13
                                ; LOE eax edx ecx ebx edi
.B1.9:                          ; Preds .B1.8
        mov       esi, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+edi+edi]                    ;35.13
                                ; LOE eax edx ebx esi
.B1.10:                         ; Preds .B1.9 .B1.56
        lea       ecx, DWORD PTR [-1+edx]                       ;35.13
        cmp       ecx, DWORD PTR [92+esp]                       ;35.13
        jae       .B1.12        ; Prob 0%                       ;35.13
                                ; LOE eax edx ebx esi
.B1.11:                         ; Preds .B1.10
        imul      ecx, edx, 152                                 ;35.13
        mov       edi, DWORD PTR [64+esp]                       ;35.13
        mov       ecx, DWORD PTR [8+ecx+eax]                    ;35.13
        lea       edx, DWORD PTR [edi+edx*4]                    ;35.13
        mov       edi, DWORD PTR [4+esp]                        ;35.13
        mov       DWORD PTR [edx+edi], ecx                      ;35.13
                                ; LOE eax ebx esi
.B1.12:                         ; Preds .B1.10 .B1.11
        mov       edx, DWORD PTR [56+esp]                       ;34.10
        add       esi, edx                                      ;34.10
        mov       ecx, DWORD PTR [8+esp]                        ;34.10
        inc       ecx                                           ;34.10
        add       DWORD PTR [4+esp], edx                        ;34.10
        mov       DWORD PTR [8+esp], ecx                        ;34.10
        cmp       ecx, DWORD PTR [88+esp]                       ;34.10
        jb        .B1.6         ; Prob 82%                      ;34.10
                                ; LOE eax ebx esi
.B1.13:                         ; Preds .B1.12
        mov       ecx, DWORD PTR [60+esp]                       ;
                                ; LOE ecx
.B1.14:                         ; Preds .B1.13 .B1.57
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+44] ;58.11
        mov       DWORD PTR [4+esp], edi                        ;58.11
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;58.11
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+52] ;58.11
        mov       DWORD PTR [12+esp], edi                       ;58.11
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;58.11
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+56] ;58.11
        mov       DWORD PTR [36+esp], esi                       ;58.11
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+24] ;58.11
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+40] ;58.11
        mov       DWORD PTR [32+esp], edi                       ;58.11
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;58.11
        mov       DWORD PTR [8+esp], eax                        ;58.11
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+48] ;58.11
        test      eax, eax                                      ;58.11
        mov       DWORD PTR [44+esp], ebx                       ;58.11
        mov       DWORD PTR [40+esp], esi                       ;58.11
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+32] ;58.11
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY] ;58.11
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;58.11
        mov       DWORD PTR [28+esp], edi                       ;58.11
        jle       .B1.36        ; Prob 3%                       ;58.11
                                ; LOE eax edx ecx ebx esi
.B1.15:                         ; Preds .B1.14
        mov       DWORD PTR [16+esp], eax                       ;
        mov       eax, DWORD PTR [8+esp]                        ;
        imul      eax, DWORD PTR [36+esp]                       ;
        mov       DWORD PTR [60+esp], ecx                       ;
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       ecx, DWORD PTR [40+esp]                       ;
        imul      edi, ecx                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       esi, ebx                                      ;
        shl       edx, 2                                        ;
        mov       DWORD PTR [4+esp], edi                        ;
        sub       esi, edx                                      ;
        sub       edx, eax                                      ;
        sub       edi, ecx                                      ;
        sub       eax, DWORD PTR [4+esp]                        ;
        add       esi, edx                                      ;
        add       esi, eax                                      ;
        sub       ebx, edi                                      ;
        add       esi, ecx                                      ;
        mov       ecx, DWORD PTR [12+esp]                       ;
        imul      ecx, DWORD PTR [28+esp]                       ;
        mov       edi, DWORD PTR [44+esp]                       ;
        mov       edx, edi                                      ;
        mov       eax, DWORD PTR [esp]                          ;
        shl       eax, 3                                        ;
        mov       DWORD PTR [24+esp], esi                       ;
        mov       esi, DWORD PTR [32+esp]                       ;
        shr       edx, 31                                       ;
        sub       esi, eax                                      ;
        sub       eax, ecx                                      ;
        add       edx, edi                                      ;
        add       esi, eax                                      ;
        sar       edx, 1                                        ;
        add       esi, ecx                                      ;
        mov       eax, DWORD PTR [16+esp]                       ;58.11
        test      edi, edi                                      ;58.11
        mov       ecx, DWORD PTR [60+esp]                       ;58.11
        mov       DWORD PTR [20+esp], 0                         ;
        jle       .B1.36        ; Prob 50%                      ;58.11
                                ; LOE eax edx ecx ebx esi al cl ah ch
.B1.16:                         ; Preds .B1.15
        mov       DWORD PTR [60+esp], ecx                       ;
        mov       DWORD PTR [4+esp], esi                        ;
        mov       DWORD PTR [68+esp], edx                       ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       DWORD PTR [16+esp], eax                       ;
        mov       ecx, DWORD PTR [20+esp]                       ;
                                ; LOE ecx
.B1.17:                         ; Preds .B1.66 .B1.94 .B1.16
        mov       esi, DWORD PTR [28+esp]                       ;
        xor       edi, edi                                      ;
        mov       edx, DWORD PTR [36+esp]                       ;
        imul      esi, ecx                                      ;
        imul      edx, ecx                                      ;
        mov       ebx, DWORD PTR [32+esp]                       ;
        mov       eax, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [52+esp], edi                       ;
        mov       DWORD PTR [20+esp], ecx                       ;
        add       ebx, esi                                      ;
        mov       DWORD PTR [72+esp], ebx                       ;
        add       eax, edx                                      ;
        add       edx, DWORD PTR [esp]                          ;
        add       esi, DWORD PTR [4+esp]                        ;
        mov       ebx, edi                                      ;
        mov       DWORD PTR [48+esp], edx                       ;
        mov       DWORD PTR [12+esp], eax                       ;
                                ; LOE ebx esi edi
.B1.18:                         ; Preds .B1.23 .B1.93 .B1.17
        cmp       DWORD PTR [68+esp], 0                         ;58.11
        jbe       .B1.92        ; Prob 0%                       ;58.11
                                ; LOE ebx esi edi
.B1.19:                         ; Preds .B1.18
        mov       eax, DWORD PTR [48+esp]                       ;
        xor       edx, edx                                      ;
        mov       ecx, DWORD PTR [12+esp]                       ;
        mov       DWORD PTR [52+esp], ebx                       ;
        mov       DWORD PTR [8+esp], edi                        ;
        add       eax, ebx                                      ;
        add       ecx, ebx                                      ;
                                ; LOE eax edx ecx esi
.B1.20:                         ; Preds .B1.20 .B1.19
        mov       ebx, edx                                      ;58.11
        shl       ebx, 4                                        ;58.11
        mov       edi, DWORD PTR [72+esp]                       ;58.11
        mov       edi, DWORD PTR [edi+ebx]                      ;58.11
        mov       ebx, DWORD PTR [8+esi+ebx]                    ;58.11
        mov       DWORD PTR [ecx+edx*8], edi                    ;58.11
        mov       DWORD PTR [4+eax+edx*8], ebx                  ;58.11
        inc       edx                                           ;58.11
        cmp       edx, DWORD PTR [68+esp]                       ;58.11
        jb        .B1.20        ; Prob 64%                      ;58.11
                                ; LOE eax edx ecx esi
.B1.21:                         ; Preds .B1.20
        mov       ebx, DWORD PTR [52+esp]                       ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;58.11
        mov       edi, DWORD PTR [8+esp]                        ;
                                ; LOE eax ebx esi edi
.B1.22:                         ; Preds .B1.21 .B1.92
        lea       edx, DWORD PTR [-1+eax]                       ;58.11
        cmp       edx, DWORD PTR [44+esp]                       ;58.11
        jae       .B1.93        ; Prob 0%                       ;58.11
                                ; LOE eax ebx esi edi
.B1.23:                         ; Preds .B1.22
        mov       edx, DWORD PTR [48+esp]                       ;58.11
        inc       edi                                           ;57.10
        lea       ecx, DWORD PTR [edx+eax*4]                    ;58.11
        mov       eax, DWORD PTR [-8+esi+eax*8]                 ;58.11
        mov       DWORD PTR [-4+ecx+ebx], eax                   ;58.11
        add       ebx, DWORD PTR [40+esp]                       ;57.10
        cmp       edi, DWORD PTR [88+esp]                       ;57.10
        jb        .B1.18        ; Prob 82%                      ;57.10
        jmp       .B1.66        ; Prob 100%                     ;57.10
                                ; LOE ebx esi edi
.B1.24:                         ; Preds .B1.2
        mov       ebx, DWORD PTR [12+ebp]                       ;62.10
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITI_NUEP] ;61.10
        mov       DWORD PTR [24+esp], esi                       ;61.10
        mov       eax, DWORD PTR [ebx]                          ;62.10
        mov       DWORD PTR [28+esp], eax                       ;62.10
        mov       edx, DWORD PTR [88+esp]                       ;62.68
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;62.10
        lea       eax, DWORD PTR [-1+eax]                       ;62.56
        imul      esi, eax                                      ;62.59
        lea       edi, DWORD PTR [edx+esi]                      ;62.68
        cmp       edi, ebx                                      ;62.10
        cmovl     ebx, edi                                      ;62.10
        mov       edx, ebx                                      ;62.10
        sub       edx, esi                                      ;62.10
        mov       DWORD PTR [44+esp], edx                       ;62.10
        cmp       DWORD PTR [92+esp], 0                         ;42.4
        jle       .B1.97        ; Prob 2%                       ;42.4
                                ; LOE eax ecx ebx esi
.B1.25:                         ; Preds .B1.24
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME] ;74.12
        inc       esi                                           ;44.10
        mov       DWORD PTR [40+esp], edx                       ;74.12
        cmp       ebx, esi                                      ;44.10
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+44] ;74.40
        mov       DWORD PTR [8+esp], edx                        ;74.40
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+40] ;74.40
        mov       DWORD PTR [52+esp], edx                       ;74.40
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+32] ;74.12
        mov       DWORD PTR [16+esp], edx                       ;74.12
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;47.12
        mov       DWORD PTR [12+esp], edx                       ;47.12
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;47.70
        mov       DWORD PTR [36+esp], edx                       ;47.70
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;47.70
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;46.8
        mov       DWORD PTR [56+esp], edx                       ;47.70
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;47.12
        mov       DWORD PTR [32+esp], edi                       ;46.8
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;46.8
        mov       DWORD PTR [20+esp], edx                       ;47.12
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIMEWEIGHT] ;47.91
        jl        .B1.33        ; Prob 0%                       ;44.10
                                ; LOE eax ecx ebx esi edi xmm2
.B1.26:                         ; Preds .B1.25
        movss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;47.122
        xor       edx, edx                                      ;
        divss     xmm2, xmm1                                    ;47.122
        mov       DWORD PTR [esp], ebx                          ;
        imul      ebx, edi, -152                                ;
        imul      eax, DWORD PTR [24+esp]                       ;44.27
        movss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;47.105
        subss     xmm0, xmm2                                    ;47.105
        add       DWORD PTR [32+esp], ebx                       ;
        mov       edi, DWORD PTR [52+esp]                       ;
        mov       ebx, DWORD PTR [8+esp]                        ;
        imul      ebx, edi                                      ;
        mov       DWORD PTR [4+esp], esi                        ;
        mov       esi, DWORD PTR [40+esp]                       ;
        sub       esi, ebx                                      ;
        mov       ebx, DWORD PTR [16+esp]                       ;
        shl       ebx, 2                                        ;
        neg       ebx                                           ;
        add       ebx, edi                                      ;
        add       esi, ebx                                      ;
        mov       ebx, DWORD PTR [36+esp]                       ;
        neg       ebx                                           ;
        add       ebx, eax                                      ;
        mov       eax, DWORD PTR [56+esp]                       ;
        mov       DWORD PTR [40+esp], esi                       ;
        imul      ebx, eax                                      ;
        mov       esi, DWORD PTR [20+esp]                       ;
        shl       esi, 2                                        ;
        neg       esi                                           ;
        add       esi, eax                                      ;
        xor       eax, eax                                      ;
        mov       edi, DWORD PTR [12+esp]                       ;
        add       edi, esi                                      ;
        mov       DWORD PTR [48+esp], edx                       ;
        add       ebx, edi                                      ;
        mov       DWORD PTR [36+esp], ebx                       ;
        mov       edi, edx                                      ;
        mov       DWORD PTR [60+esp], ecx                       ;
                                ; LOE eax edx edi xmm0 xmm1
.B1.27:                         ; Preds .B1.31 .B1.26
        mov       esi, DWORD PTR [40+esp]                       ;
        xor       ebx, ebx                                      ;
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       DWORD PTR [12+esp], eax                       ;
        add       esi, edx                                      ;
        mov       DWORD PTR [8+esp], edx                        ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [16+esp], esi                       ;
        mov       DWORD PTR [48+esp], edi                       ;
        mov       edx, ebx                                      ;
        mov       eax, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.28:                         ; Preds .B1.30 .B1.27
        movsx     edi, WORD PTR [300+edx+eax]                   ;46.11
        cmp       edi, 1                                        ;46.53
        mov       esi, DWORD PTR [164+edx+eax]                  ;47.12
        cvtsi2ss  xmm2, DWORD PTR [4+ecx+ebx*4]                 ;47.70
        je        .B1.96        ; Prob 16%                      ;46.53
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2
.B1.29:                         ; Preds .B1.28
        divss     xmm2, xmm1                                    ;49.9
        mov       edi, DWORD PTR [16+esp]                       ;49.9
        movss     DWORD PTR [edi+esi*4], xmm2                   ;49.9
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.30:                         ; Preds .B1.96 .B1.29
        inc       ebx                                           ;44.10
        add       edx, 152                                      ;44.10
        cmp       ebx, DWORD PTR [92+esp]                       ;44.10
        jb        .B1.28        ; Prob 82%                      ;44.10
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.31:                         ; Preds .B1.30
        mov       eax, DWORD PTR [12+esp]                       ;
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       edi, DWORD PTR [48+esp]                       ;
        inc       edi                                           ;44.10
        add       edx, DWORD PTR [52+esp]                       ;44.10
        add       eax, DWORD PTR [56+esp]                       ;44.10
        cmp       edi, DWORD PTR [44+esp]                       ;44.10
        jb        .B1.27        ; Prob 82%                      ;44.10
                                ; LOE eax edx edi xmm0 xmm1
.B1.32:                         ; Preds .B1.31
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [60+esp]                       ;
                                ; LOE ecx ebx esi
.B1.33:                         ; Preds .B1.32 .B1.97 .B1.25
        cmp       ebx, esi                                      ;62.10
        jl        .B1.36        ; Prob 10%                      ;62.10
                                ; LOE ecx
.B1.34:                         ; Preds .B1.33
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY] ;74.40
        mov       DWORD PTR [56+esp], edi                       ;74.40
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+52] ;74.57
        mov       DWORD PTR [52+esp], edi                       ;74.57
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+44] ;74.57
        mov       DWORD PTR [8+esp], edi                        ;74.57
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+40] ;74.57
        mov       DWORD PTR [64+esp], edi                       ;74.57
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY] ;64.12
        mov       DWORD PTR [84+esp], edi                       ;64.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+52] ;64.12
        mov       DWORD PTR [72+esp], edi                       ;64.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+44] ;64.12
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+56] ;64.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+32] ;64.12
        mov       DWORD PTR [esp], edi                          ;64.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+40] ;64.12
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+48] ;64.12
        test      ebx, ebx                                      ;64.12
        mov       DWORD PTR [4+esp], edx                        ;64.12
        mov       DWORD PTR [12+esp], eax                       ;64.12
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+56] ;64.12
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+32] ;74.40
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+24] ;64.12
        mov       DWORD PTR [76+esp], edi                       ;64.12
        mov       DWORD PTR [80+esp], 0                         ;
        jle       .B1.36        ; Prob 10%                      ;64.12
                                ; LOE eax edx ecx ebx esi
.B1.35:                         ; Preds .B1.34
        test      esi, esi                                      ;64.12
        jg        .B1.49        ; Prob 50%                      ;64.12
                                ; LOE eax edx ecx ebx esi
.B1.36:                         ; Preds .B1.15 .B1.35 .B1.34 .B1.67 .B1.95
                                ;       .B1.14 .B1.57 .B1.33
        cmp       DWORD PTR [92+esp], 0                         ;69.8
        jle       .B1.61        ; Prob 0%                       ;69.8
                                ; LOE ecx
.B1.37:                         ; Preds .B1.36
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME] ;74.12
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+40] ;74.40
        mov       DWORD PTR [64+esp], ebx                       ;74.12
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+44] ;74.40
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+32] ;74.12
        mov       DWORD PTR [56+esp], eax                       ;74.40
                                ; LOE ecx ebx edi
.B1.38:                         ; Preds .B1.4 .B1.37
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+56] ;74.57
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+32] ;74.40
        mov       DWORD PTR [8+esp], edx                        ;74.57
        mov       DWORD PTR [esp], esi                          ;74.40
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+52] ;74.57
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+44] ;74.57
        mov       DWORD PTR [24+esp], edx                       ;74.57
        mov       DWORD PTR [12+esp], esi                       ;74.57
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY] ;74.40
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL] ;74.56
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+56] ;74.12
        mov       DWORD PTR [48+esp], eax                       ;74.40
        mov       DWORD PTR [52+esp], edx                       ;74.56
        mov       DWORD PTR [4+esp], esi                        ;74.12
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+40] ;74.57
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+52] ;74.12
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+44] ;74.12
        mov       DWORD PTR [68+esp], eax                       ;74.57
        mov       DWORD PTR [32+esp], edx                       ;74.12
        mov       DWORD PTR [16+esp], esi                       ;74.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;71.10
        test      eax, eax                                      ;71.10
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+40] ;74.12
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+32] ;74.56
        mov       DWORD PTR [28+esp], eax                       ;71.10
        mov       DWORD PTR [76+esp], edx                       ;74.12
        mov       DWORD PTR [20+esp], esi                       ;74.56
        jle       .B1.61        ; Prob 3%                       ;71.10
                                ; LOE ecx ebx edi
.B1.39:                         ; Preds .B1.38
        mov       DWORD PTR [60+esp], ecx                       ;
        mov       ecx, DWORD PTR [56+esp]                       ;
        imul      ebx, ecx                                      ;
        mov       esi, DWORD PTR [64+esp]                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
        sub       esi, ebx                                      ;
        mov       ebx, DWORD PTR [32+esp]                       ;
        add       esi, ecx                                      ;
        imul      eax, ebx                                      ;
        shl       edi, 2                                        ;
        mov       edx, DWORD PTR [92+esp]                       ;
        sub       esi, edi                                      ;
        mov       edi, edx                                      ;
        shr       edi, 31                                       ;
        add       edi, edx                                      ;
        mov       edx, DWORD PTR [52+esp]                       ;
        sub       edx, eax                                      ;
        mov       eax, DWORD PTR [76+esp]                       ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        imul      ecx, eax                                      ;
        sar       edi, 1                                        ;
        neg       ecx                                           ;
        mov       DWORD PTR [72+esp], edi                       ;
        add       ecx, ebx                                      ;
        mov       edi, DWORD PTR [20+esp]                       ;
        add       edx, ecx                                      ;
        shl       edi, 2                                        ;
        mov       ebx, edi                                      ;
        mov       DWORD PTR [64+esp], esi                       ;
        mov       esi, eax                                      ;
        neg       ebx                                           ;
        sub       esi, edi                                      ;
        mov       ecx, DWORD PTR [esp]                          ;
        add       ebx, edx                                      ;
        shl       ecx, 2                                        ;
        add       esi, edx                                      ;
        mov       DWORD PTR [40+esp], ebx                       ;
        add       eax, eax                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
        sub       eax, edi                                      ;
        mov       ebx, DWORD PTR [48+esp]                       ;
        add       edx, eax                                      ;
        mov       esi, DWORD PTR [24+esp]                       ;
        sub       ebx, ecx                                      ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        imul      ecx, esi                                      ;
        sub       ecx, esi                                      ;
        mov       esi, DWORD PTR [68+esp]                       ;
        sub       ebx, ecx                                      ;
        mov       ecx, DWORD PTR [12+esp]                       ;
        imul      ecx, esi                                      ;
        sub       ecx, esi                                      ;
        mov       DWORD PTR [36+esp], 0                         ;
        sub       ebx, ecx                                      ;
        mov       DWORD PTR [48+esp], ebx                       ;
        mov       DWORD PTR [52+esp], edx                       ;
        mov       ebx, DWORD PTR [36+esp]                       ;70.9
        mov       ecx, DWORD PTR [60+esp]                       ;70.9
        cmp       DWORD PTR [88+esp], 0                         ;70.9
        jle       .B1.61        ; Prob 0%                       ;70.9
                                ; LOE ecx ebx cl bl ch bh
.B1.41:                         ; Preds .B1.39 .B1.59
        cmp       ecx, 2                                        ;72.26
        je        .B1.59        ; Prob 16%                      ;72.26
                                ; LOE ecx ebx
.B1.42:                         ; Preds .B1.41
        mov       esi, DWORD PTR [32+esp]                       ;
        xor       eax, eax                                      ;
        imul      esi, ebx                                      ;
        mov       edi, DWORD PTR [40+esp]                       ;
        mov       edx, DWORD PTR [44+esp]                       ;
        mov       DWORD PTR [36+esp], ebx                       ;
        mov       DWORD PTR [60+esp], ecx                       ;
        add       edi, esi                                      ;
        mov       DWORD PTR [16+esp], edi                       ;
        add       edx, esi                                      ;
        mov       edi, DWORD PTR [24+esp]                       ;
        imul      edi, ebx                                      ;
        add       edi, DWORD PTR [48+esp]                       ;
        add       esi, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [12+esp], esi                       ;
        mov       DWORD PTR [8+esp], edi                        ;
        mov       DWORD PTR [4+esp], edx                        ;
                                ; LOE eax
.B1.43:                         ; Preds .B1.48 .B1.63 .B1.42
        cmp       DWORD PTR [72+esp], 0                         ;69.8
        jbe       .B1.65        ; Prob 0%                       ;69.8
                                ; LOE eax
.B1.44:                         ; Preds .B1.43
        mov       ecx, DWORD PTR [56+esp]                       ;
        xor       edi, edi                                      ;
        mov       ebx, DWORD PTR [68+esp]                       ;
        imul      ecx, eax                                      ;
        imul      ebx, eax                                      ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
        mov       DWORD PTR [esp], eax                          ;
        lea       esi, DWORD PTR [esi+eax*4]                    ;
        add       ecx, DWORD PTR [64+esp]                       ;
        lea       edx, DWORD PTR [edx+eax*4]                    ;
        add       ebx, DWORD PTR [8+esp]                        ;
        mov       eax, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.45:                         ; Preds .B1.45 .B1.44
        movss     xmm0, DWORD PTR [4+ecx+edi*8]                 ;74.40
        movss     xmm1, DWORD PTR [8+ecx+edi*8]                 ;74.40
        addss     xmm0, DWORD PTR [4+ebx+edi*8]                 ;74.12
        addss     xmm1, DWORD PTR [8+ebx+edi*8]                 ;74.12
        inc       edi                                           ;69.8
        movss     DWORD PTR [4+esi+eax*2], xmm0                 ;74.12
        movss     DWORD PTR [4+edx+eax*2], xmm1                 ;74.12
        add       eax, DWORD PTR [76+esp]                       ;69.8
        cmp       edi, DWORD PTR [72+esp]                       ;69.8
        jb        .B1.45        ; Prob 63%                      ;69.8
                                ; LOE eax edx ecx ebx esi edi
.B1.46:                         ; Preds .B1.45
        mov       eax, DWORD PTR [esp]                          ;
        lea       esi, DWORD PTR [1+edi+edi]                    ;69.8
                                ; LOE eax esi
.B1.47:                         ; Preds .B1.46 .B1.65
        lea       edx, DWORD PTR [-1+esi]                       ;69.8
        cmp       edx, DWORD PTR [92+esp]                       ;69.8
        jae       .B1.63        ; Prob 0%                       ;69.8
                                ; LOE eax esi
.B1.48:                         ; Preds .B1.47
        mov       edi, DWORD PTR [64+esp]                       ;74.12
        mov       ebx, DWORD PTR [56+esp]                       ;74.12
        imul      ebx, eax                                      ;74.12
        mov       edx, DWORD PTR [68+esp]                       ;74.12
        lea       ecx, DWORD PTR [edi+esi*4]                    ;74.12
        mov       edi, DWORD PTR [8+esp]                        ;74.12
        imul      edx, eax                                      ;74.12
        movss     xmm0, DWORD PTR [ecx+ebx]                     ;74.40
        lea       edi, DWORD PTR [edi+esi*4]                    ;74.12
        imul      esi, DWORD PTR [76+esp]                       ;74.12
        addss     xmm0, DWORD PTR [edi+edx]                     ;74.12
        add       esi, DWORD PTR [16+esp]                       ;74.12
        movss     DWORD PTR [4+esi+eax*4], xmm0                 ;74.12
        inc       eax                                           ;70.9
        cmp       eax, DWORD PTR [88+esp]                       ;70.9
        jb        .B1.43        ; Prob 82%                      ;70.9
        jmp       .B1.58        ; Prob 100%                     ;70.9
                                ; LOE eax
.B1.49:                         ; Preds .B1.35
        mov       DWORD PTR [40+esp], ebx                       ;
        mov       ebx, DWORD PTR [28+esp]                       ;62.24
        dec       ebx                                           ;62.24
        mov       DWORD PTR [20+esp], esi                       ;
        mov       esi, DWORD PTR [24+esp]                       ;62.27
        imul      esi, ebx                                      ;62.27
        imul      edx, DWORD PTR [52+esp]                       ;
        imul      esi, DWORD PTR [76+esp]                       ;62.27
        mov       DWORD PTR [60+esp], ecx                       ;
        mov       DWORD PTR [24+esp], esi                       ;62.27
        mov       esi, DWORD PTR [64+esp]                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        imul      ecx, esi                                      ;
        shl       eax, 2                                        ;
        mov       ebx, DWORD PTR [56+esp]                       ;
        mov       edi, edx                                      ;
        sub       ebx, eax                                      ;
        sub       eax, edx                                      ;
        neg       edx                                           ;
        sub       edi, ecx                                      ;
        sub       ecx, esi                                      ;
        add       ebx, eax                                      ;
        mov       eax, DWORD PTR [56+esp]                       ;
        add       edx, eax                                      ;
        sub       eax, ecx                                      ;
        add       ebx, edi                                      ;
        mov       DWORD PTR [56+esp], eax                       ;
        add       edx, edi                                      ;
        mov       eax, DWORD PTR [76+esp]                       ;
        add       ebx, esi                                      ;
        mov       edi, DWORD PTR [esp]                          ;
        add       edx, esi                                      ;
        imul      edi, eax                                      ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        imul      ecx, DWORD PTR [72+esp]                       ;
        mov       DWORD PTR [68+esp], ebx                       ;
        mov       esi, DWORD PTR [12+esp]                       ;
        mov       ebx, DWORD PTR [84+esp]                       ;
        mov       DWORD PTR [36+esp], edx                       ;
        mov       edx, ebx                                      ;
        shl       esi, 2                                        ;
        sub       edx, esi                                      ;
        sub       esi, ecx                                      ;
        mov       DWORD PTR [48+esp], edx                       ;
        mov       edx, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [esp], edi                          ;
        sub       edi, edx                                      ;
        sub       ebx, edi                                      ;
        add       edx, eax                                      ;
        add       ebx, eax                                      ;
        mov       DWORD PTR [84+esp], ebx                       ;
        mov       ebx, DWORD PTR [48+esp]                       ;
        add       ebx, esi                                      ;
        mov       esi, DWORD PTR [20+esp]                       ;
        sub       ecx, DWORD PTR [esp]                          ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [16+esp], 0                         ;
        lea       eax, DWORD PTR [esi*4]                        ;
        add       ebx, ecx                                      ;
        mov       DWORD PTR [48+esp], ebx                       ;
        mov       ecx, DWORD PTR [60+esp]                       ;
        mov       edx, DWORD PTR [36+esp]                       ;
        mov       ebx, DWORD PTR [16+esp]                       ;
        mov       DWORD PTR [32+esp], eax                       ;
                                ; LOE ebx esi
.B1.50:                         ; Preds .B1.54 .B1.52 .B1.49
        cmp       esi, 24                                       ;64.12
        jle       .B1.68        ; Prob 0%                       ;64.12
                                ; LOE ebx esi
.B1.51:                         ; Preds .B1.50
        mov       edx, ebx                                      ;64.12
        mov       edi, ebx                                      ;64.12
        imul      edx, DWORD PTR [52+esp]                       ;64.12
        imul      edi, DWORD PTR [72+esp]                       ;64.12
        mov       eax, DWORD PTR [64+esp]                       ;64.12
        mov       ecx, DWORD PTR [80+esp]                       ;64.12
        imul      eax, ecx                                      ;64.12
        add       edx, DWORD PTR [56+esp]                       ;64.12
        add       eax, edx                                      ;64.12
        mov       edx, DWORD PTR [76+esp]                       ;64.12
        imul      edx, ecx                                      ;64.12
        add       edi, DWORD PTR [48+esp]                       ;64.12
        push      DWORD PTR [32+esp]                            ;64.12
        add       edx, edi                                      ;64.12
        push      edx                                           ;64.12
        push      eax                                           ;64.12
        call      __intel_fast_memcpy                           ;64.12
                                ; LOE ebx esi
.B1.101:                        ; Preds .B1.51
        add       esp, 12                                       ;64.12
                                ; LOE ebx esi
.B1.52:                         ; Preds .B1.84 .B1.86 .B1.101
        inc       ebx                                           ;64.12
        cmp       ebx, DWORD PTR [40+esp]                       ;64.12
        jb        .B1.50        ; Prob 82%                      ;64.12
                                ; LOE ebx esi
.B1.53:                         ; Preds .B1.52
        mov       eax, DWORD PTR [80+esp]                       ;62.10
        inc       eax                                           ;62.10
        mov       DWORD PTR [80+esp], eax                       ;62.10
        cmp       eax, DWORD PTR [44+esp]                       ;62.10
        jae       .B1.67        ; Prob 18%                      ;62.10
                                ; LOE esi
.B1.54:                         ; Preds .B1.53
        xor       ebx, ebx                                      ;
        jmp       .B1.50        ; Prob 100%                     ;
                                ; LOE ebx esi
.B1.56:                         ; Preds .B1.6                   ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.10        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B1.57:                         ; Preds .B1.3                   ; Infreq
        cmp       DWORD PTR [88+esp], 0                         ;57.10
        jg        .B1.14        ; Prob 100%                     ;57.10
        jmp       .B1.36        ; Prob 100%                     ;57.10
                                ; LOE ecx
.B1.58:                         ; Preds .B1.63 .B1.48           ; Infreq
        mov       ebx, DWORD PTR [36+esp]                       ;
        mov       ecx, DWORD PTR [60+esp]                       ;
                                ; LOE ecx ebx
.B1.59:                         ; Preds .B1.58 .B1.41           ; Infreq
        inc       ebx                                           ;71.10
        cmp       ebx, DWORD PTR [28+esp]                       ;71.10
        jb        .B1.41        ; Prob 81%                      ;71.10
                                ; LOE ecx ebx
.B1.61:                         ; Preds .B1.59 .B1.39 .B1.36 .B1.38 ; Infreq
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;80.12
        push      DWORD PTR [12+ebp]                            ;80.12
        push      DWORD PTR [8+ebp]                             ;80.12
        push      DWORD PTR [16+ebp]                            ;80.12
        call      _TOLL_LINK_PRICING                            ;80.12
                                ; LOE
.B1.62:                         ; Preds .B1.61                  ; Infreq
        add       esp, 116                                      ;82.1
        pop       ebx                                           ;82.1
        pop       edi                                           ;82.1
        pop       esi                                           ;82.1
        mov       esp, ebp                                      ;82.1
        pop       ebp                                           ;82.1
        ret                                                     ;82.1
                                ; LOE
.B1.63:                         ; Preds .B1.47                  ; Infreq
        inc       eax                                           ;70.9
        cmp       eax, DWORD PTR [88+esp]                       ;70.9
        jb        .B1.43        ; Prob 82%                      ;70.9
        jmp       .B1.58        ; Prob 100%                     ;70.9
                                ; LOE eax
.B1.65:                         ; Preds .B1.43                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B1.47        ; Prob 100%                     ;
                                ; LOE eax esi
.B1.66:                         ; Preds .B1.23                  ; Infreq
        mov       ecx, DWORD PTR [20+esp]                       ;
        inc       ecx                                           ;58.11
        cmp       ecx, DWORD PTR [16+esp]                       ;58.11
        jb        .B1.17        ; Prob 82%                      ;58.11
                                ; LOE ecx
.B1.67:                         ; Preds .B1.53 .B1.66           ; Infreq
        mov       ecx, DWORD PTR [60+esp]                       ;
        jmp       .B1.36        ; Prob 100%                     ;
                                ; LOE ecx
.B1.68:                         ; Preds .B1.50                  ; Infreq
        cmp       esi, 4                                        ;64.12
        jl        .B1.88        ; Prob 10%                      ;64.12
                                ; LOE ebx esi
.B1.69:                         ; Preds .B1.68                  ; Infreq
        mov       ecx, DWORD PTR [64+esp]                       ;45.5
        mov       edx, ebx                                      ;64.12
        imul      ecx, DWORD PTR [80+esp]                       ;45.5
        imul      edx, DWORD PTR [52+esp]                       ;64.12
        mov       eax, DWORD PTR [36+esp]                       ;64.12
        add       eax, ecx                                      ;64.12
        add       eax, edx                                      ;45.5
        and       eax, 15                                       ;64.12
        je        .B1.72        ; Prob 50%                      ;64.12
                                ; LOE eax edx ecx ebx esi
.B1.70:                         ; Preds .B1.69                  ; Infreq
        test      al, 3                                         ;64.12
        jne       .B1.88        ; Prob 10%                      ;64.12
                                ; LOE eax edx ecx ebx esi
.B1.71:                         ; Preds .B1.70                  ; Infreq
        neg       eax                                           ;64.12
        add       eax, 16                                       ;64.12
        shr       eax, 2                                        ;64.12
                                ; LOE eax edx ecx ebx esi
.B1.72:                         ; Preds .B1.71 .B1.69           ; Infreq
        lea       edi, DWORD PTR [4+eax]                        ;64.12
        cmp       esi, edi                                      ;64.12
        jl        .B1.88        ; Prob 10%                      ;64.12
                                ; LOE eax edx ecx ebx esi
.B1.73:                         ; Preds .B1.72                  ; Infreq
        mov       edi, esi                                      ;64.12
        sub       edi, eax                                      ;64.12
        and       edi, 3                                        ;64.12
        neg       edi                                           ;64.12
        add       edi, esi                                      ;64.12
        mov       DWORD PTR [24+esp], edi                       ;64.12
        add       ecx, DWORD PTR [68+esp]                       ;45.5
        mov       edi, DWORD PTR [76+esp]                       ;
        add       ecx, edx                                      ;
        imul      edi, DWORD PTR [80+esp]                       ;
        mov       edx, ebx                                      ;
        imul      edx, DWORD PTR [72+esp]                       ;
        mov       DWORD PTR [28+esp], ecx                       ;
        mov       ecx, DWORD PTR [84+esp]                       ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       DWORD PTR [8+esp], edi                        ;
        add       ecx, edi                                      ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [12+esp], ecx                       ;
        test      eax, eax                                      ;64.12
        mov       ecx, DWORD PTR [28+esp]                       ;64.12
        mov       edx, DWORD PTR [24+esp]                       ;64.12
        jbe       .B1.77        ; Prob 10%                      ;64.12
                                ; LOE eax edx ecx ebx esi dl cl dh ch
.B1.74:                         ; Preds .B1.73                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;
        mov       DWORD PTR [20+esp], esi                       ;
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edi, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx esi edi
.B1.75:                         ; Preds .B1.75 .B1.74           ; Infreq
        mov       ebx, DWORD PTR [edi+esi*4]                    ;64.12
        mov       DWORD PTR [ecx+esi*4], ebx                    ;64.12
        inc       esi                                           ;64.12
        cmp       esi, eax                                      ;64.12
        jb        .B1.75        ; Prob 82%                      ;64.12
                                ; LOE eax edx ecx esi edi
.B1.76:                         ; Preds .B1.75                  ; Infreq
        mov       ebx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B1.77:                         ; Preds .B1.73 .B1.76           ; Infreq
        mov       edi, DWORD PTR [8+esp]                        ;64.12
        add       edi, DWORD PTR [48+esp]                       ;64.12
        add       edi, DWORD PTR [4+esp]                        ;64.12
        lea       edi, DWORD PTR [edi+eax*4]                    ;64.12
        test      edi, 15                                       ;64.12
        je        .B1.81        ; Prob 60%                      ;64.12
                                ; LOE eax edx ecx ebx esi
.B1.78:                         ; Preds .B1.77                  ; Infreq
        mov       edi, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.79:                         ; Preds .B1.79 .B1.78           ; Infreq
        movups    xmm0, XMMWORD PTR [edi+eax*4]                 ;64.12
        movaps    XMMWORD PTR [ecx+eax*4], xmm0                 ;64.12
        add       eax, 4                                        ;64.12
        cmp       eax, edx                                      ;64.12
        jb        .B1.79        ; Prob 82%                      ;64.12
        jmp       .B1.84        ; Prob 100%                     ;64.12
                                ; LOE eax edx ecx ebx esi edi
.B1.81:                         ; Preds .B1.77                  ; Infreq
        mov       edi, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.82:                         ; Preds .B1.82 .B1.81           ; Infreq
        movaps    xmm0, XMMWORD PTR [edi+eax*4]                 ;64.12
        movaps    XMMWORD PTR [ecx+eax*4], xmm0                 ;64.12
        add       eax, 4                                        ;64.12
        cmp       eax, edx                                      ;64.12
        jb        .B1.82        ; Prob 82%                      ;64.12
                                ; LOE eax edx ecx ebx esi edi
.B1.84:                         ; Preds .B1.82 .B1.79 .B1.88    ; Infreq
        cmp       edx, esi                                      ;64.12
        jae       .B1.52        ; Prob 10%                      ;64.12
                                ; LOE edx ebx esi
.B1.85:                         ; Preds .B1.84                  ; Infreq
        mov       edi, DWORD PTR [64+esp]                       ;45.5
        mov       ecx, ebx                                      ;
        mov       eax, DWORD PTR [80+esp]                       ;45.5
        imul      edi, eax                                      ;45.5
        imul      ecx, DWORD PTR [52+esp]                       ;
        add       edi, DWORD PTR [68+esp]                       ;45.5
        add       edi, ecx                                      ;
        mov       ecx, DWORD PTR [76+esp]                       ;
        imul      ecx, eax                                      ;
        mov       eax, ebx                                      ;
        imul      eax, DWORD PTR [72+esp]                       ;
        add       ecx, DWORD PTR [84+esp]                       ;
        add       ecx, eax                                      ;
                                ; LOE edx ecx ebx esi edi
.B1.86:                         ; Preds .B1.86 .B1.85           ; Infreq
        mov       eax, DWORD PTR [ecx+edx*4]                    ;64.12
        mov       DWORD PTR [edi+edx*4], eax                    ;64.12
        inc       edx                                           ;64.12
        cmp       edx, esi                                      ;64.12
        jb        .B1.86        ; Prob 82%                      ;64.12
        jmp       .B1.52        ; Prob 100%                     ;64.12
                                ; LOE edx ecx ebx esi edi
.B1.88:                         ; Preds .B1.68 .B1.72 .B1.70    ; Infreq
        xor       edx, edx                                      ;64.12
        jmp       .B1.84        ; Prob 100%                     ;64.12
                                ; LOE edx ebx esi
.B1.92:                         ; Preds .B1.18                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.22        ; Prob 100%                     ;
                                ; LOE eax ebx esi edi
.B1.93:                         ; Preds .B1.22                  ; Infreq
        inc       edi                                           ;57.10
        add       ebx, DWORD PTR [40+esp]                       ;57.10
        cmp       edi, DWORD PTR [88+esp]                       ;57.10
        jb        .B1.18        ; Prob 82%                      ;57.10
                                ; LOE ebx esi edi
.B1.94:                         ; Preds .B1.93                  ; Infreq
        DB        15                                            ;
        DB        31                                            ;
        DB        68                                            ;
        DB        0                                             ;
        DB        0                                             ;
        mov       ecx, DWORD PTR [20+esp]                       ;
        inc       ecx                                           ;58.11
        cmp       ecx, DWORD PTR [16+esp]                       ;58.11
        jb        .B1.17        ; Prob 82%                      ;58.11
                                ; LOE ecx
.B1.95:                         ; Preds .B1.94                  ; Infreq
        mov       ecx, DWORD PTR [60+esp]                       ;
        jmp       .B1.36        ; Prob 100%                     ;
                                ; LOE ecx
.B1.96:                         ; Preds .B1.28                  ; Infreq
        divss     xmm2, xmm1                                    ;47.91
        mulss     xmm2, xmm0                                    ;47.12
        mov       edi, DWORD PTR [16+esp]                       ;47.12
        movss     DWORD PTR [edi+esi*4], xmm2                   ;47.12
        jmp       .B1.30        ; Prob 100%                     ;47.12
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.97:                         ; Preds .B1.24                  ; Infreq
        inc       esi                                           ;62.10
        jmp       .B1.33        ; Prob 100%                     ;62.10
        ALIGN     16
                                ; LOE ecx ebx esi
; mark_end;
_TDSP_MAIN_MIVA ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	1
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _TDSP_MAIN_MIVA
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
_2il0floatpacket.3	DD	042c80000H
_2il0floatpacket.4	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIMEWEIGHT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AGGINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITI_NUEP:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITI_NU:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_KAY:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_KPATHS:BYTE
_DATA	ENDS
EXTRN	_TOLL_LINK_PRICING:PROC
EXTRN	__intel_fast_memcpy:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
