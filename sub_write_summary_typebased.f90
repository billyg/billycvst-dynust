      SUBROUTINE WRITE_SUMMARY_TYPEBASED(VehID,EndID,ctime)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 
      USE DYNUST_MAIN_MODULE
      USE DYNUST_VEH_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE

      REAL ctime
      INTEGER VehID
	  LOGICAL EndID
	  INTEGER date_time (8)
	  CHARACTER (LEN = 12) REAL_CLOCK(3)

      REAL,save,dimension(nu_types):: timeclassTP,disclassTP,STOPtimeclassTP,timeentryTP,totaltimeclassTP,expenseTP
      INTEGER,save,dimension(nu_types):: numberclassTP
      INTEGER,save::AllNumberIMP
	  REAL,save::AlltimeIMP,AlldistIMP,AllSTOPIMP
      LOGICAL,save::AlloID = .false.
	  INTEGER,save::I_Tag_1_Stage,I_Tag_2_Stage,I_Tag_1_Roll,I_Tag_2_Roll
	  REAL,save::Trip_Time_Total,Trip_Time_Total_Itag1,Trip_Time_Total_Itag2,Trip_Time_Roll,Trip_Time_Roll_Itag1,Trip_Time_Roll_Itag2,Trip_Time_Total_W_Q,Trip_Time_Roll_W_Q
	  REAL,save::Trip_Distance_Total,Trip_Distance_Total_Itag1,Trip_Distance_Total_Itag2,Trip_Distance_Roll,Trip_Distance_Roll_Itag1,Trip_Distance_Roll_Itag2
      REAL,save::STOP_Time_Total,STOP_Time_Total_Itag1,STOP_Time_Total_Itag2,STOP_Time_Roll,STOP_Time_Roll_Itag1,STOP_Time_Roll_Itag2
      REAL,ALLOCATABLE::timeIMP(:,:),distIMP(:,:),STOPIMP(:,:),entryIMP(:,:),totalIMP(:,:)
      REAL,ALLOCATABLE::WZtimeIMP(:,:),WZdistIMP(:,:),WZSTOPIMP(:,:),WZentryIMP(:,:),WZtotalIMP(:,:)
	  INTEGER,ALLOCATABLE::numberIMP(:,:)
	  INTEGER,ALLOCATABLE::WZnumberIMP(:,:)

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
      
	 IF(.not.AlloID) THEN
      IF(inci_num > 0) THEN
	    ALLOCATE(timeIMP(2,inci_num))
	    ALLOCATE(distIMP(2,inci_num))
	    ALLOCATE(STOPIMP(2,inci_num))
	    ALLOCATE(entryIMP(2,inci_num))
	    ALLOCATE(totalIMP(2,inci_num))
        ALLOCATE(numberIMP(2,inci_num))
	  timeIMP(:,:) = 0.0
	  distIMP(:,:) = 0.0
	  STOPIMP(:,:) = 0.0
	  entryIMP(:,:) = 0.0
	  totalIMP(:,:) = 0.0
      numberIMP(:,:) = 0.0

	  ENDIF
      IF(WorkZoneNum > 0) THEN
	    ALLOCATE(WZtimeIMP(2,WorkZoneNum))
	    ALLOCATE(WZdistIMP(2,WorkZoneNum))
	    ALLOCATE(WZSTOPIMP(2,WorkZoneNum))
	    ALLOCATE(WZentryIMP(2,WorkZoneNum))
	    ALLOCATE(WZtotalIMP(2,WorkZoneNum))
        ALLOCATE(WZnumberIMP(2,WorkZoneNum))
	  WZtimeIMP(:,:) = 0.0
	  WZdistIMP(:,:) = 0.0
	  WZSTOPIMP(:,:) = 0.0
	  WZentryIMP(:,:) = 0.0
	  WZtotalIMP(:,:) = 0.0
      WZnumberIMP(:,:) = 0.0

	  ENDIF
      
	  AlltimeIMP  = 0
	  AlldistIMP  = 0
	  AllSTOPIMP  = 0

      AlloID = .true.     
	 ENDIF

     IF(.not.IniID2) THEN
      timeclassTP(:) = 0.0
      numberclassTP(:) = 0
      disclassTP(:) = 0.0
      STOPtimeclassTP(:) = 0.0
      timeentryTP(:) =0.0
      totaltimeclassTP(:) = 0.0 
      expenseTP(:) = 0.0
          
	  I_Tag_1_Stage=0
	  I_Tag_2_Stage=0
	  I_Tag_1_Roll=0
	  I_Tag_2_Roll=0

	  Trip_Time_Total=0.0
	  Trip_Time_Total_Itag1=0.0
	  Trip_Time_Total_Itag2=0.0
	  Trip_Time_Roll=0.0
	  Trip_Time_Roll_Itag1=0.0
	  Trip_Time_Roll_Itag2=0.0

	  Trip_Time_Total_W_Q=0.0
	  Trip_Time_Roll_W_Q=0.0

	  Trip_Distance_Total=0.0
	  Trip_Distance_Total_Itag1=0.0
	  Trip_Distance_Total_Itag2=0.0
	  Trip_Distance_Roll=0.0
	  Trip_Distance_Roll_Itag1=0.0
	  Trip_Distance_Roll_Itag2=0.0
      STOP_Time_Total=0.0
      STOP_Time_Total_Itag1=0.0
      STOP_Time_Total_Itag2=0.0
      STOP_Time_Roll=0.0
      STOP_Time_Roll_Itag1=0.0
      STOP_Time_Roll_Itag2=0.0
      IniID2=.true.

     ENDIF
      
	 IF(.not.EndID) THEN	
      i = VehID
	  IF(m_dynust_last_stand(i)%stime >= starttm.and.m_dynust_last_stand(i)%stime < endtm) THEN
       IF(m_dynust_veh(i)%itag == 2) THEN
        iv=m_dynust_last_stand(i)%vehtype      
        totaltimeclassTP(iv)=totaltimeclassTP(iv)+(m_dynust_last_stand(i)%atime-m_dynust_last_stand(i)%stime)
        timeclassTP(iv)=timeclassTP(iv)+m_dynust_veh(i)%ttilnow
        numberclassTP(iv)=numberclassTP(iv)+1
        disclassTP(iv)=disclassTP(iv)+m_dynust_veh(i)%distans
        STOPtimeclassTP(iv)=STOPtimeclassTP(iv)+m_dynust_veh(i)%ttSTOP
        timeentryTP(iv)=timeentryTP(iv)+max(0.0,m_dynust_last_stand(i)%atime-m_dynust_last_stand(i)%stime-m_dynust_veh(i)%ttilnow)
        IF(iv == 1) THEN
          expenseTP(iv)=expenseTP(iv)+m_dynust_last_stand(i)%expense*(TollVOTA/60.0)
        ELSEIF(iv == 2) THEN
          expenseTP(iv)=expenseTP(iv)+m_dynust_last_stand(i)%expense*(TollVOTT/60.0)
        ELSEIF(iv == 3) THEN
          expenseTP(iv)=expenseTP(iv)+m_dynust_last_stand(i)%expense*(TollVOTH/60.0)
        ENDIF
       ENDIF
     ENDIF


    ELSE !IF(EndID) last vehicle
      
	  IniID2 = .false.


	  Write (180,*) '-------------------------------------------------'
      write (180,*) 'The following MUT information is only for	    '
	  write (180,*) 'Those vehicles that have reached the destinations'
	  write (180,*) '-------------------------------------------------'
	    WRITE(180,*)
        DO 1200 i=1,nu_types
          IF(i == 1) WRITE(180,*) ' Auto  Vehicle ------------------------'
          IF(i == 2) WRITE(180,*) ' Truck Vehicle ------------------------'
          IF(i == 3) WRITE(180,*) ' HOV   Vehicle ------------------------'        
          WRITE(180,*)' --------------------------------------------------'
          WRITE(180,'(" Type Number                 = ",i15)') i
          IF(numberclassTP(i) == 0) goto 1200
          WRITE(180,'(" number of vehicles           = ",i15)') numberclassTP(i)
          WRITE(180,*)'------------------'
          WRITE(180,'("Total Overall Travel Time(min)= ",f15.3)') totaltimeclassTP(i)
          WRITE(180,'("Total Trip Times(min)         = ",f15.3)') timeclassTP(i)
          WRITE(180,'("Total Entry Queue Time(min)   = ",f15.3)') timeentryTP(i)
          WRITE(180,'("Total Trip Distance(ml)       = ",f15.3)') disclassTP(i)
          WRITE(180,'("Total STOP Time(min)          = ",f15.3)') STOPtimeclassTP(i)
          WRITE(180,'("Average Overall Trip Time(min)= ",f15.3)') totaltimeclassTP(i)/numberclassTP(i)
          WRITE(180,'("Average Trip Times(min)       = ",f15.3)') timeclassTP(i)/numberclassTP(i)
          WRITE(180,'("Average Entry Q Time(min)     = ",f15.3)') timeentryTP(i)/numberclassTP(i)
          WRITE(180,'("Average STOP Time(min)        = ",f15.3)') STOPtimeclassTP(i)/numberclassTP(i)
          WRITE(180,'("Average Trip Distance(ml)     = ",f15.3)') disclassTP(i)/numberclassTP(i)
          WRITE(180,'("Average Toll Expense($)       = ",f15.3)') expenseTP(i)/numberclassTP(i)
          WRITE(180,*)'----------------------------------'
1200  CONTINUE

1     FORMAT(f15.3)
2     FORMAT(i15)


      IF(inci_num > 0) THEN
	    IF(ALLOCATED(timeIMP)) DEALLOCATE(timeIMP)
	    IF(ALLOCATED(distIMP)) DEALLOCATE(distIMP)
	    IF(ALLOCATED(STOPIMP)) DEALLOCATE(STOPIMP)
	    IF(ALLOCATED(entryIMP)) DEALLOCATE(entryIMP)
	    IF(ALLOCATED(totalIMP)) DEALLOCATE(totalIMP)
        IF(ALLOCATED(numberIMP)) DEALLOCATE(numberIMP)
      ENDIF

      IF(WorkZoneNum > 0) THEN
	    IF(ALLOCATED(WZtimeIMP)) DEALLOCATE(WZtimeIMP)
	    IF(ALLOCATED(WZdistIMP)) DEALLOCATE(WZdistIMP)
	    IF(ALLOCATED(WZSTOPIMP)) DEALLOCATE(WZSTOPIMP)
	    IF(ALLOCATED(WZentryIMP)) DEALLOCATE(WZentryIMP)
	    IF(ALLOCATED(WZtotalIMP)) DEALLOCATE(WZtotalIMP)
        IF(ALLOCATED(WZnumberIMP)) DEALLOCATE(WZnumberIMP)
      ENDIF

    ENDIF !IF (EndID)


END SUBROUTINE
