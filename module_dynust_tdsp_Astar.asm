; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE$
_DYNUST_TDSP_ASTAR_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET
_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET	PROC NEAR 
; parameter 1: 8 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;38.12
        mov       ebp, esp                                      ;38.12
        and       esp, -16                                      ;38.12
        push      esi                                           ;38.12
        push      edi                                           ;38.12
        push      ebx                                           ;38.12
        sub       esp, 20                                       ;38.12
        mov       eax, 1                                        ;40.5
        mov       edx, DWORD PTR [8+ebp]                        ;38.12
        mov       ebx, 8                                        ;40.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+16], eax ;40.5
        xor       ecx, ecx                                      ;40.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+32], eax ;40.5
        mov       eax, DWORD PTR [edx]                          ;40.5
        test      eax, eax                                      ;40.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+12], 133 ;40.5
        lea       edx, DWORD PTR [4+esp]                        ;40.5
        push      ebx                                           ;40.5
        cmovl     eax, ecx                                      ;40.5
        push      eax                                           ;40.5
        push      2                                             ;40.5
        push      edx                                           ;40.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+4], ebx ;40.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+8], ecx ;40.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+24], eax ;40.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+28], ebx ;40.5
        call      _for_check_mult_overflow                      ;40.5
                                ; LOE eax
.B2.2:                          ; Preds .B2.1
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+12] ;40.5
        and       eax, 1                                        ;40.5
        and       edx, 1                                        ;40.5
        shl       eax, 4                                        ;40.5
        add       edx, edx                                      ;40.5
        or        edx, eax                                      ;40.5
        or        edx, 262144                                   ;40.5
        push      edx                                           ;40.5
        push      OFFSET FLAT: _DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET ;40.5
        push      DWORD PTR [28+esp]                            ;40.5
        call      _for_alloc_allocatable                        ;40.5
                                ; LOE
.B2.45:                         ; Preds .B2.2
        add       esp, 28                                       ;40.5
                                ; LOE
.B2.3:                          ; Preds .B2.45
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET] ;40.5
        mov       edx, DWORD PTR [4+esp]                        ;40.5
        lea       eax, DWORD PTR [edx+ecx]                      ;40.5
        cmp       ecx, eax                                      ;40.5
        jae       .B2.11        ; Prob 50%                      ;40.5
                                ; LOE edx ecx
.B2.4:                          ; Preds .B2.3
        add       edx, 7                                        ;40.5
        mov       eax, edx                                      ;40.5
        sar       eax, 2                                        ;40.5
        shr       eax, 29                                       ;40.5
        add       eax, edx                                      ;40.5
        sar       eax, 3                                        ;40.5
        mov       edx, eax                                      ;40.5
        shr       edx, 1                                        ;40.5
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET$BLK..T750_] ;40.5
        test      edx, edx                                      ;40.5
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET$BLK..T750_+4] ;40.5
        ja        .B2.6         ; Prob 67%                      ;40.5
                                ; LOE eax edx ecx esi edi
.B2.5:                          ; Preds .B2.4
        mov       edx, 1                                        ;
        jmp       .B2.9         ; Prob 100%                     ;
                                ; LOE eax edx ecx esi edi
.B2.6:                          ; Preds .B2.4
        mov       DWORD PTR [esp], eax                          ;
        xor       ebx, ebx                                      ;
                                ; LOE edx ecx ebx esi edi
.B2.7:                          ; Preds .B2.7 .B2.6
        mov       eax, ebx                                      ;40.5
        inc       ebx                                           ;40.5
        shl       eax, 4                                        ;40.5
        cmp       ebx, edx                                      ;40.5
        mov       DWORD PTR [ecx+eax], edi                      ;40.5
        mov       DWORD PTR [4+eax+ecx], esi                    ;40.5
        mov       DWORD PTR [8+ecx+eax], edi                    ;40.5
        mov       DWORD PTR [12+eax+ecx], esi                   ;40.5
        jb        .B2.7         ; Prob 24%                      ;40.5
                                ; LOE edx ecx ebx esi edi
.B2.8:                          ; Preds .B2.7
        mov       eax, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+ebx+ebx]                    ;40.5
                                ; LOE eax edx ecx esi edi
.B2.9:                          ; Preds .B2.8 .B2.5
        dec       edx                                           ;40.5
        cmp       edx, eax                                      ;40.5
        jae       .B2.11        ; Prob 33%                      ;40.5
                                ; LOE edx ecx esi edi
.B2.10:                         ; Preds .B2.9
        mov       DWORD PTR [ecx+edx*8], edi                    ;40.5
        mov       DWORD PTR [4+ecx+edx*8], esi                  ;40.5
                                ; LOE
.B2.11:                         ; Preds .B2.3 .B2.9 .B2.10
        mov       edx, DWORD PTR [8+ebp]                        ;42.5
        mov       ebx, 4                                        ;42.5
        xor       ecx, ecx                                      ;42.5
        lea       edi, DWORD PTR [esp]                          ;42.5
        push      ebx                                           ;42.5
        mov       esi, DWORD PTR [edx]                          ;42.5
        test      esi, esi                                      ;42.5
        cmovl     esi, ecx                                      ;42.5
        mov       eax, 1                                        ;42.5
        push      esi                                           ;42.5
        push      2                                             ;42.5
        push      edi                                           ;42.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+12], 133 ;42.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+4], ebx ;42.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+16], eax ;42.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+8], ecx ;42.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+32], eax ;42.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+24], esi ;42.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+28], ebx ;42.5
        call      _for_check_mult_overflow                      ;42.5
                                ; LOE eax
.B2.12:                         ; Preds .B2.11
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+12] ;42.5
        and       eax, 1                                        ;42.5
        and       edx, 1                                        ;42.5
        shl       eax, 4                                        ;42.5
        add       edx, edx                                      ;42.5
        or        edx, eax                                      ;42.5
        or        edx, 262144                                   ;42.5
        push      edx                                           ;42.5
        push      OFFSET FLAT: _DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY ;42.5
        push      DWORD PTR [24+esp]                            ;42.5
        call      _for_alloc_allocatable                        ;42.5
                                ; LOE
.B2.47:                         ; Preds .B2.12
        add       esp, 28                                       ;42.5
                                ; LOE
.B2.13:                         ; Preds .B2.47
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+24] ;43.5
        test      edx, edx                                      ;43.5
        jle       .B2.16        ; Prob 50%                      ;43.5
                                ; LOE edx
.B2.14:                         ; Preds .B2.13
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY] ;43.5
        cmp       edx, 24                                       ;43.5
        jle       .B2.23        ; Prob 0%                       ;43.5
                                ; LOE edx ecx
.B2.15:                         ; Preds .B2.14
        shl       edx, 2                                        ;43.5
        push      edx                                           ;43.5
        push      0                                             ;43.5
        push      ecx                                           ;43.5
        call      __intel_fast_memset                           ;43.5
                                ; LOE
.B2.48:                         ; Preds .B2.15
        add       esp, 12                                       ;43.5
                                ; LOE
.B2.16:                         ; Preds .B2.37 .B2.13 .B2.35 .B2.48
        mov       eax, DWORD PTR [8+ebp]                        ;45.5
        mov       esi, DWORD PTR [eax]                          ;45.5
        test      esi, esi                                      ;45.5
        jle       .B2.22        ; Prob 2%                       ;45.5
                                ; LOE esi
.B2.17:                         ; Preds .B2.16
        mov       ebx, 1                                        ;
                                ; LOE ebx esi
.B2.18:                         ; Preds .B2.20 .B2.17
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+32] ;46.14
        shl       eax, 3                                        ;46.14
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET] ;46.14
        sub       edx, eax                                      ;46.14
        lea       ecx, DWORD PTR [edx+ebx*8]                    ;46.14
        push      ecx                                           ;46.14
        call      _LINK_LIST_mp_CREATE_QUEUE_LINKLIST           ;46.14
                                ; LOE ebx esi
.B2.19:                         ; Preds .B2.18
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+32] ;47.14
        shl       eax, 3                                        ;47.14
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET] ;47.14
        sub       edx, eax                                      ;47.14
        push      OFFSET FLAT: __NLITPACK_1.0.2                 ;47.14
        push      OFFSET FLAT: __NLITPACK_0.0.2                 ;47.14
        lea       ecx, DWORD PTR [edx+ebx*8]                    ;47.14
        push      ecx                                           ;47.14
        call      _LINK_LIST_mp_ENQUEUE_LINKLIST                ;47.14
                                ; LOE ebx esi
.B2.49:                         ; Preds .B2.19
        add       esp, 16                                       ;47.14
                                ; LOE ebx esi
.B2.20:                         ; Preds .B2.49
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+32] ;48.9
        neg       edx                                           ;48.9
        add       edx, ebx                                      ;48.9
        inc       ebx                                           ;49.5
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY] ;48.9
        cmp       ebx, esi                                      ;49.5
        mov       DWORD PTR [eax+edx*4], 1                      ;48.9
        jle       .B2.18        ; Prob 82%                      ;49.5
                                ; LOE ebx esi
.B2.22:                         ; Preds .B2.20 .B2.16
        add       esp, 20                                       ;51.1
        pop       ebx                                           ;51.1
        pop       edi                                           ;51.1
        pop       esi                                           ;51.1
        mov       esp, ebp                                      ;51.1
        pop       ebp                                           ;51.1
        ret                                                     ;51.1
                                ; LOE
.B2.23:                         ; Preds .B2.14                  ; Infreq
        cmp       edx, 4                                        ;43.5
        jl        .B2.39        ; Prob 10%                      ;43.5
                                ; LOE edx ecx
.B2.24:                         ; Preds .B2.23                  ; Infreq
        mov       eax, ecx                                      ;43.5
        and       eax, 15                                       ;43.5
        je        .B2.27        ; Prob 50%                      ;43.5
                                ; LOE eax edx ecx
.B2.25:                         ; Preds .B2.24                  ; Infreq
        test      al, 3                                         ;43.5
        jne       .B2.39        ; Prob 10%                      ;43.5
                                ; LOE eax edx ecx
.B2.26:                         ; Preds .B2.25                  ; Infreq
        neg       eax                                           ;43.5
        add       eax, 16                                       ;43.5
        shr       eax, 2                                        ;43.5
                                ; LOE eax edx ecx
.B2.27:                         ; Preds .B2.26 .B2.24           ; Infreq
        lea       ebx, DWORD PTR [4+eax]                        ;43.5
        cmp       edx, ebx                                      ;43.5
        jl        .B2.39        ; Prob 10%                      ;43.5
                                ; LOE eax edx ecx
.B2.28:                         ; Preds .B2.27                  ; Infreq
        mov       esi, edx                                      ;43.5
        sub       esi, eax                                      ;43.5
        and       esi, 3                                        ;43.5
        neg       esi                                           ;43.5
        add       esi, edx                                      ;43.5
        test      eax, eax                                      ;43.5
        jbe       .B2.32        ; Prob 10%                      ;43.5
                                ; LOE eax edx ecx esi
.B2.29:                         ; Preds .B2.28                  ; Infreq
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.30:                         ; Preds .B2.30 .B2.29           ; Infreq
        mov       DWORD PTR [ecx+ebx*4], 0                      ;43.5
        inc       ebx                                           ;43.5
        cmp       ebx, eax                                      ;43.5
        jb        .B2.30        ; Prob 82%                      ;43.5
                                ; LOE eax edx ecx ebx esi
.B2.32:                         ; Preds .B2.30 .B2.28           ; Infreq
        pxor      xmm0, xmm0                                    ;43.5
                                ; LOE eax edx ecx esi xmm0
.B2.33:                         ; Preds .B2.33 .B2.32           ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;43.5
        add       eax, 4                                        ;43.5
        cmp       eax, esi                                      ;43.5
        jb        .B2.33        ; Prob 82%                      ;43.5
                                ; LOE eax edx ecx esi xmm0
.B2.35:                         ; Preds .B2.33 .B2.39           ; Infreq
        cmp       esi, edx                                      ;43.5
        jae       .B2.16        ; Prob 10%                      ;43.5
                                ; LOE edx ecx esi
.B2.37:                         ; Preds .B2.35 .B2.37           ; Infreq
        mov       DWORD PTR [ecx+esi*4], 0                      ;43.5
        inc       esi                                           ;43.5
        cmp       esi, edx                                      ;43.5
        jb        .B2.37        ; Prob 82%                      ;43.5
        jmp       .B2.16        ; Prob 100%                     ;43.5
                                ; LOE edx ecx esi
.B2.39:                         ; Preds .B2.23 .B2.27 .B2.25    ; Infreq
        xor       esi, esi                                      ;43.5
        jmp       .B2.35        ; Prob 100%                     ;43.5
        ALIGN     16
                                ; LOE edx ecx esi
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_1.0.2	DD	04b189680H
__NLITPACK_0.0.2	DD	999999
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_SCAN_ELIGIBLE_SET
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_SCAN_ELIGIBLE_SET
_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_SCAN_ELIGIBLE_SET	PROC NEAR 
; parameter 1: 12 + esp
.B3.1:                          ; Preds .B3.0
        push      esi                                           ;54.12
        push      esi                                           ;54.12
        mov       eax, DWORD PTR [12+esp]                       ;54.12
        mov       eax, DWORD PTR [eax]                          ;56.5
        test      eax, eax                                      ;56.5
        jle       .B3.6         ; Prob 2%                       ;56.5
                                ; LOE eax ebx ebp edi
.B3.2:                          ; Preds .B3.1
        mov       DWORD PTR [esp], edi                          ;
        mov       esi, 1                                        ;
        mov       edi, eax                                      ;
                                ; LOE ebx ebp esi edi
.B3.3:                          ; Preds .B3.4 .B3.2
        mov       ecx, esi                                      ;57.14
        sub       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+32] ;57.14
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET] ;57.14
        lea       edx, DWORD PTR [edx+ecx*8]                    ;57.14
        push      edx                                           ;57.14
        call      _LINK_LIST_mp_DESTROY_QUEUE_LINKLIST          ;57.14
                                ; LOE ebx ebp esi edi
.B3.11:                         ; Preds .B3.3
        add       esp, 4                                        ;57.14
                                ; LOE ebx ebp esi edi
.B3.4:                          ; Preds .B3.11
        inc       esi                                           ;58.5
        cmp       esi, edi                                      ;58.5
        jle       .B3.3         ; Prob 82%                      ;58.5
                                ; LOE ebx ebp esi edi
.B3.5:                          ; Preds .B3.4
        mov       edi, DWORD PTR [esp]                          ;
                                ; LOE ebx ebp edi
.B3.6:                          ; Preds .B3.5 .B3.1
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+12] ;60.5
        mov       edx, esi                                      ;60.5
        shr       edx, 1                                        ;60.5
        mov       eax, esi                                      ;60.5
        and       edx, 1                                        ;60.5
        and       eax, 1                                        ;60.5
        shl       edx, 2                                        ;60.5
        add       eax, eax                                      ;60.5
        or        edx, eax                                      ;60.5
        or        edx, 262144                                   ;60.5
        push      edx                                           ;60.5
        push      DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY] ;60.5
        call      _for_dealloc_allocatable                      ;60.5
                                ; LOE ebx ebp esi edi
.B3.7:                          ; Preds .B3.6
        and       esi, -2                                       ;60.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+12], esi ;60.5
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+12] ;61.5
        mov       edx, esi                                      ;61.5
        shr       edx, 1                                        ;61.5
        mov       eax, esi                                      ;61.5
        and       edx, 1                                        ;61.5
        and       eax, 1                                        ;61.5
        shl       edx, 2                                        ;61.5
        add       eax, eax                                      ;61.5
        or        edx, eax                                      ;61.5
        or        edx, 262144                                   ;61.5
        push      edx                                           ;61.5
        push      DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET] ;61.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY], 0 ;60.5
        call      _for_dealloc_allocatable                      ;61.5
                                ; LOE ebx ebp esi edi
.B3.8:                          ; Preds .B3.7
        and       esi, -2                                       ;61.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET], 0 ;61.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+12], esi ;61.5
        add       esp, 20                                       ;63.1
        pop       esi                                           ;63.1
        ret                                                     ;63.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_SCAN_ELIGIBLE_SET ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_SCAN_ELIGIBLE_SET
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET
_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET	PROC NEAR 
.B4.1:                          ; Preds .B4.0
        mov       edx, 8                                        ;68.5
        push      262146                                        ;68.5
        push      OFFSET FLAT: _DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET ;68.5
        push      edx                                           ;68.5
        mov       eax, 1                                        ;68.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+12], 133 ;68.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+4], edx ;68.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+16], eax ;68.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+8], 0 ;68.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+32], eax ;68.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+24], eax ;68.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+28], edx ;68.5
        call      _for_alloc_allocatable                        ;68.5
                                ; LOE ebx ebp esi edi
.B4.8:                          ; Preds .B4.1
        add       esp, 12                                       ;68.5
                                ; LOE ebx ebp esi edi
.B4.2:                          ; Preds .B4.8
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET] ;68.5
        lea       edx, DWORD PTR [8+eax]                        ;68.5
        cmp       eax, edx                                      ;68.5
        jae       .B4.4         ; Prob 50%                      ;68.5
                                ; LOE eax ebx ebp esi edi
.B4.3:                          ; Preds .B4.2
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET$BLK..T782_] ;68.5
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET$BLK..T782_+4] ;68.5
        mov       DWORD PTR [eax], edx                          ;68.5
        mov       DWORD PTR [4+eax], ecx                        ;68.5
                                ; LOE eax ebx ebp esi edi
.B4.4:                          ; Preds .B4.2 .B4.3
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+32] ;69.10
        shl       edx, 3                                        ;69.10
        sub       eax, edx                                      ;69.10
        add       eax, 8                                        ;69.10
        push      eax                                           ;69.10
        call      _LINK_LIST_mp_CREATE_QUEUE_LINKLIST           ;69.10
                                ; LOE ebx ebp esi edi
.B4.5:                          ; Preds .B4.4
        pop       ecx                                           ;71.1
        ret                                                     ;71.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_FIXED_LABEL_SET
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_FIXED_LABEL_SET
_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_FIXED_LABEL_SET	PROC NEAR 
.B5.1:                          ; Preds .B5.0
        push      edi                                           ;74.12
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+32] ;76.10
        shl       eax, 3                                        ;76.10
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET] ;76.10
        sub       edx, eax                                      ;76.10
        add       edx, 8                                        ;76.10
        push      edx                                           ;76.10
        call      _LINK_LIST_mp_DESTROY_QUEUE_LINKLIST          ;76.10
                                ; LOE ebx ebp esi
.B5.2:                          ; Preds .B5.1
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+12] ;77.5
        mov       edx, edi                                      ;77.5
        shr       edx, 1                                        ;77.5
        mov       eax, edi                                      ;77.5
        and       edx, 1                                        ;77.5
        and       eax, 1                                        ;77.5
        shl       edx, 2                                        ;77.5
        add       eax, eax                                      ;77.5
        or        edx, eax                                      ;77.5
        or        edx, 262144                                   ;77.5
        push      edx                                           ;77.5
        push      DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET] ;77.5
        call      _for_dealloc_allocatable                      ;77.5
                                ; LOE ebx ebp esi edi
.B5.3:                          ; Preds .B5.2
        and       edi, -2                                       ;77.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET], 0 ;77.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+12], edi ;77.5
        add       esp, 12                                       ;79.1
        pop       edi                                           ;79.1
        ret                                                     ;79.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_FIXED_LABEL_SET ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_FIXED_LABEL_SET
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_LABELS
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_LABELS
_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_LABELS	PROC NEAR 
; parameter 1: 8 + ebp
.B6.1:                          ; Preds .B6.0
        push      ebp                                           ;82.12
        mov       ebp, esp                                      ;82.12
        and       esp, -16                                      ;82.12
        push      esi                                           ;82.12
        push      edi                                           ;82.12
        push      ebx                                           ;82.12
        sub       esp, 20                                       ;82.12
        mov       eax, 1                                        ;85.2
        mov       ebx, DWORD PTR [8+ebp]                        ;82.12
        mov       ecx, 4                                        ;85.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+16], eax ;85.2
        xor       edx, edx                                      ;85.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+32], eax ;85.2
        mov       eax, DWORD PTR [ebx]                          ;85.2
        test      eax, eax                                      ;85.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+8], edx ;85.2
        cmovl     eax, edx                                      ;85.2
        lea       edx, DWORD PTR [esp]                          ;85.2
        push      ecx                                           ;85.2
        push      eax                                           ;85.2
        push      2                                             ;85.2
        push      edx                                           ;85.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+12], 133 ;85.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+4], ecx ;85.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+24], eax ;85.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+28], ecx ;85.2
        call      _for_check_mult_overflow                      ;85.2
                                ; LOE eax ebx
.B6.2:                          ; Preds .B6.1
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+12] ;85.2
        and       eax, 1                                        ;85.2
        and       edx, 1                                        ;85.2
        shl       eax, 4                                        ;85.2
        add       edx, edx                                      ;85.2
        or        edx, eax                                      ;85.2
        or        edx, 262144                                   ;85.2
        push      edx                                           ;85.2
        push      OFFSET FLAT: _DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL ;85.2
        push      DWORD PTR [24+esp]                            ;85.2
        call      _for_alloc_allocatable                        ;85.2
                                ; LOE ebx
.B6.95:                         ; Preds .B6.2
        add       esp, 28                                       ;85.2
                                ; LOE ebx
.B6.3:                          ; Preds .B6.95
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+24] ;86.2
        test      esi, esi                                      ;86.2
        jle       .B6.20        ; Prob 0%                       ;86.2
                                ; LOE ebx esi
.B6.4:                          ; Preds .B6.3
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;86.2
        cmp       esi, 8                                        ;86.2
        jl        .B6.64        ; Prob 10%                      ;86.2
                                ; LOE ecx ebx esi
.B6.5:                          ; Preds .B6.4
        mov       edx, ecx                                      ;86.2
        and       edx, 15                                       ;86.2
        je        .B6.8         ; Prob 50%                      ;86.2
                                ; LOE edx ecx ebx esi
.B6.6:                          ; Preds .B6.5
        test      dl, 3                                         ;86.2
        jne       .B6.64        ; Prob 10%                      ;86.2
                                ; LOE edx ecx ebx esi
.B6.7:                          ; Preds .B6.6
        neg       edx                                           ;86.2
        add       edx, 16                                       ;86.2
        shr       edx, 2                                        ;86.2
                                ; LOE edx ecx ebx esi
.B6.8:                          ; Preds .B6.7 .B6.5
        lea       eax, DWORD PTR [8+edx]                        ;86.2
        cmp       esi, eax                                      ;86.2
        jl        .B6.64        ; Prob 10%                      ;86.2
                                ; LOE edx ecx ebx esi
.B6.9:                          ; Preds .B6.8
        mov       edi, esi                                      ;86.2
        sub       edi, edx                                      ;86.2
        and       edi, 7                                        ;86.2
        neg       edi                                           ;86.2
        add       edi, esi                                      ;86.2
        test      edx, edx                                      ;86.2
        jbe       .B6.13        ; Prob 10%                      ;86.2
                                ; LOE edx ecx ebx esi edi
.B6.10:                         ; Preds .B6.9
        xor       eax, eax                                      ;
        mov       ebx, 1203982336                               ;
                                ; LOE eax edx ecx ebx esi edi
.B6.11:                         ; Preds .B6.11 .B6.10
        mov       DWORD PTR [ecx+eax*4], ebx                    ;86.2
        inc       eax                                           ;86.2
        cmp       eax, edx                                      ;86.2
        jb        .B6.11        ; Prob 82%                      ;86.2
                                ; LOE eax edx ecx ebx esi edi
.B6.12:                         ; Preds .B6.11
        mov       ebx, DWORD PTR [8+ebp]                        ;
                                ; LOE edx ecx ebx esi edi
.B6.13:                         ; Preds .B6.9 .B6.12
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.14]       ;86.2
                                ; LOE edx ecx ebx esi edi xmm0
.B6.14:                         ; Preds .B6.14 .B6.13
        movaps    XMMWORD PTR [ecx+edx*4], xmm0                 ;86.2
        movaps    XMMWORD PTR [16+ecx+edx*4], xmm0              ;86.2
        add       edx, 8                                        ;86.2
        cmp       edx, edi                                      ;86.2
        jb        .B6.14        ; Prob 82%                      ;86.2
                                ; LOE edx ecx ebx esi edi xmm0
.B6.16:                         ; Preds .B6.14 .B6.64
        cmp       edi, esi                                      ;86.2
        jae       .B6.20        ; Prob 10%                      ;86.2
                                ; LOE ecx ebx esi edi
.B6.17:                         ; Preds .B6.16
        mov       eax, 1203982336                               ;86.2
                                ; LOE eax ecx ebx esi edi
.B6.18:                         ; Preds .B6.18 .B6.17
        mov       DWORD PTR [ecx+edi*4], eax                    ;86.2
        inc       edi                                           ;86.2
        cmp       edi, esi                                      ;86.2
        jb        .B6.18        ; Prob 82%                      ;86.2
                                ; LOE eax ecx ebx esi edi
.B6.20:                         ; Preds .B6.18 .B6.3 .B6.16
        mov       esi, DWORD PTR [ebx]                          ;87.2
        xor       edx, edx                                      ;87.2
        test      esi, esi                                      ;87.2
        cmovl     esi, edx                                      ;87.2
        mov       ecx, 4                                        ;87.2
        lea       edi, DWORD PTR [4+esp]                        ;87.2
        push      ecx                                           ;87.2
        push      esi                                           ;87.2
        push      2                                             ;87.2
        push      edi                                           ;87.2
        mov       eax, 1                                        ;87.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+12], 133 ;87.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+4], ecx ;87.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+16], eax ;87.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+8], edx ;87.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+32], eax ;87.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+24], esi ;87.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+28], ecx ;87.2
        call      _for_check_mult_overflow                      ;87.2
                                ; LOE eax ebx
.B6.21:                         ; Preds .B6.20
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+12] ;87.2
        and       eax, 1                                        ;87.2
        and       edx, 1                                        ;87.2
        shl       eax, 4                                        ;87.2
        add       edx, edx                                      ;87.2
        or        edx, eax                                      ;87.2
        or        edx, 262144                                   ;87.2
        push      edx                                           ;87.2
        push      OFFSET FLAT: _DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL ;87.2
        push      DWORD PTR [28+esp]                            ;87.2
        call      _for_alloc_allocatable                        ;87.2
                                ; LOE ebx
.B6.97:                         ; Preds .B6.21
        add       esp, 28                                       ;87.2
                                ; LOE ebx
.B6.22:                         ; Preds .B6.97
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+24] ;88.2
        test      esi, esi                                      ;88.2
        jle       .B6.39        ; Prob 0%                       ;88.2
                                ; LOE ebx esi
.B6.23:                         ; Preds .B6.22
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL] ;88.2
        cmp       esi, 8                                        ;88.2
        jl        .B6.65        ; Prob 10%                      ;88.2
                                ; LOE ecx ebx esi
.B6.24:                         ; Preds .B6.23
        mov       edx, ecx                                      ;88.2
        and       edx, 15                                       ;88.2
        je        .B6.27        ; Prob 50%                      ;88.2
                                ; LOE edx ecx ebx esi
.B6.25:                         ; Preds .B6.24
        test      dl, 3                                         ;88.2
        jne       .B6.65        ; Prob 10%                      ;88.2
                                ; LOE edx ecx ebx esi
.B6.26:                         ; Preds .B6.25
        neg       edx                                           ;88.2
        add       edx, 16                                       ;88.2
        shr       edx, 2                                        ;88.2
                                ; LOE edx ecx ebx esi
.B6.27:                         ; Preds .B6.26 .B6.24
        lea       eax, DWORD PTR [8+edx]                        ;88.2
        cmp       esi, eax                                      ;88.2
        jl        .B6.65        ; Prob 10%                      ;88.2
                                ; LOE edx ecx ebx esi
.B6.28:                         ; Preds .B6.27
        mov       edi, esi                                      ;88.2
        sub       edi, edx                                      ;88.2
        and       edi, 7                                        ;88.2
        neg       edi                                           ;88.2
        add       edi, esi                                      ;88.2
        test      edx, edx                                      ;88.2
        jbe       .B6.32        ; Prob 10%                      ;88.2
                                ; LOE edx ecx ebx esi edi
.B6.29:                         ; Preds .B6.28
        xor       eax, eax                                      ;
        mov       ebx, 1203982336                               ;
                                ; LOE eax edx ecx ebx esi edi
.B6.30:                         ; Preds .B6.30 .B6.29
        mov       DWORD PTR [ecx+eax*4], ebx                    ;88.2
        inc       eax                                           ;88.2
        cmp       eax, edx                                      ;88.2
        jb        .B6.30        ; Prob 82%                      ;88.2
                                ; LOE eax edx ecx ebx esi edi
.B6.31:                         ; Preds .B6.30
        mov       ebx, DWORD PTR [8+ebp]                        ;
                                ; LOE edx ecx ebx esi edi
.B6.32:                         ; Preds .B6.28 .B6.31
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.14]       ;88.2
                                ; LOE edx ecx ebx esi edi xmm0
.B6.33:                         ; Preds .B6.33 .B6.32
        movaps    XMMWORD PTR [ecx+edx*4], xmm0                 ;88.2
        movaps    XMMWORD PTR [16+ecx+edx*4], xmm0              ;88.2
        add       edx, 8                                        ;88.2
        cmp       edx, edi                                      ;88.2
        jb        .B6.33        ; Prob 82%                      ;88.2
                                ; LOE edx ecx ebx esi edi xmm0
.B6.35:                         ; Preds .B6.33 .B6.65
        cmp       edi, esi                                      ;88.2
        jae       .B6.39        ; Prob 10%                      ;88.2
                                ; LOE ecx ebx esi edi
.B6.36:                         ; Preds .B6.35
        mov       eax, 1203982336                               ;88.2
                                ; LOE eax ecx ebx esi edi
.B6.37:                         ; Preds .B6.37 .B6.36
        mov       DWORD PTR [ecx+edi*4], eax                    ;88.2
        inc       edi                                           ;88.2
        cmp       edi, esi                                      ;88.2
        jb        .B6.37        ; Prob 82%                      ;88.2
                                ; LOE eax ecx ebx esi edi
.B6.39:                         ; Preds .B6.37 .B6.22 .B6.35
        mov       esi, DWORD PTR [ebx]                          ;89.2
        xor       edx, edx                                      ;89.2
        test      esi, esi                                      ;89.2
        cmovl     esi, edx                                      ;89.2
        mov       ecx, 4                                        ;89.2
        lea       edi, DWORD PTR [8+esp]                        ;89.2
        push      ecx                                           ;89.2
        push      esi                                           ;89.2
        push      2                                             ;89.2
        push      edi                                           ;89.2
        mov       eax, 1                                        ;89.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+12], 133 ;89.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+4], ecx ;89.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+16], eax ;89.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+8], edx ;89.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+32], eax ;89.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+24], esi ;89.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+28], ecx ;89.2
        call      _for_check_mult_overflow                      ;89.2
                                ; LOE eax ebx
.B6.40:                         ; Preds .B6.39
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+12] ;89.2
        and       eax, 1                                        ;89.2
        and       edx, 1                                        ;89.2
        shl       eax, 4                                        ;89.2
        add       edx, edx                                      ;89.2
        or        edx, eax                                      ;89.2
        or        edx, 262144                                   ;89.2
        push      edx                                           ;89.2
        push      OFFSET FLAT: _DYNUST_TDSP_ASTAR_MODULE_mp_EJD ;89.2
        push      DWORD PTR [32+esp]                            ;89.2
        call      _for_alloc_allocatable                        ;89.2
                                ; LOE ebx
.B6.99:                         ; Preds .B6.40
        add       esp, 28                                       ;89.2
                                ; LOE ebx
.B6.41:                         ; Preds .B6.99
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+24] ;90.2
        test      esi, esi                                      ;90.2
        jle       .B6.58        ; Prob 0%                       ;90.2
                                ; LOE ebx esi
.B6.42:                         ; Preds .B6.41
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD] ;90.2
        cmp       esi, 8                                        ;90.2
        jl        .B6.66        ; Prob 10%                      ;90.2
                                ; LOE ecx ebx esi
.B6.43:                         ; Preds .B6.42
        mov       edx, ecx                                      ;90.2
        and       edx, 15                                       ;90.2
        je        .B6.46        ; Prob 50%                      ;90.2
                                ; LOE edx ecx ebx esi
.B6.44:                         ; Preds .B6.43
        test      dl, 3                                         ;90.2
        jne       .B6.66        ; Prob 10%                      ;90.2
                                ; LOE edx ecx ebx esi
.B6.45:                         ; Preds .B6.44
        neg       edx                                           ;90.2
        add       edx, 16                                       ;90.2
        shr       edx, 2                                        ;90.2
                                ; LOE edx ecx ebx esi
.B6.46:                         ; Preds .B6.45 .B6.43
        lea       eax, DWORD PTR [8+edx]                        ;90.2
        cmp       esi, eax                                      ;90.2
        jl        .B6.66        ; Prob 10%                      ;90.2
                                ; LOE edx ecx ebx esi
.B6.47:                         ; Preds .B6.46
        mov       edi, esi                                      ;90.2
        sub       edi, edx                                      ;90.2
        and       edi, 7                                        ;90.2
        neg       edi                                           ;90.2
        add       edi, esi                                      ;90.2
        test      edx, edx                                      ;90.2
        jbe       .B6.51        ; Prob 10%                      ;90.2
                                ; LOE edx ecx ebx esi edi
.B6.48:                         ; Preds .B6.47
        xor       eax, eax                                      ;
        mov       ebx, 1203982336                               ;
                                ; LOE eax edx ecx ebx esi edi
.B6.49:                         ; Preds .B6.49 .B6.48
        mov       DWORD PTR [ecx+eax*4], ebx                    ;90.2
        inc       eax                                           ;90.2
        cmp       eax, edx                                      ;90.2
        jb        .B6.49        ; Prob 82%                      ;90.2
                                ; LOE eax edx ecx ebx esi edi
.B6.50:                         ; Preds .B6.49
        mov       ebx, DWORD PTR [8+ebp]                        ;
                                ; LOE edx ecx ebx esi edi
.B6.51:                         ; Preds .B6.47 .B6.50
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.14]       ;90.2
                                ; LOE edx ecx ebx esi edi xmm0
.B6.52:                         ; Preds .B6.52 .B6.51
        movaps    XMMWORD PTR [ecx+edx*4], xmm0                 ;90.2
        movaps    XMMWORD PTR [16+ecx+edx*4], xmm0              ;90.2
        add       edx, 8                                        ;90.2
        cmp       edx, edi                                      ;90.2
        jb        .B6.52        ; Prob 82%                      ;90.2
                                ; LOE edx ecx ebx esi edi xmm0
.B6.54:                         ; Preds .B6.52 .B6.66
        cmp       edi, esi                                      ;90.2
        jae       .B6.58        ; Prob 10%                      ;90.2
                                ; LOE ecx ebx esi edi
.B6.55:                         ; Preds .B6.54
        mov       eax, 1203982336                               ;90.2
                                ; LOE eax ecx ebx esi edi
.B6.56:                         ; Preds .B6.56 .B6.55
        mov       DWORD PTR [ecx+edi*4], eax                    ;90.2
        inc       edi                                           ;90.2
        cmp       edi, esi                                      ;90.2
        jb        .B6.56        ; Prob 82%                      ;90.2
                                ; LOE eax ecx ebx esi edi
.B6.58:                         ; Preds .B6.56 .B6.41 .B6.54
        mov       ebx, DWORD PTR [ebx]                          ;91.2
        xor       edx, edx                                      ;91.2
        test      ebx, ebx                                      ;91.2
        cmovl     ebx, edx                                      ;91.2
        mov       ecx, 4                                        ;91.2
        lea       esi, DWORD PTR [12+esp]                       ;91.2
        push      ecx                                           ;91.2
        push      ebx                                           ;91.2
        push      2                                             ;91.2
        push      esi                                           ;91.2
        mov       eax, 1                                        ;91.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+12], 133 ;91.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+4], ecx ;91.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+16], eax ;91.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+8], edx ;91.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+32], eax ;91.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+24], ebx ;91.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+28], ecx ;91.2
        call      _for_check_mult_overflow                      ;91.2
                                ; LOE eax
.B6.59:                         ; Preds .B6.58
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+12] ;91.2
        and       eax, 1                                        ;91.2
        and       edx, 1                                        ;91.2
        shl       eax, 4                                        ;91.2
        add       edx, edx                                      ;91.2
        or        edx, eax                                      ;91.2
        or        edx, 262144                                   ;91.2
        push      edx                                           ;91.2
        push      OFFSET FLAT: _DYNUST_TDSP_ASTAR_MODULE_mp_PRED ;91.2
        push      DWORD PTR [36+esp]                            ;91.2
        call      _for_alloc_allocatable                        ;91.2
                                ; LOE
.B6.101:                        ; Preds .B6.59
        add       esp, 28                                       ;91.2
                                ; LOE
.B6.60:                         ; Preds .B6.101
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+24] ;92.2
        test      edx, edx                                      ;92.2
        jle       .B6.63        ; Prob 50%                      ;92.2
                                ; LOE edx
.B6.61:                         ; Preds .B6.60
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED] ;92.2
        cmp       edx, 24                                       ;92.2
        jle       .B6.67        ; Prob 0%                       ;92.2
                                ; LOE edx ecx
.B6.62:                         ; Preds .B6.61
        shl       edx, 2                                        ;92.2
        push      edx                                           ;92.2
        push      0                                             ;92.2
        push      ecx                                           ;92.2
        call      __intel_fast_memset                           ;92.2
                                ; LOE
.B6.102:                        ; Preds .B6.62
        add       esp, 12                                       ;92.2
                                ; LOE
.B6.63:                         ; Preds .B6.81 .B6.102 .B6.60 .B6.79
        add       esp, 20                                       ;94.1
        pop       ebx                                           ;94.1
        pop       edi                                           ;94.1
        pop       esi                                           ;94.1
        mov       esp, ebp                                      ;94.1
        pop       ebp                                           ;94.1
        ret                                                     ;94.1
                                ; LOE
.B6.64:                         ; Preds .B6.4 .B6.8 .B6.6       ; Infreq
        xor       edi, edi                                      ;86.2
        jmp       .B6.16        ; Prob 100%                     ;86.2
                                ; LOE ecx ebx esi edi
.B6.65:                         ; Preds .B6.23 .B6.27 .B6.25    ; Infreq
        xor       edi, edi                                      ;88.2
        jmp       .B6.35        ; Prob 100%                     ;88.2
                                ; LOE ecx ebx esi edi
.B6.66:                         ; Preds .B6.42 .B6.46 .B6.44    ; Infreq
        xor       edi, edi                                      ;90.2
        jmp       .B6.54        ; Prob 100%                     ;90.2
                                ; LOE ecx ebx esi edi
.B6.67:                         ; Preds .B6.61                  ; Infreq
        cmp       edx, 4                                        ;92.2
        jl        .B6.83        ; Prob 10%                      ;92.2
                                ; LOE edx ecx
.B6.68:                         ; Preds .B6.67                  ; Infreq
        mov       eax, ecx                                      ;92.2
        and       eax, 15                                       ;92.2
        je        .B6.71        ; Prob 50%                      ;92.2
                                ; LOE eax edx ecx
.B6.69:                         ; Preds .B6.68                  ; Infreq
        test      al, 3                                         ;92.2
        jne       .B6.83        ; Prob 10%                      ;92.2
                                ; LOE eax edx ecx
.B6.70:                         ; Preds .B6.69                  ; Infreq
        neg       eax                                           ;92.2
        add       eax, 16                                       ;92.2
        shr       eax, 2                                        ;92.2
                                ; LOE eax edx ecx
.B6.71:                         ; Preds .B6.70 .B6.68           ; Infreq
        lea       ebx, DWORD PTR [4+eax]                        ;92.2
        cmp       edx, ebx                                      ;92.2
        jl        .B6.83        ; Prob 10%                      ;92.2
                                ; LOE eax edx ecx
.B6.72:                         ; Preds .B6.71                  ; Infreq
        mov       esi, edx                                      ;92.2
        sub       esi, eax                                      ;92.2
        and       esi, 3                                        ;92.2
        neg       esi                                           ;92.2
        add       esi, edx                                      ;92.2
        test      eax, eax                                      ;92.2
        jbe       .B6.76        ; Prob 10%                      ;92.2
                                ; LOE eax edx ecx esi
.B6.73:                         ; Preds .B6.72                  ; Infreq
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B6.74:                         ; Preds .B6.74 .B6.73           ; Infreq
        mov       DWORD PTR [ecx+ebx*4], 0                      ;92.2
        inc       ebx                                           ;92.2
        cmp       ebx, eax                                      ;92.2
        jb        .B6.74        ; Prob 82%                      ;92.2
                                ; LOE eax edx ecx ebx esi
.B6.76:                         ; Preds .B6.74 .B6.72           ; Infreq
        pxor      xmm0, xmm0                                    ;92.2
                                ; LOE eax edx ecx esi xmm0
.B6.77:                         ; Preds .B6.77 .B6.76           ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;92.2
        add       eax, 4                                        ;92.2
        cmp       eax, esi                                      ;92.2
        jb        .B6.77        ; Prob 82%                      ;92.2
                                ; LOE eax edx ecx esi xmm0
.B6.79:                         ; Preds .B6.77 .B6.83           ; Infreq
        cmp       esi, edx                                      ;92.2
        jae       .B6.63        ; Prob 10%                      ;92.2
                                ; LOE edx ecx esi
.B6.81:                         ; Preds .B6.79 .B6.81           ; Infreq
        mov       DWORD PTR [ecx+esi*4], 0                      ;92.2
        inc       esi                                           ;92.2
        cmp       esi, edx                                      ;92.2
        jb        .B6.81        ; Prob 82%                      ;92.2
        jmp       .B6.63        ; Prob 100%                     ;92.2
                                ; LOE edx ecx esi
.B6.83:                         ; Preds .B6.67 .B6.71 .B6.69    ; Infreq
        xor       esi, esi                                      ;92.2
        jmp       .B6.79        ; Prob 100%                     ;92.2
        ALIGN     16
                                ; LOE edx ecx esi
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_LABELS ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_LABELS
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_LABELS
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_LABELS
_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_LABELS	PROC NEAR 
.B7.1:                          ; Preds .B7.0
        push      ebp                                           ;97.12
        mov       ebp, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+12] ;99.2
        mov       edx, ebp                                      ;99.2
        shr       edx, 1                                        ;99.2
        mov       eax, ebp                                      ;99.2
        and       edx, 1                                        ;99.2
        and       eax, 1                                        ;99.2
        shl       edx, 2                                        ;99.2
        add       eax, eax                                      ;99.2
        or        edx, eax                                      ;99.2
        or        edx, 262144                                   ;99.2
        push      edx                                           ;99.2
        push      DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;99.2
        call      _for_dealloc_allocatable                      ;99.2
                                ; LOE ebx ebp esi edi
.B7.2:                          ; Preds .B7.1
        and       ebp, -2                                       ;99.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+12], ebp ;99.2
        mov       ebp, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+12] ;100.2
        mov       edx, ebp                                      ;100.2
        shr       edx, 1                                        ;100.2
        mov       eax, ebp                                      ;100.2
        and       edx, 1                                        ;100.2
        and       eax, 1                                        ;100.2
        shl       edx, 2                                        ;100.2
        add       eax, eax                                      ;100.2
        or        edx, eax                                      ;100.2
        or        edx, 262144                                   ;100.2
        push      edx                                           ;100.2
        push      DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL] ;100.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL], 0 ;99.2
        call      _for_dealloc_allocatable                      ;100.2
                                ; LOE ebx ebp esi edi
.B7.3:                          ; Preds .B7.2
        and       ebp, -2                                       ;100.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+12], ebp ;100.2
        mov       ebp, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+12] ;101.2
        mov       edx, ebp                                      ;101.2
        shr       edx, 1                                        ;101.2
        mov       eax, ebp                                      ;101.2
        and       edx, 1                                        ;101.2
        and       eax, 1                                        ;101.2
        shl       edx, 2                                        ;101.2
        add       eax, eax                                      ;101.2
        or        edx, eax                                      ;101.2
        or        edx, 262144                                   ;101.2
        push      edx                                           ;101.2
        push      DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD]  ;101.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL], 0 ;100.2
        call      _for_dealloc_allocatable                      ;101.2
                                ; LOE ebx ebp esi edi
.B7.4:                          ; Preds .B7.3
        and       ebp, -2                                       ;101.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+12], ebp ;101.2
        mov       ebp, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+12] ;102.2
        mov       edx, ebp                                      ;102.2
        shr       edx, 1                                        ;102.2
        mov       eax, ebp                                      ;102.2
        and       edx, 1                                        ;102.2
        and       eax, 1                                        ;102.2
        shl       edx, 2                                        ;102.2
        add       eax, eax                                      ;102.2
        or        edx, eax                                      ;102.2
        or        edx, 262144                                   ;102.2
        push      edx                                           ;102.2
        push      DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED] ;102.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD], 0 ;101.2
        call      _for_dealloc_allocatable                      ;102.2
                                ; LOE ebx ebp esi edi
.B7.5:                          ; Preds .B7.4
        and       ebp, -2                                       ;102.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED], 0 ;102.2
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+12], ebp ;102.2
        add       esp, 32                                       ;104.1
        pop       ebp                                           ;104.1
        ret                                                     ;104.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_LABELS ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_LABELS
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH
_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
; parameter 6: 28 + ebp
.B8.1:                          ; Preds .B8.0
        push      ebp                                           ;151.12
        mov       ebp, esp                                      ;151.12
        and       esp, -16                                      ;151.12
        push      esi                                           ;151.12
        push      edi                                           ;151.12
        push      ebx                                           ;151.12
        sub       esp, 100                                      ;151.12
        mov       eax, DWORD PTR [24+ebp]                       ;151.12
        cmp       DWORD PTR [eax], 0                            ;154.17
        jne       .B8.35        ; Prob 67%                      ;154.17
                                ; LOE
.B8.2:                          ; Preds .B8.1
        mov       ebx, DWORD PTR [20+ebp]                       ;151.12
        xor       eax, eax                                      ;156.18
        movss     xmm3, DWORD PTR [_2il0floatpacket.45]         ;156.18
        movss     xmm1, DWORD PTR [_2il0floatpacket.46]         ;156.18
        movss     xmm2, DWORD PTR [ebx]                         ;156.18
        movss     xmm4, DWORD PTR [_2il0floatpacket.47]         ;156.18
        mov       edx, DWORD PTR [8+ebp]                        ;151.12
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+24] ;156.18
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDTHIS.0.10], eax ;156.18
        mov       ecx, DWORD PTR [edx]                          ;156.18
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDITEM.0.10], eax ;156.18
        mov       DWORD PTR [68+esp], ecx                       ;156.18
        subss     xmm2, DWORD PTR [_2il0floatpacket.43]         ;156.18
        andps     xmm3, xmm2                                    ;156.18
        movaps    xmm0, xmm3                                    ;156.18
        pxor      xmm0, xmm2                                    ;156.18
        cmpltss   xmm0, xmm1                                    ;156.18
        andps     xmm1, xmm0                                    ;156.18
        orps      xmm1, xmm3                                    ;156.18
        movaps    xmm6, xmm1                                    ;156.18
        addss     xmm6, xmm2                                    ;156.18
        subss     xmm6, xmm1                                    ;156.18
        movaps    xmm5, xmm6                                    ;156.18
        subss     xmm5, xmm2                                    ;156.18
        cmpnless  xmm5, xmm3                                    ;156.18
        andps     xmm5, xmm4                                    ;156.18
        subss     xmm6, xmm5                                    ;156.18
        cvtss2si  ebx, xmm6                                     ;156.18
        inc       ebx                                           ;156.18
        test      esi, esi                                      ;156.18
        jle       .B8.5         ; Prob 50%                      ;156.18
                                ; LOE ebx esi
.B8.3:                          ; Preds .B8.2
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED] ;156.18
        cmp       esi, 24                                       ;156.18
        jle       .B8.36        ; Prob 0%                       ;156.18
                                ; LOE ecx ebx esi
.B8.4:                          ; Preds .B8.3
        shl       esi, 2                                        ;156.18
        push      esi                                           ;156.18
        push      0                                             ;156.18
        push      ecx                                           ;156.18
        call      __intel_fast_memset                           ;156.18
                                ; LOE ebx
.B8.64:                         ; Preds .B8.4
        add       esp, 12                                       ;156.18
                                ; LOE ebx
.B8.5:                          ; Preds .B8.50 .B8.64 .B8.2 .B8.48
        movss     xmm1, DWORD PTR [_2il0floatpacket.47]         ;156.18
        xor       esi, esi                                      ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.44]         ;156.18
        mov       eax, DWORD PTR [68+esp]                       ;156.18
        mov       DWORD PTR [48+esp], ebx                       ;156.18
                                ; LOE eax esi
.B8.6:                          ; Preds .B8.34 .B8.72 .B8.5
        sub       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+32] ;156.18
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;156.18
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+32] ;156.18
        shl       ecx, 3                                        ;156.18
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET] ;156.18
        lea       eax, DWORD PTR [ebx+eax*4]                    ;156.18
        push      eax                                           ;156.18
        sub       edx, ecx                                      ;156.18
        lea       edi, DWORD PTR [72+esp]                       ;156.18
        push      edi                                           ;156.18
        add       edx, 8                                        ;156.18
        push      edx                                           ;156.18
        call      _LINK_LIST_mp_ENQUEUE_LINKLIST                ;156.18
                                ; LOE esi
.B8.65:                         ; Preds .B8.6
        add       esp, 12                                       ;156.18
                                ; LOE esi
.B8.7:                          ; Preds .B8.65
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;156.18
        neg       edi                                           ;156.18
        mov       eax, DWORD PTR [68+esp]                       ;156.18
        add       edi, eax                                      ;156.18
        imul      ebx, edi, 152                                 ;156.18
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;156.18
        mov       ecx, DWORD PTR [12+ebp]                       ;156.18
        mov       edx, DWORD PTR [24+edi+ebx]                   ;156.18
        cmp       edx, DWORD PTR [ecx]                          ;156.18
        je        .B8.60        ; Prob 20%                      ;156.18
                                ; LOE eax ebx esi edi
.B8.8:                          ; Preds .B8.7
        movsx     ecx, BYTE PTR [32+edi+ebx]                    ;156.18
        test      ecx, ecx                                      ;156.18
        jle       .B8.26        ; Prob 2%                       ;156.18
                                ; LOE eax ecx esi edi
.B8.9:                          ; Preds .B8.8
        mov       edx, 1                                        ;
        mov       DWORD PTR [80+esp], edx                       ;
        mov       DWORD PTR [76+esp], ecx                       ;
        mov       DWORD PTR [40+esp], esi                       ;
                                ; LOE eax edi
.B8.10:                         ; Preds .B8.73 .B8.9
        imul      edx, eax, 152                                 ;156.18
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;156.18
        add       edx, edi                                      ;156.18
        sub       edx, ecx                                      ;156.18
        mov       esi, DWORD PTR [80+esp]                       ;156.18
        sub       esi, DWORD PTR [68+edx]                       ;156.18
        imul      esi, DWORD PTR [64+edx]                       ;156.18
        mov       ebx, DWORD PTR [36+edx]                       ;156.18
        mov       edx, DWORD PTR [ebx+esi]                      ;156.18
        mov       ebx, DWORD PTR [28+ebp]                       ;156.18
        mov       DWORD PTR [72+esp], edx                       ;156.18
        cmp       DWORD PTR [ebx], 0                            ;156.18
        jne       .B8.12        ; Prob 50%                      ;156.18
                                ; LOE eax edx ecx edi
.B8.11:                         ; Preds .B8.10
        mov       ebx, 1                                        ;156.18
        jmp       .B8.13        ; Prob 100%                     ;156.18
                                ; LOE edx ecx ebx edi
.B8.12:                         ; Preds .B8.10
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+32] ;156.18
        neg       esi                                           ;156.18
        add       esi, eax                                      ;156.18
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;156.18
        cvtsi2ss  xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;156.18
        movss     xmm3, DWORD PTR [ebx+esi*4]                   ;156.18
        divss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;156.18
        divss     xmm3, xmm0                                    ;156.18
        movss     xmm4, DWORD PTR [_2il0floatpacket.45]         ;156.18
        andps     xmm4, xmm3                                    ;156.18
        movaps    xmm1, xmm4                                    ;156.18
        movss     xmm2, DWORD PTR [_2il0floatpacket.46]         ;156.18
        pxor      xmm1, xmm3                                    ;156.18
        movss     xmm5, DWORD PTR [_2il0floatpacket.47]         ;156.18
        cmpltss   xmm1, xmm2                                    ;156.18
        andps     xmm2, xmm1                                    ;156.18
        orps      xmm2, xmm4                                    ;156.18
        movaps    xmm7, xmm2                                    ;156.18
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;156.18
        addss     xmm7, xmm3                                    ;156.18
        dec       ebx                                           ;156.18
        subss     xmm7, xmm2                                    ;156.18
        movaps    xmm6, xmm7                                    ;156.18
        subss     xmm6, xmm3                                    ;156.18
        cmpnless  xmm6, xmm4                                    ;156.18
        andps     xmm6, xmm5                                    ;156.18
        subss     xmm7, xmm6                                    ;156.18
        cvtss2si  eax, xmm7                                     ;156.18
        inc       eax                                           ;156.18
        cmp       eax, ebx                                      ;156.18
        cmovl     ebx, eax                                      ;156.18
                                ; LOE edx ecx ebx edi
.B8.13:                         ; Preds .B8.11 .B8.12
        imul      eax, edx, 152                                 ;156.18
        sub       edi, ecx                                      ;156.18
        mov       edx, DWORD PTR [12+eax+edi]                   ;156.18
        lea       ecx, DWORD PTR [72+esp]                       ;156.18
        mov       DWORD PTR [88+esp], edx                       ;156.18
        push      ecx                                           ;156.18
        lea       esi, DWORD PTR [72+esp]                       ;156.18
        push      esi                                           ;156.18
        call      _DYNUST_NETWORK_MODULE_mp_GETBSTMOVE          ;156.18
                                ; LOE eax ebx
.B8.66:                         ; Preds .B8.13
        add       esp, 8                                        ;156.18
                                ; LOE eax ebx
.B8.14:                         ; Preds .B8.66
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES_ORG] ;156.18
        mov       DWORD PTR [92+esp], edi                       ;156.18
        mov       edi, DWORD PTR [88+esp]                       ;156.18
        sub       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+44] ;156.18
        sub       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+56] ;156.18
        imul      edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+40] ;156.18
        imul      eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+52] ;156.18
        movss     xmm0, DWORD PTR [_2il0floatpacket.44]         ;156.18
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;156.18
        neg       esi                                           ;
        add       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL] ;156.18
        mov       ecx, DWORD PTR [72+esp]                       ;156.18
        add       esi, ecx                                      ;
        add       eax, edi                                      ;156.18
        mov       DWORD PTR [84+esp], ecx                       ;156.18
        imul      ecx, esi, 152                                 ;
        sub       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+32] ;156.18
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;156.18
        movss     xmm4, DWORD PTR [eax+ebx*4]                   ;156.18
        mov       eax, DWORD PTR [24+edx+ecx]                   ;156.18
        comiss    xmm0, xmm4                                    ;156.18
        jbe       .B8.74        ; Prob 50%                      ;156.18
                                ; LOE eax edx ecx xmm4
.B8.15:                         ; Preds .B8.14
        cmp       eax, DWORD PTR [92+esp]                       ;156.18
        jl        .B8.17        ; Prob 50%                      ;156.18
        jmp       .B8.16        ; Prob 100%                     ;156.18
                                ; LOE edx ecx xmm4
.B8.74:                         ; Preds .B8.14
        cmp       eax, DWORD PTR [92+esp]                       ;156.18
                                ; LOE edx ecx xmm4
.B8.16:                         ; Preds .B8.74 .B8.15
        jle       .B8.24        ; Prob 50%                      ;156.18
                                ; LOE edx ecx xmm4
.B8.17:                         ; Preds .B8.15 .B8.16
        mov       eax, DWORD PTR [84+esp]                       ;156.18
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+32] ;156.18
        mov       esi, DWORD PTR [68+esp]                       ;156.18
        mov       DWORD PTR [64+esp], esi                       ;156.18
        lea       ebx, DWORD PTR [eax*4]                        ;156.18
        mov       DWORD PTR [52+esp], ebx                       ;156.18
        mov       ebx, DWORD PTR [28+edx+ecx]                   ;156.18
        lea       ecx, DWORD PTR [edi*4]                        ;156.18
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL] ;156.18
        mov       DWORD PTR [56+esp], ecx                       ;156.18
        sub       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+32] ;156.18
        lea       edx, DWORD PTR [edx+eax*4]                    ;156.18
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;156.18
        sub       edx, ecx                                      ;156.18
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+32] ;156.18
        shl       ecx, 2                                        ;156.18
        lea       esi, DWORD PTR [eax+esi*4]                    ;156.18
        sub       esi, ecx                                      ;156.18
        mov       DWORD PTR [60+esp], esi                       ;156.18
        movss     xmm0, DWORD PTR [edx]                         ;156.18
        movss     xmm2, DWORD PTR [esi]                         ;156.18
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD] ;156.18
        addss     xmm2, xmm4                                    ;156.18
        movss     xmm3, DWORD PTR [esi+ebx*4]                   ;156.18
        movaps    xmm1, xmm3                                    ;156.18
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL] ;156.18
        addss     xmm1, xmm2                                    ;156.18
        comiss    xmm0, xmm1                                    ;156.18
        jbe       .B8.24        ; Prob 50%                      ;156.18
                                ; LOE eax edx ecx ebx edi xmm2 xmm3 xmm4
.B8.18:                         ; Preds .B8.17
        mov       esi, DWORD PTR [84+esp]                       ;156.18
        lea       eax, DWORD PTR [eax+esi*4]                    ;156.18
        sub       eax, ecx                                      ;156.18
        mov       ecx, DWORD PTR [60+esp]                       ;156.18
        movss     DWORD PTR [eax], xmm2                         ;156.18
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+32] ;156.18
        shl       eax, 2                                        ;156.18
        addss     xmm4, DWORD PTR [ecx]                         ;156.18
        neg       eax                                           ;156.18
        addss     xmm4, xmm3                                    ;156.18
        add       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED] ;156.18
        mov       ecx, DWORD PTR [52+esp]                       ;156.18
        movss     DWORD PTR [edx], xmm4                         ;156.18
        mov       edx, DWORD PTR [64+esp]                       ;156.18
        mov       DWORD PTR [eax+ecx], edx                      ;156.18
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE] ;156.18
        mov       esi, edx                                      ;156.18
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+32] ;156.18
        shl       eax, 2                                        ;156.18
        sub       esi, eax                                      ;156.18
        cmp       DWORD PTR [esi+ecx], 0                        ;156.18
        jle       .B8.22        ; Prob 41%                      ;156.18
                                ; LOE eax edx ebx edi
.B8.19:                         ; Preds .B8.18
        mov       ecx, DWORD PTR [84+esp]                       ;156.18
        push      OFFSET FLAT: _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDITEM.0.10 ;156.18
        lea       esi, DWORD PTR [76+esp]                       ;156.18
        push      esi                                           ;156.18
        lea       edx, DWORD PTR [edx+ecx*4]                    ;156.18
        sub       edx, eax                                      ;156.18
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET] ;156.18
        mov       ebx, DWORD PTR [edx]                          ;156.18
        sub       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+32] ;156.18
        lea       edi, DWORD PTR [eax+ebx*8]                    ;156.18
        push      edi                                           ;156.18
        call      _LINK_LIST_mp_DISQUEUE_TARGET                 ;156.18
                                ; LOE
.B8.67:                         ; Preds .B8.19
        add       esp, 12                                       ;156.18
                                ; LOE
.B8.20:                         ; Preds .B8.67
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+32] ;156.18
        shl       edx, 2                                        ;156.18
        mov       esi, DWORD PTR [72+esp]                       ;156.18
        neg       edx                                           ;156.18
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE] ;156.18
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY] ;156.18
        mov       DWORD PTR [84+esp], esi                       ;156.18
        lea       ecx, DWORD PTR [esi*4]                        ;156.18
        mov       DWORD PTR [52+esp], ecx                       ;156.18
        lea       ecx, DWORD PTR [ebx+esi*4]                    ;156.18
        mov       edi, DWORD PTR [edx+ecx]                      ;156.18
        sub       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+32] ;156.18
        mov       DWORD PTR [edx+ecx], 0                        ;156.18
        dec       DWORD PTR [eax+edi*4]                         ;156.18
        test      BYTE PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDITEM.0.10], 1 ;156.18
        je        .B8.55        ; Prob 7%                       ;156.18
                                ; LOE
.B8.21:                         ; Preds .B8.70 .B8.20
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+32] ;156.18
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL] ;156.18
        lea       eax, DWORD PTR [edi*4]                        ;
        mov       DWORD PTR [56+esp], eax                       ;
                                ; LOE ebx edi
.B8.22:                         ; Preds .B8.21 .B8.18
        mov       esi, DWORD PTR [84+esp]                       ;156.18
        sub       esi, edi                                      ;156.18
        movss     xmm6, DWORD PTR [_2il0floatpacket.45]         ;156.18
        movss     xmm2, DWORD PTR [_2il0floatpacket.46]         ;156.18
        movss     xmm7, DWORD PTR [_2il0floatpacket.47]         ;156.18
        movss     xmm5, DWORD PTR [ebx+esi*4]                   ;156.18
        andps     xmm6, xmm5                                    ;156.18
        movaps    xmm4, xmm5                                    ;156.18
        pxor      xmm4, xmm6                                    ;156.18
        movaps    xmm3, xmm5                                    ;156.18
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+32] ;156.18
        cmpltss   xmm4, xmm2                                    ;156.18
        andps     xmm4, xmm2                                    ;156.18
        neg       ecx                                           ;156.18
        orps      xmm4, xmm6                                    ;156.18
        mov       edi, DWORD PTR [52+esp]                       ;156.18
        addss     xmm3, xmm4                                    ;156.18
        add       edi, ebx                                      ;156.18
        subss     xmm3, xmm4                                    ;156.18
        movaps    xmm1, xmm3                                    ;156.18
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;156.18
        subss     xmm1, xmm5                                    ;156.18
        movss     xmm5, DWORD PTR [_2il0floatpacket.48]         ;156.18
        cmpnless  xmm1, xmm6                                    ;156.18
        andps     xmm1, xmm7                                    ;156.18
        sub       edi, DWORD PTR [56+esp]                       ;156.18
        subss     xmm3, xmm1                                    ;156.18
        movss     xmm1, DWORD PTR [_2il0floatpacket.45]         ;156.18
        andps     xmm1, xmm4                                    ;156.18
        pxor      xmm4, xmm1                                    ;156.18
        movaps    xmm0, xmm4                                    ;156.18
        cvtss2si  eax, xmm3                                     ;156.18
        cmpltss   xmm0, xmm2                                    ;156.18
        andps     xmm2, xmm0                                    ;156.18
        movaps    xmm0, xmm4                                    ;156.18
        inc       eax                                           ;156.18
        addss     xmm0, xmm2                                    ;156.18
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET] ;156.18
        subss     xmm0, xmm2                                    ;156.18
        movaps    xmm2, xmm0                                    ;156.18
        push      edi                                           ;156.18
        subss     xmm2, xmm4                                    ;156.18
        movaps    xmm4, xmm5                                    ;156.18
        addss     xmm4, xmm5                                    ;156.18
        movaps    xmm6, xmm2                                    ;156.18
        lea       ebx, DWORD PTR [76+esp]                       ;156.18
        push      ebx                                           ;156.18
        cmpnless  xmm6, xmm5                                    ;156.18
        cmpless   xmm2, DWORD PTR [_2il0floatpacket.49]         ;156.18
        andps     xmm6, xmm4                                    ;156.18
        andps     xmm2, xmm4                                    ;156.18
        subss     xmm0, xmm6                                    ;156.18
        addss     xmm0, xmm2                                    ;156.18
        orps      xmm0, xmm1                                    ;156.18
        cvtss2si  esi, xmm0                                     ;156.18
        cmp       eax, esi                                      ;156.18
        cmovl     esi, eax                                      ;156.18
        add       ecx, esi                                      ;156.18
        lea       eax, DWORD PTR [edx+ecx*8]                    ;156.18
        push      eax                                           ;156.18
        call      _LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST        ;156.18
                                ; LOE esi
.B8.68:                         ; Preds .B8.22
        add       esp, 12                                       ;156.18
                                ; LOE esi
.B8.23:                         ; Preds .B8.68
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+32] ;156.18
        neg       edx                                           ;156.18
        mov       ebx, DWORD PTR [72+esp]                       ;156.18
        add       edx, esi                                      ;156.18
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY] ;156.18
        sub       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+32] ;156.18
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE] ;156.18
        inc       DWORD PTR [eax+edx*4]                         ;156.18
        mov       DWORD PTR [ecx+ebx*4], esi                    ;156.18
                                ; LOE
.B8.24:                         ; Preds .B8.17 .B8.16 .B8.23
        mov       eax, DWORD PTR [80+esp]                       ;156.18
        inc       eax                                           ;156.18
        mov       DWORD PTR [80+esp], eax                       ;156.18
        cmp       eax, DWORD PTR [76+esp]                       ;156.18
        jg        .B8.25        ; Prob 18%                      ;156.18
                                ; LOE
.B8.73:                         ; Preds .B8.24
        mov       eax, DWORD PTR [68+esp]                       ;156.18
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;156.18
        jmp       .B8.10        ; Prob 100%                     ;156.18
                                ; LOE eax edi
.B8.25:                         ; Preds .B8.24
        mov       esi, DWORD PTR [40+esp]                       ;
                                ; LOE esi
.B8.26:                         ; Preds .B8.25 .B8.8
        inc       esi                                           ;156.18
        test      BYTE PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDTHIS.0.10], 1 ;156.18
        jne       .B8.35        ; Prob 18%                      ;156.18
                                ; LOE esi
.B8.27:                         ; Preds .B8.26
        test      esi, esi                                      ;156.18
        jle       .B8.72        ; Prob 16%                      ;156.18
                                ; LOE esi
.B8.28:                         ; Preds .B8.27
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY] ;156.18
        mov       edx, DWORD PTR [48+esp]                       ;156.18
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+32] ;156.18
        shl       edi, 2                                        ;156.18
        lea       ecx, DWORD PTR [eax+edx*4]                    ;156.18
        sub       ecx, edi                                      ;156.18
        lea       ebx, DWORD PTR [edx*4]                        ;156.18
        cmp       DWORD PTR [ecx], 1                            ;156.18
        jne       .B8.33        ; Prob 37%                      ;156.18
                                ; LOE eax edx ebx esi edi dl dh
.B8.29:                         ; Preds .B8.28
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;156.18
        sub       eax, edi                                      ;
        subss     xmm0, DWORD PTR [_2il0floatpacket.47]         ;156.18
                                ; LOE eax edx ebx esi xmm0
.B8.30:                         ; Preds .B8.31 .B8.29
        cvtsi2ss  xmm1, edx                                     ;156.18
        comiss    xmm0, xmm1                                    ;156.18
        jb        .B8.32        ; Prob 20%                      ;156.18
                                ; LOE eax edx ebx esi xmm0
.B8.31:                         ; Preds .B8.30
        inc       edx                                           ;156.18
        lea       ebx, DWORD PTR [edx*4]                        ;156.18
        cmp       DWORD PTR [eax+ebx], 1                        ;156.18
        je        .B8.30        ; Prob 82%                      ;156.18
                                ; LOE eax edx ebx esi xmm0
.B8.32:                         ; Preds .B8.30 .B8.31
        mov       DWORD PTR [48+esp], edx                       ;
                                ; LOE edx ebx esi dl dh
.B8.33:                         ; Preds .B8.32 .B8.28
        lea       ecx, DWORD PTR [44+esp]                       ;156.18
        sub       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+32] ;156.18
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET] ;156.18
        push      ecx                                           ;156.18
        lea       edi, DWORD PTR [72+esp]                       ;156.18
        push      edi                                           ;156.18
        lea       eax, DWORD PTR [eax+edx*8]                    ;156.18
        push      eax                                           ;156.18
        call      _LINK_LIST_mp_DISQUEUE_LINKLIST               ;156.18
                                ; LOE ebx esi
.B8.69:                         ; Preds .B8.33
        add       esp, 12                                       ;156.18
                                ; LOE ebx esi
.B8.34:                         ; Preds .B8.69
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+32] ;156.18
        shl       edi, 2                                        ;156.18
        mov       eax, DWORD PTR [68+esp]                       ;156.18
        mov       ecx, eax                                      ;156.18
        neg       edi                                           ;156.18
        sub       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+32] ;156.18
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE] ;156.18
        add       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY] ;156.18
        mov       DWORD PTR [edx+ecx*4], 0                      ;156.18
        dec       DWORD PTR [edi+ebx]                           ;156.18
        jmp       .B8.6         ; Prob 100%                     ;156.18
                                ; LOE eax esi
.B8.35:                         ; Preds .B8.26 .B8.1
        add       esp, 100                                      ;162.1
        pop       ebx                                           ;162.1
        pop       edi                                           ;162.1
        pop       esi                                           ;162.1
        mov       esp, ebp                                      ;162.1
        pop       ebp                                           ;162.1
        ret                                                     ;162.1
                                ; LOE
.B8.36:                         ; Preds .B8.3                   ; Infreq
        cmp       esi, 4                                        ;156.18
        jl        .B8.52        ; Prob 10%                      ;156.18
                                ; LOE ecx ebx esi
.B8.37:                         ; Preds .B8.36                  ; Infreq
        mov       edx, ecx                                      ;156.18
        and       edx, 15                                       ;156.18
        je        .B8.40        ; Prob 50%                      ;156.18
                                ; LOE edx ecx ebx esi
.B8.38:                         ; Preds .B8.37                  ; Infreq
        test      dl, 3                                         ;156.18
        jne       .B8.52        ; Prob 10%                      ;156.18
                                ; LOE edx ecx ebx esi
.B8.39:                         ; Preds .B8.38                  ; Infreq
        neg       edx                                           ;156.18
        add       edx, 16                                       ;156.18
        shr       edx, 2                                        ;156.18
                                ; LOE edx ecx ebx esi
.B8.40:                         ; Preds .B8.39 .B8.37           ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;156.18
        cmp       esi, eax                                      ;156.18
        jl        .B8.52        ; Prob 10%                      ;156.18
                                ; LOE edx ecx ebx esi
.B8.41:                         ; Preds .B8.40                  ; Infreq
        mov       eax, esi                                      ;156.18
        sub       eax, edx                                      ;156.18
        and       eax, 3                                        ;156.18
        neg       eax                                           ;156.18
        add       eax, esi                                      ;156.18
        test      edx, edx                                      ;156.18
        jbe       .B8.45        ; Prob 11%                      ;156.18
                                ; LOE eax edx ecx ebx esi
.B8.42:                         ; Preds .B8.41                  ; Infreq
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B8.43:                         ; Preds .B8.43 .B8.42           ; Infreq
        mov       DWORD PTR [ecx+edi*4], 0                      ;156.18
        inc       edi                                           ;156.18
        cmp       edi, edx                                      ;156.18
        jb        .B8.43        ; Prob 82%                      ;156.18
                                ; LOE eax edx ecx ebx esi edi
.B8.45:                         ; Preds .B8.43 .B8.41           ; Infreq
        pxor      xmm0, xmm0                                    ;156.18
                                ; LOE eax edx ecx ebx esi xmm0
.B8.46:                         ; Preds .B8.46 .B8.45           ; Infreq
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;156.18
        add       edx, 4                                        ;156.18
        cmp       edx, eax                                      ;156.18
        jb        .B8.46        ; Prob 82%                      ;156.18
                                ; LOE eax edx ecx ebx esi xmm0
.B8.48:                         ; Preds .B8.46 .B8.52           ; Infreq
        cmp       eax, esi                                      ;156.18
        jae       .B8.5         ; Prob 11%                      ;156.18
                                ; LOE eax ecx ebx esi
.B8.50:                         ; Preds .B8.48 .B8.50           ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;156.18
        inc       eax                                           ;156.18
        cmp       eax, esi                                      ;156.18
        jb        .B8.50        ; Prob 82%                      ;156.18
        jmp       .B8.5         ; Prob 100%                     ;156.18
                                ; LOE eax ecx ebx esi
.B8.52:                         ; Preds .B8.36 .B8.40 .B8.38    ; Infreq
        xor       eax, eax                                      ;156.18
        jmp       .B8.48        ; Prob 100%                     ;156.18
                                ; LOE eax ecx ebx esi
.B8.55:                         ; Preds .B8.20                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;156.18
        lea       edx, DWORD PTR [esp]                          ;156.18
        mov       DWORD PTR [32+esp], 49                        ;156.18
        lea       eax, DWORD PTR [32+esp]                       ;156.18
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;156.18
        push      32                                            ;156.18
        push      eax                                           ;156.18
        push      OFFSET FLAT: __STRLITPACK_3.0.10              ;156.18
        push      -2088435968                                   ;156.18
        push      911                                           ;156.18
        push      edx                                           ;156.18
        call      _for_write_seq_lis                            ;156.18
                                ; LOE
.B8.56:                         ; Preds .B8.55                  ; Infreq
        push      32                                            ;156.18
        xor       eax, eax                                      ;156.18
        push      eax                                           ;156.18
        push      eax                                           ;156.18
        push      -2088435968                                   ;156.18
        push      eax                                           ;156.18
        push      OFFSET FLAT: __STRLITPACK_4                   ;156.18
        call      _for_stop_core                                ;156.18
                                ; LOE
.B8.70:                         ; Preds .B8.56                  ; Infreq
        add       esp, 48                                       ;156.18
        jmp       .B8.21        ; Prob 100%                     ;156.18
                                ; LOE
.B8.60:                         ; Preds .B8.7                   ; Infreq
        mov       ebx, DWORD PTR [16+ebp]                       ;151.12
        lea       edx, DWORD PTR [68+esp]                       ;
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+32] ;156.18
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+32] ;156.18
        mov       DWORD PTR [ebx], eax                          ;156.18
        sub       eax, edi                                      ;156.18
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;156.18
        shl       esi, 3                                        ;156.18
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET] ;156.18
        sub       ecx, esi                                      ;156.18
        lea       eax, DWORD PTR [ebx+eax*4]                    ;156.18
        push      eax                                           ;156.18
        push      edx                                           ;156.18
        add       ecx, 8                                        ;156.18
        push      ecx                                           ;156.18
        call      _LINK_LIST_mp_ENQUEUE_LINKLIST                ;156.18
                                ; LOE
.B8.61:                         ; Preds .B8.60                  ; Infreq
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDTHIS.0.10], -1 ;156.18
        add       esp, 112                                      ;156.18
        pop       ebx                                           ;156.18
        pop       edi                                           ;156.18
        pop       esi                                           ;156.18
        mov       esp, ebp                                      ;156.18
        pop       ebp                                           ;156.18
        ret                                                     ;156.18
                                ; LOE
.B8.72:                         ; Preds .B8.27                  ; Infreq
        mov       eax, DWORD PTR [68+esp]                       ;156.18
        jmp       .B8.6         ; Prob 100%                     ;156.18
        ALIGN     16
                                ; LOE eax esi
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_ARRIVAL
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_ARRIVAL
_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_ARRIVAL	PROC NEAR 
; parameter 1: 4 + esp
; parameter 2: 8 + esp
; parameter 3: 12 + esp
; parameter 4: 16 + esp
; parameter 5: 20 + esp
; parameter 6: 24 + esp
.B9.1:                          ; Preds .B9.0
        ret                                                     ;257.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_ARRIVAL ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_ARRIVAL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE
_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
; parameter 6: 28 + ebp
.B10.1:                         ; Preds .B10.0
        push      ebp                                           ;165.12
        mov       ebp, esp                                      ;165.12
        and       esp, -16                                      ;165.12
        push      esi                                           ;165.12
        push      edi                                           ;165.12
        push      ebx                                           ;165.12
        sub       esp, 116                                      ;165.12
        xor       eax, eax                                      ;177.5
        mov       ebx, DWORD PTR [20+ebp]                       ;165.12
        movss     xmm3, DWORD PTR [_2il0floatpacket.74]         ;181.21
        movss     xmm1, DWORD PTR [_2il0floatpacket.75]         ;181.21
        movss     xmm2, DWORD PTR [ebx]                         ;181.21
        movss     xmm4, DWORD PTR [_2il0floatpacket.76]         ;181.21
        mov       edx, DWORD PTR [8+ebp]                        ;165.12
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+24] ;183.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDTHIS.0.10], eax ;177.5
        mov       ecx, DWORD PTR [edx]                          ;180.5
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDITEM.0.10], eax ;178.5
        mov       DWORD PTR [72+esp], ecx                       ;180.5
        subss     xmm2, DWORD PTR [_2il0floatpacket.72]         ;181.21
        andps     xmm3, xmm2                                    ;181.21
        movaps    xmm0, xmm3                                    ;181.21
        pxor      xmm0, xmm2                                    ;181.21
        cmpltss   xmm0, xmm1                                    ;181.21
        andps     xmm1, xmm0                                    ;181.21
        orps      xmm1, xmm3                                    ;181.21
        movaps    xmm6, xmm1                                    ;181.21
        addss     xmm6, xmm2                                    ;181.21
        subss     xmm6, xmm1                                    ;181.21
        movaps    xmm5, xmm6                                    ;181.21
        subss     xmm5, xmm2                                    ;181.21
        cmpnless  xmm5, xmm3                                    ;181.21
        andps     xmm5, xmm4                                    ;181.21
        subss     xmm6, xmm5                                    ;181.21
        cvtss2si  ebx, xmm6                                     ;181.21
        inc       ebx                                           ;181.5
        test      esi, esi                                      ;183.5
        jle       .B10.4        ; Prob 50%                      ;183.5
                                ; LOE ebx esi
.B10.2:                         ; Preds .B10.1
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED] ;183.5
        cmp       esi, 24                                       ;183.5
        jle       .B10.36       ; Prob 0%                       ;183.5
                                ; LOE ecx ebx esi
.B10.3:                         ; Preds .B10.2
        shl       esi, 2                                        ;183.5
        push      esi                                           ;183.5
        push      0                                             ;183.5
        push      ecx                                           ;183.5
        call      __intel_fast_memset                           ;183.5
                                ; LOE ebx
.B10.64:                        ; Preds .B10.3
        add       esp, 12                                       ;183.5
                                ; LOE ebx
.B10.4:                         ; Preds .B10.50 .B10.64 .B10.1 .B10.48
        mov       eax, DWORD PTR [12+ebp]                       ;165.12
        xor       esi, esi                                      ;
        mov       edx, DWORD PTR [28+ebp]                       ;165.12
        movss     xmm1, DWORD PTR [_2il0floatpacket.76]         ;187.99
        mov       ecx, DWORD PTR [eax]                          ;197.12
        mov       eax, DWORD PTR [edx]                          ;207.17
        movss     xmm0, DWORD PTR [_2il0floatpacket.73]         ;216.57
        mov       DWORD PTR [84+esp], eax                       ;216.57
        mov       DWORD PTR [52+esp], ecx                       ;216.57
        mov       DWORD PTR [48+esp], ebx                       ;216.57
        lea       ebx, DWORD PTR [72+esp]                       ;216.57
                                ; LOE ebx esi
.B10.5:                         ; Preds .B10.33 .B10.4
        test      esi, esi                                      ;186.20
        jle       .B10.61       ; Prob 16%                      ;186.20
                                ; LOE ebx esi
.B10.6:                         ; Preds .B10.5
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY] ;187.13
        mov       ecx, DWORD PTR [48+esp]                       ;187.13
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+32] ;187.13
        shl       edx, 2                                        ;187.13
        lea       edi, DWORD PTR [ecx*4]                        ;187.13
        lea       ecx, DWORD PTR [eax+ecx*4]                    ;187.13
        sub       ecx, edx                                      ;187.13
        cmp       DWORD PTR [ecx], 1                            ;187.48
        jne       .B10.11       ; Prob 37%                      ;187.48
                                ; LOE eax edx ebx esi edi
.B10.7:                         ; Preds .B10.6
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;187.57
        sub       eax, edx                                      ;
        mov       edx, DWORD PTR [48+esp]                       ;187.99
        subss     xmm0, DWORD PTR [_2il0floatpacket.76]         ;187.99
                                ; LOE eax edx ebx esi edi xmm0
.B10.8:                         ; Preds .B10.9 .B10.7
        cvtsi2ss  xmm1, edx                                     ;187.57
        comiss    xmm0, xmm1                                    ;187.71
        jb        .B10.10       ; Prob 20%                      ;187.71
                                ; LOE eax edx ebx esi edi xmm0
.B10.9:                         ; Preds .B10.8
        inc       edx                                           ;188.17
        lea       edi, DWORD PTR [edx*4]                        ;181.5
        cmp       DWORD PTR [eax+edi], 1                        ;187.48
        je        .B10.8        ; Prob 82%                      ;187.48
                                ; LOE eax edx ebx esi edi xmm0
.B10.10:                        ; Preds .B10.8 .B10.9
        mov       DWORD PTR [48+esp], edx                       ;
                                ; LOE ebx esi edi
.B10.11:                        ; Preds .B10.10 .B10.6
        mov       edx, DWORD PTR [48+esp]                       ;190.18
        lea       ecx, DWORD PTR [44+esp]                       ;190.18
        sub       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+32] ;190.18
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET] ;190.18
        push      ecx                                           ;190.18
        push      ebx                                           ;190.18
        lea       eax, DWORD PTR [eax+edx*8]                    ;190.18
        push      eax                                           ;190.18
        call      _LINK_LIST_mp_DISQUEUE_LINKLIST               ;190.18
                                ; LOE ebx esi edi
.B10.65:                        ; Preds .B10.11
        add       esp, 12                                       ;190.18
                                ; LOE ebx esi edi
.B10.12:                        ; Preds .B10.65
        mov       ecx, DWORD PTR [72+esp]                       ;191.13
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE] ;191.13
        add       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY] ;192.13
        lea       eax, DWORD PTR [ecx*4]                        ;191.13
        lea       ecx, DWORD PTR [edx+ecx*4]                    ;191.13
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+32] ;191.13
        shl       edx, 2                                        ;191.13
        neg       edx                                           ;191.13
        mov       DWORD PTR [edx+ecx], 0                        ;191.13
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+32] ;192.13
        shl       edx, 2                                        ;192.13
        neg       edx                                           ;192.13
        dec       DWORD PTR [edx+edi]                           ;192.13
                                ; LOE eax ebx esi
.B10.13:                        ; Preds .B10.12 .B10.61
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+32] ;194.14
        add       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;194.14
        shl       ecx, 2                                        ;194.14
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+32] ;194.14
        sub       eax, ecx                                      ;194.14
        shl       edx, 3                                        ;194.14
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET] ;194.14
        push      eax                                           ;194.14
        sub       edi, edx                                      ;194.14
        push      ebx                                           ;194.14
        add       edi, 8                                        ;194.14
        push      edi                                           ;194.14
        call      _LINK_LIST_mp_ENQUEUE_LINKLIST                ;194.14
                                ; LOE ebx esi
.B10.66:                        ; Preds .B10.13
        add       esp, 12                                       ;194.14
                                ; LOE ebx esi
.B10.14:                        ; Preds .B10.66
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;197.12
        neg       eax                                           ;197.51
        mov       edi, DWORD PTR [72+esp]                       ;197.12
        add       eax, edi                                      ;197.51
        imul      ecx, eax, 152                                 ;197.51
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;197.9
        mov       edx, DWORD PTR [52+esp]                       ;197.51
        cmp       edx, DWORD PTR [24+eax+ecx]                   ;197.51
        je        .B10.58       ; Prob 20%                      ;197.51
                                ; LOE eax ecx ebx esi edi
.B10.15:                        ; Preds .B10.14
        movsx     ecx, BYTE PTR [32+eax+ecx]                    ;205.22
        test      ecx, ecx                                      ;205.12
        jle       .B10.33       ; Prob 2%                       ;205.12
                                ; LOE eax ecx ebx esi edi
.B10.16:                        ; Preds .B10.15
        mov       edx, 1                                        ;
        mov       DWORD PTR [88+esp], edx                       ;
        mov       DWORD PTR [80+esp], ecx                       ;
        mov       DWORD PTR [40+esp], esi                       ;
                                ; LOE eax edi
.B10.17:                        ; Preds .B10.72 .B10.16
        imul      edx, edi, 152                                 ;206.17
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;206.17
        add       edx, eax                                      ;206.17
        sub       edx, ecx                                      ;206.17
        mov       esi, DWORD PTR [88+esp]                       ;206.17
        sub       esi, DWORD PTR [68+edx]                       ;206.17
        imul      esi, DWORD PTR [64+edx]                       ;206.17
        mov       ebx, DWORD PTR [36+edx]                       ;206.25
        cmp       DWORD PTR [84+esp], 0                         ;207.28
        mov       edx, DWORD PTR [ebx+esi]                      ;206.17
        mov       DWORD PTR [76+esp], edx                       ;206.17
        jne       .B10.19       ; Prob 50%                      ;207.28
                                ; LOE eax edx ecx edi
.B10.18:                        ; Preds .B10.17
        mov       ebx, 1                                        ;209.21
        jmp       .B10.20       ; Prob 100%                     ;209.21
                                ; LOE eax edx ecx ebx
.B10.19:                        ; Preds .B10.17
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+32] ;211.37
        neg       esi                                           ;211.37
        add       esi, edi                                      ;211.37
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;211.37
        cvtsi2ss  xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;211.37
        movss     xmm3, DWORD PTR [ebx+esi*4]                   ;211.37
        divss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;211.37
        divss     xmm3, xmm0                                    ;211.37
        movss     xmm4, DWORD PTR [_2il0floatpacket.74]         ;211.37
        andps     xmm4, xmm3                                    ;211.37
        movaps    xmm1, xmm4                                    ;211.37
        movss     xmm2, DWORD PTR [_2il0floatpacket.75]         ;211.37
        pxor      xmm1, xmm3                                    ;211.37
        movss     xmm5, DWORD PTR [_2il0floatpacket.76]         ;211.37
        cmpltss   xmm1, xmm2                                    ;211.37
        andps     xmm2, xmm1                                    ;211.37
        orps      xmm2, xmm4                                    ;211.37
        movaps    xmm7, xmm2                                    ;211.37
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;211.86
        addss     xmm7, xmm3                                    ;211.37
        dec       ebx                                           ;211.98
        subss     xmm7, xmm2                                    ;211.37
        movaps    xmm6, xmm7                                    ;211.37
        subss     xmm6, xmm3                                    ;211.37
        cmpnless  xmm6, xmm4                                    ;211.37
        andps     xmm6, xmm5                                    ;211.37
        subss     xmm7, xmm6                                    ;211.37
        cvtss2si  edi, xmm7                                     ;211.37
        inc       edi                                           ;211.86
        cmp       edi, ebx                                      ;211.21
        cmovl     ebx, edi                                      ;211.21
                                ; LOE eax edx ecx ebx
.B10.20:                        ; Preds .B10.18 .B10.19
        imul      edx, edx, 152                                 ;213.17
        sub       eax, ecx                                      ;213.17
        mov       eax, DWORD PTR [12+edx+eax]                   ;213.17
        lea       ecx, DWORD PTR [76+esp]                       ;214.22
        mov       DWORD PTR [96+esp], eax                       ;213.17
        push      ecx                                           ;214.22
        lea       esi, DWORD PTR [76+esp]                       ;214.22
        push      esi                                           ;214.22
        call      _DYNUST_NETWORK_MODULE_mp_GETBSTMOVE          ;214.22
                                ; LOE eax ebx
.B10.67:                        ; Preds .B10.20
        add       esp, 8                                        ;214.22
                                ; LOE eax ebx
.B10.21:                        ; Preds .B10.67
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES_ORG] ;216.72
        mov       DWORD PTR [100+esp], edi                      ;216.72
        mov       edi, DWORD PTR [96+esp]                       ;216.126
        sub       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+44] ;216.126
        sub       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+56] ;216.126
        imul      edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+40] ;216.126
        imul      eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+52] ;216.126
        movss     xmm0, DWORD PTR [_2il0floatpacket.73]         ;216.57
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;216.72
        neg       esi                                           ;
        add       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL] ;216.126
        mov       ecx, DWORD PTR [76+esp]                       ;216.72
        add       esi, ecx                                      ;
        add       eax, edi                                      ;216.126
        mov       DWORD PTR [92+esp], ecx                       ;216.72
        imul      ecx, esi, 152                                 ;
        sub       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+32] ;216.126
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;216.57
        movss     xmm4, DWORD PTR [eax+ebx*4]                   ;216.21
        mov       eax, DWORD PTR [24+edx+ecx]                   ;216.72
        comiss    xmm0, xmm4                                    ;216.57
        jbe       .B10.73       ; Prob 50%                      ;216.57
                                ; LOE eax edx ecx xmm4
.B10.22:                        ; Preds .B10.21
        cmp       eax, DWORD PTR [100+esp]                      ;216.110
        jl        .B10.24       ; Prob 50%                      ;216.110
        jmp       .B10.23       ; Prob 100%                     ;216.110
                                ; LOE edx ecx xmm4
.B10.73:                        ; Preds .B10.21
        cmp       eax, DWORD PTR [100+esp]                      ;216.110
                                ; LOE edx ecx xmm4
.B10.23:                        ; Preds .B10.73 .B10.22
        jle       .B10.31       ; Prob 50%                      ;216.168
                                ; LOE edx ecx xmm4
.B10.24:                        ; Preds .B10.22 .B10.23
        mov       eax, DWORD PTR [92+esp]                       ;222.35
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+32] ;222.21
        mov       esi, DWORD PTR [72+esp]                       ;218.37
        mov       DWORD PTR [68+esp], esi                       ;218.37
        lea       ebx, DWORD PTR [eax*4]                        ;222.35
        mov       DWORD PTR [56+esp], ebx                       ;222.35
        mov       ebx, DWORD PTR [28+edx+ecx]                   ;218.95
        lea       ecx, DWORD PTR [edi*4]                        ;222.35
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL] ;222.35
        mov       DWORD PTR [60+esp], ecx                       ;222.35
        sub       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+32] ;222.35
        lea       edx, DWORD PTR [edx+eax*4]                    ;222.35
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;222.35
        sub       edx, ecx                                      ;222.35
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+32] ;222.35
        shl       ecx, 2                                        ;222.35
        lea       esi, DWORD PTR [eax+esi*4]                    ;222.35
        sub       esi, ecx                                      ;222.35
        mov       DWORD PTR [64+esp], esi                       ;222.35
        movss     xmm0, DWORD PTR [edx]                         ;222.37
        movss     xmm2, DWORD PTR [esi]                         ;218.37
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD] ;218.95
        addss     xmm2, xmm4                                    ;218.55
        movss     xmm3, DWORD PTR [esi+ebx*4]                   ;218.95
        movaps    xmm1, xmm3                                    ;218.25
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL] ;222.35
        addss     xmm1, xmm2                                    ;218.25
        comiss    xmm0, xmm1                                    ;222.35
        jbe       .B10.31       ; Prob 50%                      ;222.35
                                ; LOE eax edx ecx ebx edi xmm2 xmm3 xmm4
.B10.25:                        ; Preds .B10.24
        mov       esi, DWORD PTR [92+esp]                       ;223.25
        lea       eax, DWORD PTR [eax+esi*4]                    ;223.25
        sub       eax, ecx                                      ;223.25
        mov       ecx, DWORD PTR [64+esp]                       ;225.66
        movss     DWORD PTR [eax], xmm2                         ;223.25
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+32] ;229.25
        shl       eax, 2                                        ;229.25
        addss     xmm4, DWORD PTR [ecx]                         ;225.66
        neg       eax                                           ;229.25
        addss     xmm4, xmm3                                    ;225.29
        add       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED] ;229.25
        mov       ecx, DWORD PTR [56+esp]                       ;229.25
        movss     DWORD PTR [edx], xmm4                         ;225.29
        mov       edx, DWORD PTR [68+esp]                       ;229.25
        mov       DWORD PTR [eax+ecx], edx                      ;229.25
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE] ;230.25
        mov       esi, edx                                      ;230.41
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+32] ;230.25
        shl       eax, 2                                        ;230.41
        sub       esi, eax                                      ;230.41
        cmp       DWORD PTR [esi+ecx], 0                        ;230.41
        jle       .B10.29       ; Prob 41%                      ;230.41
                                ; LOE eax edx ebx edi
.B10.26:                        ; Preds .B10.25
        mov       ecx, DWORD PTR [92+esp]                       ;231.34
        push      OFFSET FLAT: _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDITEM.0.10 ;231.34
        lea       esi, DWORD PTR [80+esp]                       ;231.34
        push      esi                                           ;231.34
        lea       edx, DWORD PTR [edx+ecx*4]                    ;231.34
        sub       edx, eax                                      ;231.34
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET] ;231.34
        mov       ebx, DWORD PTR [edx]                          ;231.34
        sub       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+32] ;231.34
        lea       edi, DWORD PTR [eax+ebx*8]                    ;231.34
        push      edi                                           ;231.34
        call      _LINK_LIST_mp_DISQUEUE_TARGET                 ;231.34
                                ; LOE
.B10.68:                        ; Preds .B10.26
        add       esp, 12                                       ;231.34
                                ; LOE
.B10.27:                        ; Preds .B10.68
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+32] ;232.29
        shl       edx, 2                                        ;232.29
        mov       esi, DWORD PTR [76+esp]                       ;232.29
        neg       edx                                           ;232.29
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE] ;232.29
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY] ;232.29
        mov       DWORD PTR [92+esp], esi                       ;232.29
        lea       ecx, DWORD PTR [esi*4]                        ;232.29
        mov       DWORD PTR [56+esp], ecx                       ;232.29
        lea       ecx, DWORD PTR [ebx+esi*4]                    ;232.29
        mov       edi, DWORD PTR [edx+ecx]                      ;232.54
        sub       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+32] ;232.29
        mov       DWORD PTR [edx+ecx], 0                        ;233.29
        dec       DWORD PTR [eax+edi*4]                         ;232.29
        test      BYTE PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDITEM.0.10], 1 ;234.37
        je        .B10.55       ; Prob 7%                       ;234.37
                                ; LOE
.B10.28:                        ; Preds .B10.70 .B10.27
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+32] ;239.75
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL] ;239.75
        lea       eax, DWORD PTR [edi*4]                        ;
        mov       DWORD PTR [60+esp], eax                       ;
                                ; LOE ebx edi
.B10.29:                        ; Preds .B10.28 .B10.25
        mov       esi, DWORD PTR [92+esp]                       ;239.75
        sub       esi, edi                                      ;239.75
        movss     xmm6, DWORD PTR [_2il0floatpacket.74]         ;239.75
        movss     xmm2, DWORD PTR [_2il0floatpacket.75]         ;239.75
        movss     xmm7, DWORD PTR [_2il0floatpacket.76]         ;239.75
        movss     xmm5, DWORD PTR [ebx+esi*4]                   ;239.75
        andps     xmm6, xmm5                                    ;239.75
        movaps    xmm4, xmm5                                    ;239.75
        pxor      xmm4, xmm6                                    ;239.75
        movaps    xmm3, xmm5                                    ;239.75
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET+32] ;240.30
        cmpltss   xmm4, xmm2                                    ;239.75
        andps     xmm4, xmm2                                    ;239.75
        neg       ecx                                           ;240.30
        orps      xmm4, xmm6                                    ;239.75
        mov       edi, DWORD PTR [56+esp]                       ;240.30
        addss     xmm3, xmm4                                    ;239.75
        add       edi, ebx                                      ;240.30
        subss     xmm3, xmm4                                    ;239.75
        movaps    xmm1, xmm3                                    ;239.75
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;239.43
        subss     xmm1, xmm5                                    ;239.75
        movss     xmm5, DWORD PTR [_2il0floatpacket.77]         ;239.43
        cmpnless  xmm1, xmm6                                    ;239.75
        andps     xmm1, xmm7                                    ;239.75
        sub       edi, DWORD PTR [60+esp]                       ;240.30
        subss     xmm3, xmm1                                    ;239.75
        movss     xmm1, DWORD PTR [_2il0floatpacket.74]         ;239.43
        andps     xmm1, xmm4                                    ;239.43
        pxor      xmm4, xmm1                                    ;239.43
        movaps    xmm0, xmm4                                    ;239.43
        cvtss2si  eax, xmm3                                     ;239.75
        cmpltss   xmm0, xmm2                                    ;239.43
        andps     xmm2, xmm0                                    ;239.43
        movaps    xmm0, xmm4                                    ;239.43
        inc       eax                                           ;239.114
        addss     xmm0, xmm2                                    ;239.43
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET] ;240.30
        subss     xmm0, xmm2                                    ;239.43
        movaps    xmm2, xmm0                                    ;239.43
        push      edi                                           ;240.30
        subss     xmm2, xmm4                                    ;239.43
        movaps    xmm4, xmm5                                    ;239.43
        addss     xmm4, xmm5                                    ;239.43
        movaps    xmm6, xmm2                                    ;239.43
        lea       ebx, DWORD PTR [80+esp]                       ;240.30
        push      ebx                                           ;240.30
        cmpnless  xmm6, xmm5                                    ;239.43
        cmpless   xmm2, DWORD PTR [_2il0floatpacket.78]         ;239.43
        andps     xmm6, xmm4                                    ;239.43
        andps     xmm2, xmm4                                    ;239.43
        subss     xmm0, xmm6                                    ;239.43
        addss     xmm0, xmm2                                    ;239.43
        orps      xmm0, xmm1                                    ;239.43
        cvtss2si  esi, xmm0                                     ;239.43
        cmp       eax, esi                                      ;239.25
        cmovl     esi, eax                                      ;239.25
        add       ecx, esi                                      ;240.30
        lea       eax, DWORD PTR [edx+ecx*8]                    ;240.30
        push      eax                                           ;240.30
        call      _LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST        ;240.30
                                ; LOE esi
.B10.69:                        ; Preds .B10.29
        add       esp, 12                                       ;240.30
                                ; LOE esi
.B10.30:                        ; Preds .B10.69
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY+32] ;241.25
        neg       edx                                           ;241.25
        mov       ebx, DWORD PTR [76+esp]                       ;242.25
        add       edx, esi                                      ;241.25
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY] ;241.25
        sub       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+32] ;242.25
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE] ;242.25
        inc       DWORD PTR [eax+edx*4]                         ;241.25
        mov       DWORD PTR [ecx+ebx*4], esi                    ;242.25
                                ; LOE
.B10.31:                        ; Preds .B10.23 .B10.30 .B10.24
        mov       eax, DWORD PTR [88+esp]                       ;245.12
        inc       eax                                           ;245.12
        mov       DWORD PTR [88+esp], eax                       ;245.12
        cmp       eax, DWORD PTR [80+esp]                       ;245.12
        jg        .B10.32       ; Prob 18%                      ;245.12
                                ; LOE
.B10.72:                        ; Preds .B10.31
        mov       edi, DWORD PTR [72+esp]                       ;191.13
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;191.13
        jmp       .B10.17       ; Prob 100%                     ;191.13
                                ; LOE eax edi
.B10.32:                        ; Preds .B10.31
        mov       esi, DWORD PTR [40+esp]                       ;
        lea       ebx, DWORD PTR [72+esp]                       ;
                                ; LOE ebx esi
.B10.33:                        ; Preds .B10.32 .B10.15
        inc       esi                                           ;247.9
        test      BYTE PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDTHIS.0.10], 1 ;185.22
        je        .B10.5        ; Prob 82%                      ;185.22
                                ; LOE ebx esi
.B10.35:                        ; Preds .B10.33
        add       esp, 116                                      ;250.1
        pop       ebx                                           ;250.1
        pop       edi                                           ;250.1
        pop       esi                                           ;250.1
        mov       esp, ebp                                      ;250.1
        pop       ebp                                           ;250.1
        ret                                                     ;250.1
                                ; LOE
.B10.36:                        ; Preds .B10.2                  ; Infreq
        cmp       esi, 4                                        ;183.5
        jl        .B10.52       ; Prob 10%                      ;183.5
                                ; LOE ecx ebx esi
.B10.37:                        ; Preds .B10.36                 ; Infreq
        mov       edx, ecx                                      ;183.5
        and       edx, 15                                       ;183.5
        je        .B10.40       ; Prob 50%                      ;183.5
                                ; LOE edx ecx ebx esi
.B10.38:                        ; Preds .B10.37                 ; Infreq
        test      dl, 3                                         ;183.5
        jne       .B10.52       ; Prob 10%                      ;183.5
                                ; LOE edx ecx ebx esi
.B10.39:                        ; Preds .B10.38                 ; Infreq
        neg       edx                                           ;183.5
        add       edx, 16                                       ;183.5
        shr       edx, 2                                        ;183.5
                                ; LOE edx ecx ebx esi
.B10.40:                        ; Preds .B10.39 .B10.37         ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;183.5
        cmp       esi, eax                                      ;183.5
        jl        .B10.52       ; Prob 10%                      ;183.5
                                ; LOE edx ecx ebx esi
.B10.41:                        ; Preds .B10.40                 ; Infreq
        mov       eax, esi                                      ;183.5
        sub       eax, edx                                      ;183.5
        and       eax, 3                                        ;183.5
        neg       eax                                           ;183.5
        add       eax, esi                                      ;183.5
        test      edx, edx                                      ;183.5
        jbe       .B10.45       ; Prob 10%                      ;183.5
                                ; LOE eax edx ecx ebx esi
.B10.42:                        ; Preds .B10.41                 ; Infreq
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.43:                        ; Preds .B10.43 .B10.42         ; Infreq
        mov       DWORD PTR [ecx+edi*4], 0                      ;183.5
        inc       edi                                           ;183.5
        cmp       edi, edx                                      ;183.5
        jb        .B10.43       ; Prob 82%                      ;183.5
                                ; LOE eax edx ecx ebx esi edi
.B10.45:                        ; Preds .B10.43 .B10.41         ; Infreq
        pxor      xmm0, xmm0                                    ;183.5
                                ; LOE eax edx ecx ebx esi xmm0
.B10.46:                        ; Preds .B10.46 .B10.45         ; Infreq
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;183.5
        add       edx, 4                                        ;183.5
        cmp       edx, eax                                      ;183.5
        jb        .B10.46       ; Prob 82%                      ;183.5
                                ; LOE eax edx ecx ebx esi xmm0
.B10.48:                        ; Preds .B10.46 .B10.52         ; Infreq
        cmp       eax, esi                                      ;183.5
        jae       .B10.4        ; Prob 10%                      ;183.5
                                ; LOE eax ecx ebx esi
.B10.50:                        ; Preds .B10.48 .B10.50         ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;183.5
        inc       eax                                           ;183.5
        cmp       eax, esi                                      ;183.5
        jb        .B10.50       ; Prob 82%                      ;183.5
        jmp       .B10.4        ; Prob 100%                     ;183.5
                                ; LOE eax ecx ebx esi
.B10.52:                        ; Preds .B10.36 .B10.40 .B10.38 ; Infreq
        xor       eax, eax                                      ;183.5
        jmp       .B10.48       ; Prob 100%                     ;183.5
                                ; LOE eax ecx ebx esi
.B10.55:                        ; Preds .B10.27                 ; Infreq
        mov       DWORD PTR [esp], 0                            ;235.33
        lea       edx, DWORD PTR [esp]                          ;235.33
        mov       DWORD PTR [32+esp], 49                        ;235.33
        lea       eax, DWORD PTR [32+esp]                       ;235.33
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;235.33
        push      32                                            ;235.33
        push      eax                                           ;235.33
        push      OFFSET FLAT: __STRLITPACK_3.0.10              ;235.33
        push      -2088435968                                   ;235.33
        push      911                                           ;235.33
        push      edx                                           ;235.33
        call      _for_write_seq_lis                            ;235.33
                                ; LOE
.B10.56:                        ; Preds .B10.55                 ; Infreq
        push      32                                            ;236.33
        xor       eax, eax                                      ;236.33
        push      eax                                           ;236.33
        push      eax                                           ;236.33
        push      -2088435968                                   ;236.33
        push      eax                                           ;236.33
        push      OFFSET FLAT: __STRLITPACK_4                   ;236.33
        call      _for_stop_core                                ;236.33
                                ; LOE
.B10.70:                        ; Preds .B10.56                 ; Infreq
        add       esp, 48                                       ;236.33
        jmp       .B10.28       ; Prob 100%                     ;236.33
                                ; LOE
.B10.58:                        ; Preds .B10.14                 ; Infreq
        mov       eax, DWORD PTR [16+ebp]                       ;165.12
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;199.18
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+32] ;199.18
        mov       DWORD PTR [eax], edi                          ;198.13
        sub       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+32] ;199.18
        shl       edx, 3                                        ;199.18
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET] ;199.18
        sub       esi, edx                                      ;199.18
        lea       edi, DWORD PTR [ecx+edi*4]                    ;199.18
        push      edi                                           ;199.18
        add       esi, 8                                        ;199.18
        lea       ebx, DWORD PTR [76+esp]                       ;199.18
        push      ebx                                           ;199.18
        push      esi                                           ;199.18
        call      _LINK_LIST_mp_ENQUEUE_LINKLIST                ;199.18
                                ; LOE
.B10.59:                        ; Preds .B10.58                 ; Infreq
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDTHIS.0.10], -1 ;202.13
        add       esp, 128                                      ;202.13
        pop       ebx                                           ;202.13
        pop       edi                                           ;202.13
        pop       esi                                           ;202.13
        mov       esp, ebp                                      ;202.13
        pop       ebp                                           ;202.13
        ret                                                     ;202.13
                                ; LOE
.B10.61:                        ; Preds .B10.5                  ; Infreq
        mov       eax, DWORD PTR [72+esp]                       ;194.61
        shl       eax, 2                                        ;194.61
        jmp       .B10.13       ; Prob 100%                     ;194.61
        ALIGN     16
                                ; LOE eax ebx esi
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE_mp_RETRIEVETDSP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_RETRIEVETDSP
_DYNUST_TDSP_ASTAR_MODULE_mp_RETRIEVETDSP	PROC NEAR 
; parameter 1: 20 + esp
; parameter 2: 24 + esp
; parameter 3: 28 + esp
; parameter 4: 32 + esp
.B11.1:                         ; Preds .B11.0
        push      esi                                           ;260.12
        push      edi                                           ;260.12
        push      ebx                                           ;260.12
        push      ebp                                           ;260.12
        mov       eax, DWORD PTR [24+esp]                       ;260.12
        mov       edx, DWORD PTR [20+esp]                       ;260.12
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;271.9
        mov       ebp, DWORD PTR [eax]                          ;269.5
        mov       esi, DWORD PTR [edx]                          ;270.5
        imul      edx, ebp, 152                                 ;271.9
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;271.9
        add       edx, ebx                                      ;271.9
        sub       edx, eax                                      ;271.9
        mov       edi, DWORD PTR [28+esp]                       ;260.12
        cmp       esi, ebp                                      ;270.15
        mov       ecx, DWORD PTR [24+edx]                       ;271.9
        mov       DWORD PTR [edi], ecx                          ;271.9
        jne       .B11.3        ; Prob 50%                      ;270.15
                                ; LOE eax edx ebx ebp esi edi
.B11.2:                         ; Preds .B11.1
        xor       ecx, ecx                                      ;272.9
        jmp       .B11.4        ; Prob 100%                     ;272.9
                                ; LOE eax ecx ebx ebp esi edi
.B11.3:                         ; Preds .B11.1
        mov       edx, DWORD PTR [28+edx]                       ;275.9
        mov       ecx, 1                                        ;276.9
        mov       DWORD PTR [4+edi], edx                        ;275.9
                                ; LOE eax ecx ebx ebp esi edi
.B11.4:                         ; Preds .B11.2 .B11.3
        cmp       ebp, esi                                      ;280.21
        je        .B11.7        ; Prob 10%                      ;280.21
                                ; LOE eax ecx ebx ebp esi edi
.B11.5:                         ; Preds .B11.4
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED+32] ;284.9
        sub       ebx, eax                                      ;
        shl       edx, 2                                        ;
        neg       edx                                           ;
        add       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_PRED] ;
                                ; LOE edx ecx ebx ebp esi edi
.B11.6:                         ; Preds .B11.6 .B11.5
        imul      eax, ebp, 152                                 ;282.9
        inc       ecx                                           ;281.9
        mov       eax, DWORD PTR [28+ebx+eax]                   ;282.9
        mov       ebp, DWORD PTR [edx+ebp*4]                    ;284.9
        cmp       ebp, esi                                      ;280.21
        mov       DWORD PTR [-4+edi+ecx*4], eax                 ;282.9
        jne       .B11.6        ; Prob 82%                      ;280.21
                                ; LOE edx ecx ebx ebp esi edi
.B11.7:                         ; Preds .B11.4 .B11.6
        mov       eax, DWORD PTR [32+esp]                       ;281.9
        mov       DWORD PTR [eax], ecx                          ;281.9
                                ; LOE
.B11.8:                         ; Preds .B11.7
        pop       ebp                                           ;287.1
        pop       ebx                                           ;287.1
        pop       edi                                           ;287.1
        pop       esi                                           ;287.1
        ret                                                     ;287.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE_mp_RETRIEVETDSP ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE_mp_RETRIEVETDSP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_INITIALIZATION
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_INITIALIZATION
_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_INITIALIZATION	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
.B12.1:                         ; Preds .B12.0
        push      ebp                                           ;107.12
        mov       ebp, esp                                      ;107.12
        and       esp, -16                                      ;107.12
        push      esi                                           ;107.12
        push      edi                                           ;107.12
        push      ebx                                           ;107.12
        sub       esp, 52                                       ;107.12
        test      BYTE PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+12], 1 ;117.13
        jne       .B12.4        ; Prob 40%                      ;117.13
                                ; LOE
.B12.2:                         ; Preds .B12.1
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;117.31
        xor       edx, edx                                      ;117.31
        test      ebx, ebx                                      ;117.31
        cmovle    ebx, edx                                      ;117.31
        mov       ecx, 4                                        ;117.31
        lea       esi, DWORD PTR [esp]                          ;117.31
        push      ecx                                           ;117.31
        push      ebx                                           ;117.31
        push      2                                             ;117.31
        push      esi                                           ;117.31
        mov       eax, 1                                        ;117.31
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+12], 133 ;117.31
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+4], ecx ;117.31
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+16], eax ;117.31
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+8], edx ;117.31
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+32], eax ;117.31
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+24], ebx ;117.31
        mov       DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+28], ecx ;117.31
        call      _for_check_mult_overflow                      ;117.31
                                ; LOE eax
.B12.3:                         ; Preds .B12.2
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+12] ;117.31
        and       eax, 1                                        ;117.31
        and       edx, 1                                        ;117.31
        shl       eax, 4                                        ;117.31
        add       edx, edx                                      ;117.31
        or        edx, eax                                      ;117.31
        or        edx, 262144                                   ;117.31
        push      edx                                           ;117.31
        push      OFFSET FLAT: _DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE ;117.31
        push      DWORD PTR [24+esp]                            ;117.31
        call      _for_alloc_allocatable                        ;117.31
                                ; LOE
.B12.168:                       ; Preds .B12.3
        add       esp, 28                                       ;117.31
                                ; LOE
.B12.4:                         ; Preds .B12.1 .B12.168
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE+24] ;118.5
        test      edx, edx                                      ;118.5
        jle       .B12.7        ; Prob 50%                      ;118.5
                                ; LOE edx
.B12.5:                         ; Preds .B12.4
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE] ;118.5
        cmp       edx, 24                                       ;118.5
        jle       .B12.122      ; Prob 0%                       ;118.5
                                ; LOE edx ecx
.B12.6:                         ; Preds .B12.5
        shl       edx, 2                                        ;118.5
        push      edx                                           ;118.5
        push      0                                             ;118.5
        push      ecx                                           ;118.5
        call      __intel_fast_memset                           ;118.5
                                ; LOE
.B12.169:                       ; Preds .B12.6
        add       esp, 12                                       ;118.5
                                ; LOE
.B12.7:                         ; Preds .B12.136 .B12.4 .B12.134 .B12.169
        mov       edx, DWORD PTR [20+ebp]                       ;107.12
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;121.17
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD] ;134.9
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_EJD+32] ;134.9
        cmp       DWORD PTR [edx], 0                            ;120.17
        jne       .B12.64       ; Prob 50%                      ;120.17
                                ; LOE eax ebx esi
.B12.8:                         ; Preds .B12.7
        test      esi, esi                                      ;122.16
        jle       .B12.152      ; Prob 2%                       ;122.16
                                ; LOE eax ebx esi
.B12.9:                         ; Preds .B12.8
        mov       edx, DWORD PTR [12+ebp]                       ;122.47
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_X+32] ;122.42
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_Y+32] ;122.64
        shl       ecx, 2                                        ;122.59
        shl       edi, 2                                        ;122.81
        neg       ecx                                           ;122.59
        neg       edi                                           ;122.81
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_Y]  ;122.81
        mov       edx, DWORD PTR [edx]                          ;122.47
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_X]  ;122.59
        shl       ebx, 2                                        ;
        sub       eax, ebx                                      ;
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DISTSCALE] ;122.16
        cmp       esi, 4                                        ;122.16
        movss     xmm3, DWORD PTR [ecx+edx*4]                   ;122.47
        movss     xmm1, DWORD PTR [edi+edx*4]                   ;122.69
        jl        .B12.141      ; Prob 10%                      ;122.16
                                ; LOE eax ecx esi edi xmm1 xmm2 xmm3
.B12.10:                        ; Preds .B12.9
        lea       edx, DWORD PTR [4+eax]                        ;122.16
        and       edx, 15                                       ;122.16
        je        .B12.13       ; Prob 50%                      ;122.16
                                ; LOE eax edx ecx esi edi xmm1 xmm2 xmm3
.B12.11:                        ; Preds .B12.10
        test      dl, 3                                         ;122.16
        jne       .B12.141      ; Prob 10%                      ;122.16
                                ; LOE eax edx ecx esi edi xmm1 xmm2 xmm3
.B12.12:                        ; Preds .B12.11
        neg       edx                                           ;122.16
        add       edx, 16                                       ;122.16
        shr       edx, 2                                        ;122.16
                                ; LOE eax edx ecx esi edi xmm1 xmm2 xmm3
.B12.13:                        ; Preds .B12.12 .B12.10
        lea       ebx, DWORD PTR [4+edx]                        ;122.16
        cmp       esi, ebx                                      ;122.16
        jl        .B12.141      ; Prob 10%                      ;122.16
                                ; LOE eax edx ecx esi edi xmm1 xmm2 xmm3
.B12.14:                        ; Preds .B12.13
        mov       ebx, esi                                      ;122.16
        sub       ebx, edx                                      ;122.16
        and       ebx, 3                                        ;122.16
        neg       ebx                                           ;122.16
        add       ebx, esi                                      ;122.16
        mov       DWORD PTR [4+esp], ebx                        ;122.16
        test      edx, edx                                      ;122.16
        jbe       .B12.18       ; Prob 0%                       ;122.16
                                ; LOE eax edx ecx esi edi xmm1 xmm2 xmm3
.B12.15:                        ; Preds .B12.14
        movss     xmm4, DWORD PTR [_2il0floatpacket.92]         ;122.95
        xor       ebx, ebx                                      ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.93]         ;122.86
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4
.B12.16:                        ; Preds .B12.16 .B12.15
        movss     xmm6, DWORD PTR [4+ecx+ebx*4]                 ;122.42
        movss     xmm5, DWORD PTR [4+edi+ebx*4]                 ;122.64
        subss     xmm6, xmm3                                    ;122.59
        subss     xmm5, xmm1                                    ;122.81
        mulss     xmm6, xmm6                                    ;122.59
        mulss     xmm5, xmm5                                    ;122.81
        addss     xmm6, xmm5                                    ;122.62
        sqrtss    xmm6, xmm6                                    ;122.36
        mulss     xmm6, xmm2                                    ;122.35
        divss     xmm6, xmm0                                    ;122.86
        mulss     xmm6, xmm4                                    ;122.16
        movss     DWORD PTR [4+eax+ebx*4], xmm6                 ;122.16
        inc       ebx                                           ;122.16
        cmp       ebx, edx                                      ;122.16
        jb        .B12.16       ; Prob 82%                      ;122.16
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4
.B12.18:                        ; Preds .B12.16 .B12.14
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.95]       ;122.86
        lea       ebx, DWORD PTR [4+edi+edx*4]                  ;122.16
        test      bl, 15                                        ;122.16
        je        .B12.22       ; Prob 60%                      ;122.16
                                ; LOE eax edx ecx esi edi xmm0 xmm1 xmm2 xmm3
.B12.19:                        ; Preds .B12.18
        movaps    xmm7, xmm2                                    ;122.95
        movaps    xmm6, xmm3                                    ;122.42
        movaps    xmm5, xmm1                                    ;122.64
        shufps    xmm7, xmm7, 0                                 ;122.95
        shufps    xmm6, xmm6, 0                                 ;122.42
        shufps    xmm5, xmm5, 0                                 ;122.64
        rcpps     xmm4, xmm0                                    ;122.64
        movss     DWORD PTR [8+esp], xmm1                       ;122.64
        movss     DWORD PTR [12+esp], xmm3                      ;122.64
        movss     DWORD PTR [16+esp], xmm2                      ;122.64
        mov       ebx, DWORD PTR [4+esp]                        ;122.64
        ALIGN     16
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm4 xmm5 xmm6 xmm7
.B12.20:                        ; Preds .B12.20 .B12.19
        movups    xmm2, XMMWORD PTR [4+ecx+edx*4]               ;122.42
        movups    xmm3, XMMWORD PTR [4+edi+edx*4]               ;122.64
        subps     xmm2, xmm6                                    ;122.59
        subps     xmm3, xmm5                                    ;122.81
        mulps     xmm2, xmm2                                    ;122.59
        mulps     xmm3, xmm3                                    ;122.81
        movaps    xmm1, XMMWORD PTR [_2il0floatpacket.100]      ;122.35
        addps     xmm2, xmm3                                    ;122.62
        movaps    xmm3, XMMWORD PTR [_2il0floatpacket.101]      ;122.35
        andps     xmm3, xmm2                                    ;122.35
        cmpleps   xmm1, xmm3                                    ;122.35
        rsqrtps   xmm3, xmm2                                    ;122.35
        andps     xmm1, xmm3                                    ;122.35
        movaps    xmm3, xmm4                                    ;122.86
        mulps     xmm2, xmm1                                    ;122.35
        addps     xmm3, xmm4                                    ;122.86
        mulps     xmm1, xmm2                                    ;122.35
        subps     xmm1, XMMWORD PTR [_2il0floatpacket.98]       ;122.35
        mulps     xmm2, xmm1                                    ;122.35
        movaps    xmm1, xmm4                                    ;122.86
        mulps     xmm1, xmm0                                    ;122.86
        mulps     xmm2, XMMWORD PTR [_2il0floatpacket.99]       ;122.35
        mulps     xmm1, xmm4                                    ;122.86
        mulps     xmm2, xmm7                                    ;122.35
        subps     xmm3, xmm1                                    ;122.86
        mulps     xmm2, xmm3                                    ;122.86
        mulps     xmm2, XMMWORD PTR [_2il0floatpacket.94]       ;122.16
        movaps    XMMWORD PTR [4+eax+edx*4], xmm2               ;122.16
        add       edx, 4                                        ;122.16
        cmp       edx, ebx                                      ;122.16
        jb        .B12.20       ; Prob 82%                      ;122.16
        jmp       .B12.24       ; Prob 100%                     ;122.16
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm4 xmm5 xmm6 xmm7
.B12.22:                        ; Preds .B12.18
        movaps    xmm6, xmm2                                    ;122.95
        movaps    xmm5, xmm3                                    ;122.42
        movaps    xmm4, xmm1                                    ;122.64
        shufps    xmm6, xmm6, 0                                 ;122.95
        shufps    xmm5, xmm5, 0                                 ;122.42
        shufps    xmm4, xmm4, 0                                 ;122.64
        rcpps     xmm7, xmm0                                    ;122.64
        movss     DWORD PTR [8+esp], xmm1                       ;122.64
        movss     DWORD PTR [12+esp], xmm3                      ;122.64
        movss     DWORD PTR [16+esp], xmm2                      ;122.64
        mov       ebx, DWORD PTR [4+esp]                        ;122.64
        ALIGN     16
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm4 xmm5 xmm6 xmm7
.B12.23:                        ; Preds .B12.23 .B12.22
        movups    xmm2, XMMWORD PTR [4+ecx+edx*4]               ;122.42
        movaps    xmm3, XMMWORD PTR [4+edi+edx*4]               ;122.64
        subps     xmm2, xmm5                                    ;122.59
        subps     xmm3, xmm4                                    ;122.81
        mulps     xmm2, xmm2                                    ;122.59
        mulps     xmm3, xmm3                                    ;122.81
        movaps    xmm1, XMMWORD PTR [_2il0floatpacket.100]      ;122.35
        addps     xmm2, xmm3                                    ;122.62
        movaps    xmm3, XMMWORD PTR [_2il0floatpacket.101]      ;122.35
        andps     xmm3, xmm2                                    ;122.35
        cmpleps   xmm1, xmm3                                    ;122.35
        rsqrtps   xmm3, xmm2                                    ;122.35
        andps     xmm1, xmm3                                    ;122.35
        movaps    xmm3, xmm7                                    ;122.86
        mulps     xmm2, xmm1                                    ;122.35
        addps     xmm3, xmm7                                    ;122.86
        mulps     xmm1, xmm2                                    ;122.35
        subps     xmm1, XMMWORD PTR [_2il0floatpacket.98]       ;122.35
        mulps     xmm2, xmm1                                    ;122.35
        movaps    xmm1, xmm7                                    ;122.86
        mulps     xmm1, xmm0                                    ;122.86
        mulps     xmm2, XMMWORD PTR [_2il0floatpacket.99]       ;122.35
        mulps     xmm1, xmm7                                    ;122.86
        mulps     xmm2, xmm6                                    ;122.35
        subps     xmm3, xmm1                                    ;122.86
        mulps     xmm2, xmm3                                    ;122.86
        mulps     xmm2, XMMWORD PTR [_2il0floatpacket.94]       ;122.16
        movaps    XMMWORD PTR [4+eax+edx*4], xmm2               ;122.16
        add       edx, 4                                        ;122.16
        cmp       edx, ebx                                      ;122.16
        jb        .B12.23       ; Prob 82%                      ;122.16
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm4 xmm5 xmm6 xmm7
.B12.24:                        ; Preds .B12.20 .B12.23
        mov       DWORD PTR [4+esp], ebx                        ;
        movss     xmm1, DWORD PTR [8+esp]                       ;
        movss     xmm3, DWORD PTR [12+esp]                      ;
        movss     xmm2, DWORD PTR [16+esp]                      ;
                                ; LOE eax ecx esi edi xmm1 xmm2 xmm3
.B12.25:                        ; Preds .B12.24 .B12.141
        cmp       esi, DWORD PTR [4+esp]                        ;122.16
        jbe       .B12.29       ; Prob 0%                       ;122.16
                                ; LOE eax ecx esi edi xmm1 xmm2 xmm3
.B12.26:                        ; Preds .B12.25
        movss     xmm4, DWORD PTR [_2il0floatpacket.92]         ;122.95
        movss     xmm0, DWORD PTR [_2il0floatpacket.93]         ;122.86
        mov       edx, DWORD PTR [4+esp]                        ;122.86
                                ; LOE eax edx ecx esi edi xmm0 xmm1 xmm2 xmm3 xmm4
.B12.27:                        ; Preds .B12.27 .B12.26
        movss     xmm6, DWORD PTR [4+ecx+edx*4]                 ;122.42
        movss     xmm5, DWORD PTR [4+edi+edx*4]                 ;122.64
        subss     xmm6, xmm3                                    ;122.59
        subss     xmm5, xmm1                                    ;122.81
        mulss     xmm6, xmm6                                    ;122.59
        mulss     xmm5, xmm5                                    ;122.81
        addss     xmm6, xmm5                                    ;122.62
        sqrtss    xmm6, xmm6                                    ;122.36
        mulss     xmm6, xmm2                                    ;122.35
        divss     xmm6, xmm0                                    ;122.86
        mulss     xmm6, xmm4                                    ;122.16
        movss     DWORD PTR [4+eax+edx*4], xmm6                 ;122.16
        inc       edx                                           ;122.16
        cmp       edx, esi                                      ;122.16
        jb        .B12.27       ; Prob 82%                      ;122.16
                                ; LOE eax edx ecx esi edi xmm0 xmm1 xmm2 xmm3 xmm4
.B12.29:                        ; Preds .B12.27 .B12.152 .B12.25
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;133.9
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+24] ;131.9
        test      ebx, ebx                                      ;131.9
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+32] ;131.9
        mov       DWORD PTR [24+esp], ecx                       ;133.9
        jle       .B12.149      ; Prob 10%                      ;131.9
                                ; LOE eax edx ecx ebx cl ch
.B12.30:                        ; Preds .B12.29
        cmp       ebx, 8                                        ;131.9
        lea       ecx, DWORD PTR [edx*4]                        ;
        mov       DWORD PTR [20+esp], ecx                       ;
        jl        .B12.142      ; Prob 10%                      ;131.9
                                ; LOE eax edx ebx
.B12.31:                        ; Preds .B12.30
        mov       ecx, DWORD PTR [24+esp]                       ;131.9
        and       ecx, 15                                       ;131.9
        je        .B12.34       ; Prob 50%                      ;131.9
                                ; LOE eax edx ecx ebx
.B12.32:                        ; Preds .B12.31
        test      cl, 3                                         ;131.9
        jne       .B12.142      ; Prob 10%                      ;131.9
                                ; LOE eax edx ecx ebx
.B12.33:                        ; Preds .B12.32
        neg       ecx                                           ;131.9
        add       ecx, 16                                       ;131.9
        shr       ecx, 2                                        ;131.9
                                ; LOE eax edx ecx ebx
.B12.34:                        ; Preds .B12.33 .B12.31
        lea       esi, DWORD PTR [8+ecx]                        ;131.9
        cmp       ebx, esi                                      ;131.9
        jl        .B12.142      ; Prob 10%                      ;131.9
                                ; LOE eax edx ecx ebx
.B12.35:                        ; Preds .B12.34
        mov       esi, ebx                                      ;131.9
        sub       esi, ecx                                      ;131.9
        and       esi, 7                                        ;131.9
        mov       edi, DWORD PTR [24+esp]                       ;
        neg       esi                                           ;131.9
        sub       edi, DWORD PTR [20+esp]                       ;
        add       esi, ebx                                      ;131.9
        mov       DWORD PTR [16+esp], edi                       ;
        test      ecx, ecx                                      ;131.9
        jbe       .B12.39       ; Prob 0%                       ;131.9
                                ; LOE eax edx ecx ebx esi
.B12.36:                        ; Preds .B12.35
        xor       edi, edi                                      ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       edx, 1203982336                               ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B12.37:                        ; Preds .B12.37 .B12.36
        mov       DWORD PTR [edi+ebx*4], edx                    ;131.9
        inc       ebx                                           ;131.9
        cmp       ebx, ecx                                      ;131.9
        jb        .B12.37       ; Prob 82%                      ;131.9
                                ; LOE eax edx ecx ebx esi edi
.B12.38:                        ; Preds .B12.37
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       edx, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx esi
.B12.39:                        ; Preds .B12.35 .B12.38
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.97]       ;131.9
        mov       edi, DWORD PTR [24+esp]                       ;131.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.40:                        ; Preds .B12.40 .B12.39
        movaps    XMMWORD PTR [edi+ecx*4], xmm0                 ;131.9
        movaps    XMMWORD PTR [16+edi+ecx*4], xmm0              ;131.9
        add       ecx, 8                                        ;131.9
        cmp       ecx, esi                                      ;131.9
        jb        .B12.40       ; Prob 82%                      ;131.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.42:                        ; Preds .B12.40 .B12.142
        cmp       esi, ebx                                      ;131.9
        jae       .B12.46       ; Prob 0%                       ;131.9
                                ; LOE eax edx ebx esi
.B12.43:                        ; Preds .B12.42
        mov       ecx, DWORD PTR [16+esp]                       ;
        lea       ecx, DWORD PTR [ecx+edx*4]                    ;
        mov       edx, 1203982336                               ;131.9
                                ; LOE eax edx ecx ebx esi
.B12.44:                        ; Preds .B12.44 .B12.43
        mov       DWORD PTR [ecx+esi*4], edx                    ;131.9
        inc       esi                                           ;131.9
        cmp       esi, ebx                                      ;131.9
        jb        .B12.44       ; Prob 82%                      ;131.9
                                ; LOE eax edx ecx ebx esi
.B12.46:                        ; Preds .B12.44 .B12.42 .B12.149
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL] ;134.41
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+24] ;132.9
        test      ebx, ebx                                      ;132.9
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+32] ;132.9
        mov       DWORD PTR [28+esp], edx                       ;134.41
        jle       .B12.146      ; Prob 10%                      ;132.9
                                ; LOE eax edx ecx ebx dl dh
.B12.47:                        ; Preds .B12.46
        cmp       ebx, 8                                        ;132.9
        lea       edx, DWORD PTR [ecx*4]                        ;
        jl        .B12.143      ; Prob 10%                      ;132.9
                                ; LOE eax edx ecx ebx
.B12.48:                        ; Preds .B12.47
        mov       esi, DWORD PTR [28+esp]                       ;132.9
        and       esi, 15                                       ;132.9
        je        .B12.51       ; Prob 50%                      ;132.9
                                ; LOE eax edx ecx ebx esi
.B12.49:                        ; Preds .B12.48
        test      esi, 3                                        ;132.9
        jne       .B12.143      ; Prob 10%                      ;132.9
                                ; LOE eax edx ecx ebx esi
.B12.50:                        ; Preds .B12.49
        neg       esi                                           ;132.9
        add       esi, 16                                       ;132.9
        shr       esi, 2                                        ;132.9
                                ; LOE eax edx ecx ebx esi
.B12.51:                        ; Preds .B12.50 .B12.48
        mov       DWORD PTR [4+esp], eax                        ;
        lea       eax, DWORD PTR [8+esi]                        ;132.9
        cmp       ebx, eax                                      ;132.9
        mov       eax, DWORD PTR [4+esp]                        ;132.9
        jl        .B12.143      ; Prob 10%                      ;132.9
                                ; LOE eax edx ecx ebx esi al ah
.B12.52:                        ; Preds .B12.51
        mov       edi, ebx                                      ;132.9
        neg       edx                                           ;
        sub       edi, esi                                      ;132.9
        and       edi, 7                                        ;132.9
        neg       edi                                           ;132.9
        add       edx, DWORD PTR [28+esp]                       ;
        add       edi, ebx                                      ;132.9
        mov       DWORD PTR [12+esp], edi                       ;132.9
        test      esi, esi                                      ;132.9
        jbe       .B12.56       ; Prob 0%                       ;132.9
                                ; LOE eax edx ecx ebx esi al ah
.B12.53:                        ; Preds .B12.52
        xor       edi, edi                                      ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       ecx, 1203982336                               ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B12.54:                        ; Preds .B12.54 .B12.53
        mov       DWORD PTR [edi+ebx*4], ecx                    ;132.9
        inc       ebx                                           ;132.9
        cmp       ebx, esi                                      ;132.9
        jb        .B12.54       ; Prob 82%                      ;132.9
                                ; LOE eax edx ecx ebx esi edi
.B12.55:                        ; Preds .B12.54
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       ecx, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx esi
.B12.56:                        ; Preds .B12.52 .B12.55
        mov       DWORD PTR [8+esp], ecx                        ;132.9
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.97]       ;132.9
        mov       ecx, DWORD PTR [12+esp]                       ;132.9
        mov       edi, DWORD PTR [28+esp]                       ;132.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.57:                        ; Preds .B12.57 .B12.56
        movaps    XMMWORD PTR [edi+esi*4], xmm0                 ;132.9
        movaps    XMMWORD PTR [16+edi+esi*4], xmm0              ;132.9
        add       esi, 8                                        ;132.9
        cmp       esi, ecx                                      ;132.9
        jb        .B12.57       ; Prob 82%                      ;132.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.58:                        ; Preds .B12.57
        mov       DWORD PTR [12+esp], ecx                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx
.B12.59:                        ; Preds .B12.58 .B12.143
        cmp       ebx, DWORD PTR [12+esp]                       ;132.9
        jbe       .B12.63       ; Prob 0%                       ;132.9
                                ; LOE eax edx ecx ebx
.B12.60:                        ; Preds .B12.59
        mov       edi, DWORD PTR [12+esp]                       ;
        lea       ecx, DWORD PTR [edx+ecx*4]                    ;
        mov       esi, 1203982336                               ;
                                ; LOE eax edx ecx ebx esi edi
.B12.61:                        ; Preds .B12.61 .B12.60
        mov       DWORD PTR [ecx+edi*4], esi                    ;132.9
        inc       edi                                           ;132.9
        cmp       edi, ebx                                      ;132.9
        jb        .B12.61       ; Prob 82%                      ;132.9
                                ; LOE eax edx ecx ebx esi edi
.B12.63:                        ; Preds .B12.61 .B12.59 .B12.146
        mov       esi, DWORD PTR [8+ebp]                        ;133.9
        mov       ecx, DWORD PTR [16+ebp]                       ;133.9
        mov       ebx, DWORD PTR [esi]                          ;133.9
        mov       esi, DWORD PTR [16+esp]                       ;133.9
        mov       edi, DWORD PTR [ecx]                          ;133.9
        mov       DWORD PTR [esi+ebx*4], edi                    ;133.9
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;134.9
        neg       edi                                           ;134.9
        add       edi, ebx                                      ;134.9
        imul      edi, edi, 152                                 ;134.9
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;134.9
        mov       esi, DWORD PTR [28+esi+edi]                   ;134.42
        movss     xmm0, DWORD PTR [eax+esi*4]                   ;134.42
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+32] ;143.14
        shl       eax, 3                                        ;143.14
        addss     xmm0, DWORD PTR [ecx]                         ;134.9
        movss     DWORD PTR [edx+ebx*4], xmm0                   ;134.9
        mov       edx, DWORD PTR [24+esp]                       ;143.14
        sub       edx, DWORD PTR [20+esp]                       ;143.14
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET] ;143.14
        sub       ecx, eax                                      ;143.14
        add       ecx, 8                                        ;143.14
        lea       eax, DWORD PTR [edx+ebx*4]                    ;143.14
        push      eax                                           ;143.14
        push      DWORD PTR [8+ebp]                             ;143.14
        push      ecx                                           ;143.14
        call      _LINK_LIST_mp_ENQUEUE_LINKLIST                ;143.14
        jmp       .B12.171      ; Prob 100%                     ;143.14
                                ; LOE
.B12.64:                        ; Preds .B12.7
        test      esi, esi                                      ;126.16
        jle       .B12.164      ; Prob 2%                       ;126.16
                                ; LOE eax ebx esi
.B12.65:                        ; Preds .B12.64
        mov       ecx, DWORD PTR [8+ebp]                        ;126.93
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;126.42
        neg       edi                                           ;126.93
        add       edi, DWORD PTR [ecx]                          ;126.93
        imul      edi, edi, 152                                 ;126.93
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DISTSCALE] ;126.16
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;126.47
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_X+32] ;126.42
        shl       edx, 2                                        ;126.93
        neg       edx                                           ;126.93
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_X]  ;126.93
        mov       edi, DWORD PTR [28+ecx+edi]                   ;126.47
        mov       DWORD PTR [28+esp], edx                       ;126.93
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_Y+32] ;126.149
        movss     xmm3, DWORD PTR [edx+edi*4]                   ;126.47
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_Y]  ;126.149
        shl       ecx, 2                                        ;126.149
        shl       ebx, 2                                        ;
        sub       eax, ebx                                      ;
        lea       edi, DWORD PTR [edx+edi*4]                    ;126.149
        sub       edi, ecx                                      ;126.149
        cmp       esi, 4                                        ;126.16
        movss     xmm0, DWORD PTR [edi]                         ;126.103
        jl        .B12.153      ; Prob 10%                      ;126.16
                                ; LOE eax edx ecx esi xmm0 xmm2 xmm3
.B12.66:                        ; Preds .B12.65
        lea       edi, DWORD PTR [4+eax]                        ;126.16
        and       edi, 15                                       ;126.16
        je        .B12.69       ; Prob 50%                      ;126.16
                                ; LOE eax edx ecx esi edi xmm0 xmm2 xmm3
.B12.67:                        ; Preds .B12.66
        test      edi, 3                                        ;126.16
        jne       .B12.153      ; Prob 10%                      ;126.16
                                ; LOE eax edx ecx esi edi xmm0 xmm2 xmm3
.B12.68:                        ; Preds .B12.67
        neg       edi                                           ;126.16
        add       edi, 16                                       ;126.16
        shr       edi, 2                                        ;126.16
                                ; LOE eax edx ecx esi edi xmm0 xmm2 xmm3
.B12.69:                        ; Preds .B12.68 .B12.66
        lea       ebx, DWORD PTR [4+edi]                        ;126.16
        cmp       esi, ebx                                      ;126.16
        jl        .B12.153      ; Prob 10%                      ;126.16
                                ; LOE eax edx ecx esi edi xmm0 xmm2 xmm3
.B12.70:                        ; Preds .B12.69
        mov       ebx, esi                                      ;126.16
        sub       ebx, edi                                      ;126.16
        and       ebx, 3                                        ;126.16
        neg       ebx                                           ;126.16
        add       ebx, esi                                      ;126.16
        mov       DWORD PTR [12+esp], ebx                       ;126.16
        mov       ebx, edx                                      ;
        sub       ebx, ecx                                      ;
        mov       DWORD PTR [32+esp], ebx                       ;
        test      edi, edi                                      ;126.16
        jbe       .B12.74       ; Prob 0%                       ;126.16
                                ; LOE eax edx ecx esi edi xmm0 xmm2 xmm3
.B12.71:                        ; Preds .B12.70
        mov       DWORD PTR [4+esp], ecx                        ;126.154
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [8+esp], esi                        ;126.154
        movss     xmm4, DWORD PTR [_2il0floatpacket.92]         ;126.163
        movss     xmm1, DWORD PTR [_2il0floatpacket.93]         ;126.154
        mov       ecx, DWORD PTR [32+esp]                       ;126.154
        mov       esi, DWORD PTR [28+esp]                       ;126.154
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4
.B12.72:                        ; Preds .B12.72 .B12.71
        movss     xmm6, DWORD PTR [4+esi+ebx*4]                 ;126.42
        movss     xmm5, DWORD PTR [4+ecx+ebx*4]                 ;126.98
        subss     xmm6, xmm3                                    ;126.93
        subss     xmm5, xmm0                                    ;126.149
        mulss     xmm6, xmm6                                    ;126.93
        mulss     xmm5, xmm5                                    ;126.149
        addss     xmm6, xmm5                                    ;126.96
        sqrtss    xmm6, xmm6                                    ;126.36
        mulss     xmm6, xmm2                                    ;126.35
        divss     xmm6, xmm1                                    ;126.154
        mulss     xmm6, xmm4                                    ;126.16
        movss     DWORD PTR [4+eax+ebx*4], xmm6                 ;126.16
        inc       ebx                                           ;126.16
        cmp       ebx, edi                                      ;126.16
        jb        .B12.72       ; Prob 82%                      ;126.16
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4
.B12.73:                        ; Preds .B12.72
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx esi edi xmm0 xmm2 xmm3
.B12.74:                        ; Preds .B12.70 .B12.73
        mov       ebx, DWORD PTR [32+esp]                       ;126.16
        movaps    xmm1, XMMWORD PTR [_2il0floatpacket.95]       ;126.154
        lea       ebx, DWORD PTR [4+ebx+edi*4]                  ;126.16
        test      bl, 15                                        ;126.16
        je        .B12.78       ; Prob 60%                      ;126.16
                                ; LOE eax edx ecx esi edi xmm0 xmm1 xmm2 xmm3
.B12.75:                        ; Preds .B12.74
        movaps    xmm7, xmm3                                    ;126.42
        movaps    xmm6, xmm0                                    ;126.98
        movaps    xmm5, xmm2                                    ;126.163
        mov       DWORD PTR [4+esp], ecx                        ;126.163
        mov       DWORD PTR [8+esp], esi                        ;126.163
        shufps    xmm7, xmm7, 0                                 ;126.42
        shufps    xmm6, xmm6, 0                                 ;126.98
        shufps    xmm5, xmm5, 0                                 ;126.163
        rcpps     xmm4, xmm1                                    ;126.163
        movss     DWORD PTR [16+esp], xmm0                      ;126.163
        movss     DWORD PTR [20+esp], xmm3                      ;126.163
        movss     DWORD PTR [24+esp], xmm2                      ;126.163
        movaps    xmm1, XMMWORD PTR [_2il0floatpacket.94]       ;126.163
        mov       ecx, DWORD PTR [12+esp]                       ;126.163
        mov       ebx, DWORD PTR [32+esp]                       ;126.163
        mov       esi, DWORD PTR [28+esp]                       ;126.163
        ALIGN     16
                                ; LOE eax edx ecx ebx esi edi xmm1 xmm4 xmm5 xmm6 xmm7
.B12.76:                        ; Preds .B12.76 .B12.75
        movups    xmm2, XMMWORD PTR [4+esi+edi*4]               ;126.42
        movups    xmm3, XMMWORD PTR [4+ebx+edi*4]               ;126.98
        subps     xmm2, xmm7                                    ;126.93
        subps     xmm3, xmm6                                    ;126.149
        mulps     xmm2, xmm2                                    ;126.93
        mulps     xmm3, xmm3                                    ;126.149
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.100]      ;126.35
        addps     xmm2, xmm3                                    ;126.96
        movaps    xmm3, XMMWORD PTR [_2il0floatpacket.101]      ;126.35
        andps     xmm3, xmm2                                    ;126.35
        cmpleps   xmm0, xmm3                                    ;126.35
        rsqrtps   xmm3, xmm2                                    ;126.35
        andps     xmm0, xmm3                                    ;126.35
        movaps    xmm3, xmm4                                    ;126.154
        mulps     xmm2, xmm0                                    ;126.35
        addps     xmm3, xmm4                                    ;126.154
        mulps     xmm0, xmm2                                    ;126.35
        subps     xmm0, XMMWORD PTR [_2il0floatpacket.98]       ;126.35
        mulps     xmm2, xmm0                                    ;126.35
        movaps    xmm0, xmm4                                    ;126.154
        mulps     xmm0, XMMWORD PTR [_2il0floatpacket.95]       ;126.154
        mulps     xmm2, XMMWORD PTR [_2il0floatpacket.99]       ;126.35
        mulps     xmm0, xmm4                                    ;126.154
        mulps     xmm2, xmm5                                    ;126.35
        subps     xmm3, xmm0                                    ;126.154
        mulps     xmm2, xmm3                                    ;126.154
        mulps     xmm2, xmm1                                    ;126.16
        movaps    XMMWORD PTR [4+eax+edi*4], xmm2               ;126.16
        add       edi, 4                                        ;126.16
        cmp       edi, ecx                                      ;126.16
        jb        .B12.76       ; Prob 82%                      ;126.16
        jmp       .B12.80       ; Prob 100%                     ;126.16
                                ; LOE eax edx ecx ebx esi edi xmm1 xmm4 xmm5 xmm6 xmm7
.B12.78:                        ; Preds .B12.74
        movaps    xmm7, xmm3                                    ;126.42
        movaps    xmm6, xmm0                                    ;126.98
        movaps    xmm5, xmm2                                    ;126.163
        mov       DWORD PTR [4+esp], ecx                        ;126.163
        mov       DWORD PTR [8+esp], esi                        ;126.163
        shufps    xmm7, xmm7, 0                                 ;126.42
        shufps    xmm6, xmm6, 0                                 ;126.98
        shufps    xmm5, xmm5, 0                                 ;126.163
        rcpps     xmm4, xmm1                                    ;126.163
        movss     DWORD PTR [16+esp], xmm0                      ;126.163
        movss     DWORD PTR [20+esp], xmm3                      ;126.163
        movss     DWORD PTR [24+esp], xmm2                      ;126.163
        movaps    xmm1, XMMWORD PTR [_2il0floatpacket.94]       ;126.163
        mov       ecx, DWORD PTR [12+esp]                       ;126.163
        mov       ebx, DWORD PTR [32+esp]                       ;126.163
        mov       esi, DWORD PTR [28+esp]                       ;126.163
        ALIGN     16
                                ; LOE eax edx ecx ebx esi edi xmm1 xmm4 xmm5 xmm6 xmm7
.B12.79:                        ; Preds .B12.79 .B12.78
        movups    xmm2, XMMWORD PTR [4+esi+edi*4]               ;126.42
        movaps    xmm3, XMMWORD PTR [4+ebx+edi*4]               ;126.98
        subps     xmm2, xmm7                                    ;126.93
        subps     xmm3, xmm6                                    ;126.149
        mulps     xmm2, xmm2                                    ;126.93
        mulps     xmm3, xmm3                                    ;126.149
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.100]      ;126.35
        addps     xmm2, xmm3                                    ;126.96
        movaps    xmm3, XMMWORD PTR [_2il0floatpacket.101]      ;126.35
        andps     xmm3, xmm2                                    ;126.35
        cmpleps   xmm0, xmm3                                    ;126.35
        rsqrtps   xmm3, xmm2                                    ;126.35
        andps     xmm0, xmm3                                    ;126.35
        movaps    xmm3, xmm4                                    ;126.154
        mulps     xmm2, xmm0                                    ;126.35
        addps     xmm3, xmm4                                    ;126.154
        mulps     xmm0, xmm2                                    ;126.35
        subps     xmm0, XMMWORD PTR [_2il0floatpacket.98]       ;126.35
        mulps     xmm2, xmm0                                    ;126.35
        movaps    xmm0, xmm4                                    ;126.154
        mulps     xmm0, XMMWORD PTR [_2il0floatpacket.95]       ;126.154
        mulps     xmm2, XMMWORD PTR [_2il0floatpacket.99]       ;126.35
        mulps     xmm0, xmm4                                    ;126.154
        mulps     xmm2, xmm5                                    ;126.35
        subps     xmm3, xmm0                                    ;126.154
        mulps     xmm2, xmm3                                    ;126.154
        mulps     xmm2, xmm1                                    ;126.16
        movaps    XMMWORD PTR [4+eax+edi*4], xmm2               ;126.16
        add       edi, 4                                        ;126.16
        cmp       edi, ecx                                      ;126.16
        jb        .B12.79       ; Prob 82%                      ;126.16
                                ; LOE eax edx ecx ebx esi edi xmm1 xmm4 xmm5 xmm6 xmm7
.B12.80:                        ; Preds .B12.76 .B12.79
        mov       DWORD PTR [12+esp], ecx                       ;
        movss     xmm0, DWORD PTR [16+esp]                      ;
        movss     xmm3, DWORD PTR [20+esp]                      ;
        movss     xmm2, DWORD PTR [24+esp]                      ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx esi xmm0 xmm2 xmm3
.B12.81:                        ; Preds .B12.80 .B12.153
        cmp       esi, DWORD PTR [12+esp]                       ;126.16
        jbe       .B12.85       ; Prob 0%                       ;126.16
                                ; LOE eax edx ecx esi xmm0 xmm2 xmm3
.B12.82:                        ; Preds .B12.81
        movss     xmm4, DWORD PTR [_2il0floatpacket.92]         ;126.163
        sub       edx, ecx                                      ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.93]         ;126.154
        mov       ecx, DWORD PTR [12+esp]                       ;126.154
        mov       ebx, DWORD PTR [28+esp]                       ;126.154
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4
.B12.83:                        ; Preds .B12.83 .B12.82
        movss     xmm6, DWORD PTR [4+ebx+ecx*4]                 ;126.42
        movss     xmm5, DWORD PTR [4+edx+ecx*4]                 ;126.98
        subss     xmm6, xmm3                                    ;126.93
        subss     xmm5, xmm0                                    ;126.149
        mulss     xmm6, xmm6                                    ;126.93
        mulss     xmm5, xmm5                                    ;126.149
        addss     xmm6, xmm5                                    ;126.96
        sqrtss    xmm6, xmm6                                    ;126.36
        mulss     xmm6, xmm2                                    ;126.35
        divss     xmm6, xmm1                                    ;126.154
        mulss     xmm6, xmm4                                    ;126.16
        movss     DWORD PTR [4+eax+ecx*4], xmm6                 ;126.16
        inc       ecx                                           ;126.16
        cmp       ecx, esi                                      ;126.16
        jb        .B12.83       ; Prob 82%                      ;126.16
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4
.B12.85:                        ; Preds .B12.83 .B12.164 .B12.81
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL] ;138.9
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+24] ;136.9
        test      esi, esi                                      ;136.9
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL+32] ;136.9
        mov       DWORD PTR [20+esp], ecx                       ;138.9
        jle       .B12.161      ; Prob 10%                      ;136.9
                                ; LOE eax edx ecx esi cl ch
.B12.86:                        ; Preds .B12.85
        cmp       esi, 8                                        ;136.9
        lea       ecx, DWORD PTR [edx*4]                        ;
        mov       DWORD PTR [16+esp], ecx                       ;
        jl        .B12.154      ; Prob 10%                      ;136.9
                                ; LOE eax edx esi
.B12.87:                        ; Preds .B12.86
        mov       ebx, DWORD PTR [20+esp]                       ;136.9
        and       ebx, 15                                       ;136.9
        je        .B12.90       ; Prob 50%                      ;136.9
                                ; LOE eax edx ebx esi
.B12.88:                        ; Preds .B12.87
        test      bl, 3                                         ;136.9
        jne       .B12.154      ; Prob 10%                      ;136.9
                                ; LOE eax edx ebx esi
.B12.89:                        ; Preds .B12.88
        neg       ebx                                           ;136.9
        add       ebx, 16                                       ;136.9
        shr       ebx, 2                                        ;136.9
                                ; LOE eax edx ebx esi
.B12.90:                        ; Preds .B12.89 .B12.87
        lea       ecx, DWORD PTR [8+ebx]                        ;136.9
        cmp       esi, ecx                                      ;136.9
        jl        .B12.154      ; Prob 10%                      ;136.9
                                ; LOE eax edx ebx esi
.B12.91:                        ; Preds .B12.90
        mov       ecx, esi                                      ;136.9
        sub       ecx, ebx                                      ;136.9
        and       ecx, 7                                        ;136.9
        neg       ecx                                           ;136.9
        add       ecx, esi                                      ;136.9
        mov       DWORD PTR [12+esp], ecx                       ;136.9
        mov       ecx, DWORD PTR [20+esp]                       ;
        sub       ecx, DWORD PTR [16+esp]                       ;
        test      ebx, ebx                                      ;136.9
        jbe       .B12.95       ; Prob 0%                       ;136.9
                                ; LOE eax edx ecx ebx esi
.B12.92:                        ; Preds .B12.91
        xor       edi, edi                                      ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       eax, 1203982336                               ;
        mov       edx, edi                                      ;
        mov       edi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B12.93:                        ; Preds .B12.93 .B12.92
        mov       DWORD PTR [edi+edx*4], eax                    ;136.9
        inc       edx                                           ;136.9
        cmp       edx, ebx                                      ;136.9
        jb        .B12.93       ; Prob 82%                      ;136.9
                                ; LOE eax edx ecx ebx esi edi
.B12.94:                        ; Preds .B12.93
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       eax, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx ebx esi
.B12.95:                        ; Preds .B12.91 .B12.94
        mov       DWORD PTR [4+esp], eax                        ;136.9
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.97]       ;136.9
        mov       eax, DWORD PTR [12+esp]                       ;136.9
        mov       edi, DWORD PTR [20+esp]                       ;136.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.96:                        ; Preds .B12.96 .B12.95
        movaps    XMMWORD PTR [edi+ebx*4], xmm0                 ;136.9
        movaps    XMMWORD PTR [16+edi+ebx*4], xmm0              ;136.9
        add       ebx, 8                                        ;136.9
        cmp       ebx, eax                                      ;136.9
        jb        .B12.96       ; Prob 82%                      ;136.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.97:                        ; Preds .B12.96
        mov       DWORD PTR [12+esp], eax                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx esi
.B12.98:                        ; Preds .B12.97 .B12.154
        cmp       esi, DWORD PTR [12+esp]                       ;136.9
        jbe       .B12.102      ; Prob 0%                       ;136.9
                                ; LOE eax edx ecx esi
.B12.99:                        ; Preds .B12.98
        mov       edi, DWORD PTR [12+esp]                       ;136.9
        lea       ebx, DWORD PTR [ecx+edx*4]                    ;
        mov       edx, 1203982336                               ;136.9
                                ; LOE eax edx ecx ebx esi edi
.B12.100:                       ; Preds .B12.100 .B12.99
        mov       DWORD PTR [ebx+edi*4], edx                    ;136.9
        inc       edi                                           ;136.9
        cmp       edi, esi                                      ;136.9
        jb        .B12.100      ; Prob 82%                      ;136.9
                                ; LOE eax edx ecx ebx esi edi
.B12.102:                       ; Preds .B12.100 .B12.98 .B12.161
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+32] ;137.9
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL] ;139.31
        mov       DWORD PTR [24+esp], esi                       ;137.9
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL+24] ;137.9
        test      esi, esi                                      ;137.9
        mov       DWORD PTR [28+esp], edx                       ;139.31
        jle       .B12.158      ; Prob 10%                      ;137.9
                                ; LOE eax edx ecx esi dl dh
.B12.103:                       ; Preds .B12.102
        mov       edx, DWORD PTR [24+esp]                       ;
        cmp       esi, 8                                        ;137.9
        lea       edx, DWORD PTR [edx*4]                        ;
        jl        .B12.155      ; Prob 10%                      ;137.9
                                ; LOE eax edx ecx esi
.B12.104:                       ; Preds .B12.103
        mov       ebx, DWORD PTR [28+esp]                       ;137.9
        and       ebx, 15                                       ;137.9
        je        .B12.107      ; Prob 50%                      ;137.9
                                ; LOE eax edx ecx ebx esi
.B12.105:                       ; Preds .B12.104
        test      bl, 3                                         ;137.9
        jne       .B12.155      ; Prob 10%                      ;137.9
                                ; LOE eax edx ecx ebx esi
.B12.106:                       ; Preds .B12.105
        neg       ebx                                           ;137.9
        add       ebx, 16                                       ;137.9
        shr       ebx, 2                                        ;137.9
                                ; LOE eax edx ecx ebx esi
.B12.107:                       ; Preds .B12.106 .B12.104
        mov       DWORD PTR [8+esp], ecx                        ;
        lea       ecx, DWORD PTR [8+ebx]                        ;137.9
        cmp       esi, ecx                                      ;137.9
        mov       ecx, DWORD PTR [8+esp]                        ;137.9
        jl        .B12.155      ; Prob 10%                      ;137.9
                                ; LOE eax edx ecx ebx esi cl ch
.B12.108:                       ; Preds .B12.107
        mov       edi, esi                                      ;137.9
        neg       edx                                           ;
        sub       edi, ebx                                      ;137.9
        and       edi, 7                                        ;137.9
        neg       edi                                           ;137.9
        add       edx, DWORD PTR [28+esp]                       ;
        add       edi, esi                                      ;137.9
        mov       DWORD PTR [12+esp], edi                       ;137.9
        test      ebx, ebx                                      ;137.9
        jbe       .B12.112      ; Prob 0%                       ;137.9
                                ; LOE eax edx ecx ebx esi cl ch
.B12.109:                       ; Preds .B12.108
        xor       edi, edi                                      ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       eax, 1203982336                               ;
        mov       esi, edi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B12.110:                       ; Preds .B12.110 .B12.109
        mov       DWORD PTR [edi+esi*4], eax                    ;137.9
        inc       esi                                           ;137.9
        cmp       esi, ebx                                      ;137.9
        jb        .B12.110      ; Prob 82%                      ;137.9
                                ; LOE eax edx ecx ebx esi edi
.B12.111:                       ; Preds .B12.110
        mov       esi, DWORD PTR [8+esp]                        ;
        mov       eax, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx ebx esi
.B12.112:                       ; Preds .B12.108 .B12.111
        mov       DWORD PTR [4+esp], eax                        ;137.9
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.97]       ;137.9
        mov       eax, DWORD PTR [12+esp]                       ;137.9
        mov       edi, DWORD PTR [28+esp]                       ;137.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.113:                       ; Preds .B12.113 .B12.112
        movaps    XMMWORD PTR [edi+ebx*4], xmm0                 ;137.9
        movaps    XMMWORD PTR [16+edi+ebx*4], xmm0              ;137.9
        add       ebx, 8                                        ;137.9
        cmp       ebx, eax                                      ;137.9
        jb        .B12.113      ; Prob 82%                      ;137.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.114:                       ; Preds .B12.113
        mov       DWORD PTR [12+esp], eax                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx esi
.B12.115:                       ; Preds .B12.114 .B12.155
        cmp       esi, DWORD PTR [12+esp]                       ;137.9
        jbe       .B12.119      ; Prob 0%                       ;137.9
                                ; LOE eax edx ecx esi
.B12.116:                       ; Preds .B12.115
        mov       ebx, DWORD PTR [24+esp]                       ;
        mov       edi, DWORD PTR [12+esp]                       ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       eax, 1203982336                               ;
        lea       ebx, DWORD PTR [edx+ebx*4]                    ;
                                ; LOE eax edx ecx ebx esi edi
.B12.117:                       ; Preds .B12.117 .B12.116
        mov       DWORD PTR [ebx+edi*4], eax                    ;137.9
        inc       edi                                           ;137.9
        cmp       edi, esi                                      ;137.9
        jb        .B12.117      ; Prob 82%                      ;137.9
                                ; LOE eax edx ecx ebx esi edi
.B12.118:                       ; Preds .B12.117
        mov       eax, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx
.B12.119:                       ; Preds .B12.118 .B12.115 .B12.158
        mov       ebx, DWORD PTR [12+ebp]                       ;138.9
        mov       edi, DWORD PTR [16+ebp]                       ;138.9
        mov       esi, DWORD PTR [ebx]                          ;138.9
        mov       edi, DWORD PTR [edi]                          ;138.9
        mov       DWORD PTR [ecx+esi*4], edi                    ;138.9
        mov       ecx, DWORD PTR [20+esp]                       ;145.14
        sub       ecx, DWORD PTR [16+esp]                       ;145.14
        mov       eax, DWORD PTR [eax+esi*4]                    ;139.9
        mov       DWORD PTR [edx+esi*4], eax                    ;139.9
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET+32] ;145.14
        shl       edx, 3                                        ;145.14
        mov       eax, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET] ;145.14
        sub       eax, edx                                      ;145.14
        lea       edx, DWORD PTR [ecx+esi*4]                    ;145.14
        push      edx                                           ;145.14
        push      ebx                                           ;145.14
        add       eax, 8                                        ;145.14
        push      eax                                           ;145.14
        call      _LINK_LIST_mp_ENQUEUE_LINKLIST                ;145.14
                                ; LOE
.B12.171:                       ; Preds .B12.63 .B12.119
        add       esp, 12                                       ;145.14
                                ; LOE
.B12.120:                       ; Preds .B12.171
        xor       eax, eax                                      ;147.11
        lea       edx, DWORD PTR [16+esp]                       ;147.11
        mov       DWORD PTR [16+esp], eax                       ;147.11
        push      32                                            ;147.11
        push      eax                                           ;147.11
        push      OFFSET FLAT: __STRLITPACK_2.0.8               ;147.11
        push      -2088435968                                   ;147.11
        push      1                                             ;147.11
        push      edx                                           ;147.11
        call      _for_close                                    ;147.11
                                ; LOE
.B12.121:                       ; Preds .B12.120
        add       esp, 76                                       ;148.1
        pop       ebx                                           ;148.1
        pop       edi                                           ;148.1
        pop       esi                                           ;148.1
        mov       esp, ebp                                      ;148.1
        pop       ebp                                           ;148.1
        ret                                                     ;148.1
                                ; LOE
.B12.122:                       ; Preds .B12.5                  ; Infreq
        cmp       edx, 4                                        ;118.5
        jl        .B12.138      ; Prob 10%                      ;118.5
                                ; LOE edx ecx
.B12.123:                       ; Preds .B12.122                ; Infreq
        mov       eax, ecx                                      ;118.5
        and       eax, 15                                       ;118.5
        je        .B12.126      ; Prob 50%                      ;118.5
                                ; LOE eax edx ecx
.B12.124:                       ; Preds .B12.123                ; Infreq
        test      al, 3                                         ;118.5
        jne       .B12.138      ; Prob 10%                      ;118.5
                                ; LOE eax edx ecx
.B12.125:                       ; Preds .B12.124                ; Infreq
        neg       eax                                           ;118.5
        add       eax, 16                                       ;118.5
        shr       eax, 2                                        ;118.5
                                ; LOE eax edx ecx
.B12.126:                       ; Preds .B12.125 .B12.123       ; Infreq
        lea       ebx, DWORD PTR [4+eax]                        ;118.5
        cmp       edx, ebx                                      ;118.5
        jl        .B12.138      ; Prob 10%                      ;118.5
                                ; LOE eax edx ecx
.B12.127:                       ; Preds .B12.126                ; Infreq
        mov       esi, edx                                      ;118.5
        sub       esi, eax                                      ;118.5
        and       esi, 3                                        ;118.5
        neg       esi                                           ;118.5
        add       esi, edx                                      ;118.5
        test      eax, eax                                      ;118.5
        jbe       .B12.131      ; Prob 10%                      ;118.5
                                ; LOE eax edx ecx esi
.B12.128:                       ; Preds .B12.127                ; Infreq
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B12.129:                       ; Preds .B12.129 .B12.128       ; Infreq
        mov       DWORD PTR [ecx+ebx*4], 0                      ;118.5
        inc       ebx                                           ;118.5
        cmp       ebx, eax                                      ;118.5
        jb        .B12.129      ; Prob 81%                      ;118.5
                                ; LOE eax edx ecx ebx esi
.B12.131:                       ; Preds .B12.129 .B12.127       ; Infreq
        pxor      xmm0, xmm0                                    ;118.5
                                ; LOE eax edx ecx esi xmm0
.B12.132:                       ; Preds .B12.132 .B12.131       ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;118.5
        add       eax, 4                                        ;118.5
        cmp       eax, esi                                      ;118.5
        jb        .B12.132      ; Prob 81%                      ;118.5
                                ; LOE eax edx ecx esi xmm0
.B12.134:                       ; Preds .B12.132 .B12.138       ; Infreq
        cmp       esi, edx                                      ;118.5
        jae       .B12.7        ; Prob 10%                      ;118.5
                                ; LOE edx ecx esi
.B12.136:                       ; Preds .B12.134 .B12.136       ; Infreq
        mov       DWORD PTR [ecx+esi*4], 0                      ;118.5
        inc       esi                                           ;118.5
        cmp       esi, edx                                      ;118.5
        jb        .B12.136      ; Prob 81%                      ;118.5
        jmp       .B12.7        ; Prob 100%                     ;118.5
                                ; LOE edx ecx esi
.B12.138:                       ; Preds .B12.122 .B12.126 .B12.124 ; Infreq
        xor       esi, esi                                      ;118.5
        jmp       .B12.134      ; Prob 100%                     ;118.5
                                ; LOE edx ecx esi
.B12.141:                       ; Preds .B12.9 .B12.13 .B12.11  ; Infreq
        xor       edx, edx                                      ;122.16
        mov       DWORD PTR [4+esp], edx                        ;122.16
        jmp       .B12.25       ; Prob 100%                     ;122.16
                                ; LOE eax ecx esi edi xmm1 xmm2 xmm3
.B12.142:                       ; Preds .B12.30 .B12.34 .B12.32 ; Infreq
        mov       ecx, DWORD PTR [24+esp]                       ;
        xor       esi, esi                                      ;131.9
        sub       ecx, DWORD PTR [20+esp]                       ;
        mov       DWORD PTR [16+esp], ecx                       ;
        jmp       .B12.42       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B12.143:                       ; Preds .B12.47 .B12.51 .B12.49 ; Infreq
        neg       edx                                           ;
        xor       esi, esi                                      ;132.9
        mov       DWORD PTR [12+esp], esi                       ;132.9
        add       edx, DWORD PTR [28+esp]                       ;
        jmp       .B12.59       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B12.146:                       ; Preds .B12.46                 ; Infreq
        shl       ecx, 2                                        ;
        sub       edx, ecx                                      ;
        jmp       .B12.63       ; Prob 100%                     ;
                                ; LOE eax edx
.B12.149:                       ; Preds .B12.29                 ; Infreq
        shl       edx, 2                                        ;
        sub       ecx, edx                                      ;
        mov       DWORD PTR [20+esp], edx                       ;
        mov       DWORD PTR [16+esp], ecx                       ;
        jmp       .B12.46       ; Prob 100%                     ;
                                ; LOE eax
.B12.152:                       ; Preds .B12.8                  ; Infreq
        shl       ebx, 2                                        ;
        sub       eax, ebx                                      ;
        jmp       .B12.29       ; Prob 100%                     ;
                                ; LOE eax
.B12.153:                       ; Preds .B12.65 .B12.69 .B12.67 ; Infreq
        xor       ebx, ebx                                      ;126.16
        mov       DWORD PTR [12+esp], ebx                       ;126.16
        jmp       .B12.81       ; Prob 100%                     ;126.16
                                ; LOE eax edx ecx esi xmm0 xmm2 xmm3
.B12.154:                       ; Preds .B12.86 .B12.90 .B12.88 ; Infreq
        xor       ecx, ecx                                      ;136.9
        mov       DWORD PTR [12+esp], ecx                       ;136.9
        mov       ecx, DWORD PTR [20+esp]                       ;
        sub       ecx, DWORD PTR [16+esp]                       ;
        jmp       .B12.98       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B12.155:                       ; Preds .B12.103 .B12.107 .B12.105 ; Infreq
        neg       edx                                           ;
        xor       ebx, ebx                                      ;137.9
        mov       DWORD PTR [12+esp], ebx                       ;137.9
        add       edx, DWORD PTR [28+esp]                       ;
        jmp       .B12.115      ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B12.158:                       ; Preds .B12.102                ; Infreq
        mov       ebx, DWORD PTR [24+esp]                       ;
        shl       ebx, 2                                        ;
        sub       edx, ebx                                      ;
        jmp       .B12.119      ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B12.161:                       ; Preds .B12.85                 ; Infreq
        shl       edx, 2                                        ;
        mov       DWORD PTR [16+esp], edx                       ;
        sub       ecx, edx                                      ;
        jmp       .B12.102      ; Prob 100%                     ;
                                ; LOE eax ecx
.B12.164:                       ; Preds .B12.64                 ; Infreq
        shl       ebx, 2                                        ;
        sub       eax, ebx                                      ;
        jmp       .B12.85       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax
; mark_end;
_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_INITIALIZATION ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__STRLITPACK_2.0.8	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_INITIALIZATION
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET$BLK..T750_
_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET$BLK..T750_	DD 2 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET$BLK..T782_
_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET$BLK..T782_	DD 2 DUP (0H)	; pad
_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDTHIS.0.10	DD 1 DUP (0H)	; pad
_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH_DEPARTURE$FOUNDITEM.0.10	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE
_DYNUST_TDSP_ASTAR_MODULE_mp_IN_SE	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY
_DYNUST_TDSP_ASTAR_MODULE_mp_SE_TALLY	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET
_DYNUST_TDSP_ASTAR_MODULE_mp_FIXED_LABEL_SET	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET
_DYNUST_TDSP_ASTAR_MODULE_mp_SCAN_ELIGIBLE_SET	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_PRED
_DYNUST_TDSP_ASTAR_MODULE_mp_PRED	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_EJD
_DYNUST_TDSP_ASTAR_MODULE_mp_EJD	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL
_DYNUST_TDSP_ASTAR_MODULE_mp_FAKELABEL	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL
_DYNUST_TDSP_ASTAR_MODULE_mp_REALLABEL	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL
_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL	DD	0
	DD	0
	DD	0
	DD	128
	DD	3
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY
_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY	DD	0
	DD	0
	DD	0
	DD	128
	DD	3
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_ASTAR_MODULE_mp_TTIME
_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.14	DD	047c35000H,047c35000H,047c35000H,047c35000H
_2il0floatpacket.94	DD	042700000H,042700000H,042700000H,042700000H
_2il0floatpacket.95	DD	042340000H,042340000H,042340000H,042340000H
_2il0floatpacket.97	DD	047c35000H,047c35000H,047c35000H,047c35000H
_2il0floatpacket.98	DD	040400000H,040400000H,040400000H,040400000H
_2il0floatpacket.99	DD	0bf000000H,0bf000000H,0bf000000H,0bf000000H
_2il0floatpacket.100	DD	000800000H,000800000H,000800000H,000800000H
_2il0floatpacket.101	DD	07fffffffH,07fffffffH,07fffffffH,07fffffffH
_2il0floatpacket.13	DD	047c35000H
__STRLITPACK_0	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	115
	DB	101
	DB	97
	DB	114
	DB	99
	DB	104
	DB	105
	DB	110
	DB	103
	DB	32
	DB	105
	DB	116
	DB	101
	DB	109
	DB	115
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	83
	DB	99
	DB	97
	DB	110
	DB	95
	DB	69
	DB	108
	DB	105
	DB	103
	DB	105
	DB	98
	DB	108
	DB	101
	DB	95
	DB	83
	DB	101
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_3.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_4	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.43	DD	03d4ccccdH
_2il0floatpacket.44	DD	047c35000H
_2il0floatpacket.45	DD	080000000H
_2il0floatpacket.46	DD	04b000000H
_2il0floatpacket.47	DD	03f800000H
_2il0floatpacket.48	DD	03f000000H
_2il0floatpacket.49	DD	0bf000000H
_2il0floatpacket.72	DD	03d4ccccdH
_2il0floatpacket.73	DD	047c35000H
_2il0floatpacket.74	DD	080000000H
_2il0floatpacket.75	DD	04b000000H
_2il0floatpacket.76	DD	03f800000H
_2il0floatpacket.77	DD	03f000000H
_2il0floatpacket.78	DD	0bf000000H
_2il0floatpacket.92	DD	042700000H
_2il0floatpacket.93	DD	042340000H
_2il0floatpacket.96	DD	047c35000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES_ORG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AGGINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERIOD:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_Y:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_X:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DISTSCALE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
	COMM _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_FIXED_LABEL_SET:BYTE:4
	COMM _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_SCAN_ELIGIBLE_SET:BYTE:4
_DATA	ENDS
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_close:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETBSTMOVE:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_LINK_LIST_mp_CREATE_QUEUE_LINKLIST:PROC
EXTRN	_LINK_LIST_mp_ENQUEUE_LINKLIST:PROC
EXTRN	_LINK_LIST_mp_DESTROY_QUEUE_LINKLIST:PROC
EXTRN	_LINK_LIST_mp_DISQUEUE_LINKLIST:PROC
EXTRN	_LINK_LIST_mp_DISQUEUE_TARGET:PROC
EXTRN	_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
