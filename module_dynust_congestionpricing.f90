MODULE DYNUST_CONGESTIONPRICING_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

USE DYNUST_MAIN_MODULE
TYPE pairset
  INTEGER NHot                  ! # of nodes ! 1: HOT lanes, 2: GP lanes
  INTEGER Ngp
  INTEGER,POINTER::hot(:)       ! node sequence THEN converted to link number
  INTEGER,POINTER::gp(:)        ! node sequence THEN converted to link number
  REAL,POINTER::price(:,:)        ! Per mile rate for the entire hot lane segment for each agg int
  REAL,POINTER::hotv(:),gpv(:)  ! average speed for entire hot and gp lanes segment for each agg int
  REAL::hot_target_v            ! targe speed for hot lane
  REAL:: minPrice, maxPrice     ! min and max price for this segment ($/mile)
  REAL:: wei,rdel               ! the two PARAMETERs used in the model, default values are 1.0 and 0.0    
  INTEGER mode                 !  operation mode. 0: HOV, 1: HOT but truck not allowed, 2: HOT and truck allowed
END TYPE

TYPE(pairset),ALLOCATABLE:: CongP(:)

LOGICAL::CONGEXT=.false.

INTEGER::NSeg = 0

CONTAINS
!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE ReadCongPricingConfig
  USE DYNUST_MAIN_MODULE
  USE DYNUST_NETWORK_MODULE
  LOGICAL::Fexist=.False.
  INTEGER itm1, itm2
  LOGICAL CongError


!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
     
   INQUIRE (FILE='CongestionPricingConfig.dat', EXIST = CONGEXT)     
   IF(CONGEXT) THEN
     OPEN(unit = 7777, File ='CongestionPricingConfig.dat',status='unknown')
     READ(7777,*) NSeg
     ALLOCATE(CongP(Nseg))
     CongP(:)%nhot = 0
     CongP(:)%ngp = 0
     CongP(:)%hot_target_v = 0     
     DO i = 1, NSeg
       READ(7777,*) CongP(i)%mode, CongP(i)%hot_target_v, CongP(i)%minPrice, CongP(i)%maxPrice, CongP(i)%wei, CongP(i)%rdel        ! read target min speed for Hot lane

       READ(7777,*) itm, CongP(i)%nhot          ! read # of nodes for HOT segment
       ALLOCATE(CongP(i)%hot(CongP(i)%nhot))    ! allocate memory for HOT segement
       CongP(i)%hot(:) = 0                      ! allocate memory for HOT segement       
       READ(7777,*) itm, CongP(i)%ngp           ! read # of nodes for GP segment 
       ALLOCATE(CongP(i)%gp(CongP(i)%ngp))      ! allocate memory for GP segement   
       CongP(i)%gp(:) = 0
       ALLOCATE(CongP(i)%price(2,aggint))       ! allocate memory for GP price for aggint FOR SOV AND TRUCK
       CongP(i)%price(:,:) = 0
       ALLOCATE(CongP(i)%hotv(aggint))          ! allocate memory for GP price for aggint
       CongP(i)%hotv(:) = 0
       ALLOCATE(CongP(i)%gpv(aggint))           ! allocate memory for GP price for aggint
       CongP(i)%gpv(:) = 0
     ENDDO

     REWIND(7777)
     READ(7777,*)                                   ! skip the NSeg reading    
     CongError = .false.
     DO i = 1, NSeg
       READ(7777,*)
       READ(7777,*) itm1, itm2, CongP(i)%hot(1:CongP(i)%nhot)           ! read node sequence for HOT lanes
       READ(7777,*) itm1, itm2, CongP(i)%gp(1:CongP(i)%ngp)             ! read node sequence for GP lanes
       IF((CongP(i)%hot(1) /= CongP(i)%gp(1))) THEN                     ! the first nodes of the segmen pair should be the same
          WRITE(911,*) "error in HOT/GP pair, for pair  ",i, STOP
       ENDIF
       ! convert to link number
       DO j = 1, CongP(i)%nhot-1
         ntmp = GetFLinkFromNode(OutToInNodeNum(CongP(i)%hot(j)),OutToInNodeNum(CongP(i)%hot(j+1)),1)  ! plug in internal node number
         IF(ntmp < 1) THEN
           WRITE(911,'("Error in Cong Price HOT Segment # ",i4," node ",i7, " to ", i7)') i, CongP(i)%hot(j),CongP(i)%hot(j+1)
           CongError = .true.
         ELSE
           CongP(i)%hot(j) = ntmp   
         ENDIF
       ENDDO
       DO j = 1, CongP(i)%ngp-1
         ntmp = GetFLinkFromNode(OutToInNodeNum(CongP(i)%gp(j)),OutToInNodeNum(CongP(i)%gp(j+1)),1)
         IF(ntmp < 1) THEN
           WRITE(911,'("Error in Cong Price GPL Segment # ",i4," node ",i7, " to ", i7)') i, CongP(i)%gp(j),CongP(i)%gp(j+1)
           CongError = .true.
         ELSE
           CongP(i)%gp(j) = ntmp   
         ENDIF
       ENDDO
     ENDDO     
     IF(CongError) STOP
     CLOSE(7777)
   ENDIF !IF(EXT2) THEN

END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE PriceCal ! calcuate the price
! note that the price calcualted here is distanced based by default
USE DYNUST_MAIN_MODULE
INTEGER t,iVehType
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

DO iVehType = 1, 2
DO i = 1, nSeg              ! process average speed for hot and gp lanes
 DO t = 1, aggint
   CongP(i)%hotv(t) = CalAvgSpeed(i,1,t)*60.0
   CongP(i)%gpv(t)  = CalAvgSpeed(i,2,t)*60.0 ! convert to mph speed
   harmV  = max(0.0,1.0/CongP(i)%gpv(t)-1.0/CongP(i)%hot_target_v)
!  ratio = min(1.0, CongP(i)%hotv(t)/CongP(i)%hot_target_v)
!  ratio = max(0.3,CongP(i)%hotv(t)/CongP(i)%hot_target_v)
   ratio = (CongP(i)%hotv(t)/CongP(i)%hot_target_v)**CongP(i)%wei
   IF(iVehType == 1) THEN       ! auto
!    CongP(i)%price(1,t) = max(CongP(i)%minPrice,TollVoTA/CongP(i)%wei*harmV + CongP(i)%rdel)  ! in case the cal price is less than min
     CongP(i)%price(1,t) = max(CongP(i)%minPrice,TollVoTA/ratio/CongP(i)%wei*(harmV+CongP(i)%rdel))  ! in case the cal price is less than min
     IF(CongP(i)%price(1,t) > CongP(i)%minPrice) THEN
       CongP(i)%price(1,t) = min(CongP(i)%maxPrice,TollVoTA/ratio/CongP(i)%wei*(harmV+CongP(i)%rdel))  ! in case the cal price is higher than max
     ENDIF
   ELSEIF(iVehType == 2) THEN ! truck to be assigned
     IF(CongP(i)%mode == 3) THEN ! truck allowed in HOT
       CongP(i)%price(2,t) = max(CongP(i)%minPrice,TollVoTT/ratio/CongP(i)%wei*(harmV+CongP(i)%rdel))  !for truck
       IF(CongP(i)%price(2,t) > CongP(i)%minPrice) THEN
         CongP(i)%price(2,t) = min(CongP(i)%maxPrice,TollVoTT/ratio/CongP(i)%wei*(harmV+CongP(i)%rdel)) !for truck
       ENDIF
     ELSE
       CongP(i)%price(2,t) = 100.0    ! high price for truck
     ENDIF
   ENDIF
 ENDDO
ENDDO

CALL PriceUpdate(iVehType)

ENDDO ! iVehType

END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
REAL FUNCTION CalAvgSpeed(ilink,itype,it)
USE DYNUST_MAIN_MODULE
USE DYNUST_NETWORK_MODULE
REAL tmpv,tmpd
INTEGER ilink, itype, it, isee
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

tmpv = 0
tmpd = 0
isee = it
isee = it
IF(itype == 1) THEN ! for hot lanes 
  DO i = 1, CongP(ilink)%nhot-1
!     tmpv = tmpv + s(CongP(ilink)%hot(i))/(link_speed(CongP(ilink)%hot(i),it)/AttScale) ! total time
      tmpv = tmpv + TravelTime(CongP(ilink)%hot(i),isee)/AttScale
      isee = min(aggint,isee + ifix((TravelTime(CongP(ilink)%hot(i),isee)/AttScale)/xminPerSimInt/simPerAgg))
      tmpd = tmpd + m_dynust_network_arc_nde(CongP(ilink)%hot(i))%s ! total distance
  ENDDO
  CalAvgSpeed = tmpd/tmpv ! total distance divided by total time
! CalAvgSpeed = CongP(ilink)%hot_target_v/60.0 ! hot land speed is the target speed mile/minute
ELSE ! for GP lanes
  DO i = 1, CongP(ilink)%ngp-1
!     tmpv = tmpv + s(CongP(ilink)%gp(i))/(link_speed(CongP(ilink)%gp(i),it)/AttScale) ! total time
      tmpv = tmpv + TravelTime(CongP(ilink)%gp(i),isee)/AttScale
      isee = min(aggint,isee + ifix((TravelTime(CongP(ilink)%gp(i),isee)/AttScale)/xminPerSimInt/simPerAgg))
      tmpd = tmpd + m_dynust_network_arc_nde(CongP(ilink)%gp(i))%s    ! total distance
  ENDDO
  CalAvgSpeed = tmpd/tmpv
ENDIF

END FUNCTION

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE PriceUpdate(ivehType) ! update the price scheme for existing datastructure
USE DYNUST_MAIN_MODULE
USE DYNUST_NETWORK_MODULE
INTEGER iVehType
!IVehType refers to auto or trucks, process both since it is free for HOT veh to drive on HOT lanes

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

DO 100 IP = 1, NTollLink
   iab = TollLink(IP)%LinkNO
   DO 20 iok = 1, TollLink(IP)%NTD
      DO i = 1, nSeg              ! process average speed for hot and gp lanes
         DO j = 1, CongP(i)%nhot
            IF(CongP(i)%hot(j) == iab) THEN ! found the toll link  
                 ist = min(aggint, ifix((TollLink(IP)%scheme(iok)%stime/xminPerSimInt)/simPerAgg)+1)
                 iet = min(aggint, ifix(((TollLink(IP)%scheme(iok)%etime-0.1)/xminPerSimInt)/simPerAgg)+1)
                 temprice = sum(CongP(i)%price(iVehType,ist:iet))/(iet-ist+1) ! the unit for temprice is per mile
                 
                 IF(TollLink(IP)%scheme(iok)%switch == 0) THEN ! distance based
                   IF(iVehType == 1) THEN
                      TollLink(IP)%scheme(iok)%sov = max(CongP(i)%minPrice,temprice)      ! the rate is distanced based
                      TollLink(IP)%scheme(iok)%sov = min(CongP(i)%maxPrice,temprice)  
                   ELSE
                      IF(TollLink(IP)%scheme(iok)%tck < 100) TollLink(IP)%scheme(iok)%tck = max(CongP(i)%minPrice,temprice)        ! the rate is distanced based
                      IF(TollLink(IP)%scheme(iok)%tck < 100) TollLink(IP)%scheme(iok)%tck = min(CongP(i)%maxPrice,temprice)        ! the rate is distanced based                      
                   ENDIF
                 ELSE  ! link based
                   IF(iVehType == 1) THEN
                     TollLink(IP)%scheme(iok)%sov = max(CongP(i)%minPrice,temprice)*m_dynust_network_arc_nde(iab)%s ! the rate is link based
                     TollLink(IP)%scheme(iok)%sov = min(CongP(i)%maxPrice,temprice)*m_dynust_network_arc_nde(iab)%s ! the rate is link based                     
                   ELSE
                     IF(TollLink(IP)%scheme(iok)%tck < 100) TollLink(IP)%scheme(iok)%tck = max(CongP(i)%minPrice,temprice)*m_dynust_network_arc_nde(iab)%s ! the rate is link based
                     IF(TollLink(IP)%scheme(iok)%tck < 100) TollLink(IP)%scheme(iok)%tck = min(CongP(i)%maxPrice,temprice)*m_dynust_network_arc_nde(iab)%s ! the rate is link based                     
                   ENDIF
                 ENDIF
           ENDIF
        ENDDO
      ENDDO
10  CONTINUE      
20	ENDDO
100 ENDDO



END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE PriceOutput ! write out the new price to a new file. This file will replace the existing file
USE DYNUST_MAIN_MODULE
USE DYNUST_NETWORK_MODULE
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

OPEN(unit = 7778, File ='Toll_CongP.dat',status='unknown')

 WRITE(7778,'(3f7.3)') TollVoTA, TollVoTH, TollVoTT
 WRITE(7778,'(f5.3,i6)') TravelTimeWeight, NTollLinktmp
 DO 100 I = 1, NTollLink
   iab = TollLink(I)%LinkNO 
   DO iok = 1, TollLink(I)%NTD
        WRITE(7778,333) m_dynust_network_node_nde(TollLink(i)%UNode)%IntoOutNodeNum,m_dynust_network_node_nde(TollLink(i)%DNode)%IntoOutNodeNum,(TollLink(i)%scheme(iok)%stime),(TollLink(i)%scheme(iok)%etime),&
                        TollLink(i)%scheme(iok)%switch, TollLink(I)%scheme(iok)%sov,TollLink(I)%scheme(iok)%hov,TollLink(I)%scheme(iok)%tck
   ENDDO
100 ENDDO

CLOSE(7778)
333 FORMAT(2i7,2f7.1,i5,3f10.3)

DO MO = 1, nSeg
  DEALLOCATE(CongP(mo)%hot)
  DEALLOCATE(CongP(mo)%gp)
  DEALLOCATE(CongP(mo)%price) 
  DEALLOCATE(CongP(mo)%hotv)
  DEALLOCATE(CongP(mo)%gpv)
ENDDO
DEALLOCATE(CongP)

END SUBROUTINE

END MODULE